# /lib/twilio_message.rb
require 'rubygems'
require 'twilio-ruby'
require 'net/http'



module TwilioFax
  # send a fax
  def TwilioFax.send_fax(params)
    sid = params['org_sid']
    org_auth_token = params['org_auth_token']
    begin
      uri = URI('https://fax.twilio.com/v1/Faxes')
      post = Net::HTTP::Post.new uri.request_uri
      request = Net::HTTP.new(uri.host, uri.port)
      request.use_ssl = true
      post.basic_auth sid, org_auth_token
      request_body = URI.encode_www_form(To: params['fax'],From: params['src'],MediaUrl: params['pdf_path'])
      request.start {|http|
        http.request(post, request_body){|response|
          puts response.read_body
        }
      }
    rescue 
      puts "---------------------------------- TwilioFax ---------------------------------"
    end
  end
end

