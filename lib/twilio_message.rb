# /lib/twilio_message.rb
require 'rubygems'
require 'twilio-ruby'

module TwilioMessage
  # send a message
  def TwilioMessage.send_message(params)
    sid = params['org_sid']
    org_auth_token = params['org_auth_token']
    begin
      if params['notification_method'] == 2 || params['notification_method'] == 3
        client = Twilio::REST::Client.new(sid, org_auth_token)
        response = client.account.messages.create(:from => params['src'], :to => params['dst'], :body => params['text'])
        if params['trackable_id'].present?
          Log.create!(trackable_id: params['trackable_id'], trackable_type: params['trackable_type'], message: params['text'], sid: response.sid, log_type: "sms")
        end
        puts response.body
      else
        puts "Patient doesn't used SMS Notification!"
      end
    rescue
      puts "---------------------------------- TwilioMessage ---------------------------------"
    end
  end
end