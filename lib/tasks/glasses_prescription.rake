namespace :glasses_prescription do
  task :update => :environment do
    GlassesPrescription.all.each do |glp|
      if glp.try(:eye_measurement).present?
        glp.glass_id = glp.try(:eye_measurement).try(:glass_id)
        glp.save!
      else
        glp.destroy!
      end
    end
  end
end