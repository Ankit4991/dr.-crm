namespace :exam_procedures do
  task :update => :environment do
    Organisation.all.each do |org|
      org.exam_procedures.each_with_index do |pro,i|
        pro.position = i + 1
        pro.save!
      end
    end
  end
end