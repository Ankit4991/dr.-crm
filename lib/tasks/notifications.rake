namespace :notifications do
  task :delete => :environment do
    notifications = Notification.all.where("updated_at::Date < ?", 3.month.ago)
  	notifications.destroy_all
  	puts " Notification updated for Older 3 months "
  end
end