namespace :recommendations do
  task :update => :environment do
    recommendations =[
        { name: 'Full Time' },
        { name: 'As Needed' },
        { name: 'Part Time' },
        { name: 'Single Vision' },
        { name: 'Bi Focal' },
        { name: 'Progressive' },
        { name: 'Distance' },
        { name: 'Near' },
        { name: 'Polycarbonate' },
        { name: 'Sunglass RX' },
        { name: 'Anti-Reflective' },
        { name: 'Transition' }
      ]
    Organisation.all.each do |org|
      recommendations.each do |recommendation|
        org.doctor_recommendations.find_or_create_by(name: recommendation[:name])
      end
    end
  end
end