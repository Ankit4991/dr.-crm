#This rake is not required not as we have a worker for it which is scheduled by sidetiq
namespace :order_subscriptions do
  task :renew => :environment do
    SubscribedOrdersWorker.new.perform
  end
end