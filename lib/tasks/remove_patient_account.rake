#This file will keep the logs of deleted patients and their customer profile card IDs
#To delete a patient and its all its dependents ,you just need to provide its email and subdomain(or organisation subdomain)
require 'csv'

task :remove_patient_account => :environment do |t|
  puts "***************************************  #{Time.zone.now.strftime("%m/%d/%Y at %I:%M%p")} ***************************************"
  puts "Enter patient EMAIL: "
  patient_email = STDIN.gets.strip.downcase
  puts "Enter the patient's SUBDOMAIN : "
  subdomain = STDIN.gets.strip.downcase
  resources = Patient.where(email: patient_email,subdomain: subdomain)
  puts "Patients Counts to : #{resources.count}"
 
  header_1,data_1 = Array.new,Array.new
  header_1 << "----------- CSV generated on #{Time.now.strftime("%d-%m-%y")} -----------"
  header_1 << "\m" 
  
  resources.each do |patient|
    data_1 << patient.id
    data_1 << patient.email
    data_1 << patient.subdomain
    data_1 << patient.try(:profile).try(:customer_profile_id)
    data_1 << patient.joint_resource.id
    data_1 << "\m"

    patient.joint_resource.destroy

  end


  user_csv = CSV.generate do |csv|  
   if data_1.any?
     header_1 = header_1.split("\m")
     header_1.each do |heading|
       csv << heading 
     end
   end
   data_1 = data_1.split("\m")
     data_1.each do |detail|
      csv << detail
     end
  end

  log_path = Rails.env.include?("staging") ? "/home/deployed/www/doctor_crm/shared/log/deleted_records.log" : (Rails.env.include?("development") ? "/home/enbake12/shared/log/deleted_records.log" : "/home/sheriff/www/doctor_crm/shared/log/deleted_records.log")
  File.open(log_path, "a+") do |f|
    f.write(user_csv)
    f.close()
  end

end