namespace :update_prescription_dates do
desc "Date showing '00' as starting two digit of year for issued_date and expired_date attributes of GlassesPrescription Table"  
  task :update => :environment do
    glass_prescriptions = GlassesPrescription.where('extract(year from expired_date) < ?',20)
    glass_prescriptions.each do |g_p|
    	g_p.update_attributes(:expired_date => g_p.expired_date + 2000.year,:issued_date => g_p.issued_date + 2000.year)
    	puts "Now year format changes starting two digit to like 20.."
    end
  end
end