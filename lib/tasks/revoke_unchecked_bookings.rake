#This rake is not required not as we have a worker for it which is scheduled by sidetiq
namespace :revoke_unchecked_bookings do
  task :examinations , [:time_in, :time_out ] => :environment do |t,args|
    puts "***************************************  #{Time.zone.now.strftime("%m/%d/%Y at %I:%M%p")} ***************************************"
    Organisation.all.each do |org|
      Time.zone = org.time_zone
      time_in = args[:time_in].present? ? DateTime.parse(args[:time_in]) : Time.zone.now-2.hours               #if this rake is breaking over server (by checking cron logs) ,then you can replace the time.now-n.days to remove missed appointment examinations as it might raise validation fails error for 'last examination not delivered yet!' 
      time_out = args[:time_out].present? ? DateTime.parse(args[:time_out]) : Time.zone.now-1.hour
      passed_bookings = org.bookings.where(end_time: time_in..time_out,check_in: nil)
      passed_bookings.each do |booking|
        booking.update_columns(:status => 3)        # no show
        puts "-----------------##{booking.id} Booking with no examination ; booking status updated to No Show"
      end
    end
  end
end