namespace :finalize_exam_rake do
desc "Finalize exams,if exam has not been finalized after 6 months."  
  task :update => :environment do
    unf_exam = Examination.where('status NOT IN (?) AND created_at <= (?)',[5,6],(Date.today - 6.months))
    unf_exam.each do |u_e|
    	u_e.update_attributes(:status => 5)
    	puts "Booking's status updated to Finalized"
    end
  end
end