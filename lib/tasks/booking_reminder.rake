namespace :booking_reminder do
  task :reminder => :environment do
    include ActionView::Helpers::NumberHelper
    puts "************************Start At  #{Time.zone.now.strftime("%m/%d/%Y at %I:%M%p")} ************************"
    Organisation.all.each do |org|
      Time.zone = org.time_zone
      time_in = Time.zone.now.to_date
      
      bookings = org.bookings.where("Date(start_time) = ?", time_in+6.months)
      puts "-----------------##{bookings.count} Booking after 6 months;"
      bookings.each do |booking|
        AppMailer.send_booking_reminder_to_patient(booking,"6 Months").deliver_later unless booking.try(:joint_resource).try(:patient).try(:email_not_supplied)
        patient = booking.join_resource.member
        one_click_url=Rails.application.routes.url_helpers.root_url(host: CrmDatabase::Application.config.action_mailer.default_url_options[:host], sudomain: patient.try(:subdomain),  patient_email: patient.try(:email), authentication_token: patient.try(:authentication_token), subdomain: patient.try(:subdomain))
        one_click_url.gsub!(patient.try(:subdomain), patient.try(:subdomain) + ".dev") if Rails.env.include?('staging') && patient.try(:subdomain) != 'dev'
        one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url 
        if patient.home_address == nil
          notification_message = AppConfig.sms_notification["appointment"] % {patient_name: booking.joint_resource.member.try(:name), doctor_name: booking.doctor.name, appointment_time: booking.start_time.strftime('%m/%d/%Y %I:%M%P'), appointment_end_time: time_in+6.months, one_click_login: one_click_url, store_name: booking.store.description , store_address: "#{booking.store.try(:office_address).try(:address1)}, #{booking.store.try(:office_address).try(:address2)}, postal code #{booking.store.try(:office_address).try(:postal_code)}", store_phone_number: "#{number_to_phone(booking.store.phone_number.to_s[2..-1].to_i, area_code: true)}", store_office_homepage: "#{booking.store.url}"}
        else
          notification_message = AppConfig.sms_notification["appointment_reminder"] % {patient_name: booking.joint_resource.member.try(:name), doctor_name: booking.doctor.name, appointment_time: booking.start_time.strftime('%m/%d/%Y %I:%M%P'), appointment_end_time: time_in+6.months, one_click_login: one_click_url, store_name: booking.store.description , store_address: "#{booking.store.try(:office_address).try(:address1)}, #{booking.store.try(:office_address).try(:address2)}, postal code #{booking.store.try(:office_address).try(:postal_code)}", store_phone_number: "#{number_to_phone(booking.store.phone_number.to_s[2..-1].to_i, area_code: true)}", store_office_homepage: "#{booking.store.url}"}
        end
        TwilioSmsWorker.perform_async({'src'=> booking.organisation.twilio_number, 'dst' => patient.phone, 'text' => notification_message, 'trackable_id'=> booking.id, 'trackable_type'=> 'Booking', 'notification_method'=> patient.notification_method,'org_sid'=> booking.organisation.twilio_acc_sid,'org_auth_token'=> booking.organisation.twilio_auth_token})
      end
      
      bookings = org.bookings.where("Date(start_time) = ?", time_in+2.months)
      puts "-----------------##{bookings.count} Booking after 2 months;"
      bookings.each do |booking|
        AppMailer.send_booking_reminder_to_patient(booking,"2 Months").deliver_later unless booking.try(:joint_resource).try(:patient).try(:email_not_supplied)
        patient = booking.join_resource.member
        one_click_url=Rails.application.routes.url_helpers.root_url(host: CrmDatabase::Application.config.action_mailer.default_url_options[:host], sudomain: patient.try(:subdomain),  patient_email: patient.try(:email), authentication_token: patient.try(:authentication_token), subdomain: patient.try(:subdomain))
        one_click_url.gsub!(patient.try(:subdomain), patient.try(:subdomain) + ".dev") if Rails.env.include?('staging') && patient.try(:subdomain) != 'dev'
        one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url 
        if patient.home_address == nil
          notification_message = AppConfig.sms_notification["appointment"] % {patient_name: booking.joint_resource.member.try(:name), doctor_name: booking.doctor.name, appointment_time: booking.start_time.strftime('%m/%d/%Y %I:%M%P'), appointment_end_time: time_in+2.months, one_click_login: one_click_url, store_name: booking.store.description , store_address: "#{booking.store.try(:office_address).try(:address1)}, #{booking.store.try(:office_address).try(:address2)}, postal code #{booking.store.try(:office_address).try(:postal_code)}", store_phone_number: "#{number_to_phone(booking.store.phone_number.to_s[2..-1].to_i, area_code: true)}", store_office_homepage: "#{booking.store.url}"}
        else
          notification_message = AppConfig.sms_notification["appointment_reminder"] % {patient_name: booking.joint_resource.member.try(:name), doctor_name: booking.doctor.name, appointment_time: booking.start_time.strftime('%m/%d/%Y %I:%M%P'), appointment_end_time: time_in+2.months, one_click_login: one_click_url, store_name: booking.store.description , store_address: "#{booking.store.try(:office_address).try(:address1)}, #{booking.store.try(:office_address).try(:address2)}, postal code #{booking.store.try(:office_address).try(:postal_code)}", store_phone_number: "#{number_to_phone(booking.store.phone_number.to_s[2..-1].to_i, area_code: true)}", store_office_homepage: "#{booking.store.url}"}
        end
        TwilioSmsWorker.perform_async({'src'=> booking.organisation.twilio_number, 'dst' => patient.phone, 'text' => notification_message, 'trackable_id'=> booking.id, 'trackable_type'=> 'Booking', 'notification_method'=> patient.notification_method,'org_sid'=> booking.organisation.twilio_acc_sid,'org_auth_token'=> booking.organisation.twilio_auth_token})
      end
      
      bookings = org.bookings.where("Date(start_time) = ?", time_in+2.weeks)
      puts "-----------------##{bookings.count} Booking after 2 weeks;"
      bookings.each do |booking|
        AppMailer.send_booking_reminder_to_patient(booking,"2 Weeks").deliver_later unless booking.try(:joint_resource).try(:patient).try(:email_not_supplied)
        patient = booking.join_resource.member
        one_click_url=Rails.application.routes.url_helpers.root_url(host: CrmDatabase::Application.config.action_mailer.default_url_options[:host], sudomain: patient.try(:subdomain),  patient_email: patient.try(:email), authentication_token: patient.try(:authentication_token), subdomain: patient.try(:subdomain))
        one_click_url.gsub!(patient.try(:subdomain), patient.try(:subdomain) + ".dev") if Rails.env.include?('staging') && patient.try(:subdomain) != 'dev'
        one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url 
        if patient.home_address == nil
          notification_message = AppConfig.sms_notification["appointment"] % {patient_name: booking.joint_resource.member.try(:name), doctor_name: booking.doctor.name, appointment_time: booking.start_time.strftime('%m/%d/%Y %I:%M%P'), appointment_end_time: time_in+2.weeks, one_click_login: one_click_url, store_name: booking.store.description , store_address: "#{booking.store.try(:office_address).try(:address1)}, #{booking.store.try(:office_address).try(:address2)}, postal code #{booking.store.try(:office_address).try(:postal_code)}", store_phone_number: "#{number_to_phone(booking.store.phone_number.to_s[2..-1].to_i, area_code: true)}", store_office_homepage: "#{booking.store.url}"}
        else
          notification_message = AppConfig.sms_notification["appointment_reminder"] % {patient_name: booking.joint_resource.member.try(:name), doctor_name: booking.doctor.name, appointment_time: booking.start_time.strftime('%m/%d/%Y %I:%M%P'), appointment_end_time: time_in+2.weeks, one_click_login: one_click_url, store_name: booking.store.description , store_address: "#{booking.store.try(:office_address).try(:address1)}, #{booking.store.try(:office_address).try(:address2)}, postal code #{booking.store.try(:office_address).try(:postal_code)}", store_phone_number: "#{number_to_phone(booking.store.phone_number.to_s[2..-1].to_i, area_code: true)}", store_office_homepage: "#{booking.store.url}"}
        end
        TwilioSmsWorker.perform_async({'src'=> booking.organisation.twilio_number, 'dst' => patient.phone, 'text' => notification_message, 'trackable_id'=> booking.id, 'trackable_type'=> 'Booking', 'notification_method'=> patient.notification_method,'org_sid'=> booking.organisation.twilio_acc_sid,'org_auth_token'=> booking.organisation.twilio_auth_token})
      end
      
      bookings = org.bookings.where("Date(start_time) = ?", time_in+1.week)
      puts "-----------------##{bookings.count} Booking after 1 week;"
      bookings.each do |booking|
        AppMailer.send_booking_reminder_to_patient(booking,"1 Week").deliver_later unless booking.try(:joint_resource).try(:patient).try(:email_not_supplied)
        patient = booking.join_resource.member
        one_click_url=Rails.application.routes.url_helpers.root_url(host: CrmDatabase::Application.config.action_mailer.default_url_options[:host], sudomain: patient.try(:subdomain),  patient_email: patient.try(:email), authentication_token: patient.try(:authentication_token), subdomain: patient.try(:subdomain))
        one_click_url.gsub!(patient.try(:subdomain), patient.try(:subdomain) + ".dev") if Rails.env.include?('staging') && patient.try(:subdomain) != 'dev'
        one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url 
        if patient.home_address == nil
          notification_message = AppConfig.sms_notification["appointment"] % {patient_name: booking.joint_resource.member.try(:name), doctor_name: booking.doctor.name, appointment_time: booking.start_time.strftime('%m/%d/%Y %I:%M%P'), appointment_end_time: time_in+1.week, one_click_login: one_click_url, store_name: booking.store.description , store_address: "#{booking.store.try(:office_address).try(:address1)}, #{booking.store.try(:office_address).try(:address2)}, postal code #{booking.store.try(:office_address).try(:postal_code)}", store_phone_number: "#{number_to_phone(booking.store.phone_number.to_s[2..-1].to_i, area_code: true)}", store_office_homepage: "#{booking.store.url}"}
        else
          notification_message = AppConfig.sms_notification["appointment_reminder"] % {patient_name: booking.joint_resource.member.try(:name), doctor_name: booking.doctor.name, appointment_time: booking.start_time.strftime('%m/%d/%Y %I:%M%P'), appointment_end_time: time_in+1.week, one_click_login: one_click_url, store_name: booking.store.description , store_address: "#{booking.store.try(:office_address).try(:address1)}, #{booking.store.try(:office_address).try(:address2)}, postal code #{booking.store.try(:office_address).try(:postal_code)}", store_phone_number: "#{number_to_phone(booking.store.phone_number.to_s[2..-1].to_i, area_code: true)}", store_office_homepage: "#{booking.store.url}"}
        end
        TwilioSmsWorker.perform_async({'src'=> booking.organisation.twilio_number, 'dst' => patient.phone, 'text' => notification_message, 'trackable_id'=> booking.id, 'trackable_type'=> 'Booking', 'notification_method'=> patient.notification_method,'org_sid'=> booking.organisation.twilio_acc_sid,'org_auth_token'=> booking.organisation.twilio_auth_token})
      end
      
      bookings = org.bookings.where("Date(start_time) = ?", time_in+2.days)
      puts "-----------------##{bookings.count} Booking after 2 days;"
      bookings.each do |booking|
        AppMailer.send_booking_reminder_to_patient(booking,"2 Days").deliver_later unless booking.try(:joint_resource).try(:patient).try(:email_not_supplied)
        patient = booking.join_resource.member
        one_click_url=Rails.application.routes.url_helpers.root_url(host: CrmDatabase::Application.config.action_mailer.default_url_options[:host], sudomain: patient.try(:subdomain),  patient_email: patient.try(:email), authentication_token: patient.try(:authentication_token), subdomain: patient.try(:subdomain))
        one_click_url.gsub!(patient.try(:subdomain), patient.try(:subdomain) + ".dev") if Rails.env.include?('staging') && patient.try(:subdomain) != 'dev'
        one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url 
        if patient.home_address == nil
          notification_message = AppConfig.sms_notification["appointment"] % {patient_name: booking.joint_resource.member.try(:name), doctor_name: booking.doctor.name, appointment_time: booking.start_time.strftime('%m/%d/%Y %I:%M%P'), appointment_end_time: time_in+2.days, one_click_login: one_click_url, store_name: booking.store.description , store_address: "#{booking.store.try(:office_address).try(:address1)}, #{booking.store.try(:office_address).try(:address2)}, postal code #{booking.store.try(:office_address).try(:postal_code)}", store_phone_number: "#{number_to_phone(booking.store.phone_number.to_s[2..-1].to_i, area_code: true)}", store_office_homepage: "#{booking.store.url}"}
        else
          notification_message = AppConfig.sms_notification["appointment_reminder"] % {patient_name: booking.joint_resource.member.try(:name), doctor_name: booking.doctor.name, appointment_time: booking.start_time.strftime('%m/%d/%Y %I:%M%P'), appointment_end_time: time_in+2.days, one_click_login: one_click_url, store_name: booking.store.description , store_address: "#{booking.store.try(:office_address).try(:address1)}, #{booking.store.try(:office_address).try(:address2)}, postal code #{booking.store.try(:office_address).try(:postal_code)}", store_phone_number: "#{number_to_phone(booking.store.phone_number.to_s[2..-1].to_i, area_code: true)}", store_office_homepage: "#{booking.store.url}"}
        end
        TwilioSmsWorker.perform_async({'src'=> booking.organisation.twilio_number, 'dst' => patient.phone, 'text' => notification_message, 'trackable_id'=> booking.id, 'trackable_type'=> 'Booking', 'notification_method'=> patient.notification_method,'org_sid'=> booking.organisation.twilio_acc_sid,'org_auth_token'=> booking.organisation.twilio_auth_token})
      end
      
      bookings = org.bookings.where("Date(start_time) = ?", time_in+1.day)
      puts "-----------------##{bookings.count} Booking after 24 hours;"
      bookings.each do |booking|
        AppMailer.send_booking_reminder_to_patient(booking,"24 Hours").deliver_later unless booking.try(:joint_resource).try(:patient).try(:email_not_supplied)
        patient = booking.join_resource.member
        one_click_url=Rails.application.routes.url_helpers.root_url(host: CrmDatabase::Application.config.action_mailer.default_url_options[:host], sudomain: patient.try(:subdomain),  patient_email: patient.try(:email), authentication_token: patient.try(:authentication_token), subdomain: patient.try(:subdomain))
        one_click_url.gsub!(patient.try(:subdomain), patient.try(:subdomain) + ".dev") if Rails.env.include?('staging') && patient.try(:subdomain) != 'dev'
        one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url 
        if patient.home_address == nil
          notification_message = AppConfig.sms_notification["appointment"] % {patient_name: booking.joint_resource.member.try(:name), doctor_name: booking.doctor.name, appointment_time: booking.start_time.strftime('%m/%d/%Y %I:%M%P'), appointment_end_time: time_in+1.day, one_click_login: one_click_url, store_name: booking.store.description , store_address: "#{booking.store.try(:office_address).try(:address1)}, #{booking.store.try(:office_address).try(:address2)}, postal code #{booking.store.try(:office_address).try(:postal_code)}", store_phone_number: "#{number_to_phone(booking.store.phone_number.to_s[2..-1].to_i, area_code: true)}", store_office_homepage: "#{booking.store.url}"}
        else
          notification_message = AppConfig.sms_notification["appointment_reminder"] % {patient_name: booking.joint_resource.member.try(:name), doctor_name: booking.doctor.name, appointment_time: booking.start_time.strftime('%m/%d/%Y %I:%M%P'), appointment_end_time: time_in+1.day, one_click_login: one_click_url, store_name: booking.store.description , store_address: "#{booking.store.try(:office_address).try(:address1)}, #{booking.store.try(:office_address).try(:address2)}, postal code #{booking.store.try(:office_address).try(:postal_code)}", store_phone_number: "#{number_to_phone(booking.store.phone_number.to_s[2..-1].to_i, area_code: true)}", store_office_homepage: "#{booking.store.url}"}
        end
        TwilioSmsWorker.perform_async({'src'=> booking.organisation.twilio_number, 'dst' => patient.phone, 'text' => notification_message, 'trackable_id'=> booking.id, 'trackable_type'=> 'Booking', 'notification_method'=> patient.notification_method,'org_sid'=> booking.organisation.twilio_acc_sid,'org_auth_token'=> booking.organisation.twilio_auth_token})
      end
    end
    puts "************************End At  #{Time.zone.now.strftime("%m/%d/%Y at %I:%M%p")} ************************"
  end
end