namespace :holiday do
  task :update => :environment do
    Organisation.all.each do |org|
      org.holidays.each do |leave|
      	if leave.end_date.nil?
	        leave.end_date = leave.date.to_time.to_date 
	        leave.save!
	      end
      end
    end
  end
end