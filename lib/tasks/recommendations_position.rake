namespace :recommendations_position do
  task :update => :environment do
    Organisation.all.each do |org|
      org.doctor_recommendations.each_with_index do |reco,i|
        reco.position = i + 1
        reco.save!
      end
    end
  end
end