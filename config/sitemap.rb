# Set the host name for URL creation
subdomains = [""]
subdomains = subdomains + Organisation.where(approved: true).map(&:domain_name)

subdomains.each do |subdomain|
  subdomain_url = subdomain + "." unless subdomain.blank?
  subdomain_url = subdomain if subdomain.blank?
  SitemapGenerator::Sitemap.default_host = "https://#{subdomain_url}itrust.io"
  SitemapGenerator::Sitemap.sitemaps_path = "sitemaps/#{subdomain}" unless subdomain.blank?
  SitemapGenerator::Sitemap.sitemaps_path = "sitemaps/itrust.io" if subdomain.blank?
  SitemapGenerator::Sitemap.create do
    # Put links creation logic here.
    #
    # The root path '/' and sitemap index file are added automatically for you.
    # Links are added to the Sitemap in the order they are specified.
    #
    # Usage: add(path, options={})
    #        (default options are used if you don't specify)
    #
    # Defaults: :priority => 0.5, :changefreq => 'weekly',
    #           :lastmod => Time.now, :host => default_host
    #
    # Examples:
    #
    # Add '/articles'
    #
    #   add articles_path, :priority => 0.7, :changefreq => 'daily'
    #
    # Add all articles:
    #
    #   Article.find_each do |article|
    #     add article_path(article), :lastmod => article.updated_at
    #   end
    add("/#about_us" ,:mobile => true)
    add("/#features" ,:mobile => true)
    add("/#prices" ,:mobile => true)
    add("/#testimonials" ,:mobile => true)
    add("/#organisation" ,:changefreq => 'monthly',:mobile => true)
    add(new_booking_path ,:changefreq => 'monthly',:mobile => true,:priority => 1.0)
    add(patient_session_path ,:changefreq => 'monthly',:mobile => true,:priority => 1.0)
    add(doctor_session_path ,:changefreq => 'monthly',:mobile => true,:priority => 1.0)
  end
end