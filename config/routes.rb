require 'sidekiq/web'
require 'subdomain'
Rails.application.routes.draw do
  comfy_route :cms_admin, :path => '/admin'

  constraints(Subdomain) do
    devise_for :doctors, controllers: {sessions: "doctor/sessions"}, :skip => [:registrations]
    as :doctor do
      get 'doctors/edit' => 'devise/registrations#edit', :as => 'edit_doctor_registration'    
      put 'doctors' => 'devise/registrations#update', :as => 'doctor_registration'            
    end
    devise_for :patients, controllers: {registrations: "patient/patients", sessions: "patient/sessions", confirmations: 'patient/confirmations'}

    authenticate :doctor, lambda { |u| u.role.include?("super_administrator")} do
      mount Sidekiq::Web => '/sidekiq'
    end 

    authenticated :doctor do
      root 'doctor/dashboards#index', as: :doctor_dashboards
      resources "dashboards", module: 'doctor' do       
       post :profile_update, on: :collection
       put :password_update, on: :collection
       get :background_update, on: :collection
       post :address, on: :collection       
       get :order_audits,on: :member
       get :exam_audits,on: :member
       post :card_verification,on: :collection
       get :email_prescription,on: :member
       get :confirm_account, on: :member
       get :reconfirm_notify ,on: :member
       #patient profile actions
       post :new_payment_profile, on: :collection
       post :payment_method_update, on: :collection
       get :account_section, on: :collection
       get :remove_card, on: :collection
       patch :update_patient,on: :member
       put :patient_password_update, on: :collection
       get :todays_appointments,on: :collection
       get :exam_section,on: :collection
       get :order_section,on: :collection
       get :follow_section,on: :collection
       get :dispense_section,on: :collection
      end 

      resources 'examinations', module: 'doctor' do 
        get :view, on: :member
        patch :upload_file, on: :member
        get :download_file, on: :collection
        put :update_medical_rx,on: :member
        get :delete_file, on: :collection
        get :remove_medical_rx, on: :collection
        resources 'referrals' do
          get :send_mail_to_patient, on: :member
          get :find_contact, on: :collection
        end
        resources "manage", controller: 'manage_examination' do
          get :delete_impression, on: :member
          get :delete_procedure, on: :member
          get :medical_history, on: :collection
          get :primilary_binocular, on: :collection
          get :refraction_contact_lens, on: :collection
          get :external_internal, on: :collection
          get :procedure_impression, on: :collection
          get :finalized_prescriptions, on: :collection
          get :attached_docs, on: :collection
          delete :remove_glass_prescription, on: :collection
          delete :remove_lens_prescription, on: :collection 
          delete :remove_lens, on: :collection
          delete :remove_glass, on: :collection         
        end
      end
      get 'doctor/patient/profile' => 'doctor/dashboards#customer_profile'
      get 'doctor/patient/logs' => 'doctor/dashboards#patient_log'
      #get 'doctor/patient/search' => 'doctor/dashboards#search'
      get 'doctor/patient/info' => 'doctor/dashboards#patient_info'
      get '/doctor/patient/:id' => 'patient/dashboards#edit' ,as: 'patient_edit'
      #get '/examination/:id/audits' => 'doctor/dashboards#',as: 'exam_audits'
      resources 'contact_lens_exam', module: 'doctor'
      resources 'dispense_logs',module: 'doctor' do
        get :all_logs,on: :collection
        get :add_new,on: :collection
        get :email_order_ready,on: :collection
      end
      resources :follow_ups,module: 'doctor' do
        get :contact_days,on: :collection
      end
      match 'examination/:examination_id/jr/:id/orders/new', to: "doctor/orders#new",via: [:get,:post], as: :new_order_for_exam
      match 'order/:order_id/jr/:id/examinations/new', to: "doctor/examinations#new",via: [:get,:post], as: :new_exam_for_order
      resources 'orders',module: 'doctor' do
        get :complete_options, on: :member
        get :reload_procedures,on: :member
        patch :add_procedures, on: :member
        delete :delete_procedures, on: :member
        #get :add_contact_lens, on: :collection
        #get :add_eyeglass,on: :collection
        #post :update_new_lens, on: :collection
        #post :update_glass, on: :collection
        #get :eyeglass_suborder, on: :collection
        #get :lens_suborder, on: :collection
        get :lens_prescriptions,on: :collection
        get :lens_prices_base_curves,on: :collection
        get :glass_prescriptions,on: :collection
        get :glass_price,on: :collection
        get :lens_addons_price,on: :collection
        get :glass_frame_price,on: :collection
        get :lens_material_price,on: :collection
        get :lab_order,on: :member
        get :invoice,on: :member
        get :checklist,on: :member
        post :update_checklist,on: :member
        get :dispense,on: :member
        get :email_invoice,on: :member
        get :email_card_warning,on: :member
        #get :glass_order_prescription,on: :collection
        #get :lens_order_prescription,on: :collection
        #get :all_suborders,on: :member
        #new order form actions
        put :refund_amount, on: :member
        get :create_glass_suborder, on: :member
        get :add_glass_addon, on: :member
        get :delete_glass_addon, on: :member
        get :delete_eyeglass_suborder, on: :member
        put :update_eyeglass_suborder, on: :member
        put :update_contact_lens_suborder, on: :member
        get :create_contact_lens_suborder, on: :member
        delete :delete_contact_lens_suborder, on: :member
        get :reload_removed_lens, on: :member
        put :calculate_lens_subtotal, on: :member
        put :calculate_glass_subtotal, on: :member
        get :show_lens_suborder, on: :member
        get :show_glass_suborder, on: :member
        get :reload_updated_lens, on: :member
        get :reload_updated_glass, on: :member
        put :hold_lens_suborder, on: :member
        put :hold_glass_suborder, on: :member
        get :holded_lens_suborder, on: :member
        get :holded_glass_suborder, on: :member
        get :reload_removed_glass, on: :member
        #users listing
        get :online_users,on: :collection
      end
      
      resources 'manage_patients', module: 'doctor', as: 'manage_patients_by_doctor' do
        get :patients_status, on: :collection
        get :todays_appointments,on: :collection
        # get :exam_procedures,on: :collection
        # put :exam_procedure_update,on: :collection
        #get :process_booking, on: :member         #this route is not in use 
      end
      
      resources 'manage_organisations', module: 'doctor' do
        delete :remove_pending_org, on: :collection
      end

      resources 'analytics', module: 'doctor' do
        get :filter, on: :collection
        get :orders_detail, on: :collection
      end

      resources 'setting_patients', module: 'doctor', only: [:index,:update] do        
        post :add_vision_insurance, on: :collection
        post :add_medical_insurance, on: :collection
        get :update_vision_insurance, on: :collection
        get :update_medical_insurance, on: :collection
        delete :delete_vision_insurance, on: :collection
        delete :delete_medical_insurance, on: :collection
        put :upload_file, on: :member
        delete :delete_file, on: :collection
      end

      resources 'setting_schedulers', module: 'doctor', only: [:index] do
        post :add_holiday, on: :collection
        delete :delete_holiday, on: :collection
        post :update_working_schedule, on: :collection
        get :working_schedule, on: :collection
        get :doctor_leave_closure, on: :collection
      end

      resources 'setting_orders', module: 'doctor', only: [:index, :update] do
        post :add_eyeglass_lens_type, on: :collection
        delete :delete_eyeglass_lens_type, on: :collection

        post :add_eyeglass_lens_material, on: :collection
        delete :delete_eyeglass_lens_material, on: :collection

        post :add_eyeglass_addon, on: :collection
        delete :delete_eyeglass_addon, on: :collection

        post :add_eyeglass_frame, on: :collection
        delete :delete_eyeglass_frame, on: :collection

        get :edit_records, on: :collection
        post :update_records, on: :collection
      end

      resources 'settings', module: 'doctor', only: [:index, :update] do 
        post :add_disease, on: :collection
        delete :delete_disease, on: :collection

        post :add_procedure, on: :collection
        delete :delete_procedure, on: :collection

        post :add_lens, on: :collection
        delete :delete_lens, on: :collection

        post :add_impression, on: :collection
        delete :delete_impression, on: :collection

        post :add_recommendation, on: :collection
        delete :delete_recommendation, on: :collection

        get :edit_records, on: :collection
        post :update_records, on: :collection

        get :update_procedure_order, on: :collection
        get :update_recommendation_order, on: :collection

        put :set_default_expiry, on: :member
        put :set_default_escribe_expiry, on: :member

        post :add_exam_cleaning_solution, on: :collection
        delete :delete_cleaning_solution, on: :collection

        put :set_defaut_slot_time, on: :member

        get :exam_form_defaults, on: :collection

        post :set_preliminaries_defaults, on: :collection

        post :set_binocular_defaults, on: :collection

        post :set_external_defaults, on: :collection

        post :set_internal_defaults, on: :collection
      end

      resources "bookings", module: "doctor"  do        
        get :calendar, on: :collection
        get :calendar_basic_requirement, on: :collection
        post :order_and_exam,on: :collection
        get :get_view,  on: :collection
        get :multiselect_procedure_select, on: :collection
        get :vision_insurance_select, on: :collection
        get :medical_insurance_select, on: :collection
        get :doctor_list_select, on: :collection
        get :check_in, on: :member
        get :restore_booking, on: :member
        get :doctor_list,  on: :collection
      end   
      resources 'campaigns', module: 'doctor' do
        get :analytic, on: :collection
        get :delivered_summary, on: :collection
        get :failed_summary, on: :collection
        get :opened_summary, on: :collection
        get :unsubscribed_summary, on: :collection
        get :delete_unsubscribe, on: :collection
        get :email_summary, on: :collection
        get :graph_view, on: :collection
        get :paginate_option, on: :collection
      end
      
      resources 'seo', module: 'doctor'
      resources 'stores', module: 'doctor' do
        get :manage_view, on: :collection
      end
      resources 'staff', module: 'doctor'
      resources 'contacts', module: 'doctor'
      resources 'contact_lens_distribution_orders', module: 'doctor' do
        get :lens_pdf, on: :collection
        get :order_contact, on: :collection
        get :send_report, on: :collection  
      end 
      resources 'favourites', module: 'doctor', only: [:show,:destroy] do
        put :status_update, on: :member
        put :checklist_update,on: :member
        get :dispense,on: :member
        get :invoice,on: :member
        get :email_invoice,on: :member
        get :order_audits,on: :member
        get :selected_patient_prescriptions, on: :collection
        get :one_click_buy, on: :collection
        get :get_1_click, on: :collection
        post :calculate_order_total, on: :collection
        post :create_favourite, on: :collection
      end

      resources "reports", module: 'doctor', except: [:index, :show, :new, :create, :edit, :update, :destroy] do 
        collection do 
          get :referral
          get :invoice
          get :prescription
        end
      end
      resources "notifications", module: 'doctor' do
        get :contact_form_notifications, on: :collection
        get :booking_notifications, on: :collection
      end
      resources "family_members", module: "doctor" do
        patch :upload_file, on: :member
        get :family_members, on: :collection
        get :reports, on: :collection
        get :detail_audits, on: :member
        put :update_relation, on: :collection
        get :download_file, on: :collection
        get :delete_file, on: :collection
      end
      resources 'order_subscriptions', module: 'patient' do
        get :selected_patient_prescriptions, on: :collection
        get :subscriptions, on: :collection
        get :selected_prescription, on: :collection
        get :interval_boxes, on: :collection
        get :audits, on: :member
      end
      resources "patients", module: "patient" do    
        patch :update_patient,on: :member
        get :sent_reset_password_instruction, on: :collection
        put :password_update, on: :collection
        get :background_update, on: :collection 
        post :address, on: :collection
        get :profile_payment, on: :collection
        get :check_duplicate_email, on: :collection
      end

    end


    authenticated :patient do
      root 'patient/dashboards#index', as: :patient_dashboards    
      resources "dashboards", module: "patient" do
        post :profile_update, on: :collection
        post :new_payment_profile, on: :collection
        post :payment_method_update, on: :collection
        get :payment_audits, on: :collection
        get :remove_card, on: :collection
        #concern :patient_account_section
        get :account_section, on: :collection
      end
      
      resources "patients", module: "patient" do    
        patch :update_patient,on: :member
        get :sent_reset_password_instruction, on: :collection
        put :password_update, on: :collection
        get :background_update, on: :collection 
        post :address, on: :collection
        get :profile_payment, on: :collection
        get :check_duplicate_email, on: :collection
      end

      resources "reports", module: 'patient', except: [:index, :show, :new, :create, :edit, :update, :destroy] do 
        collection do 
          get :referral
          get :invoice
          get :prescription
        end
      end
 
      resources 'order_subscriptions', module: 'patient' do
        get :selected_patient_prescriptions, on: :collection
        get :subscriptions, on: :collection
        get :selected_prescription, on: :collection
        get :interval_boxes, on: :collection
      end
      resources 'favourites', module: 'patient', only: [:create] do
        get :selected_patient_prescriptions, on: :collection
        get :one_click_buy, on: :collection
        get :get_1_click, on: :collection
        post :calculate_order_total, on: :collection
        post :create_favourite, on: :collection
      end

      resources "bookings", module: "patient"  do
        get :get_free_slot, on: :collection
        get :get_registration_form, on: :collection
        get :doctor_list, on: :collection
      end
      
      resources "family_members", module: "patient" do
        get :family_members, on: :collection
        get :reports, on: :collection
        put :update_relation, on: :collection
      end
      get 'patient/dashboards/:id' => 'patient/dashboards#show' ,as: 'patient_report'
      resources 'payments', module: 'patient', only: [:create]
      resources 'notifications', module: 'patient' do
        get :booking_notifications, on: :collection
      end
    end    

    resources "bookings" do
      get :get_free_slot, on: :collection
      get :get_registration_form, on: :collection
      get :doctor_list, on: :collection
      get :check_duplicate_email, on: :collection
    end
    
    resources 'payments', module: 'patient', only: [:create,:create_using_profile] do
      post :create_using_profile,on: :collection
      get :refund_using_profile,on: :member
      get :pay_online,on: :collection
      get :create_payment_method,on: :collection
      get :delete_payment_method,on: :collection
    end

    resources "contact_forms" do 
      post :reply, on: :member
    end

    resources 'backgrounds', only: [:create]# need to integrate properly
    resources :surveys, only: [:new, :create]

    resources :autocompletes do
      get :autocomplete_patient_first_name, on: :collection
      get :autocomplete_disease_name, on: :collection
      get :autocomplete_exam_procedure_name, on: :collection
      get :autocomplete_impression_name, on: :collection
      get :autocomplete_lens_types_price_name, on: :collection
      get :autocomplete_contact_name, on: :collection
    end
    
    get "/auth/google_oauth2/callback" => "omniauth_callbacks#google_oauth2"
    get 'patients/new' => 'patient/patients#new', as: 'new_pat_registration'
    post 'patient/submit_registration' => 'patient/dashboards#create', as: 'create_new_patient'
    get '/pages/store_doctors_option' => "pages#store_doctors_option", as: 'store_doctors_option'
    get '/pages/doctor_calendar_notifications' => "pages#doctor_calendar_notifications", as: 'doctor_calendar_notifications'
    get '/pages/doctor_message_notifications' => "pages#doctor_message_notifications", as: 'doctor_message_notifications'
    get '/pages/patient_calendar_notifications' => "pages#patient_calendar_notifications", as: 'patient_calendar_notifications'
    get 'not_authorised' => "pages#not_authorized", as: 'not_authorized'
  end 

  unauthenticated do
    root 'pages#index'
    get 'org_not_approved' => "pages#org_not_approved", as: 'org_not_approved'
    get 'about_us' => "pages#about_us", as: 'about_us'
    get 'contact_us' => "pages#contact_us", as: 'contact_us'
    get 'prices' => "pages#prices", as: 'prices'
    get 'features' => "pages#features", as: 'features'
    get 'testimonials' => "pages#testimonials", as: 'testimonials'
    
    get '/organisations/new' => "organisations#new", as: 'new_organisation'
    post '/organisations' => "organisations#create", as: 'organisations'
  end 

  resource :mailgun_webhooks, except: [:index, :show, :new, :create, :edit, :update, :destroy] do
    post :delivered_message, on: :collection
    post :dropped_message, on: :collection
    post :hard_bounce, on: :collection
    post :spam_complaint, on: :collection
    post :unsubscribe, on: :collection
    post :click, on: :collection
    post :open, on: :collection
    post :recive_email, on: :collection
    post :call_fallback, on: :collection
  end

  resources :twilio_webhooks, except: [:show, :new, :create, :edit, :update, :destroy] do
    post :recive_message, on: :collection
    post :call_fallback, on: :collection
    post :send_message, on: :member
    get :complete_chat, on: :collection 
    get :index, on: :collection
  end  

  get '/pages/subregion_options' => 'pages#subregion_options'
  get '/pages/resource_info' => "pages#resource_info", as: 'resource_info'
  get '/pages/update_notification' => "pages#update_notification", as: 'update_notification'
  get '/organisations/:id' => "organisations#show", as: 'organisation'
  get '/pages/send_notification' => "pages#send_notification", as: 'send_notification'

  # Make sure this routeset is defined last
  comfy_route :cms, :path => '/', :sitemap => false
  
  #use to redirect on root if route is not defined
  get '*path' => redirect('/') 
end
