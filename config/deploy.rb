# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'doctor_crm'
set :repo_url, 'git@bitbucket.org:drchrycy/enbake.git'

# Default branch is :master
ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, '/home/sheriff/www/doctor_crm'

# Default value for :scm is :git
 set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
 set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml', '.env.production')

# Default value for linked_dirs is []
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')
#set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets public/spree}
# Default value for default_env is {}
#set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Scheduled tasks
#set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }
 # set :whenever_command, 'bundle exec whenever'
 # require 'whenever/capistrano'


set :sidekiq_options_per_process, ["--queue mailers --queue sms --queue default --queue fax"]



namespace :deploy do
  namespace :sidekiq do
    task :quiet do
      on roles(:app) do
        puts capture("pgrep -f 'workers' | xargs kill -USR1") 
      end
    end

    task :restart do
      on roles(:app) do
        execute :sudo, :initctl, :stop, :workers
        execute :sudo, :initctl, :start, :workers
      end
    end
  end

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
    # Here we can do anything such as:
    # within release_path do
    #   execute :rake, 'cache:clear'
    # end
  end
  
  after 'deploy:starting', 'sidekiq:quiet'
  after 'deploy:reverted', 'sidekiq:restart'
  after 'deploy:published', 'sidekiq:restart'
  end
end
