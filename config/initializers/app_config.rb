require 'ostruct'
require 'yaml'

all_config = YAML.load_file("#{Rails.root}/config/config.yml")[Rails.env] || {}
payment_config = YAML.load_file("#{Rails.root}/config/authorize_net.yml")[Rails.env] || {}
all_config.merge! payment_config
AppConfig = OpenStruct.new(all_config)