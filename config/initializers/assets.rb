# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += %w(ie-spacer.gif)
Rails.application.config.assets.precompile += %w( gritter.png )
Rails.application.config.assets.precompile += %w( gritter-close.png )
Rails.application.config.assets.precompile += %w( icheck/line/blue.png )
Rails.application.config.assets.precompile += %w( icheck/line/line.png )
Rails.application.config.assets.precompile += %w( icheck/line/line@2x.png )
Rails.application.config.assets.precompile += %w(patient.js doctor.js unauthenticated_user.js unauthenticated_user.css)
Rails.application.config.assets.precompile += %w( endless_page.js )




