Rails.application.config.middleware.use OmniAuth::Builder do
  provider :google_oauth2, ENV["GOOGLE_CLIENT_ID"], ENV["GOOGLE_CLIENT_SECRET"], 
  {
    :scope => "email, profile, plus.me, plus.login, user.addresses.read, user.birthday.read, user.phonenumbers.read, userinfo.email, userinfo.profile"
  }
end

Rails.application.config.middleware.use OmniAuth::Builder do
  on_failure { |env| OmniauthCallbacksController.action(:failure).call(env) }
end