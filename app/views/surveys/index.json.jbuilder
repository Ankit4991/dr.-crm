json.array!(@surveys) do |survey|
  json.extract! survey, :id, :professionalism, :float,, :work_quality, :float,, :schedule_adherence, :float,, :communication, :float,, :cleanliness, :float,, :feedback
  json.url survey_url(survey, format: :json)
end
