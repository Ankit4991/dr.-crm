##
# Twilio Webhooks menage all SMS records Like Recive SMS from patient, Send SMS to patient

class TwilioWebhooksController < ApplicationController
  skip_before_filter  :verify_authenticity_token
  respond_to :html, :xml, :json

  def index
    begin
      @twilio_webhooks = current_organisation.twilio_webhooks.all.paginate(:page => params[:page], :per_page => 5)
      render partial: 'index'
    rescue
      puts "index: "
      respond_to do |format|
        format.all { render :nothing => true,:status => 200,:content_type => 'text/html' }
      end
    end
  end

  def recive_message
    begin
      params.merge!(from: params["From"], 
                    to: params["To"], 
                    message_id: params["MessageSid"], 
                    from_country: params["FromCountry"], 
                    from_city: params["FromCity"], 
                    body: params["Body"], 
                    sms_status: [params["SmsStatus"]])

      @webhook = TwilioWebhook.create!(webhook_params)
      @webhook.logs.create(message: params["Body"], log_type: "sms", action: 'recive')
      render :json => @webhook   
    rescue
      puts "recive_message:"
      respond_to do |format|
        format.all { render :nothing => true,:status => 200,:content_type => 'text/html' }
      end
    end
  end 

  def send_message
    begin
      @webhook = TwilioWebhook.find(params[:id])
      @webhook.logs.create(message: params[:sms_reply][:message], log_type: "sms", action: 'send')
      TwilioSmsWorker.perform_async({'src'=> current_organisation.twilio_number, 'dst' =>  @webhook.from, 'text' => params[:sms_reply][:message], 'trackable_id'=> @webhook.id, 'trackable_type'=> @webhook.class, 'notification_method'=> 3,'org_sid'=> current_organisation.twilio_acc_sid,'org_auth_token'=> current_organisation.twilio_auth_token})
      respond_to do |format|
        format.html { redirect_to :back }
        format.js 
      end
    rescue
      puts "send_message:"
      respond_to do |format|
        format.all { render :nothing => true,:status => 200,:content_type => 'text/html' }
      end
    end
  end

  def complete_chat
    begin
      @phone_number = params["phone_number"].gsub(" ", "")
      @complete_chat_logs = current_organisation.logs.select("logs.*, new.phone_number").joins("INNER JOIN (select id, phone_number from contact_forms UNION select twilio_webhooks.id, twilio_webhooks.from AS phone_number from twilio_webhooks) AS new ON new.id=logs.trackable_id").where("new.phone_number=?", @phone_number).order("logs.created_at DESC")
      render partial: 'complete_chat'
    rescue 
      puts "complete_chat:"
      respond_to do |format|
        format.all { render :nothing => true,:status => 200,:content_type => 'text/html' }
      end
    end
  end

  def call_fallback
    puts "hello"
  end

  private

    def webhook_params
      params.permit(:from, :to, :message_id, :from_country, :from_city, :body, :sms_status => [] )
    end
end