##
# PagesController have action for all static page
# And other action that are common in Patient and Doctor Like Notification

class PagesController < ApplicationController
  layout "application"

  def index
    if current_organisation.nil?
      @seo = Seo.find_by view_name: 'home'
        if @seo.present?
          set_meta_tags title: @seo.title,
                  description: @seo.description,
                  keywords: @seo.keywords
        end
      @landing_page = true
    else
      redirect_to new_doctor_session_url
    end
  end

  def about_us
    @seo = Seo.find_by view_name: 'about_us'
    if @seo.present?
      set_meta_tags title: @seo.title,
              description: @seo.description,
              keywords: @seo.keywords
    end
  end
  
  def contact_us
    @seo = Seo.find_by view_name: 'contact_us'
    if @seo.present?
      set_meta_tags title: @seo.title,
              description: @seo.description,
              keywords: @seo.keywords
    end
  end

  def prices
    @seo = Seo.find_by view_name: 'pricing'
    if @seo.present?
      set_meta_tags title: @seo.title,
              description: @seo.description,
              keywords: @seo.keywords
    end
  end

  def features
    @seo = Seo.find_by view_name: 'features'
    if @seo.present?
      set_meta_tags title: @seo.title,
              description: @seo.description,
              keywords: @seo.keywords
    end
  end

  def testimonials
    @seo = Seo.find_by view_name: 'testimonials'
    if @seo.present?
      set_meta_tags title: @seo.title,
              description: @seo.description,
              keywords: @seo.keywords
    end
  end
  
  def org_not_approved
  end

  def not_authorized
    flash[:notice] = 'Sorry you are not authorized for this.'
  end

  def subregion_options
    @address_name = params[:address_name].gsub("[country_id]","")
    render partial: 'subregion_select'
  end

  def store_doctors_option    
    @store = Store.find(params[:store_id])
    @dont_need_store_list = params[:dont_need_store_list] == "true" ? true  : false
    render  partial: "store_doctors_option", :locals => {store: @store}

  end

  def doctor_calendar_notifications
    # unless current_doctor.role.join(',') == 'staff'
    unless current_doctor.role.include? "staff"  
      @notes=Notification.select("notifications.*").joins("INNER JOIN doctors_notifications ON notifications.id=doctors_notifications.notification_id").where("doctors_notifications.doctor_id=#{current_doctor.id} and notifications.event_type = 'Booking'").order("notifications.created_at desc").flatten
    else
      @notes=Notification.select("notifications.*").joins("INNER JOIN doctors_notifications ON notifications.id=doctors_notifications.notification_id").where(" notifications.event_type = 'Booking'").order("notifications.created_at desc").flatten
    end
    @notifications=@notes.paginate(:page => params[:page], :per_page => 4)
    respond_to do|format|
      format.html
      format.js
    end
  end

  def doctor_message_notifications
    @notes=Notification.select("notifications.*").joins("INNER JOIN doctors_notifications ON notifications.id=doctors_notifications.notification_id").where("doctors_notifications.doctor_id IN(?) and notifications.event_type IN ('ContactForm','Log')",current_organisation.doctors.map(&:id)).order("notifications.created_at desc").order("notifications.created_at desc").uniq.flatten.last(4)
    @notifications=@notes.paginate(:page => params[:page], :per_page => 4)
    respond_to do|format|
      format.html
      format.js
    end
  end

  def patient_calendar_notifications
      @notifications=current_patient.notifications.paginate(:page => params[:page], :per_page => 2)
  end

  def resource_info
    resource=current_doctor||current_patient
    resource_type= resource.class.name
    render :json => {resource_type: resource_type, resource_id: resource.id}.to_json
  end

  def update_notification
    if params[:notify_for]=="Booking"
      if params[:resource_type]=="Doctor"
        unless Doctor.find(params[:resource_id]).role.include? 'staff'
          DoctorsNotification.select("doctors_notifications.*").joins("INNER JOIN notifications ON notifications.id=doctors_notifications.notification_id").where("doctors_notifications.doctor_id=#{current_doctor.id} and notifications.event_type='Booking'").update_all(status: true)
          render :json => {success: true}.to_json
        else
          DoctorsNotification.select("doctors_notifications.*").joins("INNER JOIN notifications ON notifications.id=doctors_notifications.notification_id").where("notifications.event_type='Booking'").update_all(status: true)
          render :json => {success: true}.to_json
        end
      elsif params[:resource_type]=="Patient"
        Patient.find(params[:resource_id]).notifications.update_all(status: true)
        render :json => {success: true}.to_json
      end
    elsif params[:notify_for]=="ContactForm" || params[:notify_for]=="Log"
      DoctorsNotification.select("doctors_notifications.*").joins("INNER JOIN notifications ON notifications.id=doctors_notifications.notification_id").where("doctors_notifications.doctor_id=#{current_doctor.id} and notifications.event_type IN ('ContactForm','Log')").update_all(status: true)
      render :json => {success: true}.to_json
    end
  end
end