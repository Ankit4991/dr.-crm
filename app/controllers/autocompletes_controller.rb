##
# We are Using rails4-autocomplete Gem, And In this controller we returing our custom values in autocomplete.
# All autocomplete method define in this controller

class AutocompletesController < ApplicationController
  
  # Search for Patient and Family member both
  def autocomplete_patient_first_name
    params[:term] = params[:term].strip
    qry = "%#{params[:term].try(:downcase)}%"
    joint_resources_patient = current_organisation.joint_resources.joins(:patient).where("CONCAT_WS(' ', lower(patients.first_name), lower(patients.last_name)) LIKE ? or CONCAT_WS(' ', lower(patients.last_name), lower(patients.first_name)) LIKE ? or lower(patients.first_name) LIKE ? or lower(patients.last_name) LIKE ? or patients.phone LIKE ? or to_char(patients.birthday, 'MM/DD/YYYY') LIKE ?", qry,qry,qry,qry,qry,qry).order("patients.updated_at DESC")
    joint_resources_family = current_organisation.joint_resources.joins(:family_member).where("CONCAT_WS(' ', lower(family_members.first_name), lower(family_members.last_name)) LIKE ? or CONCAT_WS(' ', lower(family_members.last_name), lower(family_members.first_name)) LIKE ? or lower(family_members.first_name) LIKE ? or lower(family_members.last_name) LIKE ? or family_members.phone LIKE ? or to_char(family_members.birthday, 'MM/DD/YYYY') LIKE ?", qry,qry,qry,qry,qry,qry).order("family_members.updated_at DESC")
    @joint_resources = joint_resources_patient + joint_resources_family
    json_data = @joint_resources.uniq.collect do |item|
      { "id" => item.id.to_s,
       "patient_id" => item.patient_id.to_s, 
       "family_member_id" => item.family_member_id,
       "label" => item.doctor_suggestion_list,  
       "email"=> item.email,
       "vision_insurance_id" => item.member.try(:vision_insurance_id),
       "phone"=>  item.member.phone,
       "birthday"=>  item.member.birthday.try(:strftime, '%m-%d-%y'),
       "first_name"=>  item.member.first_name,
       "last_name"=> item.member.last_name
      }
    end
    respond_to do |format|
      format.html
      format.json { 
        render json: json_data
      }
    end
  end

  def autocomplete_exam_procedure_name
    q = "%#{params[:term].try(:downcase)}%"
    q1 = "#{params[:term]}"
    @exam_procedures = current_organisation.exam_procedures.order(:name).where("lower(name) LIKE ?", q)
    json_data = @exam_procedures.collect do |item| 
      { "id" => item.id.to_s, 
       "label" => item.name,
       "code" => item.code, 
       "price" => item.price
      }
    end

    json_data = [{"id"=>"new_procedure", "label"=>q1,"value" => q1}] unless json_data.present?

    respond_to do |format|
      format.html
      format.json { 
        render json: json_data
      }
    end
  end

  def autocomplete_impression_name
    q = "%#{params[:term].try(:downcase)}%"
    q1 = "#{params[:term]}"
    @impressions = current_organisation.impressions.order(:name).where("lower(name) LIKE ?", q)
    json_data = @impressions.collect do |item| 
      { "id" => item.id.to_s, 
       "label" => item.name,
       "code" => item.code
      }
    end

    json_data = [{"id"=>"new_imp", "label"=>q1,"value" => q1}] unless json_data.present?
    
    respond_to do |format|
      format.html
      format.json { 
        render json: json_data
      }
    end
  end

  def autocomplete_disease_name
    q = "%#{params[:term].try(:downcase)}%"
    q1 = "#{params[:term].split(" ").collect{|word| word[0] = word[0].upcase; word}.join(" ")}"
    case params[:type]
    when "pmhxes", "fmhxes"
      @diseases = current_organisation.diseases.medical.order(:name).where("lower(name) LIKE ?", q)
    when "pohxes", "fohxes"
      @diseases = current_organisation.diseases.oculus.order(:name).where("lower(name) LIKE ?", q)  
    when "medications"
      @diseases = current_organisation.diseases.psychologies.order(:name).where("lower(name) LIKE ?", q)  
    when "sxhxes"
      @diseases = current_organisation.diseases.surgeries.order(:name).where("lower(name) LIKE ?", q)  
    when "allergies"
      @diseases = current_organisation.diseases.allergies.order(:name).where("lower(name) LIKE ?", q)  
    when "prescribing_medication"
      @diseases = current_organisation.diseases.prescribing_medication.order(:name).where("lower(name) LIKE ?", q)  
    when "dosage"
      @diseases = current_organisation.diseases.dosage.order(:name).where("lower(name) LIKE ?", q)
    else
      @diseases = current_organisation.diseases.order(:name).where("lower(name) LIKE ?", q)
    end
    json_data = @diseases.collect do |item| 
      { "id" => item.id.to_s, 
       "label" => item.name,
      }
    end
    if current_doctor.present?
      json_data = [{"id"=>params[:type], "label"=>q1, "value" => q1}] unless json_data.present?
    end
    respond_to do |format|
      format.html
      format.json { 
        render json: json_data
      }
    end
  end

  def autocomplete_lens_types_price_name
    q = "%#{params[:term].try(:downcase)}%"
    @lens_types = current_organisation.lens_types_prices.order(:brand_name).where("lower(brand_name) LIKE ?", q)
    json_data = @lens_types.collect do |item|
      # unless item.sphere.include?('pl')
      #   val = item.sphere.split(',').reject {|x| x.to_f > 0.0 }.first
      # else
      #   val = 'pl'
      # end
      { "id" => item.id.to_s, 
       "label" => item.brand_name,
       "lens_type"=> item.lens_type,
       "select_base_curve_options"=> item.base_curve.split(',').map{|x| "<option value='#{x}'>#{x}</option>"}.join(""),
       "select_diameter_options"=> item.diameters.split(',').map{|x| "<option value='#{x}'>#{x}</option>"}.join(""),
       "sphere" => item.sphere.split(',').map{|x| "<option value='#{x}'>#{x}</option>"}.join(""),
       "cylinder" => item.cylinder.split(',').map{|x| "<option value='#{x}'>#{x}</option>"}.join(""),
       "axis" => item.axis.split(',').map{|x| "<option value='#{x}'>#{x}</option>"}.join(""),
       "add" => item.add.split(',').map{|x| "<option value='#{x}'>#{x}</option>"}.join("")
       # "sphere_value" => val
      }
    end
    json_data = [{"id"=>"", "label"=>"No Match Found"},{"id"=>"", "label"=>"Add New"}] unless json_data.present?

    respond_to do |format|
      format.html
      format.json { 
        render json: json_data
      }
    end
  end

  def autocomplete_contact_name
    q = "%#{params[:term].try(:downcase)}%"
    @lens_types = Contact.where("lower(company_name) LIKE ?", q)
    json_data = @lens_types.collect do |item|
      { "id" => item.id.to_s, 
       "label" => item.company_name,
       "email"=> item.email,
       "store_email" => current_doctor.store.contact_email,
       "contact_acount" => [item.company_name, item.account_number],
       "fax" => item.fax
      }
    end
    json_data = [{"id"=>"", "label"=>"No Match Found"}] unless json_data.present?

    respond_to do |format|
      format.html
      format.json { 
        render json: json_data
      }
    end
  end


end