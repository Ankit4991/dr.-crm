##
# ContactFormsController Patient can submit his query through contact-us form. Also Doctor can reply on his message.

class ContactFormsController < ApplicationController
  layout :set_layout
  respond_to :html, :xml, :json

  def index
    @contact_forms = current_organisation.contact_forms.sort_by(&:created_at).paginate(:page => params[:page], :per_page => 5)
    render partial: 'index'
  end

  def new
    @contact_form = ContactForm.new
    @seo = Seo.find_by view_name: 'contact_us'
    if @seo.present?
      set_meta_tags title: @seo.title,
              description: @seo.description,
              keywords: @seo.keywords
    end
  end

  def create
    contact_form = ContactForm.new(contact_form_params)     
    if contact_form.save!
     flash[:notice] = "Your query has been submited, we will contact you soon!"
     AppMailer.send_contact_form_details(contact_form).deliver_later
    else 
      flash[:notice] = "Something went wrong!"
    end
    redirect_to :back
  end

  # Replied by doctor on Patient contact us 
  def reply
    cf = ContactForm.find(params[:id])
    patient = Patient.find_by(email: cf.email)
    if patient.present?
      msg = params[:email_reply][:message] + "One click dashbord access url: #{root_url(patient_email: patient.email, authentication_token: patient.authentication_token)}"
    else
      msg = params[:email_reply][:message]
    end  
    if params[:email_reply][:reply_with] == "email"
      AppMailer.send_contact_form_email_reply(cf,  msg).deliver_later
      cf.logs.create(message: msg, log_type: "email")
    elsif params[:email_reply][:reply_with] == "sms"
      TwilioSmsWorker.perform_async({'src'=> current_organisation.twilio_number, 'dst' => cf.phone_number, 'text' => msg, 'trackable_id'=> cf.id, 'trackable_type'=> cf.class, 'notification_method'=> 3})
      cf.logs.create(message: msg, log_type: "sms")
    else
      AppMailer.send_contact_form_email_reply(cf,  msg).deliver_later  
      TwilioSmsWorker.perform_async({'src'=> current_organisation.twilio_number, 'dst' => cf.phone_number, 'text' => msg, 'trackable_id'=> cf.id, 'trackable_type'=> cf.class, 'notification_method'=> 3})
      cf.logs.create(message: msg, log_type: "email")
    end
    redirect_to :back
  end

  private
    def contact_form_params
      params.require(:contact_form).permit!
    end

    def set_layout
      case action_name
      when "new", "create"
        "application"     
      else
        "doctor"
      end
    end
end