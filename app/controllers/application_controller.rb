##
# Patient and Doctor module base controller are inherited from ApplicationController
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception  
  layout :set_layout
  before_filter :organisation_approvel_filter
  before_filter :configure_permitted_parameters, if: :devise_controller?
  before_filter :sign_in_with_token, :set_host
  after_action :allow_brickellvision_iframe
  skip_before_action :verify_authenticity_token, if: :json_request?
  before_filter :api_called, if: :json_request?
  include ApplicationHelper
 
  #Allow app to to open in Iframe 
  def allow_brickellvision_iframe
    response.headers['X-Frame-Options'] = 'ALLOW-FROM http://brickellvision.com'
  end
  
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:account_update) { |u| 
      u.permit(:password, :password_confirmation, :current_password) 
    }
  end

  #Allow login with direct link that we sent in EMail
  def sign_in_with_token
    begin
      if params[:patient_email].present? and params[:authentication_token].present?
        patient = current_organisation.patients.find_by(email: params[:patient_email])
        if patient.present? && patient.authentication_token == params[:authentication_token]
          patient.confirm! if patient.confirmed_at.nil?
          sign_in patient
          #patient.update_attributes!(authentication_token: Devise.friendly_token)
          if patient.home_address == nil
            redirect_to edit_patient_path(patient)
          else
            redirect_to after_sign_in_path_for(patient)
          end
        end
      end
    rescue
      puts "ApplicationController"
      respond_to do |format|
        format.all { render :nothing => true,:status => 200,:content_type => 'text/html' }
      end
    end
  end

  def current_user_or_admin
    current_doctor || current_patient
  end

  #Set current_organisation from Subdomain
  def current_organisation
    subdomain = request.subdomain.present? ? (request.subdomain.include?('.') ? request.subdomain.split('.')[0] : request.subdomain) : ''
    organisation  = Organisation.find_by(domain_name: subdomain)
    if organisation.present?
      Time.zone = organisation.time_zone  
    end  
    Organisation.current =  organisation      
  end

  helper_method :current_organisation

  #check current_organisation is approved or not
  def organisation_approvel_filter
    if((current_organisation.present? && !current_organisation.approved) || (request.subdomain.present? && !current_organisation.present?))
       redirect_to org_not_approved_url(:subdomain => false)
    end
  end

  def after_sign_in_path_for(resource_or_scope)
    if current_organisation.present? and current_organisation.try(:domain_name).present?
      subdomain = request.subdomain.present? ? request.subdomain : current_organisation.try(:domain_name)
      root_url(:subdomain => subdomain)
    else
      root_url(:subdomain => false)
    end
  end

  def json_request?
    request.format == :json
  end

  def api_called
    if params[:doctor].present? && params[:doctor][:email].present?
      resource = Doctor.find_by_email(params[:doctor][:email])
      if resource.present? && resource.valid_password?(params[:doctor][:password])
        render :json => {:doctor => resource}.to_json, :status => :ok
      else
        render :json => {:doctor => {message: "Invelid Username or Password"}}.to_json
      end
    end
  end

  private
    def set_layout
      if current_doctor.present?
        "doctor"
      elsif current_patient.present?
        "patient"
      else
        "application"
      end
    end
    
    def set_host
      ActionMailer::Base.default_url_options = {:host => request.host}
    end

    def after_sign_out_path_for(resource_or_scope)
      if resource_or_scope == :patient
        new_patient_session_path
      else
        root_path
      end
    end
end
