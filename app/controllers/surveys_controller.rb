##
# We do survey and sent survey Email when doctor visit first time in our clinic
# SurveysController used to create survey given by patient

class SurveysController < ApplicationController
  before_action :set_patient, only: [:new, :create]

  def new
    if params[:survey_token].present? && (@patient.survey_token == params[:survey_token])
      @survey = Survey.new(survey_token: @patient.survey_token)
    else
      @survey = false
    end
  end

  def create
    @survey = Survey.new(survey_params.merge!(patient_id: @patient.id))
    respond_to do |format|
      if @survey.save
        @patient.survey_token=nil
        @patient.save
        format.html { render :survey_thanku }
      else
        format.html { redirect_to :back }
      end
    end
  end

  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_patient
      token = params[:survey_token] || params[:survey][:survey_token]
      @patient = current_organisation.patients.find_by(survey_token: token)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def survey_params
      params.require(:survey).permit(:professionalism, :work_quality, :schedule_adherence, :communication, :cleanliness, :feedback, :staff, :patient_id)
    end
end
