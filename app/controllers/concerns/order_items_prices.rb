module OrderItemsPrices
  extend ActiveSupport::Concern

  included do
     
    # Suborder items prices pre-populating
    def glass_price
      begin
        @glasstype = current_organisation.eyeglass_types_prices.find(params[:glasstype])
        respond_to do |format|
          format.json {render json: {:price => @glasstype.price.as_json } }
        end
      rescue Exception => e
        respond_to do |format|
          format.all { render :nothing => true,:status => 200,:content_type => 'text/html' }
        end
      end
    end
    
    def lens_addons_price
      begin
        addon = current_organisation.glass_addons_prices.find(params[:id])
        respond_to do |format|
          format.json {render json: {:price => addon.price } }
        end
      rescue Exception => e
        respond_to do |format|
          format.all { render :nothing => true,:status => 200,:content_type => 'text/html' }
        end
      end
    end
    
    def glass_frame_price
      begin
        frame = current_organisation.glass_frames_prices.find(params[:frame])
        respond_to do |format|
          format.json {render json: {:price => frame.price } }
        end
      rescue Exception => e
        respond_to do |format|
          format.all { render :nothing => true,:status => 200,:content_type => 'text/html' }
        end
      end
    end
    
    def lens_material_price
       begin
        lens_material = current_organisation.lens_materials.find(params[:material])
        respond_to do |format|
          format.json {render json: {:price => lens_material.price } }
        end
      rescue Exception => e
        respond_to do |format|
          format.all { render :nothing => true,:status => 200,:content_type => 'text/html' }
        end
      end
    end
    
    # To prepopulate lenses base curves and diameters over contacts suborder
    def lens_prices_base_curves
      @lenstypes = current_organisation.lens_types_prices.where(brand_name: params[:brand])
      @hash_form = @lenstypes.map{|obj| {id: obj.id,brand: obj.brand_name,bc: obj.base_curve,dia: obj.diameters,price: obj.price,type: obj.lens_type,sphere: obj.sphere,cylinder: obj.cylinder,axis: obj.axis,add: obj.add }}
      respond_to do |format|
        format.json {render json: {:type => @hash_form } }
      end
    end
  end
end