module OrderFinalizedPrescriptions
  extend ActiveSupport::Concern

  included do
     
     #For listing more lens prescription options from previous examinations.This is used over order form other prescription popup options
     def lens_prescriptions
        @joint_resource = current_organisation.try(:joint_resources).find(params[:jr_id])
        @prescrips = @joint_resource.try(:lens_prescriptions_in_use).map{|prescp| prescp.lens.as_json.merge({"lens_prescription_id" => prescp.id})}
        respond_to do |format|
          format.js  { render status: 200 ,msg: "ok"}
        end
     end

     #For listing more glass prescription options from previous examinations.This is used over order form other prescription popup options
     def glass_prescriptions
       @joint_resource = current_organisation.try(:joint_resources).find(params[:jr_id])
       @prescrips = @joint_resource.try(:glass_prescriptions_in_use).map{|prescp| prescp.glass.as_json.merge({"comments" => prescp.comment,"issued_date" => prescp.try(:issued_date),"finalize" => prescp.eye_measurement.finalize,"glass_prescription_id" => prescp.id,"doctor_recommendations" => prescp.doctor_recommendations.map(&:name).join(', ')})}
       respond_to do |format|
         format.js { render status: 200 ,msg: "ok"}
       end
     end
   
  end
end