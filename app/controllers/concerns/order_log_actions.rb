module OrderLogActions
  extend ActiveSupport::Concern

  included do
    
    before_action :set_order, only: [:invoice,:email_invoice,:complete_options]
    # For updating order status to dispense 
    def dispense
      @order = current_organisation.try(:orders).find(params[:id])
      if ["ordered","finalized"].include? @order.try(:status)
        @order.update!(:status => :ready_for_pick_up)
        notification_message = AppConfig.sms_notification["order_ready"] % {patient_name: @order.try(:joint_resource).try(:name) ,store_start_time: @order.try(:doctor).try(:store).try(:working_schedule).try(:get_schedule[Time.now.wday]).try(:first).try(:first) , store_close_time: @order.try(:doctor).try(:store).try(:working_schedule).try(:get_schedule[Time.now.wday]).try(:first).try(:last), store_name: @order.try(:doctor).try(:store).try(:description) , store_address: "#{@order.doctor.store.office_address.address1}, #{@order.doctor.store.office_address.address2}, postal code #{@order.doctor.store.office_address.postal_code} " , store_phone_number: "#{number_to_phone(@order.doctor.store.phone_number.to_s[2..-1].to_i, area_code: true)}", profile_url: root_url(patient_email: @order.try(:joint_resource).try(:email), authentication_token: @order.try(:joint_resource).try(:patient).try(:authentication_token))}
        day_start_time = @order.try(:doctor).try(:working_schedule).try("#{Date.today.strftime('%A').downcase}_start_time")
        AppMailer.send_order_dispensed_to_patient(@order).deliver_later(wait_until: Time.at(Time.now.to_i + (Time.now.to_i % 1.day))) unless @order.try(:joint_resource).try(:patient).try(:email_not_supplied)
        TwilioSmsWorker.perform_async({'src'=> current_organisation.try(:twilio_number), 'dst' => @order.try(:joint_resource).try(:phone), 'text' => notification_message, 'notification_method'=> @order.try(:joint_resource).try(:patient).try(:notification_method)})
        render :json => {status: 'Success', msg: "Order Ready for pick up !!"}.to_json
      elsif @order.try(:status).include?('ready_for_pick_up')
        render :json => {status: 'Error', msg: "Order Already Ready for pick up! "}.to_json
      else
        render :json => {status: 'Error', msg: "Order Not finalized yet! "}.to_json
      end
    end
    
      
    # To render quick sidebar during order form updations
    def online_users
      @online_users = current_organisation.try(:doctors).where(:id => params[:ids].split(',')).flatten
      respond_to do |format|
        format.js { render :file => "doctor/orders/online_users.js.erb" }
      end
    end
    
    # To retrieve suborder items details for lab
    def lab_order
      @eye_glass_order = current_organisation.try(:eye_glass_orders).find(params[:id])
      @store = get_store
      @joint_resource = @eye_glass_order.try(:order).try(:joint_resource)
      
      respond_to do |format|
       format.html
       format.pdf do
         render pdf: "#{@joint_resource.name.tr(' ','_')}_#{Time.now.strftime("%m_%d_%Y")}_order_#{@eye_glass_order.id}",
                layout: 'layouts/application.pdf.haml'  
       end
      end

    end
    

    #To check finalized order invoice from log action
    def invoice
      @store = get_store
      respond_to do |format|
        format.html
        format.pdf do
          render pdf: "invoice",
                 layout: 'layouts/application.pdf.haml'
        end
      end
    end
    
    # To send invoice to patient from log actions
    def email_invoice
      if @order.present?
        InvoiceMailer.send_invoice_to_patient(@order.try(:joint_resource).try(:member),@order).deliver_later unless @order.try(:joint_resource).try(:patient).try(:email_not_supplied)
        render :json => {status: 'Success', msg: "Email Successfully sent"}.to_json
      else
        render :json => {status: 'Error', msg: "Email not sent !"}.to_json
      end
    end
    
    def email_card_warning
      @joint_resource = current_organisation.try(:joint_resources).find(params[:id])
      if @joint_resource.present?
        AppMailer.send_credit_card_warning(@joint_resource).deliver_later unless @joint_resource.try(:patient).try(:email_not_supplied)
        render :json => {status: 'Success', msg: "Email Successfully sent"}.to_json
      else
        render :json => {status: 'Error', msg: "Email not sent !"}.to_json
      end
    end

    def complete_options
      @joint_resource = @order.try(:joint_resource)
      render :partial => 'doctor/orders/order_partial/order_complete_confirmation_modal'
    end

    def refund_amount
      # if @order.refund == 0
      #   render :json => {:error => 'Not Applicable', :status => 422}
      # end
      if params[:payment_type].underscore.include?('cash_payment')
        @cash_payment = current_organisation.try(:cash_payments).find(params[:id])
        @cash_payment.update(:status => :refund)
        @order = @cash_payment.orderable
        flash[:status] = "Success"
        flash[:notice] = "Payment refund has been successfully done!"
        #@order.audits.try(:audited_changes).try(:include?,'cash_refund')
      elsif params[:payment_type].underscore.include?('check_payment')
        @check_payment = current_organisation.try(:check_payments).find(params[:id])
        @check_payment.update(:status => :refund)
        @order = @check_payment.orderable
        flash[:status] = "Success"
        flash[:notice] = "Payment refund has been successfully done!"
      else
        flash[:status] = "error"
        flash[:notice] = "Something went wrong!"
      end
      respond_to do |format|
        format.json { render :json => {status: flash[:status], msg: flash[:notice], order_total: '%.2f' % @order.try(:total), balance_due: '%.2f' % @order.try(:balance_due), order_refund: '%.2f' % @order.try(:refund),order_reward: '%.2f' % @order.try(:joint_resource).try(:reward_point)}.to_json }
      end
    end

    private

    def set_order
      @order = current_organisation.try(:orders).find(params[:id])
    end

    def get_store
      @order.try(:doctor).try(:store) || @eye_glass_order.try(:doctor).try(:store)
    end
  end
end