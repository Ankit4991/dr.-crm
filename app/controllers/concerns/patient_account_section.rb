module PatientAccountSection
  extend ActiveSupport::Concern
  
  included do
  	before_action :session_check, only: [:account_section,:new_payment_profile,:payment_method_update,:remove_card]
    def account_section
  	  begin
  	    @exams = current_organisation.examinations.where(joint_resource_id: @patient.all_resources.map(&:id))     
  	    current_organisation.diseases.default_medical.each do |disease|
    	    (@patient.pmhxes.map(&:disease_id).include? disease.id) ? "" : @patient.pmhxes.build(disease: disease)
    	    (@patient.pohxes.map(&:disease_id).include? disease.id) ? "" : @patient.pohxes.build(disease: disease)      
    	    (@patient.medications.map(&:disease_id).include? disease.id) ? "" : @patient.medications.build(disease: disease)      
  	    end

  	    @profile = @patient.profile.try(:get_details)
        @customer = gateway.customer.find(@patient.customer_id_bt)
  	  rescue Exception => e
  	  end
  	  render  partial: "/patient/dashboards/dashboard_tabs/account",locals: { patient: @patient} 
  	end 

    def new_payment_profile
      begin
        @patient.create_profile(credit_card_params)
        flash[:status] = "success"
        flash[:notice] = "Profile has been successfully created!"
      rescue Exception => e
        if !e.message =~ /A duplicate record with ID/
          flash[:status] = "error"
          flash[:notice] = "something went wrong due to #{e}"
        else
          flash[:status] = "error"
          flash[:notice] = "Card has been allready used please used another card"
        end
      end
      @patient.reload if @patient.present?
      @profile = @patient.try(:profile).try(:get_details) unless @profile.present?      
      respond_to do |format|        
        format.html { render partial: "/patient/dashboards/dashboard_partial/card_detail" }
        format.json { render :json => { status: flash[:status], msg: flash[:notice]} }
      end
    end
    
    def payment_method_update
      begin
        #@patient = current_patient
        unless @patient.profile.customer_payment_profile_id.present?
          @patient.profile.create_payment_profile(credit_card_params)
        end
        @response = @patient.profile.update_details(credit_card_params)
        @validation_response = @patient.profile.validate_profile(credit_card_params['verification_value'])
        if @validation_response.success?
          @patient.profile.update_attributes!(:validated => true, :first_name => params[:creditcard][:first_name], :last_name => params[:creditcard][:last_name])
          @profile = @patient.profile.get_details
          render partial: "/patient/dashboards/dashboard_partial/card_detail"
        else
          raise "Invalid payment/credit card details"
        end
      rescue Exception => e
        render :json => {status: 'Failure', msg: "! Error due to #{e.message}"}.to_json
      end
    end
   
    def profile_payment
      if params[:order_id].present?
        @order = Order.find(params[:order_id])
      elsif params[:fav_order_id].present?
        @favourite_order = FavouriteOrder.find(params[:fav_order_id])
      end
      get_patient_profile
      #@profile = current_patient.profile.present? ? current_patient.profile.get_details : nil 
      @transaction = @order.present? ? @order.transactions.build : @favourite_order.transactions.build
      respond_to do |format|
        format.html
        format.js
      end
    end

    def update_patient
      begin
        @patient = current_organisation.patients.find(params[:id])
        @patient.skip_confirmation! if params[:patient][:email_not_supplied] == "1"
        @patient.update!(patient_params)
        flash[:status]="Success"
        flash[:notice]="Account details successfully updated!"
      rescue Exception=>e
        flash[:status]="Error"
        if params["patient"].nil?
          flash[:notice] = "Please choose avatar"
        else  
          flash[:notice] = "Something went wrong due to #{e} !! Try it again"
        end
      end
      respond_to do |format|
        format.json { render :json => {status: flash[:status], msg: flash[:notice]}.to_json }
        format.html {redirect_to :back}
      end
    end

    def remove_card
      @patient.profile.destroy!
      flash[:status]="Success"
      flash[:notice]="Profile successfully Removed"
      respond_to do |format|
        format.json { render :json => {status: flash[:status], msg: flash[:notice]}.to_json }
        format.html {redirect_to :back}
      end
    end


  	private
      def gateway
        env = ENV['BRAINTREE_ENVIRONMENT']

        @gateway ||= Braintree::Gateway.new(
          :environment => env && env.to_sym,
          :merchant_id => current_organisation.org_braintree_merchant_id,
          :public_key => current_organisation.org_braintree_public_key,
          :private_key => current_organisation.org_braintree_private_key,
        )
      end

  	  def session_check
        @patient = params[:patient_id].present? ? current_organisation.patients.find(params[:patient_id]) : current_patient
      end

      def get_patient_profile
       @resource = @order.present? ? @order.joint_resource : @favourite_order.joint_resource
       @profile =  @resource.try(:profile).try(:get_details)
      end
        
      def credit_card_params
        params.require(:creditcard).permit!
      end

      def patient_params
        params.require(:patient).permit :first_name, :last_name, :birthday, :gender, :phone,:email,:email_not_supplied,:password,:password_confirmation, :notification_method, :occupation,:employer,:referred_by,:medical_notes,:last_eye_exam,:medical_details,:referred_by_data,:contact_lenses,:interested_contact_lens,:in_lasik,:vision_insurance_id,:profile_url,:member_id,:ssn,
                                      :ethnicity,:eye_color,:intrests,:about,:avatar,
                                      :prev_exam,:place,:pregnate,:breast_feeding,:tobacco_use,:alcohol_use,:pmhxes_check,:fmhxes_check,:pohxes_check,:medications_check,:fohxes_check,:sxhxes_check,:allergies_check,:agitated,:anxious,:depressed,:oriented_x3,
                                      :history_info=> [],
                                      fohxes_attributes: [:checked,:disease_id,:notes, :id, :relation],pohxes_attributes: [:checked,:disease_id,:notes, :id],sxhxes_attributes: [:checked,:disease_id,:notes, :id],fmhxes_attributes: [:checked,:disease_id,:notes, :id, :relation],pmhxes_attributes: [:checked,:disease_id,:notes, :id],medications_attributes: [:checked,:disease_id,:notes, :id],allergies_attributes: [:checked,:disease_id,:notes, :id],
                                      home_address_attributes: [:address1,:address2,:city,:postal_code,:addr_type,:state_id,:country_id,:phone],
                                      office_address_attributes: [:address1,:address2,:city,:postal_code,:addr_type,:state_id,:country_id,:phone],
                                      relative_attributes: [:name,:relationship,:phone]
      end
  end
end