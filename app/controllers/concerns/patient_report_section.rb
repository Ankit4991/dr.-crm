module PatientReportSection
  extend ActiveSupport::Concern
  
  included do
    before_action :set_patient
  	def referral
      @referral_form = Referral.find(params[:id])
      if params[:email]
        AppMailer.send_referral_to_user(@patient, @referral_form).deliver_later(wait_until: Time.at(Time.now.to_i + (Time.now.to_i % 1.minutes))) unless @referral_form.try(:joint_resource).try(:patient).try(:email_not_supplied)
        AppMailer.send_referral_to_cc_email(@referral_form).deliver_later(wait_until: Time.at(Time.now.to_i + (Time.now.to_i % 1.minutes))) unless @referral_form.try(:joint_resource).try(:patient).try(:email_not_supplied) if @referral_form.try(:cc_to).present?
        render :json => {status: 'Success', msg: "Email has been sent successfully"}
      else
        generate_pdf('patient/reports/_referral.html','referral.pdf')
      end
    end

    def prescription
      @exam = current_organisation.examinations.find(params[:id])
      @presc_show_status = params[:name]
      if params[:email]
        AppMailer.send_prescription_to_patient(@patient, @exam).deliver_later unless @exam.try(:joint_resource).try(:patient).try(:email_not_supplied)
        render :json => {status: 'Success', msg: "Email has been sent successfully"}
      else
        if @presc_show_status == "presc_show"
          render partial: "/patient/reports/prescription_show"
        else
          generate_pdf('patient/reports/_prescription.html','prescription.pdf')
        end
      end
    end

    def invoice
      @order = current_organisation.orders.find(params[:id])
      @inv_show_status = params[:name]
      if params[:email]
        InvoiceMailer.send_invoice_to_patient(@order.try(:joint_resource).try(:member), @order).deliver_later unless @order.try(:joint_resource).try(:patient).try(:email_not_supplied)
        render :json => {status: 'Success', msg: "Email has been sent successfully"}
      else
        if @inv_show_status == "inv_show"
          render partial: "/patient/reports/invoices_show"
        else
          generate_pdf('doctor/orders/invoice.pdf.haml','invoice.pdf')
        end
      end
    end
    
    private
      def set_patient
        @patient = params[:patient_id].present? ? current_organisation.patients.find(params[:patient_id]) : (params[:family_member_id].present? ? FamilyMember.find(params[:family_member_id]) : current_patient )
      end

      def generate_pdf(template_pth,file_name)
        pdf = WickedPdf.new.pdf_from_string(
                                render_to_string(
                                  template: template_pth,
                                  layout: 'layouts/application.pdf.haml'))
        send_data(pdf,
                  filename: file_name,
                  type: 'application/pdf',
                  disposition: 'attachment')
      end
  end
end