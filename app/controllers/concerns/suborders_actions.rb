##
# SubordersActions has functionality of contact lens orders or eye glass orders actions(create,update,delete) for an order.

module SubordersActions
  extend ActiveSupport::Concern

    #New Order form actions -------start
    included do
      before_filter :set_eye_glass_suborder , only: [:delete_eyeglass_suborder, :update_eyeglass_suborder, :hold_glass_suborder, :holded_glass_suborder, :add_glass_addon,:show_glass_suborder, :reload_updated_glass, :reload_removed_glass]
      before_filter :set_contact_lens_suborder , only: [:hold_lens_suborder,:holded_lens_suborder, :delete_contact_lens_suborder, :update_contact_lens_suborder,:show_lens_suborder,:reload_updated_lens,:reload_removed_lens]
      
      #for glasses
        def update_eyeglass_suborder
          @eye_glass_order.update_attributes!(eyeglass_suborder_params)
          # @eye_glass_order.calculate_total
          @order = @eye_glass_order.order
          #Below variable gives us value when we click on Next Lab Order Btn over Order Dashboard used in this action js file to show and hide Modal.
          @lab_order_btn = params[:lab_next] 
          respond_to do |format|  
            format.js
            format.json { render :nothing => true, :status => 200, :content_type => 'text/html'}
          end
        end

        def create_glass_suborder
          @eye_glass_order =  @order.eye_glass_orders.build(doctor: current_doctor)
          @eye_glass_order.glasses.build(visual_acuity: VisualAcuity.new) 
          @eye_glass_order.addon_details.build
          @eye_glass_order.save!
          respond_to do |format|
            format.js
          end
        end

        def add_glass_addon
          @eye_glass_order.addon_details.create!
          respond_to do |format|
            format.js
          end
        end

        def delete_glass_addon
          @addon_detail = current_organisation.addon_details.find(params[:id])
          @addon_detail.destroy
          respond_to do |format|
            format.js
          end
        end

        def delete_eyeglass_suborder
          @eye_glass_order.destroy
          respond_to do |format|
            format.js
          end
        end

        def reload_removed_glass
          respond_to do |format|
            format.js { render :file => 'doctor/orders/delete_eyeglass_suborder.js.erb'}
          end
        end

        def hold_glass_suborder
          @eye_glass_order.update(:hold => !@eye_glass_order.hold, doctor: current_doctor)
          @order = @eye_glass_order.order
          respond_to do |format|
            format.js
          end
        end

        def holded_glass_suborder
          respond_to do |format|
            format.js { render :file => 'doctor/orders/hold_glass_suborder.js.erb'}
          end
        end

        def calculate_glass_subtotal
          suborder = EyeGlassOrder.new(eyeglass_suborder_params)
          glass_type_price = suborder.glass_type_price || suborder.try(:eyeglass_types_price).try(:price)
          glass_lens_material = suborder.lens_material_price || suborder.try(:lens_material).try(:price)
          glass_frame_price = suborder.frame_price || suborder.try(:glass_frames_price).try(:price)
          unless suborder.frame_price.present?
            frame_x = (suborder.try(:glass_frames_price).try(:price) >= suborder.frame_allowance) ? (suborder.try(:glass_frames_price).try(:price) - suborder.frame_allowance) : suborder.try(:glass_frames_price).try(:price)        # here we are initializing frame total price based on allowance 
          else
            frame_x = (suborder.frame_price >= suborder.frame_allowance.to_f) ? (suborder.frame_price - suborder.frame_allowance.to_f) : 0        # here we are initializing frame total price based on allowance 
          end
          suborder_frame_price = suborder.overage_discount.to_f.between?(0,100) ? (frame_x - (suborder.overage_discount.to_f  * (frame_x/100))) : (suborder.overage_discount = 0)
          # suborder.overage_discount,suborder.frame_price = 0,0 if (frame_x - (suborder.overage_discount.to_f  * (frame_x/100))) <= 0
          
          if suborder.addons_price.nil? || suborder.addons_price.zero? 
            addons_price = suborder.try(:addon_details).map{|add_on| add_on.try(:glass_addons_price).try(:price).to_f}.sum
          else
            addons_price = suborder.addons_price
          end
          subtotal = glass_type_price + addons_price + suborder_frame_price + suborder.material_copay.to_f + glass_lens_material
          total =  subtotal - (  suborder.discount.to_f * (subtotal/100))
          json_data = {
            "eyeglass_types_price" => '%.2f' % glass_type_price,
            "glass_frames_price" => '%.2f' % glass_frame_price,
            "lens_material" => '%.2f' % glass_lens_material,
            "order_addons_price" => '%.2f' % addons_price,
            "frame_allowance" => '%.2f' % suborder.frame_allowance,
            "overage_discount" => '%.2f' % suborder.overage_discount,
            "total" => '%.2f' % total
          }
          render json: json_data
        end

        def show_glass_suborder
          respond_to do |format|
            format.js { render :file => 'doctor/orders/create_glass_suborder.js.erb'}
          end
        end

        def reload_updated_glass
          respond_to do |format|
            format.js { render :file => 'doctor/orders/update_eyeglass_suborder.js.erb'}
          end
        end
       
      #for lenses
      
        def update_contact_lens_suborder
          @contact_lens_order.update_attributes!(contact_lens_suborder_params)
          @order = @contact_lens_order.order
          respond_to do |format|
            format.js
          end
        end

        def create_contact_lens_suborder
          @contact_lens_order =  @order.contact_lens_orders.build(doctor: current_doctor)
          prescribed_lens = @order.joint_resource.lens_prescriptions_in_use.try(:first).try(:lens).as_json
          order_lens = prescribed_lens.present? ? @contact_lens_order.lenses.build(prescribed_lens.except('id','visual_acuity_id')) :  @contact_lens_order.lenses.build
          @contact_lens_order.lens_prescription_id = @order.joint_resource.lens_prescriptions_in_use.try(:first).try(:id)
          @contact_lens_order.od_interval = 0
          @contact_lens_order.os_interval = 0
          @contact_lens_order.od_qnt = 0
          @contact_lens_order.os_qnt = 0
          @contact_lens_order.od_lens_types_price_id = order_lens.od_lens_types_price_id
          @contact_lens_order.os_lens_types_price_id = order_lens.os_lens_types_price_id
          @contact_lens_order.save!
          respond_to do |format|
            format.js
          end
        end

        def delete_contact_lens_suborder
            @order = @contact_lens_order.order
            @contact_lens_order.destroy
            respond_to do |format|
              format.js
            end
        end

        def reload_removed_lens
          respond_to do |format|
            format.js { render :file => 'doctor/orders/delete_contact_lens_suborder.js.erb'}
          end
        end
        
        def calculate_lens_subtotal
          suborder = ContactLensOrder.new(contact_lens_suborder_params)
          od_patient_pays = suborder.od_patient_pays || (suborder.od_price.to_f - (suborder.od_price.to_f * suborder.discount.to_f))
          os_patient_pays = suborder.os_patient_pays || (suborder.os_price.to_f - (suborder.os_price.to_f * suborder.discount.to_f))
          suborder.od_qnt = (suborder.od_interval.to_i / suborder.od_lens_modality)
          suborder.os_qnt = (suborder.os_interval.to_i / suborder.os_lens_modality)
           
          json_data = {
            "od_patient_pays" => '%.2f' % suborder.applicable_od_patient_pays,
            "os_patient_pays" => '%.2f' % suborder.applicable_os_patient_pays,
            "od_qnt" => suborder.od_qnt,
            "os_qnt" => suborder.os_qnt,
            "remaining" => '%.2f' % suborder.allowance_cl_fit_balance,
            "total" => '%.2f' % suborder.calculated_total,
            "discount" => '%.2f' % suborder.discount_option
          }
          render json: json_data
        end
        
        def hold_lens_suborder
          @contact_lens_order.update(:hold => !@contact_lens_order.hold,doctor: current_doctor)
          @order = @contact_lens_order.order
          respond_to do |format|
            format.js
          end
        end

        def holded_lens_suborder
          respond_to do |format|
            format.js { render :file => 'doctor/orders/hold_lens_suborder.js.erb'}
          end
        end


        def show_lens_suborder
          @order = @contact_lens_order.order
          respond_to do |format|
            format.js { render :file => 'doctor/orders/create_contact_lens_suborder.js.erb'}
          end
        end

        def reload_updated_lens
          respond_to do |format|
            format.js { render :file => 'doctor/orders/update_contact_lens_suborder.js.erb'}
          end
        end

      private

        def set_eye_glass_suborder
          @eye_glass_order = current_organisation.eye_glass_orders.with_deleted.find(params[:id])
          @order = @eye_glass_order.order
        end

        def set_contact_lens_suborder
          @contact_lens_order = current_organisation.contact_lens_orders.with_deleted.find(params[:id])
          @order = @contact_lens_order.order
        end

    end
    
end  
