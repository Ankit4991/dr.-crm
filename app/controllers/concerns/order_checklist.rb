module OrderChecklist
  extend ActiveSupport::Concern

  included do
    
    # To mark order checklist for items needs to be order
    def checklist
      respond_to do |format|
        format.js
      end
    end
    
    def update_checklist
      @order.update_attributes!(@order.status.include?('unforder') ? order_params.merge(status: 'ordered' ): order_params)
      respond_to do |format|
        format.json{ render :json => {:message => "ok"} }
      end
    end
    
  end
end