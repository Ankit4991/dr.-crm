##
# BookableCommon has common booking functionality of logged-in and not LoggedIn Patient

module BookableCommon
  extend ActiveSupport::Concern
  
  included do
    def new
      if params["wordpress_login"].present? && params["wordpress_login"] == "true"
        session[:wordpress_login] = true
      else
        session[:wordpress_login] = false 
      end
      if request.try(:subdomain).present?
        @stores = current_organisation.try(:stores)
        @store = current_organisation.try(:stores).try(:first)
        @doctors = current_organisation.try(:stores).try(:first).try(:doctors).try(:doctors_with_role_doctor)
        @doctor = @doctors.try(:first)
        @booking = Booking.new()
        week_hash = {"fc-mon"=>1, "fc-tue"=>2, "fc-wed"=>3, "fc-thu"=>4, "fc-fri"=>5, "fc-sat"=>6, "fc-sun"=>0}
        weekly_off = @store.try(:working_schedule).try(:off_days)
        @weekly_off = weekly_off.try(:map) {|a| week_hash[a]}
        store_leaves = Holiday.where(trackable_type: "Store",organisation_id: current_organisation.try(:id)).try(:map) { |a| a.try(:date).try(:strftime,'%m/%d/%Y')}
        doctor_leaves = Holiday.where(trackable_type: "Doctor",organisation_id: current_organisation.try(:id))
        @all_leaves = []
        doctor_leaves.each do |doc|
          if doc.try(:end_date).present?
            @all_leaves += ((doc.try(:date).try(:to_date))..(doc.try(:end_date))).to_a
          else
            @all_leaves << doc.try(:date).try(:strftime,'%m/%d/%Y')
          end
        end
        @all_leaves = @all_leaves.select{|element| @all_leaves.count(element) > 1 }
        @all_leaves = @all_leaves.try(:map) {|a| a.to_s} + store_leaves
        @all_leaves.uniq!
        rem_days = @store.try(:working_schedule).try(:remaining_month_days)
        week_days_hash = rem_days.try(:group_by,&:wday)
        @weekly_off1 = @weekly_off.try(:map) {|a| week_days_hash[a]}.try(:flatten).try(:map,&:to_s)
        if rem_days.present?
          if @weekly_off1.present?
            @rem_days = rem_days.try(:map,&:to_s) - @weekly_off.try(:map) {|a| week_days_hash[a]}.try(:flatten).try(:map,&:to_s)
          else
            @rem_days = rem_days.try(:map,&:to_s)
          end
          if @all_leaves.present?
            @rem_days = @rem_days - @all_leaves
          end
        end
        render template: "/bookings/new"
      else
        raise ActionController::RoutingError.new('Not Found')
      end
    end
    
    #get available slot
    def get_free_slot
      @view = params[:doctor_id]
      @date = params[:date].try(:to_date).try(:strftime,'%m/%d/%Y')
      @store = current_organisation.try(:stores).find_by_id(params[:store_id])
      @doctor_time_slot= []
      @first_free_time_slots= {}
      @booked_slot = []   
      @doctor_list_have_day_closure = []
      @is_doctors_on_leave = true
      @is_doctors_day_close = true
      if params[:doctor_id] == "first"
        @booked_slots = []
        @doctor = @store.try(:doctors).try(:doctors_with_role_doctor)
        @doctor.each do |doctor|
          @is_doctors_on_leave = true
          @is_doctors_day_close = true
          available_schedule = get_available_schedule(doctor) #Private Method
          @doctor_time_slot -= @booked_slot
          @booked_slots = @booked_slot unless @booked_slots.present?
          @booked_slots = @booked_slots & @booked_slot unless @is_doctors_day_close
          @doctor_time_slot.each do |time_slot|
            unless @first_free_time_slots.key?(time_slot)
              @first_free_time_slots.merge!(time_slot => doctor.try(:id))
            end
          end
        end

      else
        @doctor = current_organisation.try(:doctors).where(id: params[:doctor_id]).try(:first)
        get_available_schedule(@doctor) #Private Method
      end
      render partial: '/bookings/slots'
    end

    #this method is used for both anonymous and logged in patient with differnt view
    def get_registration_form
      @booking = Booking.new(doctor_id: params[:doctor_id], store_id: params[:store_id], start_time: params[:time_slot])
      @patient = Patient.new()
      @app_time = params[:time_slot].try(:to_time).try(:strftime,"%I:%M %p %m-%d-%Y")
    end

    def doctor_list
      @store = current_organisation.try(:stores).find_by_id(params[:store_id])
      @doctors = @store.try(:doctors).try(:doctors_with_role_doctor)
      render partial: "/bookings/doctor_list"
    end
    
    private
      def find_doctor
        doctor_id= params[:doctor_id] || (params[:booking].present? ? params[:booking][:doctor_id] : nil)
        if doctor_id.present?
          @doctor = current_organisation.try(:doctors).find_by_id(doctor_id)
        end
      end

      def set_seo_attributes
        @seo = Seo.find_by view_name: 'book_appointment'
        if @seo.present?
          set_meta_tags title: @seo.try(:title),
                  description: @seo.try(:description),
                  keywords: @seo.try(:keywords)
        end
      end

      # Here we are getting all free time slot of passed doctor according to Doctor time schedule or office schedule.
      # If Doctor Time schedule is not present we shows free slot according to office time schedule.
      def get_available_schedule doctor
        if doctor.present?
          no_of_day = params[:date].try(:to_date).try(:wday)
          doctor_week_schedule = doctor.try(:working_schedule).try(:get_schedule)
          doctor_day_schedule =  doctor_week_schedule[no_of_day].try(:first)
          store_week_schedule = @store.try(:working_schedule).try(:get_schedule)
          store_day_schedule =  store_week_schedule[no_of_day].try(:first)        
          
          if store_day_schedule.present? && store_day_schedule.try(:first).present? && store_day_schedule.try(:last).present?
            @store_day_closed = (store_day_schedule.try(:first).try(:strftime,"%I:%M %p")== "12:00 AM" && store_day_schedule.try(:last).try(:strftime,"%I:%M %p")== "12:00 AM")                         
          end
          @store_closed = @store_day_closed || @store.try(:holidays).where(date: Time.parse(params[:date]).getutc).present?
          is_doctor_on_leave = doctor.try(:leaves).where("date(date) <= ? AND date(end_date) >= ? ",params[:date],params[:date]).present?
          @is_doctors_on_leave = @is_doctors_on_leave && is_doctor_on_leave 
          if is_doctor_on_leave
            @doctor_list_have_day_closure <<  doctor.try(:id)
          end

          if doctor_day_schedule.try(:first).present? && doctor_day_schedule.try(:last).present? 
            is_doctor_day_close = doctor_day_schedule.try(:first).try(:strftime,"%I:%M %p") == "12:00 AM" && doctor_day_schedule.try(:last).try(:strftime,"%I:%M %p") == "12:00 AM"
            @is_doctors_day_close = @is_doctors_day_close && is_doctor_day_close 
            unless is_doctor_day_close || is_doctor_on_leave
              (doctor_day_schedule.try(:first).to_i..doctor_day_schedule.try(:last).to_i).step(1800) do |time|
                @doctor_time_slot << Time.at(time).try(:strftime,"%I:%M %p")
              end
            end
          elsif store_day_schedule.try(:first).present? && store_day_schedule.try(:last).present?
            (store_day_schedule.try(:first).to_i..store_day_schedule.try(:last).to_i).step(1800) do |time|
              @doctor_time_slot << Time.at(time).try(:strftime,"%I:%M %p")
            end
            @is_doctors_day_close = false
          else
            ("10:00".to_time.to_i.."18:00".to_time.to_i).step(1800) do |time|
              @doctor_time_slot << Time.at(time).try(:strftime,"%I:%M %p")
            end
            @is_doctors_day_close = false
          end
          @booked_slot = Booking.where("doctor_id = ? AND start_time::date = ?", doctor.try(:id), @date).try(:map) {|x| x.try(:start_time).try(:strftime,"%I:%M %p")}
        end
      end 
  end 
end