##
# FamilyMembers is common for Patient and Doctor Module both. 
# Because we need same functionality of Family member in Patient as well as in Doctor.

module FamilyMembers
  extend ActiveSupport::Concern

  included do
  	before_filter :set_patient

  	def index
			@family_member=FamilyMember.new
    	current_organisation.diseases.default_medical.each do |disease|
	      @family_member.pmhxes.build(disease: disease)
	      @family_member.pohxes.build(disease: disease)
	      @family_member.medications.build(disease: disease)
    	end
	    current_organisation.diseases.default_allergies.each do |disease|
	      @family_member.allergies.build(disease: disease)
	    end
	    render  partial: "/patient/dashboards/dashboard_tabs/family_member"
		end
		
  	def create
			family_member = @patient.family_members.build(params.require(:family_member).permit!)
			if family_member.save
				flash[:notice] = "Family Member added Successfully"
			else
				flash[:alert] = "Error: Family Member not added due to #{family_member.errors.full_messages.join(', ')}"
			end
			redirect_to :back
		end
		
		def edit
			@family_member=FamilyMember.find(params[:id])
			current_organisation.diseases.default_medical.each do |disease|
	      (@family_member.pmhxes.map(&:disease_id).include? disease.id) ? "" : @family_member.pmhxes.build(disease: disease)
	      (@family_member.pohxes.map(&:disease_id).include? disease.id) ? "" : @family_member.pohxes.build(disease: disease)      
	    end
	    current_organisation.diseases.default_allergies.each do |disease|
	      @family_member.allergies.present? ?  "" : @family_member.allergies.build(disease: disease)
	    end
		  render template: '/patient/family_members/edit'
		end

		def update_relation
			family_member = FamilyMember.find(params[:family_member])
			if family_member.update_attributes!(relation: params[:relation])
				flash[:notice] = "Family Member updated Successfully"
			  #render :json => {status: 'Success', msg: flash[:notice] }.to_json
			else
				flash[:alert] = "Error: Family Member not updated due to #{family_member.errors.full_messages.join(', ')}"
				#render :json => {status: 'Error', msg: flash[:alert] }.to_json
			end
			redirect_to :back
		end


		def update
			family_member = FamilyMember.find(params[:id])
			if family_member.update_attributes(params.require(:family_member).permit!)
        if family_member.patient.home_address.present?
          family_member.patient.home_address.update_attributes(params.require(:home_address).permit!)
        else
          family_member.patient.create_home_address.(params.require(:home_address).permit!)
        end
        if family_member.patient.office_address.present?
          family_member.patient.office_address.update_attributes(params.require(:office_address).permit!)
        else
          family_member.patient.create_office_address.(params.require(:office_address).permit!)
        end
				flash[:notice] = "Family Member updated Successfully"
			else
				flash[:alert] = "Error: Family Member not updated due to #{family_member.errors.full_messages.join(', ')}"
			end
			redirect_to :back
		end

		def show
			params[:q] ||= {}
			@family_member = FamilyMember.find(params[:id])
      @customer = gateway.customer.find(@family_member.customer_id_bt)
			@patient = @family_member.patient
	    	@exams = current_organisation.examinations.where(joint_resource_id: @family_member.try(:joint_resource).try(:id)).order('updated_at DESC')
	    	@search = current_organisation.orders.where(joint_resource_id: @family_member.try(:joint_resource).try(:id)).order('order_date DESC').ransack(params[:q])
	    	@orders =  @search.result.includes(:doctor).paginate(:page => params[:customer_orders_page], :per_page => 10)
	    	@appointments = current_organisation.try(:bookings).try(:with_deleted).where(joint_resource_id: @family_member.try(:joint_resource).try(:id)).order('start_time DESC')
			set_title
			render template: '/patient/family_members/show'
		end


		def reports
      		if params[:reports_referral_page].present?
        		if @family_member.present?
          			@referrals =  @family_member.referrals.paginate(:page => params[:reports_referral_page], :per_page => 4)
        		else
          			@referrals =  @patient.referrals.paginate(:page => params[:reports_referral_page], :per_page => 4)
        		end
        	  render  partial: "/patient/dashboards/dashboard_tabs/referral_report"
      		elsif params[:reports_exams_page].present?
        		if @family_member.present?
            		@exams = current_organisation.examinations.where(joint_resource_id: @family_member.joint_resource.id).paginate(:page => params[:reports_exams_page], :per_page => 4)
          		else
          			@exams = current_organisation.examinations.where(joint_resource_id: @patient.joint_resource.id).paginate(:page => params[:reports_exams_page], :per_page => 4)
          		end
        	  render  partial: "/patient/dashboards/dashboard_tabs/prescription_report"
      		elsif params[:reports_orders_page].present?
            if @family_member.present?
          			@orders = Order.joins(:joint_resource).where("joint_resources.family_member_id =?",@family_member.id).paginate(:page => params[:reports_orders_page], :per_page => 4)
        		else
            		@orders =  @patient.all_orders_reports.paginate(:page => params[:reports_orders_page], :per_page => 4)
          		end
        	  render  partial: "/patient/dashboards/dashboard_tabs/invoice_report"
      		else
        		if @family_member.present?
          			@referrals =  @family_member.referrals.paginate(:page => params[:reports_referral_page], :per_page => 4)
          			@exams = @family_member.all_prescriptions.paginate(:page => params[:reports_exams_page], :per_page => 4)
          			@orders = Order.joins(:joint_resource).where("joint_resources.family_member_id =? ",@family_member.id).paginate(:page => params[:reports_orders_page], :per_page => 4)
        		    @med_rx = 0
                @family_member.examinations.each do |p|
                  @med_rx = @med_rx + p.medical_rxes.count
                end
            else
          			@referrals =  @patient.referrals.paginate(:page => params[:reports_referral_page], :per_page => 4)
          			@exams = @patient.all_prescriptions.paginate(:page => params[:reports_exams_page], :per_page => 4)
          			@orders =  current_organisation.orders.where("joint_resource_id =? ",@patient.joint_resource.id).paginate(:page => params[:reports_orders_page], :per_page => 4)
        		    @med_rx = 0
                @patient.examinations.each do |p|
                  @med_rx = @med_rx + p.medical_rxes.count
                end
            end
        	  render  partial: "/patient/dashboards/dashboard_tabs/report"
      		end
    	end 

    	def upload_file
    		if params[:family_member].present?
    			@family_member = FamilyMember.find(params[:id]) 			
          if params[:attached_file] != "undefined"
    				begin
    					@doc = @family_member.documents.new
    					@doc.docs = params[:attached_file]
    					@doc.type_of_file = "file"
    					@doc.save!
    					render :json => {status: 'Success', msg: "Uploaded successfuly.", img_url: @doc.docs.url(:thumb) ,doc_id: @doc.id , doc_name: @doc.docs_file_name.truncate(20)}.to_json
    				rescue Exception => e
    					render :json => {status: 'Error', msg: e.message}.to_json
    				end
    			else
    				render :json => {status: 'Error', msg: "Please select a file"}.to_json
    			end
    		else
    			@patient=Patient.find(params[:id])  
    			if params[:patient].present? && params[:attached_file].present?
            if params[:attached_file] != "undefined"
      				begin
      					@doc = @patient.documents.new
      					@doc.docs = params[:attached_file]	
      					@doc.type_of_file = "file"
      					@doc.save!
      					 render :json => {status: 'Success', msg: "Uploaded successfuly.", img_url: @doc.docs.url(:thumb) ,doc_id: @doc.id , doc_name: @doc.docs_file_name.truncate(20)}.to_json
        				rescue Exception => e
          				render :json => {status: 'Error', msg: e.message}.to_json
      				end
      			else
      				render :json => {status: 'Error', msg: "Please select a file"}.to_json
      			end
    		  end
        end
    	end

    	def download_file
    		doc = Document.find(params[:doc_id])
    		file_name = doc.docs_file_name
    		file_type = doc.docs_content_type
    		file_path = doc.docs.path
    		send_file(
      			file_path,
      			filename: file_name,
      			type: file_type,
      			disposition: 'attachment'
    		)
  		end

      def delete_file
        doc = Document.find(params[:doc_id])
        doc.destroy!
        render :json => {status: 'Success', msg: "Deleted successfuly."}.to_json
      end

		def destroy
      begin
        @family_member = FamilyMember.find(params[:id])
        if @family_member.present?
          @family_member.destroy!
        end
        flash[:status]="Success"
        flash[:notice] = "Record deleted Successfully"
      rescue Exception=> e    
        flash[:notice] = "Something went wrong due to #{e.message}"
        flash[:status]="Error"
      end
      respond_to do |format|
        format.html {redirect_to :root}
        format.json { render :json => {status: flash[:status], msg: flash[:notice]}.to_json }
      end 
    end


	  private
	  	def set_patient
	  		if params[:patient_id].present?
	  		  @patient=Patient.find(params[:patient_id])
	  		elsif params[:family_member_id].present?
	  		  @family_member = FamilyMember.find(params[:family_member_id])
	  		end
	  		@patient = @patient || current_patient
	  	end
      def gateway
        env = ENV['BRAINTREE_ENVIRONMENT']

        @gateway ||= Braintree::Gateway.new(
          :environment => env && env.to_sym,
          :merchant_id => current_organisation.org_braintree_merchant_id,
          :public_key => current_organisation.org_braintree_public_key,
          :private_key => current_organisation.org_braintree_private_key,
        )
      end
	  	def set_title
	  	  if @family_member.try(:birthday).present?
	  	  	title = @family_member.try(:name).to_s + ' ' + @family_member.birthday.to_s
	      elsif current_doctor.store.description.present?
	        title = @family_member.try(:name) + ' ' + current_doctor.try(:store).try(:description)
	      else
	        title = @family_member.try(:name)
	      end
	  	  set_meta_tags title: title
	  	end
  end
end