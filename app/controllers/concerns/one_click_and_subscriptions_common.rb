module OneClickAndSubscriptionsCommon
  extend ActiveSupport::Concern

  included do
    
    def init_orders_detail
      current_patient||= @patient
      @orders =  current_patient.all_finalized_orders.flatten
      @previous_orders = current_patient.all_removed_orders.flatten
      @favourite_orders = current_patient.joint_resource.favourites
      @order_subscriptions = current_patient.all_order_subscriptions.flatten
      @all_prescriptions_in_use = current_patient.joint_resource.lens_prescriptions_in_use
      @one_click_lens_prescription = @all_prescriptions_in_use.last
      @one_click_lens = @one_click_lens_prescription.try(:lens)
    end
  end
end