module FavouritesCommon
  extend ActiveSupport::Concern
  
  included do
    
  def one_click_buy
    init_orders_detail
    if @one_click_lens_prescription.present?
      @order_total = (@one_click_lens_prescription.prescription_cost * 0.85)   #As 15 % discount is applied on all one click buy 
    end
    render  partial: "/patient/favourites/form"
  end
  
  def get_1_click
    jr_id = params[:jr_id] || current_patient.joint_resource.id
    @selected_patient = current_organisation.joint_resources.find(jr_id)
    @patient = @selected_patient.patient
    if params[:prescription_id].present?
      @selected_prescription = LensPrescription.find(params[:prescription_id])
    end
    @one_click_lens_prescription = @selected_prescription || @selected_patient.lens_prescriptions_in_use.last
    @one_click_lens = @one_click_lens_prescription.try(:lens)
    if @one_click_lens_prescription.present?
      @order_total = (@one_click_lens_prescription.prescription_cost * 0.85)   #As 15 % discount is applied on all one click buy 
    end
    render  partial: "patient/favourites/get_1_click"
  end

  def selected_patient_prescriptions
    @selected_patient = current_organisation.joint_resources.find(params[:jr_id])
    @all_prescriptions_in_use = @selected_patient.lens_prescriptions_in_use
    @one_click_lens_prescription = @all_prescriptions_in_use.last
    @one_click_lens = @one_click_lens_prescription.try(:lens)
    render  partial: "patient/favourites/selected_patient_prescriptions"
  end
  
  #To provide one click order calculations when reward points are applied or boxes per eye is changed
  def calculate_order_total
     @rewards_points = 0.0
     subtotal = (favourite_order_params[:lens_od_quantity].to_i * favourite_order_params[:lens_od_unit_price].to_f) +(favourite_order_params[:lens_os_quantity].to_i * favourite_order_params[:lens_os_unit_price].to_f)
     @total = subtotal - (subtotal * favourite_order_params[:discount].to_f)
     if favourite_order_params[:rewards_applied].include?("true")
       favourite_order = FavouriteOrder.new(favourite_order_params)
       @rewards_points = (favourite_order.joint_resource.reward_point *  0.01)
     end
     @total = @total - @rewards_points.to_f if @total > 0
     respond_to do |format|
       format.js { render :file => 'patient/favourites/calculate_order_total.js.erb'}
     end
  end
  
  def create_favourite
    begin
      @grouped_message = true
      @favourite_order = FavouriteOrder.new(favourite_order_params)
      ActiveRecord::Base.transaction do
         @profile =  @favourite_order.joint_resource.try(:profile)
         if @profile.present? && @profile.validated?
           if @favourite_order.save
             @transaction = @favourite_order.transactions.build(amount: @favourite_order.total)
             card_transaction
           else
             flash[:status] = "error"
             flash[:notice] = "Order not created due to #{@favourite_order.errors.full_messages.to_sentence}"
           end
         else
           flash[:status] = "error"
           flash[:notice] = "Please add a valid credit card to your account !"
         end
      end
      respond_to do |format|
        format.json { render :json => {status: flash[:status], msg: flash[:notice]}.to_json }
      end
    rescue Exception => e
      render :json => { status: 'Error', msg: "Something went wrong due to #{e}"}
    end
  end
    
  private
  
    def favourite_order_params
      params.require(:favourite_order).permit!
    end
    
    def card_transaction
      transaction = 
          {:transaction => {
             :type => :auth_capture, 
             :amount => @transaction.amount, 
             :customer_profile_id => @profile.customer_profile_id.to_i, 
             :customer_payment_profile_id => @profile.customer_payment_profile_id.to_i
            }
          }
      @transaction = @transaction.process_payment_using_payment_profile(transaction)
      @transaction.valid_card = @profile.validated?
      if @transaction.success?
        flash[:status] = "Success"
        flash[:notice] = (@grouped_message ? "Order and Payment has been done successfully! " : "Payment has been successfully done!")
      end
      @transaction.save
      if @transaction.errors.present?
        flash[:status] = "error"
        flash[:notice] = "Something went wrong due to #{@transaction.errors.full_messages}"
      end
      if @transaction.status == 3
        AppMailer.send_order_billing_confirmation_to_patient(@transaction.orderable).deliver_later unless @transaction.orderable.try(:joint_resource).try(:patient).try(:email_not_supplied)
      end
    end
      
  end
end