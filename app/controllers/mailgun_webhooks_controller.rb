##
# MailgunWebhooksController manage all the Mailgun webhooks Like we are saving and showing Email analytics
# Count email open, clicked and others

class MailgunWebhooksController < ApplicationController
  skip_before_filter  :verify_authenticity_token
  respond_to :html, :xml, :json

  def delivered_message
    begin
      header_params = JSON.parse(params["message-headers"]).to_h
      params.merge!(x_mailgun_sid: params["X-Mailgun-Sid"], message_id: params["Message-Id"].gsub(">","").gsub("<",""), events: [params["event"]], organisation_id: header_params["Organisation-Id"], campaign: header_params["Campaign"],trackable_id: header_params["Trackable-Id"], trackable_type: header_params["Trackable-Type"], subject: header_params["Subject"], sub_type: header_params["Sub-Type"] )
      @webhook = MailgunWebhook.create!(webhook_params)
      render :json => @webhook
    rescue
      puts "delivered_message"
      respond_to do |format|
        format.all { render :nothing => true,:status => 200,:content_type => 'text/html' }
      end
    end
  end

  def dropped_message
    begin
      if params["X-Mailgun-Sid"].present?
        header_params = JSON.parse(params["message-headers"]).to_h
        params.merge!(x_mailgun_sid: params["X-Mailgun-Sid"], message_id: params["Message-Id"].gsub(">","").gsub("<",""), events: [params["event"]],organisation_id: header_params["Organisation-Id"], campaign: header_params["Campaign"], trackable_id: header_params["Trackable-Id"], trackable_type: header_params["Trackable-Type"], subject: header_params["Subject"], sub_type: header_params["Sub-Type"] )
      end
      @webhook = MailgunWebhook.where(message_id: params["message-id"]).first_or_create(webhook_params)
      @webhook.events = (@webhook.events << params[:event]).uniq
      @webhook.save!
      render :json => @webhook
    rescue
      puts "dropped_message"
      respond_to do |format|
        format.all { render :nothing => true,:status => 200,:content_type => 'text/html' }
      end
    end
  end

  def hard_bounce
    begin
      header_params = JSON.parse(params["message-headers"]).to_h
      params.merge!(x_mailgun_sid: params["X-Mailgun-Sid"], message_id: params["Message-Id"].gsub(">","").gsub("<",""), events: [params["event"]], organisation_id: header_params["Organisation-Id"], campaign: header_params["Campaign"], trackable_id: header_params["Trackable-Id"], trackable_type: header_params["Trackable-Type"], subject: header_params["Subject"], sub_type: header_params["Sub-Type"] )
      @webhook = MailgunWebhook.create!(webhook_params)
      render :json => @webhook
    rescue
      puts "hard_bounce"
      respond_to do |format|
        format.all { render :nothing => true,:status => 200,:content_type => 'text/html' }
      end
    end
  end

  def spam_complaint
    puts "hello"
  end

  def unsubscribe
    begin
      @webhook = MailgunWebhook.find_by(message_id: params["message-id"])
      @webhook.events =  (@webhook.events << params[:event]).uniq
      @webhook.save!
      render :json => @webhook
    rescue
      puts "unsubscribe"
      respond_to do |format|
        format.all { render :nothing => true,:status => 200,:content_type => 'text/html' }
      end
    end
  end

  def click
    begin
      @webhook = MailgunWebhook.find_by(message_id: params["message-id"])
      @webhook.click_count = @webhook.click_count + 1
      @webhook.events =  (@webhook.events << params[:event]).uniq
      @webhook.save!
      render :json => @webhook
    rescue
      puts "click:"
      respond_to do |format|
        format.all { render :nothing => true,:status => 200,:content_type => 'text/html' }
      end
    end
  end

  def open
    begin
      @webhook = MailgunWebhook.find_by(message_id: params["message-id"])
      @webhook.open_count = @webhook.open_count + 1
      @webhook.events =  (@webhook.events << params[:event]).uniq
      @webhook.save!
      render :json => @webhook
    rescue
      puts "open:"
      respond_to do |format|
        format.all { render :nothing => true,:status => 200,:content_type => 'text/html' }
      end
    end
  end

  def recive_email
    begin
      references = params["References"].gsub(">","").gsub("<","").split
      parent_webhook = MailgunWebhook.find_by(message_id: references.first)
      if parent_webhook.trackable_type == "ContactForm"
        params.merge!(message_id: params["Message-Id"].gsub('>', '').gsub('<',''), events: ["recived"], organisation_id: parent_webhook.organisation_id, campaign: parent_webhook.campaign,trackable_id: parent_webhook.trackable_id, trackable_type: parent_webhook.trackable_type, subject: parent_webhook.subject, sub_type: parent_webhook.sub_type )
        @webhook = MailgunWebhook.create!(webhook_params)
        parent_webhook.trackable.logs.create(message: params["stripped-text"], log_type: "email", action: 'recive')
      end
      render :json => webhook_params
    rescue
      puts "recive_email"
      respond_to do |format|
        format.all { render :nothing => true,:status => 200,:content_type => 'text/html' }
      end
    end
  end
  

  private
    def webhook_params
      params.permit(:x_mailgun_sid, :message_id, :recipient, :reason, :description, :organisation_id, :campaign, :trackable_id, :trackable_type, :subject, :sub_type, :events=> [] )
    end

end