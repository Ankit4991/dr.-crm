##
# Background Controller will used by both Patient and Doctor to create background Image

class BackgroundsController < ApplicationController
  before_action :get_resource
  def create
    if params["background"].present?
      @background=@resource.backgrounds.create(params.require(:background).permit(:avatar)) 
      @resource.update(bgurl: @background.avatar.url)
      flash[:notice] = "Image uploaded successfully."
    else
      flash[:status] = "Error"
      flash[:alert] = "Please select a file"
    end
    redirect_to :back
  end
  private

  	def get_resource
  		@resource = (current_patient.present? ? current_patient : (params[:patient_id].present? ? Patient.find(params[:patient_id]) : false)) || current_doctor
  	end
end