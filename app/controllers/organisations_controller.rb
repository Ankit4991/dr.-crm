##
# New Organisation create request. Any organisation that want to access Itrust in his clinic
# They can request and create organisation in Itrust app.

class OrganisationsController < ApplicationController
  
  def show
  end

  def new
    @creditcard = ActiveMerchant::Billing::CreditCard.new
    @organisation = Organisation.new
  end

  def create
    begin
      params[:organisation][:company_name] = params[:organisation][:domain_name]
      if params[:organisation][:domain_name].present? && params[:organisation][:domain_name].include?(' ')
        params[:organisation][:domain_name] = params[:organisation][:domain_name].strip()
        params[:organisation][:domain_name] = params[:organisation][:domain_name].tr(" ", "_")
      end
      params[:organisation][:email] = params[:owner][:email] if params[:owner][:email].present?
      params[:owner].merge!(subdomain: params[:organisation][:domain_name].downcase, role: ['administrator'])
      @owner = Doctor.new(owner_params.merge!(password: Devise.friendly_token.first(8)))
      if @owner.bgurl.nil?
        @owner.bgurl = "/images/Eye_Doctor.jpg"
      end
      @owner.unhashed_password = params[:owner][:password]
      @owner.skip_confirmation_notification!
      @owner.save!
      params[:organisation].merge!(owner_id:  @owner.id,  admins: [@owner.id],)
      organisation = Organisation.new(organisation_params)
      organisation.save!
      @owner.organisation_id = organisation.id
      @owner.save!
      AppMailer.send_org_create_to_admin(organisation).deliver_now
      AppMailer.organization_request_email(organisation).deliver_now
      respond_to do |format|
        format.js { render :json => {status: 200, msg: "Congratulations! Your new office application has been created and is waiting for approval. We will be in contact within 1 business day."}}
      end
    rescue Exception => e
      if e == "Validation failed: Email has already been taken"
      else
        respond_to do |format|
         format.js { render :json => {status: "Error", msg: "Something went due to #{e}"}.to_json}
        end
      end
    end
  end

  private
    def organisation_params
      params.require(:organisation).permit!
    end

    def owner_params
      params.require(:owner).permit!
    end

    def card_params
    end
        
end