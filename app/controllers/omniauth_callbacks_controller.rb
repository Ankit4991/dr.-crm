class OmniauthCallbacksController < ApplicationController
  def google_oauth2
    if request.env["omniauth.params"]["user"] == "doctor"
      @user = Doctor.from_google_omniauth(request.env["omniauth.auth"],current_organisation.id)
    elsif request.env["omniauth.params"]["user"] == "patient"
      @user = Patient.from_google_omniauth(request.env["omniauth.auth"],current_organisation.id)
    else
      @user = Organisation.from_google_omniauth(request.env["omniauth.auth"])
    end

    if @user.persisted?
      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "Google"
      if request.env["omniauth.params"]["start_time"].present? && @user.class == Patient
        start_time = request.env["omniauth.params"]["start_time"]
        doctor_id = request.env["omniauth.params"]["doctor_id"]
        store_id = request.env["omniauth.params"]["store_id"]
        @booking = Booking.new(doctor_id: doctor_id, store_id: store_id, start_time: start_time,joint_resource_id: @user.joint_resource.id)
        @booking.save!
      end
      if request.env["omniauth.params"]["user"] != "organisation"
        sign_in_and_redirect @user, :event => :authentication
      else
        flash[:notice] = "Congratulations! Your new office application has been created and is waiting for approval. We will be in contact within 1 business day."
        redirect_to root_path
      end
    else
      session["devise.google_data"] = request.env["omniauth.auth"]
      redirect_to root_path
    end
  end

  def failure
    redirect_to root_path
  end
end