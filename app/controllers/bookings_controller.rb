##
# This controller will used for Anonymous(Not logged In) Calendar booking
# New patient or Existing patient can book his appointment without login

class BookingsController < ApplicationController
  layout "application"
  respond_to :html, :xml, :json
  before_action :set_seo_attributes
  before_action :find_doctor, only: [:new,:create]
  include BookableCommon #Use to get available slot

  def create
    begin
      if params[:duplicate_email] == "false"
        patient = current_organisation.patients.find_by_email(params[:patient][:email])
        if !patient.present?
          params[:patient].merge!(password: Devise.friendly_token.first(8))
          patient = Patient.new(params[:patient].permit!)
          patient.unhashed_password = params[:patient][:password]
          patient.build_joint_resource
          patient.skip_confirmation!
          patient.save!
          # create booking 
          params[:booking].merge!(joint_resource_id: patient.joint_resource.id)
          @booking = Booking.new(booking_params)      
          @booking.save!
          AppMailer.send_anonymous_booking_to_store_contact(@booking).deliver_now if @booking.try(:store).try(:contact_email).present?
          AppMailer.send_survey_link(patient).deliver_later(wait_until: @booking.end_time + 6.hours) if patient.survey_token.present?  && !(patient.email_not_supplied)
          flash[:status] = "Success"
          flash[:notice] = "Thanks #{patient.name} submission successful! Please check your email for appointment details." 
        else 
          flash[:status] = "Warning"
          flash[:notice] = "This Email #{patient.email}(#{patient.name}) all ready exists.Please login or use other Email address" 
        end
      else
        params[:booking].merge!(joint_resource_id: params[:joint_resource])
        @booking = Booking.new(booking_params)      
        @booking.save!
        flash[:status] = "Success"
        flash[:notice] = "Submission of Appointment is successful! Please check your email for appointment details." 
      end
    rescue Exception => e
      flash[:status] = "Error"
      flash[:notice] = "Something went wrong due to #{e} !! Try it again"
    end
    respond_to do |format|
      format.json { render :json => {status: flash[:status], msg: flash[:notice], redirect_to_root: false}.to_json }
    end
  end

  def check_duplicate_email
    @patient = current_organisation.patients.find_by_email(params[:email]);
    if @patient.present?
      @data_list = [[@patient.joint_resource.id,@patient.name,@patient.birthday.strftime("%m/%d")]]
      @patient.family_members.each{|fm| @data_list<<[fm.joint_resource.id,fm.name,fm.birthday.strftime("%m/%d")]}
      @data_list
    end
  end

  private
    
    def booking_params
      params.require(:booking).permit(:doctor_id,:store_id, :joint_resource_id, :start_time, :end_time, :comment, :exam_procedure_ids=> [])
    end
end
