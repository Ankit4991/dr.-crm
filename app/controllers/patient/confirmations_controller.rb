##
# Patient Confirmation controller have overide Devise confirmation Show method. 
# We need fuunctionality if Patient is created from Calender not From the registration form, 
# We will redirect to Patient registration edit page.
# So patient once must go through the same Registration process.

class Patient::ConfirmationsController < Devise::ConfirmationsController
  def show
    self.resource = resource_class.confirm_by_token(params[:confirmation_token])
    yield resource if block_given?

    if resource.errors.empty?
      set_flash_message(:notice, :confirmed) if is_flashing_format?
      sign_in(resource) # <= THIS LINE ADDED
      if resource.class.name == "Patient" && resource.home_address == nil
        respond_with_navigational(resource){ redirect_to edit_patient_path(resource) }
      else        
        respond_with_navigational(resource){ redirect_to after_confirmation_path_for(resource_name, resource) }
      end   
    else
      respond_with_navigational(resource.errors, :status => :unprocessable_entity){ render :new }
    end
  end
end