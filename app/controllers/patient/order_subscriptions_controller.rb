class Patient::OrderSubscriptionsController < Patient::BaseController
  before_action :set_order_subscription, only: [ :update,:audits]
  include OneClickAndSubscriptionsCommon
  before_action :session_check, except: [:audits,:create,:selected_prescription,:selected_patient_prescriptions]
  
  def index
    current_patient ||= @patient
    @order_subscriptions = current_patient.all_order_subscriptions.flatten
    render :file => "patient/order_subscriptions/index.js.erb"
  end

  def create
    begin
      @order_subscription = OrderSubscription.new(order_subscription_params)
      @order_subscription.end_time = @order_subscription.start_time + (@order_subscription.interval).months
      if @order_subscription.save
        flash[:status] = "Success"
        flash[:notice] = "Order has been successfully subscribed!"
        render :json => {status: flash[:status], msg: flash[:notice], current_patient: @order_subscription.joint_resource.patient.id}.to_json
      else
        flash[:status] = "error"
        flash[:notice] = "Something went wrong! due to #{@favourite_order.errors.full_messages.to_sentence}"
        render :json => {status: flash[:status], msg: flash[:notice]}.to_json
      end
    rescue Exception => e
      render :json => {status: 'Failure', msg: "! Error due to #{e.message}"}.to_json
    end
  end

  def	update
    @subscription.update(order_subscription_params)
    @order_subscriptions = @patient.all_order_subscriptions.flatten
    respond_to do |format|
      format.js { render :action => 'index', :patient_id  => @patient.id}
    end
  end

  def selected_patient_prescriptions
    @selected_patient = current_organisation.joint_resources.find(params[:jr_id])
    @all_prescriptions_in_use = @selected_patient.lens_prescriptions_in_use
    @one_click_lens_prescription = @all_prescriptions_in_use.last
    @one_click_lens = @one_click_lens_prescription.try(:lens)
    render  partial: "selected_patient_prescriptions", :locals => {selected_patient_order: @all_prescriptions_in_use}
  end

  def subscriptions
    init_orders_detail
    render  partial: "/patient/order_subscriptions/form", :locals => {current_patient: current_patient||= @patient}
  end
  
  def selected_prescription
    @selected_prescription = current_organisation.lens_prescriptions.find(params[:prescription_id])
    @order_subscription = OrderSubscription.new
    respond_to do |format|
      format.js
    end
  end
  
  def interval_boxes
    @lens_prescription = current_organisation.lens_prescriptions.find(params[:lens_prescription_id])
    @od_boxes = (params[:interval].to_i / @lens_prescription.lens_od_modality_period)
    @os_boxes = (params[:interval].to_i / @lens_prescription.lens_os_modality_period)
  end

  def audits
  end

  private

    def session_check
      @patient = params[:patient_id].present? ? current_organisation.patients.find(params[:patient_id]) : current_patient
    end
    
    def order_subscription_params
      params.require(:order_subscription).permit!
    end
    
    def set_order_subscription
      @subscription = current_organisation.order_subscriptions.find(params[:id])
    end

end