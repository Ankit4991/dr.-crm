##
# FamilyMember controller manage all patient Family member
class Patient::FamilyMembersController < ApplicationController
	skip_before_action :verify_authenticity_token
	include FamilyMembers #look at this concern to see this controller functionality
end