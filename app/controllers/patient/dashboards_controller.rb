##
# Dashborad Controller has patient profile and some patient payment settings.

class Patient::DashboardsController < Patient::BaseController
  before_action :set_seo_attributes
  before_filter :set_examination ,only: [:show]
  after_filter :views_counter,only:[:show,:index]
  before_action :get_resource, only:[:index]
  include PatientAccountSection #has functionality related to patient Payment settings

  def index
    @patient = current_patient
    @exams = current_organisation.examinations.where(joint_resource_id: current_patient.all_resources.map(&:id)).order('updated_at DESC')   
    @orders =  current_patient.all_orders_reports.order(:order_date => :desc)
    #@orders =  current_patient.all_finalized_orders.flatten
    #@dispensed_orders = current_patient.all_dispensed_orders.flatten
    #@renewed_orders = current_patient.all_renewed_orders.flatten
    @appointments = Booking.where(joint_resource_id: current_patient.all_resources.map(&:id)).order('start_time DESC')
    begin
      @profile = current_patient.profile.present? ? current_patient.profile.get_details : nil
    rescue Exception => e
    end    
  end
    
  def show
  end

  def resource_insurance
    joint_resource = current_organisation.joint_resources.find(params[:jr_id])
    @resource = joint_resource.member
    render :json => {vision_insurance_id: @resource.vision_insurance.id }
  end

  private
    
    def get_resource
      @joint_resource=current_patient.joint_resource
    end

    def cim_gateway
      ActiveMerchant::Billing::AuthorizeNetCimGateway.new(
        :login    => ENV["AUTHORIZE_NET_LOGIN"],
        :password => ENV["AUTHORIZE_NET_KEY"])
    end
    
    def set_examination
      @examination = current_organisation.examinations.find(params[:id])
    end
    
    def views_counter
      if current_patient.present?
        @patient = current_patient
        @patient.views_count += 1
        @patient.save
      end
    end

    def set_seo_attributes
      @seo = Seo.find_by view_name: 'patient_dashboard'
      if @seo.present?
        set_meta_tags title: @seo.title,
                description: @seo.description,
                keywords: @seo.keywords
      end
    end
end