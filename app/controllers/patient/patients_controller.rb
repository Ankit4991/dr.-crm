##
# PatientsController manage functinality like Create new patient , change password, Change background etc

class Patient::PatientsController < Patient::BaseController
  before_filter :set_examination ,only: [:show]
  after_filter :views_counter,only:[:show,:index]
  before_action :get_resource, only:[:index]

  include PatientAccountSection
  before_action :session_check, only: [:update,:password_update,:sent_reset_password_instruction, :background_update]

  def destroy
    begin
      @patient = Patient.find(params[:id])
      if @patient.present?
        @patient.destroy! 
      end
      flash[:status]="Success"
      flash[:notice] = "Record deleted Successfully"
    rescue Exception=> e    
      flash[:notice] = "Something went wrong due to #{e.message}"
      flash[:status]="Error"
    end
    respond_to do |format|
      format.html {redirect_to :root}
      format.json { render :json => {status: flash[:status], msg: flash[:notice]}.to_json }
    end 
  end

  def new
    @patient = Patient.new
    @home_address = @patient.build_home_address country_id: current_organisation.try(:address).try(:country_id), state_id: current_organisation.try(:address).try(:state_id)
    @office_address = @patient.build_office_address country_id: current_organisation.try(:address).try(:country_id), state_id: current_organisation.try(:address).try(:state_id)
    @joint_resource = @patient.build_joint_resource
    @relative = @patient.build_relative
    current_organisation.diseases.default_medical.each do |disease|
      @patient.pmhxes.build(disease: disease)      
      @patient.pohxes.build(disease: disease)
      @patient.medications.build(disease: disease)
    end 
    
    @seo = Seo.find_by view_name: 'sign_up'
    if @seo.present?
      set_meta_tags title: @seo.title,
              description: @seo.description,
              keywords: @seo.keywords
    end   
  end
  
  def edit
    begin
      if current_doctor.present?
        @patient = Patient.find(params[:id]) 
      else
        @patient = current_patient
      end
      @home_address = @patient.home_address || @patient.build_home_address(country_id: current_organisation.address.country_id, state_id: current_organisation.address.state_id)
      @office_address = @patient.office_address || @patient.build_office_address(country_id: current_organisation.address.country_id, state_id: current_organisation.address.state_id)
      @joint_resource = @patient.joint_resource
      @relative = @patient.relative || @patient.build_relative
      current_organisation.diseases.default_medical.each do |disease|
        (@patient.pmhxes.map(&:disease_id).include? disease.id) ? "" : @patient.pmhxes.build(disease: disease)
        (@patient.pohxes.map(&:disease_id).include? disease.id) ? "" : @patient.pohxes.build(disease: disease)      
        (@patient.medications.map(&:disease_id).include? disease.id) ? "" : @patient.medications.build(disease: disease)      
      end
    rescue Exception => e
      flash[:alert] = "There is some issue due to #{e}"
      redirect_to :root
    end
  end

  def update
    if current_doctor.present?
      @patient = Patient.find(params[:id]) 
    else
      @patient =@patient
    end
    begin     
      @patient.update(patient_params)
      sign_in @patient, :bypass => true    
      flash[:status] = "Success"
      flash[:notice] = "Profile has been successfully updated!"
    rescue Exception=>e
      flash[:status] = "Error"
      flash[:notice] = "Something went wrong due to #{e} !! Try it again"
    end
    respond_to do |format|
      format.html {redirect_to :back}
      format.json {render :json => {status: flash[:status], msg: flash[:notice], redirect_to_root: true}.to_json }
    end  
  end
  
  def create
    patient = current_organisation.patients.find_by_email(params[:patient][:email])
    unless patient.present?
      begin
        params[:patient].merge!(password: Devise.friendly_token.first(8))
        @patient  = Patient.new(patient_params)
        @patient.build_joint_resource
        @patient.unhashed_password = params[:patient][:password]
        @patient.save!
        flash[:status] = "Success"
        flash[:notice] = "Patient Profile has been successfully created!"
      rescue Exception => e
        flash[:status] = "error"
        flash[:notice] = "Something went wrong due to #{e} !! Try it again"
      end
      respond_to do |format|
        format.html {redirect_to root_url}
        format.json { render :json => {status: flash[:status], msg: flash[:notice], redirect_to_root: true}.to_json }
      end
    else
      if params[:create_family] == "Yes"
        @family_member = patient.family_members.new
        @family_member.first_name = params[:patient][:first_name]
        @family_member.last_name = params[:patient][:last_name]
        @family_member.birthday = params[:patient][:birthday]
        @family_member.phone = params[:patient][:phone]
        @family_member.occupation = params[:patient][:occupation]
        @family_member.ssn = params[:patient][:ssn]
        @family_member.ethnicity = params[:patient][:ethnicity]
        @family_member.eye_color = params[:patient][:eye_color]
        @family_member.vision_insurance_id = params[:patient][:vision_insurance_id]
        @family_member.medical_insurance_id = params[:patient][:medical_insurance_id]
        @family_member.gender = params[:patient][:gender]
        @family_member.organisation_id = patient.organisation_id
        @family_member.relation = params[:relation]
        @family_member.save!
        render :json => {status: "Success", msg: "Family Member is Created Successfully", redirect_to_root: true}.to_json
      else
        render :json => {status: "error", msg: "Patient email is wrong.", redirect_to_root: true}.to_json
      end
    end
  end

  def sent_reset_password_instruction
    begin
      @patient.send_reset_password_instructions      
      flash[:notice] = "Reset password Instruction has been sent on your email. Before opening password Instruction URL please sign out your account."
    rescue Exception => e
      flash[:alert] = "Something went wrong due to #{e} !! Try it again"
    end    
    redirect_to root_url
  end

  def password_update
    if @patient.update_with_password(params.require(:edit_patient).permit(:password, :password_confirmation, :current_password))
      # Sign in the user by passing validation in case their password changed
      sign_in @patient, :bypass => true if current_doctor.present?
      render :json => {status: 'Success', msg: "Password Successfully updated"}.to_json
    else
      render :json => {status: 'Error', msg: "Error Updating Password due to #{@patient.errors.full_messages.join(', ')}"}.to_json
    end
  end

  def background_update
    @patient.update(bgurl: Background.find(params[:bg_id]).avatar.url)
    redirect_to :back
  end

  def check_duplicate_email
    @patient = current_organisation.patients.find_by_email(params[:email]);
  end

  private
    
    def get_resource
      @joint_resource=current_patient.joint_resource
    end

    def cim_gateway
      ActiveMerchant::Billing::AuthorizeNetCimGateway.new(
        :login    => ENV["AUTHORIZE_NET_LOGIN"],
        :password => ENV["AUTHORIZE_NET_KEY"])
    end

    
    def credit_card_params
      params.require(:creditcard).permit!
    end
   
    def patient_params
      params.require(:patient).permit :first_name, :last_name, :birthday, :gender, :phone,:email,:email_not_supplied,:password,:sign_date,:accept_terms,:password_confirmation, :notification_method, :occupation,:employer,:referred_by,:medical_notes,:medical_details,:referred_by_data,:last_eye_exam,:contact_lenses,:interested_contact_lens,:in_lasik,:vision_insurance_id,:medical_insurance_id,:member_id,:ssn,
                                      :ethnicity,:eye_color,:intrests,:about,:avatar, :profile_url,
                                      :prev_exam,:place,:pregnate,:breast_feeding,:tobacco_use,:alcohol_use,:pmhxes_check,:fmhxes_check,:pohxes_check,:medications_check,:fohxes_check,:sxhxes_check,:allergies_check,
                                      :history_info=> [],
                                      fohxes_attributes: [:checked,:disease_id,:notes, :id, :relation],pohxes_attributes: [:checked,:disease_id,:notes, :id],sxhxes_attributes: [:checked,:disease_id,:notes, :id],fmhxes_attributes: [:checked,:disease_id,:notes, :id, :relation],pmhxes_attributes: [:checked,:disease_id,:notes, :id],medications_attributes: [:checked,:disease_id,:notes, :id],allergies_attributes: [:checked,:disease_id,:notes, :id],
                                      home_address_attributes: [:address1,:address2,:city,:postal_code,:addr_type,:state_id,:country_id,:phone],
                                      office_address_attributes: [:address1,:address2,:city,:postal_code,:addr_type,:state_id,:country_id,:phone],
                                      relative_attributes: [:name,:relationship,:phone]
    end   

end