##
# Patient Notification shows in topbar . That will be reated to any message or appointment related
 
class Patient::NotificationsController < Patient::BaseController

  def index
    @booking_notifications=current_patient.notifications.all.paginate(:page => params[:page], :per_page => 10)
  end

  def booking_notifications
    @booking_notifications=current_patient.notifications.all.paginate(:page => params[:page], :per_page => 10)
  end
end