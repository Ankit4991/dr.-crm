##
# SessionCntroller overide for patient, We have added funcionality to remind patient about his Uncompleted profile.

class Patient::SessionsController < Devise::SessionsController

  def new
    @seo = Seo.find_by view_name: 'login'
    if @seo.present?
      set_meta_tags title: @seo.title,
              description: @seo.description,
              keywords: @seo.keywords
    end
    super
  end

  def create
    begin
      if current_organisation.patients.find_by_email(params[:patient][:email]).confirmed_at.nil?
        flash[:notice] = "Please confirm your email first for login." 
        return redirect_to new_patient_confirmation_path
      end
    rescue Exception => e
      flash[:notice] = "You are not a valid Patient"
      return redirect_to :root
    end
    self.resource = warden.authenticate!(auth_options)
    if is_navigational_format? && resource.class.to_s == "Patient" && @patient.sign_in_count <= 3
      flash[:notice] = "Please make sure you have added your account details, medical history, address and other basic information, That will help us in Exam form and for better order delivery." 
    else 
      set_flash_message(:notice, :signed_in)
    end
    sign_in(resource_name, resource)
    if !session[:return_to].blank?
      redirect_to session[:return_to]
      session[:return_to] = nil
    else      
      respond_with resource, :location => after_sign_in_path_for(resource)
    end
  end

end