class Patient::PaymentsController < Patient::BaseController
  before_action :set_order , except: [:refund_using_profile,:pay_online,:create_payment_method,:delete_payment_method]
  include FavouritesCommon
  include ActionView::Helpers::NumberHelper
  
  def create
    @transaction = OrderTransaction.new(payment_params.merge!(amount: @order.total))
    @creditcard = ActiveMerchant::Billing::CreditCard.new(credit_card_params)
    @transaction.valid_card = @creditcard.valid?
    if @transaction.valid?
      @transaction = @transaction.process_payment(@creditcard, options_params)
      if @transaction.success?
        flash[:status] = "Success"
        flash[:notice] = "Payment has been successfully done!"
      else
        flash[:status] = "error"
        flash[:notice] = "Something went wrong!"
      end
      @transaction.save
    else
      flash[:status] = "error"
      flash[:notice] = "Invalid Credit Card!"
    end
    if @transaction.orderable.paid?
      InvoiceMailer.send_invoice_to_patient(@order.try(:joint_resource).try(:member), @order).deliver_later unless @order.try(:joint_resource).try(:patient).try(:email_not_supplied)
    end
    respond_to do |format|
      format.json { render :json => {status: flash[:status], msg: flash[:notice]}.to_json }
    end
  end

   def create_payment_method
    @resource = JointResource.find(params[:resource_id])
    customer_id_bt = @resource.member.customer_id_bt
    result = gateway.payment_method.create(
                                :customer_id => customer_id_bt,
                                :payment_method_nonce => params[:payment_method_nonce],
                                :options => {
                                  :make_default => true
                                }
                              )   
    token_generated = result.payment_method.token
    if token_generated.present?
      flash[:status] = "Success"
    else
      flash[:status] = "Error"
    end
    respond_to do |format|
      format.json { render :json => {"status" => flash[:status], "token_generated" => token_generated }}
    end
  end

  def delete_payment_method
    @patient = Patient.find(params[:id])
    customer_id_bt = @patient.customer_id_bt
    customer = gateway.customer.find(customer_id_bt)
    result = gateway.payment_method.delete(params[:token])
    if result.success?
      flash[:status] = "card has been successfully removed"
    else
      flash[:status] = "Error"
    end 
    respond_to do |format|
      format.json { render :json => {status: flash[:status], msg: flash[:notice]}.to_json }
      format.html {redirect_to :back}
    end
  end

  def pay_online
    @resource = JointResource.find(params[:resource_id])
    customer_id_bt = @resource.member.customer_id_bt
    unless customer_id_bt.present?
      cust_create_hash = Hash.new
      cust_create_hash[:first_name] = @resource.member.first_name
      cust_create_hash[:last_name] = @resource.member.last_name
      result = gateway.customer.create(cust_create_hash)
      if result.success?
        customer_id_bt = result.customer.id
        @resource.member.customer_id_bt = customer_id_bt
        @resource.member.save!
      end
      token = gateway.client_token.generate(:customer_id => customer_id_bt)
    else
      token = gateway.client_token.generate(:customer_id => customer_id_bt)
    end
    if token.present?
      flash[:status] = "Success"
    else
      flash[:status] = "Error"
    end
    respond_to do |format|
      format.json { render :json => {"status" => flash[:status], "token" => token }}
    end
  end
  
  #Create transaction using patient profile ID created over authorized.net
  def create_using_profile
    @patient = @order.joint_resource.patient
    @reward_point = @patient.try(:reward_point).to_f
    balance_due = @order.balance_due
    if !@order.paid?
      if params[:apply_reward] == "1" && @reward_point > balance_due
        remain_point = @reward_point - balance_due
        total_used_points = @order.reward_points_used.present? ? @order.reward_points_used + balance_due : balance_due
        @patient.update_attributes!(reward_point: remain_point)
        @order.update_attributes!(reward_points_used: total_used_points.to_f)
        flash[:status] = "Success"
        flash[:notice] = "Payment has been successfully applied!"
      else
        balance_due = balance_due - @reward_point if params[:apply_reward] == "1"
        @cash_check_payment,@transaction_check = false, false
        if params[:cash_amount].present? || params[:check_number].present? || params[:other_amount].present?
          @cash_payment = @order.cash_payments.build(amount: params[:cash_amount].to_f,doctor: current_doctor) if params[:cash_amount].present?
          @check_payment = @order.check_payments.build(check_number: params[:check_number],amount: params[:check_amount], doctor: current_doctor) if params[:check_number].present?
          @other_payment = @order.other_payments.build(comments: params[:comments], other_amount: params[:other_amount], additional_notes: params[:additional_notes], doctor: current_doctor) if params[:other_amount].present?
          cash_and_check_payment      #payment via cash or check options
        end
        @transaction = OrderTransaction.new(payment_params.merge!(amount: @order.balance_due))
        if @transaction.amount == 0.0   
          InvoiceMailer.send_invoice_to_patient(@order.try(:joint_resource).try(:member),@order).deliver_later unless @patient.try(:email_not_supplied)
          one_click_url = root_url(patient_email: @patient.email, authentication_token: @patient.authentication_token, subdomain: @patient.subdomain)
          one_click_url.gsub!(@patient.subdomain, @patient.subdomain + ".dev") if Rails.env.include?('staging') && @patient.subdomain != 'dev'
          one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
          notification_message = AppConfig.sms_notification["invoice_ready"] % {patient_name: @order.joint_resource.name ,one_click_login: one_click_url, store_name: @order.doctor.store.description , store_office_homepage: "#{@order.doctor.store.url}" , store_phone_number: "#{number_to_phone(@order.doctor.store.phone_number.to_s[2..-1].to_i, area_code: true)}"}
          TwilioSmsWorker.perform_async({'src'=> current_organisation.twilio_number, 'dst' => @order.try(:joint_resource).try(:member).try(:phone), 'text' => notification_message, 'notification_method'=> @patient.notification_method,'org_sid'=> current_organisation.twilio_acc_sid,'org_auth_token'=> current_organisation.twilio_auth_token})
        else
          customer_id_bt = @transaction.orderable.joint_resource.member.customer_id_bt
          if customer_id_bt.present? && params[:payment_method_token].present? && balance_due.to_f > 0  
            result = gateway.transaction.sale(
                                  :amount => @transaction.amount,
                                  :payment_method_token => params[:payment_method_token],
                                  :options => {
                                    :submit_for_settlement => true,
                                    :store_in_vault => true
                                  }
                                )
            if result.success?
              @transaction_check = true            #payment via profile card
              @transaction.reference_trans_id = result.transaction.id
              @transaction.valid_card = true
              @transaction.status = 3
              @transaction.save!
              @order.update_columns({:status => 3})
            end
            InvoiceMailer.send_invoice_to_patient(@order.try(:joint_resource).try(:member),@order).deliver_later unless @patient.try(:email_not_supplied)
            one_click_url = root_url(patient_email: @patient.email, authentication_token: @patient.authentication_token, subdomain: @patient.subdomain)
            one_click_url.gsub!(@patient.subdomain, @patient.subdomain + ".dev") if Rails.env.include?('staging') && @patient.subdomain != 'dev'
            one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
            notification_message = AppConfig.sms_notification["invoice_ready"] % {patient_name: @order.joint_resource.name ,one_click_login: one_click_url, store_name: @order.doctor.store.description , store_office_homepage: "#{@order.doctor.store.url}" , store_phone_number: "#{@order.doctor.store.phone_number}"}
            TwilioSmsWorker.perform_async({'src'=> current_organisation.twilio_number, 'dst' => @order.try(:joint_resource).try(:member).try(:phone), 'text' => notification_message, 'notification_method'=> @patient.notification_method,'org_sid'=> current_organisation.twilio_acc_sid,'org_auth_token'=> current_organisation.twilio_auth_token})
            if params[:apply_reward] == "1"
              total_used_points = @order.reward_points_used.present? ? @order.reward_points_used + @reward_point : @reward_point
              @patient.update_attributes!(reward_point: 0.0)
              @order.update_attributes!(reward_points_used: total_used_points.to_f)
            end
          elsif @cash_check_payment
            flash[:status] = "Success"
            flash[:notice] = "Payment has been successfully applied!"
            if params[:apply_reward] == "1"
              total_used_points = @order.reward_points_used.present? ? @order.reward_points_used + @reward_point : @reward_point
              @patient.update_attributes!(reward_point: 0.0)
              @order.update_attributes!(reward_points_used: total_used_points.to_f)
            end
            InvoiceMailer.send_invoice_to_patient(@order.try(:joint_resource).try(:member),@order).deliver_later unless @patient.try(:email_not_supplied)
            one_click_url = root_url(patient_email: @patient.email, authentication_token: @patient.authentication_token, subdomain: @patient.subdomain)
            one_click_url.gsub!(@patient.subdomain, @patient.subdomain + ".dev") if Rails.env.include?('staging') && @patient.subdomain != 'dev'
            one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
            notification_message = AppConfig.sms_notification["invoice_ready"] % {patient_name: @order.joint_resource.name ,one_click_login: one_click_url, store_name: @order.doctor.store.description , store_office_homepage: "#{@order.doctor.store.url}" , store_phone_number: "#{number_to_phone(@order.doctor.store.phone_number.to_s[2..-1].to_i, area_code: true)}"}
            TwilioSmsWorker.perform_async({'src'=> current_organisation.twilio_number, 'dst' => @order.try(:joint_resource).try(:member).try(:phone), 'text' => notification_message, 'notification_method'=> @patient.notification_method,'org_sid'=> current_organisation.twilio_acc_sid,'org_auth_token'=> current_organisation.twilio_auth_token})
          end
        end
      end
      @order.save!
      if !@cash_check_payment && !@transaction_check
        flash[:status] = "Error"
        flash[:notice] = "Please Select a payment mode"
      else
        flash[:status] = "Success"
        flash[:notice] = "Order has been successfully finalized!"
      end
    elsif balance_due == 0.0
      @order.update_columns({:status => 3})
      InvoiceMailer.send_invoice_to_patient(@order.try(:joint_resource).try(:member),@order).deliver_later unless @patient.try(:email_not_supplied)
      one_click_url = root_url(patient_email: @patient.email, authentication_token: @patient.authentication_token, subdomain: @patient.subdomain)
      one_click_url.gsub!(@patient.subdomain, @patient.subdomain + ".dev") if Rails.env.include?('staging') && @patient.subdomain != 'dev'
      one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
      notification_message = AppConfig.sms_notification["invoice_ready"] % {patient_name: @order.joint_resource.name ,one_click_login: one_click_url, store_name: @order.doctor.store.description , store_office_homepage: "#{@order.doctor.store.url}" , store_phone_number: "#{number_to_phone(@order.doctor.store.phone_number.to_s[2..-1].to_i, area_code: true)}"}
      TwilioSmsWorker.perform_async({'src'=> current_organisation.twilio_number, 'dst' => @order.try(:joint_resource).try(:member).try(:phone), 'text' => notification_message, 'notification_method'=> @patient.notification_method,'org_sid'=> current_organisation.twilio_acc_sid,'org_auth_token'=> current_organisation.twilio_auth_token})
      flash[:status] = "Success"
      flash[:notice] = "Order has been successfully finalized!"
    else
      flash[:status] = "error"
      flash[:notice] = "Either Payment has already been made for this order or Order Cart is empty !"
    end
    render :json => {status: flash[:status], msg: flash[:notice], order_total: '%.2f' % @order.total, balance_due: '%.2f' % @order.balance_due,order_refund: '%.2f' % @order.refund,order_reward: '%.2f' % @patient.reward_point, order_id: @order.id}.to_json
  end

  #Refund an amount with reference to a paid transaction ID over authroized.net
  def refund_using_profile
    @last_paid_transaction = current_organisation.order_transactions.find(params[:id])
    @order = @last_paid_transaction.orderable
    the_transaction_id = @last_paid_transaction.reference_trans_id
    result = gateway.transaction.refund(the_transaction_id)
    if result.success?
      @last_paid_transaction.valid_card = true
      @last_paid_transaction.update(:transaction_type => 1)
      flash[:status] = "Success" 
      flash[:notice] = "Payment refund has been successfully done!"
    elsif result.errors.present?
      flash[:status] = "error"
      flash[:notice] = "Something went wrong due to #{result.errors.first.message}"
    end
    respond_to do |format|
      format.json { render :json => {status: flash[:status], msg: flash[:notice], order_total: @order.total, balance_due: @order.balance_due, order_refund: @order.refund,order_reward: '%.2f' % @order.try(:joint_resource).try(:reward_point)}.to_json, order_id: @order.id }
    end
  end
  
  private
    def gateway
      env = ENV['BRAINTREE_ENVIRONMENT']

      @gateway ||= Braintree::Gateway.new(
        :environment => env && env.to_sym,
        :merchant_id => current_organisation.org_braintree_merchant_id,
        :public_key => current_organisation.org_braintree_public_key,
        :private_key => current_organisation.org_braintree_private_key,
      )
    end

    def payment_params
      params.require(:order_transaction).permit!
    end
    def credit_card_params
      params.require(:creditcard).permit!
    end
    def options_params
      params.require(:option).permit!
    end
    
    def card_transaction
      transaction = 
          {:transaction => {
             :type => :auth_capture, 
             :amount => @transaction.amount, 
             :customer_profile_id => @profile.customer_profile_id.to_i, 
             :customer_payment_profile_id => @profile.customer_payment_profile_id.to_i
            }
          }
      @transaction = @transaction.process_payment_using_payment_profile(transaction)
      @transaction.valid_card = @profile.validated?
      if @transaction.success?
        flash[:status] = "Success"
        flash[:notice] = "Payment has been successfully done!"
      end
      @transaction.save
      if @transaction.errors.present?
        flash[:status] = "error"
        flash[:notice] = "Something went wrong due to #{@transaction.errors.full_messages}"
      end
      if @transaction.status == 3
        @order.update_columns(:status => 3)  #finalize the order as some payment received
        return true
      else
        return false
      end
    end

    def refund_transaction
      transaction = 
          {
            :transaction => {
              :type => :refund,
              :amount => @transaction.amount,
              :customer_profile_id => @profile.customer_profile_id.to_i, 
              :customer_payment_profile_id => @profile.customer_payment_profile_id.to_i,
              :trans_id => @last_paid_transaction.response_id.to_i
            }
          }
      @transaction = @transaction.process_refund_using_payment_profile(transaction)
      @transaction.valid_card = @profile.validated?
      if @transaction.success?
        flash[:status] = "Success"
        flash[:notice] = "Payment refund has been successfully done!"
      end
      @transaction.save
      if @transaction.errors.present?
        flash[:status] = "error"
        flash[:notice] = "Something went wrong due to #{@transaction.errors.full_messages}"
      end
    end
    
    #This method is used to process cash or check payments along with gateway payment transaction as well
    def cash_and_check_payment
        if params[:cash_amount].present?
          unless @cash_payment.save
            flash[:status] = "error"
            flash[:notice] = "Something went wrong due to #{@cash_payment.errors.full_messages}"
            return true 
          else

            @cash_check_payment = true
            @order.update_columns({:status => 3})
            @order.other_payments.last.update_attributes(comments: "", additional_notes: "")
          end
        end
        if params[:check_amount].present? && params[:check_number].present?
          unless @check_payment.save
            flash[:status] = "error"
            flash[:notice] = "Something went wrong due to #{@check_payment.errors.full_messages}"
            return true
          else
            @cash_check_payment = true
            @order.update_columns({:status => 3})
            @order.other_payments.last.update_attributes(comments: "", additional_notes: "")
          end
        end
        if params[:other_amount].present?
          unless @other_payment.save!
            flash[:status] = "error"
            flash[:notice] = "Something went wrong due to #{@other_payment.errors.full_messages}"
            return true
          else
            @cash_check_payment = true
            @order.update_columns({:status => 3})
          end
        end
    end
    
    #To set order for common payment options(new orders and renewed orders)
    def set_order
      if params[:order_transaction][:orderable_type] == 'Order'
        @order = Order.find_by(id: params[:order_transaction][:orderable_id])
      elsif params[:order_transaction][:orderable_type] == 'FavouriteOrder'
        @order = FavouriteOrder.find_by(id: params[:order_transaction][:orderable_id])
      end
    end
end