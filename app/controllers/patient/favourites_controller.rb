class Patient::FavouritesController < Patient::BaseController
  include OneClickAndSubscriptionsCommon
  include FavouritesCommon
  before_action :session_check, except: [:get_1_click,:selected_patient_prescriptions]
  
  private
  
    def session_check
      @patient = params[:patient_id].present? ? current_organisation.patients.find(params[:patient_id]) : current_patient
    end

    
end
