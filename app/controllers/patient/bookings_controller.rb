##
#  BookingController for logged-in Patient 

class Patient::BookingsController < Patient::BaseController
  respond_to :html, :xml, :json
  before_action :set_seo_attributes
  before_action :find_doctor, only: [:new,:create]

  include BookableCommon #have functionality like get available time slot

  def create
    begin
      @booking = Booking.new(booking_params)
      @booking.save!       
      AppMailer.send_survey_link(current_patient).deliver_later(wait_until: @booking.end_time + 6.hours) unless current_patient.try(:email_not_supplied)
      flash[:notice] = "Appointment is booked successfully." 
    rescue Exception => e
      flash[:notice] = "Something went wrong due to #{e} !!"
    end
    redirect_to "/"
  end

  def destroy
    @booking = current_organisation.bookings.find(params[:id])
    @booking.update!(destroyer_id: current_patient.id, destroyer_type: "Patient")
    @booking.destroy
    respond_to do |format|
      format.html {redirect_to :back}
      format.json { render :json => {status: "Success", msg: "Appointment canceled Successfully!"}}
    end
  end

  private
    
    def booking_params
      params.require(:booking).permit(:doctor_id,:store_id, :joint_resource_id, :start_time, :end_time, :comment, :exam_procedure_ids=> [])
    end
end