##
# SessionCntroller overide for Doctor, We have added funcionality for login api.

class Doctor::SessionsController < Devise::SessionsController

  def new
    super
  end

  def create
    self.resource = warden.authenticate!(auth_options)
    set_flash_message(:notice, :signed_in) if is_navigational_format?
    sign_in(resource_name, resource)
    if !session[:return_to].blank?
      redirect_to session[:return_to]
      session[:return_to] = nil
    else 
      respond_to do |format|
       format.html { respond_with resource, :location => after_sign_in_path_for(resource) }
       format.json {
         data = {:doctor => current_doctor.reload}
         render :json => data.to_json, :status => :ok
       }
      end     
    end
  end

end