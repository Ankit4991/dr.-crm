##
# Doctor::SettingPatientsController Use to manage Setting => patient tab functionality
# Like we can create/remove records that we will use in Patient booking or with patient profile

class Doctor::SettingPatientsController < Doctor::BaseController
  authorize_resource :class => false
  
  def index
    @fetch_setting_id = current_organisation.setting.id
    @vision_insurances = current_organisation.vision_insurances.flatten
    @medical_insurances = current_organisation.medical_insurances.flatten
    @documents = current_organisation.setting.documents
    render partial: "index" 
  end
 
  def update
    @setting_fields_update = current_organisation.setting
    if params[:flag_for_medical].present?
      @setting_fields_update.update!(flag_for_medical: params[:flag_for_medical])
    end
  end
  
  def add_vision_insurance
    if params[:vision_insurance][:name].present? && params[:vision_insurance][:code].present?
      VisionInsurance.create(vision_insurance_params)
    else
      flash.alert = "Please enter both Name and Code."
    end
      redirect_to settings_path
  end

  def add_medical_insurance
    if params[:medical_insurance][:name].present? && params[:medical_insurance][:code].present?
      MedicalInsurance.create(medical_insurance_params)
    else
      flash.alert = "Please enter both Name and Code."
    end
      redirect_to settings_path
  end

  def upload_file
    @setting = Setting.find(params[:id])
    if params[:setting].present? && params[:setting][:attached_file].present?
      begin
        @doc = @setting.documents.new
        @doc.docs = params[:setting][:attached_file]
        @doc.type_of_file = "file"
        @doc.save!
        flash[:status] = "Success"
        flash[:notice] = "Uploaded successfuly."
      rescue Exception => e
        flash[:status] = "Error"
        flash[:alert] = e.message
      end
    else
      flash[:status] = "Error"
      flash[:alert] = "Please select a file"
    end
    redirect_to settings_path
  end

  def delete_file
    doc = Document.find(params[:doc_id])
    doc.destroy!
    flash[:status] = "Success"
    flash[:notice] = "Deleted successfuly."
    render :nothing => true, :status => 200, :content_type => 'text/html'
  end
  
  def delete_vision_insurance
    vision_insurance = current_organisation.vision_insurances.find(params[:id])
    vision_insurance.destroy
    render :nothing => true, :status => 200, :content_type => 'text/html'
  end

  def delete_medical_insurance
    medical_insurance = current_organisation.medical_insurances.find(params[:id])
    medical_insurance.destroy
    render :nothing => true, :status => 200, :content_type => 'text/html'
  end

  def update_vision_insurance
    begin
      vision_insurance = VisionInsurance.where(id: params[:vision_insurance][:vision_id]).first
      vision_insurance.name = params[:vision_insurance][:name]
      vision_insurance.code = params[:vision_insurance][:code]
      vision_insurance.save!
      flash.notice = "Updated Successfully!"
    rescue Exception => e
      flash.alert = "Something went wrong due to #{e}"
    end
    redirect_to settings_path
  end

  def update_medical_insurance
    begin
      medical_insurance = MedicalInsurance.where(id: params[:medical_insurance][:medical_id]).first
      medical_insurance.name = params[:medical_insurance][:name]
      medical_insurance.code = params[:medical_insurance][:code]
      medical_insurance.save!
      flash.notice = "Updated Successfully!"
    rescue Exception => e
      flash.alert = "Something went wrong due to #{e}"
    end
    redirect_to settings_path
  end

  private      
    
    def vision_insurance_params
      params.require(:vision_insurance).permit :name, :code
    end

    def medical_insurance_params
      params.require(:medical_insurance).permit :name, :code
    end
end