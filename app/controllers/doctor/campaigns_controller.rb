##
# We have email Campaigns. CampaignsController is responsible to show email summary like oppend, Bounce, Clicked, failed etc

class Doctor::CampaignsController < Doctor::BaseController
  authorize_resource :class => false
  def index
    @patient_surveys_done= current_organisation.surveys.order("created_at DESC").paginate(:page => params[:page], :per_page => 5)
    @patient_surveys_pending= (current_organisation.patients.joins(:joint_resource, :joint_resource => :bookings).where("survey_token is not NULL and joint_resources.family_member_id is NULL and bookings.start_time < ?", Time.now + 6.hours).uniq)
  end

  def analytic
    if params[:campaign].present?
      @data_filter = params[:campaign]
      @delivered_all = current_organisation.mailgun_webhooks.where("'delivered' = ANY(events) AND campaign = '#{params[:campaign]}'").count
      @opened_all = current_organisation.mailgun_webhooks.where("'opened' = ANY(events) AND campaign = '#{params[:campaign]}'").count
      @dropped_all = current_organisation.mailgun_webhooks.where("'dropped' = ANY(events) AND campaign = '#{params[:campaign]}'").count
      unsubscriber = RestClient.get "https://api:#{AppConfig.mailgun_webhook_key}"\
      "@api.mailgun.net/v3/#{AppConfig.mailgun_webhook_domain}/unsubscribes"
      @unsubscribed_all = @unsubscribed_users = JSON.parse(unsubscriber)["items"].select{|x| x["tags"]==[params[:campaign]]}.count
      @delivered = current_organisation.mailgun_webhooks.where("'delivered' = ANY(events) and created_at::date > CURRENT_DATE-7 and created_at::date<=CURRENT_DATE AND campaign = '#{params[:campaign]}'").group_by{|x| x.created_at.to_date}.map{|x,y| [x,y.count]}.to_h
      @opened = current_organisation.mailgun_webhooks.where("'opened' = ANY(events) and created_at::date > CURRENT_DATE-7 and created_at::date<=CURRENT_DATE AND campaign = '#{params[:campaign]}'").group_by{|x| x.created_at.to_date}.map{|x,y| [x,y.count]}.to_h
      @failed = current_organisation.mailgun_webhooks.where("'dropped' = ANY(events) and created_at::date > CURRENT_DATE-7 and created_at::date<=CURRENT_DATE AND campaign = '#{params[:campaign]}'").group_by{|x| x.created_at.to_date}.map{|x,y| [x,y.count]}.to_h
      @filter_result = [{name: "Delivered", data: @delivered}, {name: "Failed", data: @failed}, {name: "Opens", data: @opened}]   
      @mailgun_webhooks = current_organisation.mailgun_webhooks.where(campaign: params[:campaign]).paginate(:page => params[:page], :per_page => 10)
      render partial: 'campaign_tab' and return
    else
      @data_filter = "overall"
      @delivered_all = current_organisation.mailgun_webhooks.where("'delivered' = ANY(events)").count
      @opened_all = current_organisation.mailgun_webhooks.where("'opened' = ANY(events)").count
      @dropped_all = current_organisation.mailgun_webhooks.where("'dropped' = ANY(events)").count
      unsubscriber = RestClient.get "https://api:#{AppConfig.mailgun_webhook_key}"\
      "@api.mailgun.net/v3/#{AppConfig.mailgun_webhook_domain}/unsubscribes"
      @unsubscribed_all = JSON.parse(unsubscriber)["items"].count
      @delivered = current_organisation.mailgun_webhooks.where("'delivered' = ANY(events) and created_at::date > CURRENT_DATE-7 and created_at::date<=CURRENT_DATE").group_by{|x| x.created_at.to_date}.map{|x,y| [x,y.count]}.to_h
      @opened = current_organisation.mailgun_webhooks.where("'opened' = ANY(events) and created_at::date > CURRENT_DATE-7 and created_at::date<=CURRENT_DATE").group_by{|x| x.created_at.to_date}.map{|x,y| [x,y.count]}.to_h
      @failed = current_organisation.mailgun_webhooks.where("'dropped' = ANY(events) and created_at::date > CURRENT_DATE-7 and created_at::date<=CURRENT_DATE").group_by{|x| x.created_at.to_date}.map{|x,y| [x,y.count]}.to_h
      @filter_result = [{name: "Delivered", data: @delivered}, {name: "Failed", data: @failed}, {name: "Opens", data: @opened}]   
      @mailgun_webhooks = current_organisation.mailgun_webhooks.order("created_at DESC").paginate(:page => params[:page], :per_page => 10)
    end
  end

  def opened_summary
    @data_filter=params[:campaign]
    if params[:campaign].present? && params[:campaign] != "overall"
      @mailgun_webhooks = current_organisation.mailgun_webhooks.order('created_at DESC').where("'opened' = ANY(events) AND campaign = '#{params[:campaign]}'").paginate(:page => params[:page], :per_page => 10)
    else
      @mailgun_webhooks = current_organisation.mailgun_webhooks.order('created_at DESC').where("'opened' = ANY(events)").paginate(:page => params[:page], :per_page =>10)
    end
    render partial: 'opened_summary'
  end

  def delivered_summary
    @data_filter=params[:campaign]
    if params[:campaign].present? && params[:campaign] != "overall"
      @mailgun_webhooks = current_organisation.mailgun_webhooks.order('created_at DESC').where("'delivered' = ANY(events) AND campaign = '#{params[:campaign]}'").paginate(:page => params[:page], :per_page => 10)
    else
      @mailgun_webhooks = current_organisation.mailgun_webhooks.order('created_at DESC').where("'delivered' = ANY(events)").paginate(:page => params[:page], :per_page => 10)
    end
    render partial: 'delivered_summary'
  end

  def failed_summary
    @data_filter=params[:campaign]
    if params[:campaign].present? && params[:campaign] != "overall"
      @mailgun_webhooks = current_organisation.mailgun_webhooks.order('created_at DESC').where("'dropped' = ANY(events) AND campaign = '#{params[:campaign]}'").paginate(:page => params[:page], :per_page => 10)
    else
      @mailgun_webhooks = current_organisation.mailgun_webhooks.order('created_at DESC').where("'dropped' = ANY(events)").paginate(:page => params[:page], :per_page => 10)
    end
    render partial: 'failed_summary'
  end

  def unsubscribed_summary
    @data_filter=params[:campaign]
    unsubscriber = RestClient.get "https://api:#{AppConfig.mailgun_webhook_key}"\
    "@api.mailgun.net/v3/#{AppConfig.mailgun_webhook_domain}/unsubscribes"
    if params[:campaign].present? && params[:campaign] != "overall"
      @unsubscribed_users = JSON.parse(unsubscriber)["items"].select{|x| x["tags"]==[params[:campaign]]}.paginate(:page => params[:page], :per_page => 10)
    else
      @unsubscribed_users = JSON.parse(unsubscriber)["items"].paginate(:page => params[:page], :per_page => 10)
    end
    render partial: 'unsubscribed_summary'
  end

  def get_unsubscribes
    RestClient.get "https://api:#{AppConfig.mailgun_webhook_key}"\
    "@api.mailgun.net/v3/#{AppConfig.mailgun_webhook_domain}/unsubscribes", :tag => "" 
  end

  def email_summary    
    @data_filter = params[:campaign]
    if params[:campaign].present? && params[:campaign] != "overall"
      @mailgun_webhooks = current_organisation.mailgun_webhooks.order('created_at DESC').where("campaign = '#{params[:campaign]}'").paginate(:page => params[:page], :per_page => 10)
    else
      @mailgun_webhooks = current_organisation.mailgun_webhooks.order("created_at DESC").paginate(:page => params[:page], :per_page => 10)
    end
    render partial: 'email_summary'    
  end

  def graph_view
    @data_filter=params[:campaign]
    if params[:campaign]=="overall"
      if params[:view_type]!= "week"
        @delivered = current_organisation.mailgun_webhooks.where("'delivered' = ANY(events) ").group_by{|x| params[:view_type]=="year" ? x.created_at.to_date.year : Date::MONTHNAMES[x.created_at.to_date.month]}.map{|x,y| [x,y.count]}.to_h
        @opened = current_organisation.mailgun_webhooks.where("'opened' = ANY(events)").group_by{|x| params[:view_type]=="year" ? x.created_at.to_date.year : Date::MONTHNAMES[x.created_at.to_date.month]}.map{|x,y| [x,y.count]}.to_h
        @failed = current_organisation.mailgun_webhooks.where("'dropped' = ANY(events)").group_by{|x| params[:view_type]=="year" ? x.created_at.to_date.year : Date::MONTHNAMES[x.created_at.to_date.month]}.map{|x,y| [x,y.count]}.to_h
        @filter_result = [{name: "Delivered", data: @delivered}, {name: "Failed", data: @failed}, {name: "Opens", data: @opened}]    
      else
        @delivered = current_organisation.mailgun_webhooks.where("'delivered' = ANY(events) and created_at::date > CURRENT_DATE-7 and created_at::date<=CURRENT_DATE").group_by{|x| x.created_at.to_date}.map{|x,y| [x,y.count]}.to_h
        @opened = current_organisation.mailgun_webhooks.where("'opened' = ANY(events) and created_at::date > CURRENT_DATE-7 and created_at::date<=CURRENT_DATE").group_by{|x| x.created_at.to_date}.map{|x,y| [x,y.count]}.to_h
        @failed = current_organisation.mailgun_webhooks.where("'dropped' = ANY(events) and created_at::date > CURRENT_DATE-7 and created_at::date<=CURRENT_DATE").group_by{|x| x.created_at.to_date}.map{|x,y| [x,y.count]}.to_h
        @filter_result = [{name: "Delivered", data: @delivered}, {name: "Failed", data: @failed}, {name: "Opens", data: @opened}]   
      end  
    else
      if params[:view_type]!= "week"
        @delivered = current_organisation.mailgun_webhooks.where("'delivered' = ANY(events) AND campaign = '#{params[:campaign]}'").group_by{|x| params[:view_type]=="year" ? x.created_at.to_date.year : Date::MONTHNAMES[x.created_at.to_date.month]}.map{|x,y| [x,y.count]}.to_h
        @opened = current_organisation.mailgun_webhooks.where("'opened' = ANY(events) AND campaign = '#{params[:campaign]}'").group_by{|x| params[:view_type]=="year" ? x.created_at.to_date.year : Date::MONTHNAMES[x.created_at.to_date.month]}.map{|x,y| [x,y.count]}.to_h
        @failed = current_organisation.mailgun_webhooks.where("'dropped' = ANY(events) AND campaign = '#{params[:campaign]}'").group_by{|x| params[:view_type]=="year" ? x.created_at.to_date.year : Date::MONTHNAMES[x.created_at.to_date.month]}.map{|x,y| [x,y.count]}.to_h
        @filter_result = [{name: "Delivered", data: @delivered}, {name: "Failed", data: @failed}, {name: "Opens", data: @opened}]    
      else
        @delivered = current_organisation.mailgun_webhooks.where("'delivered' = ANY(events) AND campaign = '#{params[:campaign]}' and created_at::date > CURRENT_DATE-7 and created_at::date<=CURRENT_DATE").group_by{|x| x.created_at.to_date}.map{|x,y| [x,y.count]}.to_h
        @opened = current_organisation.mailgun_webhooks.where("'opened' = ANY(events) AND campaign = '#{params[:campaign]}' and created_at::date > CURRENT_DATE-7 and created_at::date<=CURRENT_DATE").group_by{|x| x.created_at.to_date}.map{|x,y| [x,y.count]}.to_h
        @failed = current_organisation.mailgun_webhooks.where("'dropped' = ANY(events) AND campaign = '#{params[:campaign]}' and created_at::date > CURRENT_DATE-7 and created_at::date<=CURRENT_DATE").group_by{|x| x.created_at.to_date}.map{|x,y| [x,y.count]}.to_h
        @filter_result = [{name: "Delivered", data: @delivered}, {name: "Failed", data: @failed}, {name: "Opens", data: @opened}]   
      end
    end
    render partial: 'graph_filter'
  end
  
  # If any user unsubscribe our email campaign, we delete from unsubscribe list
  def delete_unsubscribe
    email=params[:email]
    tags=params[:tags]
    RestClient.delete "https://api:#{AppConfig.mailgun_webhook_key}"\
    "@api.mailgun.net/v3/#{AppConfig.mailgun_webhook_domain}/unsubscribes",
    :address => email,
    :tag => tags
    render :json => {status: "Success",  notice: "Your appointment is successfully booked"}
  end

  def paginate_option
    if params[:campaign] == "done"
      @patient_surveys_done= current_organisation.surveys.order("created_at DESC").paginate(:page => params[:page], :per_page => 5)
      @patient_surveys_pending= (current_organisation.patients.joins(:joint_resource, :joint_resource => :bookings).where("survey_token is not NULL and joint_resources.family_member_id is NULL and bookings.start_time < ?", Time.now + 6.hours).uniq)
    end
    render partial: 'paginate_option'
  end
end