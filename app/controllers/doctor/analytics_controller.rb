##
# AnalyticsController have all functionality for our Exam and order related analytics
# This controller we had created in starting may be we need to update some queries.

class Doctor::AnalyticsController < Doctor::BaseController
  authorize_resource :class => false

  def index
    @examinations = current_organisation.examinations.removed.where('extract(year from updated_at::date) = ?', Time.now.year.to_s).group_by{|trial| trial.updated_at.beginning_of_month.to_date}
    @orders = current_organisation.orders.dispensed.where('extract(year from updated_at::date) = ?', Time.now.year.to_s).group_by{|trial| trial.updated_at.beginning_of_month.to_date}
    @renewed_orders = current_organisation.favourite_orders.where('extract(year from updated_at::date) = ?', Time.now.year.to_s).group_by{|trial| trial.updated_at.beginning_of_month.to_date}
    @lenses = current_organisation.lenses.where('extract(year from updated_at::date) = ?', Time.now.year.to_s).group_by{|trial| trial.updated_at.beginning_of_month.to_date}
    @eyeglasses = current_organisation.eye_glass_orders.where('extract(year from updated_at::date) = ?', Time.now.year.to_s).group_by{|trial| trial.updated_at.beginning_of_month.to_date}
    @addon = current_organisation.glass_addons_prices.select("glass_addons_prices.*,addon_details.updated_at::date AS updated, count(addon_details.glass_addons_price_id) AS no_of_glass_addons, SUM(glass_addons_prices.price) AS cost").joins("INNER JOIN addon_details ON glass_addons_prices.id=addon_details.glass_addons_price_id").where("extract(year from addon_details.updated_at::date) = ?", Time.now.year.to_s).group("glass_addons_prices.id, addon_details.glass_addons_price_id, updated").order("no_of_glass_addons DESC").group_by{|x| x.updated.beginning_of_month.to_date}.sort_by{|k,v| v.inject(0){|sum,i| sum+i.cost.to_f}}.reverse!
    @addon = @addon.sort.to_h
    @frame = current_organisation.glass_frames_prices.select("glass_frames_prices.*, eye_glass_orders.updated_at::date AS updated, SUM(glass_frames_prices.price) AS cost, count(eye_glass_orders.glass_frames_price_id) AS no_of_glass_frames").joins("INNER JOIN eye_glass_orders ON glass_frames_prices.id=eye_glass_orders.glass_frames_price_id").where("extract(year from eye_glass_orders.updated_at::date) = ?", Time.now.year.to_s).group("glass_frames_prices.id, eye_glass_orders.glass_frames_price_id, updated").order("no_of_glass_frames DESC").group_by{|x| x.updated.beginning_of_month.to_date}.sort_by{|k,v| v.inject(0){|sum,i| sum+i.cost.to_f}}.reverse!
    @frame = @frame.sort.to_h
    @exam_record = current_organisation.exam_procedures.select("exam_procedures.*, SUM(exam_procedures.price) AS cost,COUNT(exam_procedures.id) AS no_of_exam_procedure, exam_procedures_examinations.updated_at::date AS updated").joins("INNER JOIN exam_procedures_examinations  ON exam_procedures_examinations.exam_procedure_id=exam_procedures.id").where("extract(year from exam_procedures_examinations.updated_at::date) = ?", Time.now.year.to_s).group("exam_procedures.id, updated").order("no_of_exam_procedure DESC").group_by{|x| x.updated.beginning_of_month.to_date}.sort_by{|k,v| v.inject(0){|sum,i| sum+i.cost.to_f} }
    @lenses_type = current_organisation.lens_types_prices.select("lens_types_prices.*, contact_lens_orders.updated_at::date AS updated, SUM(lens_types_prices.price) AS cost, count(contact_lens_orders.od_lens_types_price_id) AS no_of_lens").joins("INNER JOIN contact_lens_orders ON lens_types_prices.id=contact_lens_orders.od_lens_types_price_id").where("extract(year from contact_lens_orders.updated_at::date) = ?", Time.now.year.to_s).group("lens_types_prices.id, contact_lens_orders.od_lens_types_price_id, updated").order("no_of_lens DESC").group_by{|x| x.updated.beginning_of_month.to_date}.sort_by{|k,v| v.inject(0){|sum,i| sum+i.cost.to_f}}.reverse!
        @lenses_type = @lenses_type.sort.to_h
        
    @exam_record = @exam_record.sort.to_h
      
    @lens_material = current_organisation.lens_materials.select("lens_materials.*, eye_glass_orders.updated_at::date AS updated, SUM(lens_materials.price) AS cost, count(eye_glass_orders.lens_material_id) AS no_of_glass_frames").joins("INNER JOIN eye_glass_orders ON lens_materials.id=eye_glass_orders.lens_material_id").where(" extract(year from eye_glass_orders.updated_at::date) = ?", Time.now.year.to_s).group("lens_materials.id, eye_glass_orders.lens_material_id, updated").order("no_of_glass_frames DESC").group_by{|x| x.updated.beginning_of_month.to_date}.sort_by{|k,v| v.inject(0){|sum,i| sum+i.cost.to_f}}.reverse!
    @lens_material = @lens_material.sort.to_h

    @lens = current_organisation.eyeglass_types_prices.select("eyeglass_types_prices.*, eye_glass_orders.updated_at::date AS updated, SUM(eyeglass_types_prices.price) AS cost, count(eye_glass_orders.eyeglass_types_price_id) AS no_of_glass_frames").joins("INNER JOIN eye_glass_orders ON eyeglass_types_prices.id=eye_glass_orders.eyeglass_types_price_id").where(" extract(year from eye_glass_orders.updated_at::date) = ?", Time.now.year.to_s).group("eyeglass_types_prices.id, eye_glass_orders.eyeglass_types_price_id, updated").order("no_of_glass_frames DESC").group_by{|x| x.updated.beginning_of_month.to_date}.sort_by{|k,v| v.inject(0){|sum,i| sum+i.cost.to_f}}.reverse!
      @lens = @lens.sort.to_h
      
    order_record


    @total_patient = current_organisation.patients.group_by_day(:created_at, range: 2.weeks.ago.midnight..Time.now).count
    total_appointments = current_organisation.bookings.where('joint_resource_id IS NOT NULL')
    @total_appointments = total_appointments.group_by_day(:start_time, range: 2.weeks.ago.midnight..Time.now).count
    @total_payments = total_payment.select{|k,v| (2.weeks.ago.midnight..Time.now).cover?(k)}
    @total_eyeglass_orders = current_organisation.eye_glass_orders.group_by_day(:created_at, range: 2.weeks.ago.midnight..Time.now).sum(:total)
    @total_lens_orders = current_organisation.contact_lens_orders.group_by_day(:created_at, range: 2.weeks.ago.midnight..Time.now).sum(:total)
    @exam_procedures = current_organisation.exam_procedures.group_by_day(:created_at, range: 2.weeks.ago.midnight..Time.now).sum(:price)
    @returning_visitor = current_organisation.joint_resources.all.select{|p| p.examinations.count>1}.count
    @new_visitor = current_organisation.joint_resources.all.count - @returning_visitor
    undelivered_orders = current_organisation.orders.where("status < ?", Order.statuses[:dispensed])
    @contact_lens_order_payments = total_contactlens_payment.select{|k,v| (2.weeks.ago.midnight..Time.now).cover?(k)}
    @undelivered_orders = undelivered_orders.group_by_day(:created_at, range: 2.weeks.ago.midnight..Time.now).sum(:total)
    @eye_glass_order_payments = total_eyeglass_payment.select{|k,v| (2.weeks.ago.midnight..Time.now).cover?(k)}
    undelivered_renewed_orders = current_organisation.favourite_orders.where("status < ?", Order.statuses[:dispensed])
    @total_exam_proc_payment = ProcedureAdjustment.group("procedure_adjustments.id").where("order_id IN (?)",total_examprocedure_payment).group_by_day(:created_at, range: 2.weeks.ago.midnight..Time.now).sum(:price)
    @undelivered_renewed_orders = undelivered_renewed_orders.group_by_day(:created_at, range: 2.weeks.ago.midnight..Time.now).sum(:total)

    if current_doctor.role.include?("staff") && current_doctor.role.reject(&:empty?).length == 1
      @filter_result = [{name: "Appointments", data: current_doctor.bookings.group_by_day(:start_time, format: "%b %d %Y", range: 2.weeks.ago.midnight..Time.now).count}, {name: "Fresh Orders", data: current_doctor.orders.group_by_day(:created_at, format: "%b %d %Y", range: 2.weeks.ago.midnight..Time.now).sum(:total)}, {name: "Re-Newed Orders", data: current_doctor.favourite_orders.group_by_day(:created_at, format: "%b %d %Y", range: 2.weeks.ago.midnight..Time.now).sum(:total)}]
    else
      @filter_result = current_organisation.orders.group_by_day(:created_at, format: "%b %d %Y", range: 2.weeks.ago.midnight..Time.now).sum(:total)
    end
  end

  def filter
    object = params[:analytic][:filter_class]
    case object
    when "order"
      @filter_result = current_organisation.orders.group_by_day(:created_at, format: "%b %d %Y", range: params[:analytic][:start_date].to_time..params[:analytic][:end_date].to_time.end_of_day).sum(:total)
    when "booking"
      total_appointments = current_organisation.bookings.where('joint_resource_id IS NOT NULL')
      @filter_result =total_appointments.group_by_day(:start_time, format: "%b %d %Y", range: params[:analytic][:start_date].to_time..params[:analytic][:end_date].to_time.end_of_day).count
    when "eye_glass_order"
      @filter_result = current_organisation.eye_glass_orders.group_by_day(:created_at, format: "%b %d %Y", range: params[:analytic][:start_date].to_time..params[:analytic][:end_date].to_time.end_of_day).sum(:total)
    when "contact_lens_order"
      @filter_result = current_organisation.contact_lens_orders.group_by_day(:created_at, format: "%b %d %Y", range: params[:analytic][:start_date].to_time..params[:analytic][:end_date].to_time.end_of_day).sum(:total)
    when "favourite_order"
      @filter_result = current_organisation.favourite_orders.group_by_day(:created_at, format: "%b %d %Y", range: params[:analytic][:start_date].to_time..params[:analytic][:end_date].to_time.end_of_day).sum(:total)
    when "exam_procedures"
      @filter_result = current_organisation.exam_procedures.group_by_day(:created_at, format: "%b %d %Y", range: params[:analytic][:start_date].to_time..params[:analytic][:end_date].to_time.end_of_day).sum(:price)
   else
      doctor = current_organisation.doctors.find(params[:analytic][:filter_class])
      @filter_result = [{name: "Appointments", data: doctor.bookings.group_by_day(:start_time, format: "%b %d %Y", range: params[:analytic][:start_date].to_time..params[:analytic][:end_date].to_time.end_of_day).count}, {name: "Fresh Orders", data: doctor.orders.group_by_day(:created_at, format: "%b %d %Y", range: params[:analytic][:start_date].to_time..params[:analytic][:end_date].to_time.end_of_day).sum(:total)}, {name: "Re-Newed Orders", data: doctor.favourite_orders.group_by_day(:created_at, format: "%b %d %Y", range: params[:analytic][:start_date].to_time..params[:analytic][:end_date].to_time.end_of_day).sum(:total)}]   
    end
    total_appointments = current_organisation.bookings.where('joint_resource_id IS NOT NULL')
    @total_appointments =total_appointments.group_by_day(:start_time, range: params[:analytic][:start_date].to_time..params[:analytic][:end_date].to_time.end_of_day).count
    @total_patient = current_organisation.patients.group_by_day(:created_at, range: params[:analytic][:start_date].to_time..params[:analytic][:end_date].to_time.end_of_day).count
    @total_eyeglass_orders = current_organisation.eye_glass_orders.group_by_day(:created_at, range: params[:analytic][:start_date].to_time..params[:analytic][:end_date].to_time.end_of_day).sum(:total)
    @total_lens_orders = current_organisation.contact_lens_orders.group_by_day(:created_at, range: params[:analytic][:start_date].to_time..params[:analytic][:end_date].to_time.end_of_day).sum(:total)
    @exam_procedures = current_organisation.exam_procedures.group_by_day(:created_at, range: params[:analytic][:start_date].to_time..params[:analytic][:end_date].to_time.end_of_day).sum(:price)
    @eye_glass_order_payments = total_eyeglass_payment.select{|k,v| (params[:analytic][:start_date].to_time..params[:analytic][:end_date].to_time.end_of_day).cover?(k)}
    undelivered_orders = current_organisation.orders.where("status < ?", Order.statuses[:dispensed])
    @undelivered_orders = undelivered_orders.group_by_day(:created_at, range: params[:analytic][:start_date].to_time..params[:analytic][:end_date].to_time.end_of_day).sum(:total)
    @contact_lens_order_payments = total_contactlens_payment.select{|k,v| (params[:analytic][:start_date].to_time..params[:analytic][:end_date].to_time.end_of_day).cover?(k)}
    undelivered_renewed_orders = current_organisation.favourite_orders.where("status < ?", Order.statuses[:dispensed])
    @undelivered_renewed_orders = undelivered_renewed_orders.group_by_day(:created_at, range: params[:analytic][:start_date].to_time..params[:analytic][:end_date].to_time.end_of_day).sum(:total)
    @total_exam_proc_payment = ProcedureAdjustment.group("procedure_adjustments.id").where("order_id IN (?)",total_examprocedure_payment).group_by_day(:created_at, range: params[:analytic][:start_date].to_time..params[:analytic][:end_date].to_time.end_of_day).sum(:price)
    @total_payments = current_organisation.orders.group_by_day(:created_at, format: "%b %d %Y", range: params[:analytic][:start_date].to_time..params[:analytic][:end_date].to_time.end_of_day).sum(:total)
	end

  # Bottom Filter records according to selected filter like day, month, week, year
  def orders_detail
    @view_name = params[:analytic][:select_duration]
    if params[:analytic][:date].present? && @view_name == 'day'
      @examinations  = get_removed_examination_for_date params[:analytic][:date].to_date.strftime("%Y-%m-%d")
      @orders = get_removed_orders_for_date params[:analytic][:date].to_date.strftime("%Y-%m-%d")
      @renewed_orders = get_removed_favourite_orders_for_date params[:analytic][:date].to_date.strftime("%Y-%m-%d")
      @total_order_subscriptions = current_organisation.order_subscriptions.where("active = ? and created_at::date = ?", true,params[:analytic][:date].to_date)
    
    elsif params[:analytic][:month].present? && @view_name == 'month'
      @examinations = current_organisation.examinations.removed.where('extract(month from updated_at::date) = ?', params[:analytic][:month]).group_by{|trial| trial.updated_at.to_date}
      @orders = current_organisation.orders.dispensed.where('extract(month from updated_at::date) = ?', params[:analytic][:month]).group_by{|trial| trial.updated_at.to_date}
      @renewed_orders = current_organisation.favourite_orders.where('extract(month from updated_at::date) = ?', params[:analytic][:month]).group_by{|trial| trial.updated_at.to_date}
      @lenses = current_organisation.lenses.where('extract(month from updated_at::date) = ?', params[:analytic][:month]).group_by{|trial| trial.updated_at.to_date}
      @total_order_subscriptions = current_organisation.order_subscriptions.where("active = ? and extract(month from created_at::date) = ?", true,params[:analytic][:month])
      @eyeglasses = current_organisation.eye_glass_orders.where('extract(month from updated_at::date) = ?', params[:analytic][:month]).group_by{|trial| trial.updated_at.to_date}
      @addon = current_organisation.glass_addons_prices.select("glass_addons_prices.*, addon_details.updated_at::date AS updated, count(addon_details.glass_addons_price_id) AS no_of_glass_addons, SUM(glass_addons_prices.price) AS cost").joins("INNER JOIN addon_details ON glass_addons_prices.id=addon_details.glass_addons_price_id").where("extract(month from addon_details.updated_at::date) = ? AND extract(year from addon_details.updated_at::date) = ?" , params[:analytic][:month], params[:analytic][:year]).group("glass_addons_prices.id, addon_details.glass_addons_price_id, updated").order("no_of_glass_addons DESC").group_by{|x| x.updated}.sort_by{|k,v| v.inject(0){|sum,i| sum+i.cost}}.reverse!
      @addon = @addon.sort.to_h
      @frame = current_organisation.glass_frames_prices.select("glass_frames_prices.*, eye_glass_orders.updated_at::date AS updated, SUM(glass_frames_prices.price) AS cost, count(eye_glass_orders.glass_frames_price_id) AS no_of_glass_frames").joins("INNER JOIN eye_glass_orders ON glass_frames_prices.id=eye_glass_orders.glass_frames_price_id").where("extract(month from eye_glass_orders.updated_at::date) = ? AND  extract(year from eye_glass_orders.updated_at::date) = ?", params[:analytic][:month], params[:analytic][:year]).group("glass_frames_prices.id, eye_glass_orders.glass_frames_price_id, updated").order("no_of_glass_frames DESC").group_by{|x| x.updated.to_date}.sort_by{|k,v| v.inject(0){|sum,i| sum+i.cost}}.reverse!
      @frame = @frame.sort.to_h

      @lens_material = current_organisation.lens_materials.select("lens_materials.*, eye_glass_orders.updated_at::date AS updated, SUM(lens_materials.price) AS cost, count(eye_glass_orders.lens_material_id) AS no_of_glass_frames").joins("INNER JOIN eye_glass_orders ON lens_materials.id=eye_glass_orders.lens_material_id").where("extract(month from eye_glass_orders.updated_at::date) = ? AND  extract(year from eye_glass_orders.updated_at::date) = ?", params[:analytic][:month], params[:analytic][:year]).group("lens_materials.id, eye_glass_orders.lens_material_id, updated").order("no_of_glass_frames DESC").group_by{|x| x.updated.to_date}.sort_by{|k,v| v.inject(0){|sum,i| sum+i.cost}}.reverse!
      @lens_material = @lens_material.sort.to_h
     
      @lens = current_organisation.eyeglass_types_prices.select("eyeglass_types_prices.*, eye_glass_orders.updated_at::date AS updated, SUM(eyeglass_types_prices.price) AS cost, count(eye_glass_orders.eyeglass_types_price_id) AS no_of_glass_frames").joins("INNER JOIN eye_glass_orders ON eyeglass_types_prices.id=eye_glass_orders.eyeglass_types_price_id").where("extract(month from eye_glass_orders.updated_at::date) = ? AND  extract(year from eye_glass_orders.updated_at::date) = ?", params[:analytic][:month], params[:analytic][:year]).group("eyeglass_types_prices.id, eye_glass_orders.eyeglass_types_price_id, updated").order("no_of_glass_frames DESC").group_by{|x| x.updated.to_date}.sort_by{|k,v| v.inject(0){|sum,i| sum+i.cost}}.reverse!
      @lens = @lens.sort.to_h

      @lenses_type = current_organisation.lens_types_prices.select("lens_types_prices.*, contact_lens_orders.updated_at::date AS updated, SUM(lens_types_prices.price) AS cost, count(contact_lens_orders.od_lens_types_price_id) AS no_of_lens").joins("INNER JOIN contact_lens_orders ON lens_types_prices.id=contact_lens_orders.od_lens_types_price_id").where("extract(month from contact_lens_orders.updated_at::date) = ? AND  extract(year from contact_lens_orders.updated_at::date) = ?", params[:analytic][:month], params[:analytic][:year]).group("lens_types_prices.id, contact_lens_orders.od_lens_types_price_id, updated").order("no_of_lens DESC").group_by{|x| x.updated.to_date}.sort_by{|k,v| v.inject(0){|sum,i| sum+i.cost}}.reverse!
        @lenses_type = @lenses_type.sort.to_h
        
      @exam_record = current_organisation.exam_procedures.select("exam_procedures.*, SUM(exam_procedures.price) AS cost,COUNT(exam_procedures.id) AS no_of_exam_procedure, exam_procedures_examinations.updated_at::date AS updated").joins("INNER JOIN exam_procedures_examinations  ON exam_procedures_examinations.exam_procedure_id=exam_procedures.id").where("extract(month from exam_procedures_examinations.updated_at::date) = ?", params[:analytic][:month]).group("exam_procedures.id, updated").order("no_of_exam_procedure DESC").group_by{|x| x.updated.to_date}.sort_by{|k,v| v.inject(0){|sum,i| sum+i.cost} }
        
      @exam_record = @exam_record.sort.to_h
      order_record

    elsif params[:analytic][:week].present? && @view_name == 'week'
      week = params[:analytic][:week].to_date
      @examinations = current_organisation.examinations.removed.where("updated_at::date >= ? AND updated_at::date <= ?", week.at_beginning_of_week,week.at_end_of_week).group_by{|trial| trial.updated_at.to_date}
      @orders = current_organisation.orders.dispensed.where("updated_at::date >= ? AND updated_at::date <= ?", week.at_beginning_of_week,week.at_end_of_week).group_by{|trial| trial.updated_at.to_date}
      @renewed_orders = current_organisation.favourite_orders.dispensed.where("updated_at::date >= ? AND updated_at::date <= ?", week.at_beginning_of_week,week.at_end_of_week).group_by{|trial| trial.updated_at.to_date}
      @lenses = current_organisation.lenses.where("updated_at::date >= ? AND updated_at::date <= ?", week.at_beginning_of_week,week.at_end_of_week).group_by{|trial| trial.updated_at.to_date}
      @eyeglasses = current_organisation.eye_glass_orders.where("updated_at::date >= ? AND updated_at::date <= ?", week.at_beginning_of_week,week.at_end_of_week).group_by{|trial| trial.updated_at.to_date}
      @addon = current_organisation.glass_addons_prices.select("glass_addons_prices.*,addon_details.updated_at::date AS updated, count(addon_details.glass_addons_price_id) AS no_of_glass_addons, SUM(glass_addons_prices.price) AS cost").joins("INNER JOIN addon_details ON glass_addons_prices.id=addon_details.glass_addons_price_id").where("addon_details.updated_at::date >= ? AND addon_details.updated_at::date <= ?", week.at_beginning_of_week,week.at_end_of_week).group("glass_addons_prices.id, addon_details.glass_addons_price_id, updated").order("no_of_glass_addons DESC").group_by{|x| x.updated.to_date}.sort_by{|k,v| v.inject(0){|sum,i| sum+i.cost}}.reverse!
      @addon = @addon.sort.to_h
      @frame = current_organisation.glass_frames_prices.select("glass_frames_prices.*, eye_glass_orders.updated_at::date AS updated, SUM(glass_frames_prices.price) AS cost, count(eye_glass_orders.glass_frames_price_id) AS no_of_glass_frames").joins("INNER JOIN eye_glass_orders ON glass_frames_prices.id=eye_glass_orders.glass_frames_price_id").where("eye_glass_orders.updated_at::date >= ? AND eye_glass_orders.updated_at::date <= ?", week.at_beginning_of_week,week.at_end_of_week).group("glass_frames_prices.id, eye_glass_orders.glass_frames_price_id, updated").order("no_of_glass_frames DESC").group_by{|x| x.updated.to_date}.sort_by{|k,v| v.inject(0){|sum,i| sum+i.cost}}.reverse!
      @frame = @frame.sort.to_h
      @exam_record = current_organisation.exam_procedures.select("exam_procedures.*, SUM(exam_procedures.price) AS cost,COUNT(exam_procedures.id) AS no_of_exam_procedure, exam_procedures_examinations.updated_at::date AS updated").joins("INNER JOIN exam_procedures_examinations  ON exam_procedures_examinations.exam_procedure_id=exam_procedures.id").where("exam_procedures_examinations.updated_at::date >= ? AND exam_procedures_examinations.updated_at::date <= ?", week.at_beginning_of_week,week.at_end_of_week).group("exam_procedures.id, updated").order("no_of_exam_procedure DESC").group_by{|x| x.updated.to_date}.sort_by{|k,v| v.inject(0){|sum,i| sum+i.cost} }
      
      @exam_record = @exam_record.sort.to_h
      order_record
    
    elsif params[:analytic][:year].present? && @view_name == 'year'
      @examinations = current_organisation.examinations.removed.where('extract(year from updated_at::date) = ?', params[:analytic][:year]).group_by{|trial| trial.updated_at.beginning_of_month.to_date}
      @orders = current_organisation.orders.dispensed.where('extract(year from updated_at::date) = ?', params[:analytic][:year]).group_by{|trial| trial.updated_at.beginning_of_month.to_date}
      @renewed_orders = current_organisation.favourite_orders.where('extract(year from updated_at::date) = ?', params[:analytic][:year]).group_by{|trial| trial.updated_at.beginning_of_month.to_date}
      @lenses = current_organisation.lenses.where('extract(year from updated_at::date) = ?', params[:analytic][:year]).group_by{|trial| trial.updated_at.beginning_of_month.to_date}
      @eyeglasses = current_organisation.eye_glass_orders.where('extract(year from updated_at::date) = ?', params[:analytic][:year]).group_by{|trial| trial.updated_at.beginning_of_month.to_date}
      @addon = current_organisation.glass_addons_prices.select("glass_addons_prices.*,addon_details.updated_at::date AS updated, count(addon_details.glass_addons_price_id) AS no_of_glass_addons, SUM(glass_addons_prices.price) AS cost").joins("INNER JOIN addon_details ON glass_addons_prices.id=addon_details.glass_addons_price_id").where("extract(year from addon_details.updated_at::date) = ?", params[:analytic][:year]).group("glass_addons_prices.id, addon_details.glass_addons_price_id, updated").order("no_of_glass_addons DESC").group_by{|x| x.updated.beginning_of_month.to_date}.sort_by{|k,v| v.inject(0){|sum,i| sum+i.cost}}.reverse!
      @addon = @addon.sort.to_h
      @frame = current_organisation.glass_frames_prices.select("glass_frames_prices.*, eye_glass_orders.updated_at::date AS updated, SUM(glass_frames_prices.price) AS cost, count(eye_glass_orders.glass_frames_price_id) AS no_of_glass_frames").joins("INNER JOIN eye_glass_orders ON glass_frames_prices.id=eye_glass_orders.glass_frames_price_id").where("extract(year from eye_glass_orders.updated_at::date) = ?", params[:analytic][:year]).group("glass_frames_prices.id, eye_glass_orders.glass_frames_price_id, updated").order("no_of_glass_frames DESC").group_by{|x| x.updated.beginning_of_month.to_date}.sort_by{|k,v| v.inject(0){|sum,i| sum+i.cost}}.reverse!
      @frame = @frame.sort.to_h
      @exam_record = current_organisation.exam_procedures.select("exam_procedures.*, SUM(exam_procedures.price) AS cost,COUNT(exam_procedures.id) AS no_of_exam_procedure, exam_procedures_examinations.updated_at::date AS updated").joins("INNER JOIN exam_procedures_examinations  ON exam_procedures_examinations.exam_procedure_id=exam_procedures.id").where("extract(year from exam_procedures_examinations.updated_at::date) = ?", params[:analytic][:year]).group("exam_procedures.id, updated").order("no_of_exam_procedure DESC").group_by{|x| x.updated.beginning_of_month.to_date}.sort_by{|k,v| v.inject(0){|sum,i| sum+i.cost} }
        
      @exam_record = @exam_record.sort.to_h

      @lens_material = current_organisation.lens_materials.select("lens_materials.*, eye_glass_orders.updated_at::date AS updated, SUM(lens_materials.price) AS cost, count(eye_glass_orders.lens_material_id) AS no_of_glass_frames").joins("INNER JOIN eye_glass_orders ON lens_materials.id=eye_glass_orders.lens_material_id").where(" extract(year from eye_glass_orders.updated_at::date) = ?", params[:analytic][:year]).group("lens_materials.id, eye_glass_orders.lens_material_id, updated").order("no_of_glass_frames DESC").group_by{|x| x.updated.beginning_of_month.to_date}.sort_by{|k,v| v.inject(0){|sum,i| sum+i.cost}}.reverse!
      @lens_material = @lens_material.sort.to_h

      @lenses_type = current_organisation.lens_types_prices.select("lens_types_prices.*, contact_lens_orders.updated_at::date AS updated, SUM(lens_types_prices.price) AS cost, count(contact_lens_orders.od_lens_types_price_id) AS no_of_lens").joins("INNER JOIN contact_lens_orders ON lens_types_prices.id=contact_lens_orders.od_lens_types_price_id").where("extract(year from contact_lens_orders.updated_at::date) = ?", Time.now.year.to_s).group("lens_types_prices.id, contact_lens_orders.od_lens_types_price_id, updated").order("no_of_lens DESC").group_by{|x| x.updated.beginning_of_month.to_date}.sort_by{|k,v| v.inject(0){|sum,i| sum+i.cost}}.reverse!
      @lenses_type = @lenses_type.sort.to_h

      @lens = current_organisation.eyeglass_types_prices.select("eyeglass_types_prices.*, eye_glass_orders.updated_at::date AS updated, SUM(eyeglass_types_prices.price) AS cost, count(eye_glass_orders.eyeglass_types_price_id) AS no_of_glass_frames").joins("INNER JOIN eye_glass_orders ON eyeglass_types_prices.id=eye_glass_orders.eyeglass_types_price_id").where(" extract(year from eye_glass_orders.updated_at::date) = ?", params[:analytic][:year]).group("eyeglass_types_prices.id, eye_glass_orders.eyeglass_types_price_id, updated").order("no_of_glass_frames DESC").group_by{|x| x.updated.beginning_of_month.to_date}.sort_by{|k,v| v.inject(0){|sum,i| sum+i.cost}}.reverse!
      @lens = @lens.sort.to_h
      order_record
    end
  end


  private
    def get_removed_examination_for_date date
      current_organisation.examinations.removed.where("updated_at::date = ?", date)
    end
    
    def get_removed_favourite_orders_for_date date
      current_organisation.favourite_orders.finalized.where("updated_at::date = ?", date)
    end
    
    def get_removed_orders_for_date date
      current_organisation.orders.finalized.where("updated_at::date = ?", date)
    end
    
    def order_record
      examination_record
      contact_lens_record
      eyeglass_record
    end 
    def examination_record
      @all_records = {}
      @exam_procedure = {}
      @procedure=current_organisation.exam_procedures.joins("INNER JOIN exam_procedures_examinations ON exam_procedures_examinations.exam_procedure_id = exam_procedures.id").group("exam_procedures.id").order("count(exam_procedures.id) desc").first(4) 
      @examinations.each do |key,examinations|
        @new_procedure_hash = {}
        record = {number_of_entries: nil, total: 0, eye_glass_payments: 0, contact_lens_payments: 0}
        @procedure.each do |proc|
          @new_procedure_hash.store(proc.name, 0)
        end
        examinations.each do |examination|
          record[:number_of_entries] = examinations.count
          if examination.try(:order).present?  #added this if conditions as it is throughing error when order becomes emplty
            record[:total] = record[:total] + examination.try(:order).try(:total)
            record[:eye_glass_payments] = record[:eye_glass_payments] + examination.order.eye_glass_orders.sum(:total) 
            record[:contact_lens_payments] = record[:contact_lens_payments] + examination.order.contact_lens_orders.sum(:total)
            @new_procedure_hash.each do |key1,value|
              if examination.try(:procedure_adjustments).map(&:exam_procedure).compact.map(&:name).include?(key1)
                @new_procedure_hash[key1]= @new_procedure_hash[key1]+1
              end
            end
          end
        end
        record[:exam_payments]= record[:total]-record[:eye_glass_payments]+record[:contact_lens_payments]
        @all_records.merge!(key=> record)
        @exam_procedure.store(key , @new_procedure_hash)
      end
      @all_records = @all_records.sort.to_h
    end

    def contact_lens_record
      @all_lens_types = {}
      @lenses.each do |lens, lenstypes|
        @lens_types = {}
        @lens_types = current_organisation.lenses.group('od_type').order('count_od_type DESC').limit(4).count('od_type')
        @lens_types.each do |key2,value|
          @lens_types[key2]=0
        end
        lenstypes.each do |lns|
          @lens_types.each do |key3,value|
            if lns.od_type == key3
              @lens_types[key3]= @lens_types[key3]+1
            end
          end
        end

        @all_lens_types.store(lens, @lens_types)
      end
      @all_lens_types = @all_lens_types.sort.to_h
    end

    def eyeglass_record
      @eyeglass_record = {}

      @eyeglasses.each do |key, glasstypes|
        @glass_types = {}
        @glass_types = current_organisation.glasses.group('glasses_type').order('count_glasses_type DESC').limit(2).count('glasses_type')
        @glass_types.each do |key2,value|
          @glass_types[key2]=0
        end
        glasstypes.each do |gls|
          @glass_types.each do |key3,value|
            if gls.glasses.map(&:glasses_type).include?(key3)
              @glass_types[key3]= @glass_types[key3]+1
            end
          end
        @eyeglass_record.merge!(key=> {glass_type: @glass_types})
        end
      end
      @eyeglass_record = @eyeglass_record.sort.to_h
    end
    
    def total_payment
      orders = Organisation.select(" SUM(sample.order_total) AS total, sample.date AS date").joins("INNER JOIN(select id AS order_id, total AS order_total, organisation_id AS org_id, updated_at::date AS date from orders where organisation_id =#{current_organisation.id} UNION select id AS order_id, total AS order_total, organisation_id AS org_id, updated_at::date AS date from favourite_orders where organisation_id =#{current_organisation.id}) AS sample ON sample.org_id=organisations.id").group("sample.date" ).flatten.as_json
      group_hash = {}
      orders.each do |order|
        group_hash[order['date']] = order['total']
      end
      return group_hash
    end
    
    def total_eyeglass_payment
      orders = Organisation.select(" SUM(sample.order_total) AS total, sample.date AS date").joins("INNER JOIN(select id AS order_id, total AS order_total, organisation_id AS org_id, updated_at::date AS date from eye_glass_orders where organisation_id =#{current_organisation.id} ) AS sample ON sample.org_id=organisations.id").group("sample.date" ).flatten.as_json
      group_hash = {}
      orders.each do |order|
        group_hash[order['date']] = order['total']
      end
      return group_hash
    end

    def total_contactlens_payment
      orders = Organisation.select(" SUM(sample.order_total) AS total, sample.date AS date").joins("INNER JOIN(select id AS order_id, total AS order_total, organisation_id AS org_id, updated_at::date AS date from contact_lens_orders where organisation_id =#{current_organisation.id} ) AS sample ON sample.org_id=organisations.id").group("sample.date" ).flatten.as_json
      group_hash = {}
      orders.each do |order|
        group_hash[order['date']] = order['total']
      end
      return group_hash
    end

    def total_examprocedure_payment
      current_organisation.orders.map(&:id)
    end
  
  end
