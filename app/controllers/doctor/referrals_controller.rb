##
#  Doctor::ReferralsController WE can create referral form from Exam. 

class Doctor::ReferralsController < Doctor::BaseController
  before_action :set_exam
  before_action :set_referral, only: [:edit, :update, :destroy, :send_mail_to_patient ]
  include ActionView::Helpers::NumberHelper

  def index
    @form_url =  examination_referrals_path(@exam.id)
    @old_referrals = @exam.referrals.all
  end

  def new
    @referral = Referral.new(examination_id: @exam.id)
    @form_url =  examination_referrals_path(@exam.id)
    @other_referrals = @exam.referrals 
  end

  def show
    @referral_form = Referral.find(params[:id])
    @exam = @referral_form.examination
    @patient = @exam.joint_resource.member
  end
  
  def create
    @referral = Referral.new(referral_params)
    if @referral.save
      @step_partial =  "referral_second_form"
      set_edit_instance
      render :edit and return
    else
      render :json => {status: 'Error', msg: "Something went wrong!"}.to_json 
    end    
  end
  
  def edit
    if params[:step] == "two_request"
      @step_partial = "referral_second_form"
    else
      @step_partial = "visibility_form"
    end
    set_edit_instance
  end
  
  def update
    @referral.update_attributes!(referral_params)  
    AppMailer.send_referral_to_cc_email(@referral).deliver_later if @referral.try(:cc_to).present?
    set_edit_instance
    if params[:step] == "two_request"
      @step_partial = "referral_second_form"      
      render :edit and return
    end
    flash[:notice] = "Notification sent successfully."
    send_mail_to_patient
  end
  
  def destroy
    @referral.destroy
    respond_to do |format|
      format.json {render :json => {status: 'Success', msg: "Referral has been deleted successfully!"}}
    end
  end

  #Send refrral mail to patient when Refrral will be ready. 
  def send_mail_to_patient
    begin
      patient = @exam.try(:joint_resource).try(:patient)
      one_click_url = root_url(patient_email: patient.email, authentication_token: patient.authentication_token, subdomain: patient.subdomain)
      one_click_url.gsub!(patient.subdomain, patient.subdomain + ".dev") if Rails.env.include?('staging') && patient.subdomain != 'dev'
      one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
      AppMailer.send_referral_to_user(@exam.try(:joint_resource).try(:member), @referral).deliver_later unless patient.try(:email_not_supplied)
      notification_message = AppConfig.sms_notification["referral_ready"] % {patient_name: @exam.try(:joint_resource).try(:name), referr_to: @referral.try(:refer_to), reason: @referral.try(:referral_reason),custom_message: @referral.try(:message), one_click_login: one_click_url , store_name: @exam.try(:store).try(:description) ,store_office_homepage: "#{@exam.try(:store).url}", store_phone_number: "#{number_to_phone(@exam.try(:store).try(:phone_number).to_s[2..-1].to_i, area_code: true)}"} 
      TwilioSmsWorker.perform_async({'src'=> current_organisation.twilio_number, 'dst' => @exam.try(:joint_resource).try(:member).try(:phone), 'text' => notification_message, 'notification_method'=> patient.notification_method,'org_sid'=> current_organisation.twilio_acc_sid,'org_auth_token'=> current_organisation.twilio_auth_token})
      if params[:fax].present?
        @fax = params[:fax]
        @referral_form = Referral.find(params[:id])
        AppMailer.generate_pdf_fax_worker(@referral_form,'referral.pdf',request.base_url,@fax).deliver_later
      end
      flash[:status] = "Success"
      flash[:notice] = "Referral has been successfully sent." 
    rescue Exception => e
      flash[:status] = 'Error'
      flash[:notice] = "Error due to #{e.message}"
    end
    redirect_to :back
  end

  def find_contact
    if params[:id].present?
      @contact = Contact.find params[:id]
    end
    respond_to do |format| 
      format.html
      format.js  
    end
  end
  
  private
    def set_exam
      @exam = current_organisation.examinations.find_by(id: params[:examination_id])
    end

    def set_referral
      @referral = Referral.find(params[:id])
    end

    def referral_params
      params.require(:referral).permit!
    end

    def set_edit_instance
      @form_url = examination_referral_path(@exam.id, @referral.id)
      @patient = @exam.joint_resource.member
      @other_referrals = @exam.referrals.where("id != ?", @referral.id)
    end
    
  end