##
#  All doctor namespace controller inherited from BaseController

class Doctor::BaseController < ApplicationController
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  rescue_from CanCan::AccessDenied do |exception|
    flash[:notice] = "Access denied."
    redirect_to root_url
  end

  def current_ability
    @current_ability ||= Ability.new(current_doctor)
  end
  
end
