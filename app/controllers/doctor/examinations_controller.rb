##
# Examination Controller has exam form for practice. 

class Doctor::ExaminationsController < Doctor::BaseController
  before_action :set_examination_and_patient, only: [:view, :show, :edit, :update, :destroy]
  before_action :get_resource, only: [:new]  
  include AppConstant

  ## We don't have new view, when user open exam we just build all object in backend and create exam and redirect to Exam Edit.
  def new
    @today_exam = Examination.where("Date(created_at) = ? and joint_resource_id = ?", Date.today,@joint_resource.id)
    unless @today_exam.present?  
      @examination = current_doctor.examinations.build
      @examination.joint_resource_id=@joint_resource.id
      @resource=@examination.joint_resource.member
      @last_exam = @joint_resource.examinations.last
      if params[:booking_id].present?
        @booking = Booking.find(params[:booking_id])
        @examination.booking_id = @booking.id
        @examination.doctor_id = @booking.doctor.id
        @booking.exam_procedure_ids.each do |exam_procedure_id|
          @examination.procedure_adjustments.build(exam_procedure_id: exam_procedure_id) 
        end
      end
      @examination.build_social_history
      @last_exam.present? ? (@examination.build_binocular_testing(@last_exam.binocular_testing.attributes.except("id","examination_id", "created_at", "updated_at"))): @examination.build_binocular_testing
      @last_exam.present? ? (@examination.build_primlinary_exam(@last_exam.primlinary_exam.attributes.except("id","examination_id", "created_at", "updated_at"))) : @examination.build_primlinary_exam
      @examination.visual_acuities.build(using_glasses: false)
      @examination.visual_acuities.build(using_glasses: true)
      @last_exam.present? ? (@examination.glasses.build(@last_exam.glasses.last.attributes.except("id","visual_acuity_id","created_at","updated_at","examination_id","eye_glass_order_id","organisation_id","joint_resource_id","selected_finalize"))) : @examination.glasses.build
      @examination.eye_measurements.build(measurement_type: "subjective", default_refraction: true)
      @examination.eye_measurements.build(measurement_type: "objective", default_refraction: true)
      @examination.eye_measurements.build(measurement_type: "bincular_balance", default_refraction: true)
      @examination.eye_measurements.build(measurement_type: "cyclopelgic", default_refraction: true)
      @examination.build_contact_lens_exam.lenses.build(lens_type: "current_lens")
      @last_exam.present? ? (@examination.build_external_exam(@last_exam.external_exam.attributes.except("updated_at","id","examination_id","created_at"))) :  @examination.build_external_exam
      @last_exam.present? ? (@examination.build_internal_exam(@last_exam.internal_exam.attributes.except("updated_at","id","examination_id","created_at"))) :  @examination.build_internal_exam    
      begin
        if @examination.save
          redirect_to edit_examination_path(@examination.id)    
          flash[:notice] = "Exam has been created succesfully"
        else
          flash[:alert] = "Last exam has not been finalized yet! Please finalize previous exam, and then proceed to create a new exam."
          redirect_to :back
        end
      rescue Exception => e
        flash[:alert] = "Something went wrong due to #{e} !!"
        redirect_to :back
      end
    else
      redirect_to :back
    end
  end

  def create
    begin
      @examination = Examination.create!(examination_params)
      if params[:order_id].present?
        @order = Order.find(params[:order_id])
        @order.update_attributes!(:examination_id => @examination.id)
        @examination.update_attributes!(:status => 'unfexam')
      end
      glasses = []      
      flash[:notice] = "Exam has been created succesfully"
      redirect_to edit_examination_path(@examination.id), change: :exam_form
    rescue Exception => e
      flash[:alert] = "Something went wrong due to #{e} !!"
      redirect_to :back
    end
  end

  def edit    
    if params[:changecall].present?
      case params[:changecall]
      when "pending_exam"
        @examination.status = 1
      when "pending_order"
        @examination.status = 2
      end
      @examination.save
    end
    if params[:prior_exam_follow_up].present?
      exam_procedure = @examination.procedure_adjustments.where("prior_exam_follow_up = ?", true)
      unless exam_procedure.present?
        @examination.procedure_adjustments.create(prior_exam_follow_up:true, exam_procedure_id: params[:exam_procedure_id])
      end  
    end
    @all_exam = @joint_resource.examinations
    @doctors = current_organisation.doctors.doctors_with_role_doctor
    show_view 
  end
  
  #Exam form update with ajax and have differnt action that will save existing value in exam form as well as add some new object.
  # We have many action on exam form so we are managing update request with params.
  def update
    if params[:examination][:contact_lens_exam_attributes].present? && !(params[:examination][:contact_lens_exam_attributes][:dominated_eye_od].present?)
      params[:examination][:contact_lens_exam_attributes][:dominated_eye_od] = "off"
    end
    if params[:examination][:medication].present?
      current_organisation.diseases.prescribing_medication.find_or_create_by(name: params[:examination][:medication], :groups => "{prescribing_medication}")
      current_organisation.diseases.dosage.find_or_create_by(name: params[:examination][:dosage], :groups => "{dosage}")
      @medical_rx = @examination.medical_rxes.find_or_create_by(medication: params[:examination][:medication],dosage: params[:examination][:dosage], refill: params[:examination][:refill_start], generic: params[:generic])
      @medical_rx.save!
      @plan = params[:examination][:medication].to_s + ' ' + params[:examination][:dosage] + " with " + params[:examination][:refill_start] + " refills"
      params[:examination][:impression].each do |k,v|
        @impression = current_organisation.impressions.find_or_create_by(name: v, code: '321')
        @exam_impression = @examination.exam_impressions.find_or_create_by(impression_id: @impression.id,plan: @plan, eye: params[:eye].present? ? params[:eye][k] : 'OU')
        @exam_impression.save!
      end
    end
    if params[:examination][:social_history_attributes].present?
      params[:examination][:social_history_attributes][:neurological_psych_status_ids] = [] unless params[:examination][:social_history_attributes][:neurological_psych_status_ids].present?
    end

    if @examination.update_attributes!(examination_params)
      case params[:commit]
        when "Finalized"
          @examination.status = 'finalized'
          @examination.save
          flash[:status] = "Success"
          flash[:notice] = "Exam has been successfully Finalized"
          @examination.booking.update_columns(:status => 5)    if @examination.booking.present? # booking is finalized here
          AppMailer.send_reinvite_for_new_exam_one_year_follow_up(@examination).deliver_later(wait: 1.year) unless @examination.try(:joint_resource).try(:patient).try(:email_not_supplied)
          render js: "window.location = '#{root_path}'" and return
        when "add_contact_lens"
          lens =  @examination.contact_lens_exam.lenses.order("created_at ASC").last.deep_clone include: :visual_acuity
          lens.lens_date = Date.today
          lens.notes =  ''
          lens.save!
        when "add_glass"
          glass = @examination.glasses.last.deep_clone include: :visual_acuity
          glass.save!
        when "add_lens_prescription"
          lens_prescription = @examination.try(:lens_prescriptions).try(:last).deep_clone include: {lens: [:visual_acuity, :contact_lens_exam]}
          lens_prescription.save!   
        when  "add_glass_prescription"
          glass_prescription = @examination.try(:glasses_prescriptions).try(:last).deep_clone include: {glass: glass,eye_measurement: glass },  except: {eye_measurement: :default_refraction}
          glass_prescription.save!
        when "add_gat"
          gat = @examination.external_exam.gats.new
          gat.save!
        when "add_nct"
          nct = @examination.external_exam.ncts.new
          nct.save!
        when "primlinary_exam_notes"
          notes = @examination.primlinary_exam.primlinary_exam_notes.new
          notes.save!
        when "copy_to_subjective"  
          @objective = @examination.eye_measurements.where(measurement_type: "objective", default_refraction: true).first
          @subjective = @examination.eye_measurements.where(measurement_type: "subjective", default_refraction: true).first
          @subjective.notes = @objective.notes
          @subjective.show = true
          @objective.update(show: true)
          @o_glass = JSON.parse @objective.glass.to_json
          @o_visual_acuity = JSON.parse @objective.glass.visual_acuity.to_json        
          @subjective.glass.update(@o_glass.slice( "od_sphere", "od_cylinder", "od_axis", "os_sphere", "os_cylinder", "os_axis", "od_prism", "os_prism", "od_segment_height", "os_segment_height", "od_base_curve", "os_base_curve", "ou_add"))
          @subjective.glass.visual_acuity.update(@o_visual_acuity.slice("near_od", "near_os", "near_ou", "distance_od", "distance_os", "distance_ou"))
          @subjective.save!
        when "copy_to_objective"
          @objective = @examination.eye_measurements.where(measurement_type: "objective", default_refraction: true).first
          @subjective = @examination.eye_measurements.where(measurement_type: "subjective", default_refraction: true).first
          @objective.notes = @subjective.notes
          @objective.show = true
          @subjective.update(show: true)
          @s_glass = JSON.parse @subjective.glass.to_json
          @s_visual_acuity = JSON.parse @subjective.glass.visual_acuity.to_json        
          @objective.glass.update(@s_glass.slice( "od_sphere", "od_cylinder", "od_axis", "os_sphere", "os_cylinder", "os_axis", "od_prism", "os_prism", "od_segment_height", "os_segment_height", "od_base_curve", "os_base_curve", "ou_add"))
          @objective.glass.visual_acuity.update(@s_visual_acuity.slice("near_od", "near_os", "near_ou", "distance_od", "distance_os", "distance_ou"))
          @objective.save!
        when "copy_to_bincular_from_objective"  
          @objective = @examination.eye_measurements.where(measurement_type: "objective", default_refraction: true).first
          @bincular = @examination.eye_measurements.where(measurement_type: "bincular_balance", default_refraction: true).first
          @bincular.notes = @objective.notes
          @bincular.show = true
          @objective.update(show: true)
          @o_glass = JSON.parse @objective.glass.to_json
          @o_visual_acuity = JSON.parse @objective.glass.visual_acuity.to_json        
          @bincular.glass.update(@o_glass.slice( "od_sphere", "od_cylinder", "od_axis", "os_sphere", "os_cylinder", "os_axis", "od_prism", "os_prism", "od_segment_height", "os_segment_height", "od_base_curve", "os_base_curve", "ou_add"))
          @bincular.glass.visual_acuity.update(@o_visual_acuity.slice("near_od", "near_os", "near_ou", "distance_od", "distance_os", "distance_ou"))
          @bincular.save!
        when "copy_to_bincular_from_subjective"
          @subjective = @examination.eye_measurements.where(measurement_type: "subjective", default_refraction: true).first
          @bincular = @examination.eye_measurements.where(measurement_type: "bincular_balance", default_refraction: true).first
          @bincular.notes = @subjective.notes
          @bincular.show = true
          @subjective.update(show: true)
          @o_glass = JSON.parse @subjective.glass.to_json
          @o_visual_acuity = JSON.parse @subjective.glass.visual_acuity.to_json        
          @bincular.glass.update(@o_glass.slice( "od_sphere", "od_cylinder", "od_axis", "os_sphere", "os_cylinder", "os_axis", "od_prism", "os_prism", "od_segment_height", "os_segment_height", "od_base_curve", "os_base_curve", "ou_add"))
          @bincular.glass.visual_acuity.update(@o_visual_acuity.slice("near_od", "near_os", "near_ou", "distance_od", "distance_os", "distance_ou"))
          @bincular.save!
        when "copy_to_cyclopelgic_from_objective" 
          @objective = @examination.eye_measurements.where(measurement_type: "objective", default_refraction: true).first
          @cyclopelgic = @examination.eye_measurements.where(measurement_type: "cyclopelgic", default_refraction: true).first
          @cyclopelgic.notes = @objective.notes
          @cyclopelgic.show = true
          @objective.update(show: true)
          @o_glass = JSON.parse @objective.glass.to_json
          @o_visual_acuity = JSON.parse @objective.glass.visual_acuity.to_json        
          @cyclopelgic.glass.update(@o_glass.slice( "od_sphere", "od_cylinder", "od_axis", "os_sphere", "os_cylinder", "os_axis", "od_prism", "os_prism", "od_segment_height", "os_segment_height", "od_base_curve", "os_base_curve", "ou_add"))
          @cyclopelgic.glass.visual_acuity.update(@o_visual_acuity.slice("near_od", "near_os", "near_ou", "distance_od", "distance_os", "distance_ou"))
          @cyclopelgic.save!
        when "copy_to_cyclopelgic_from_subjective"  
          @subjective = @examination.eye_measurements.where(measurement_type: "subjective", default_refraction: true).first
          @cyclopelgic = @examination.eye_measurements.where(measurement_type: "cyclopelgic", default_refraction: true).first
          @cyclopelgic.notes = @subjective.notes
          @cyclopelgic.show = true
          @subjective.update(show: true)
          @o_glass = JSON.parse @subjective.glass.to_json
          @o_visual_acuity = JSON.parse @subjective.glass.visual_acuity.to_json        
          @cyclopelgic.glass.update(@o_glass.slice( "od_sphere", "od_cylinder", "od_axis", "os_sphere", "os_cylinder", "os_axis", "od_prism", "os_prism", "od_segment_height", "os_segment_height", "od_base_curve", "os_base_curve", "ou_add"))
          @cyclopelgic.glass.visual_acuity.update(@o_visual_acuity.slice("near_od", "near_os", "near_ou", "distance_od", "distance_os", "distance_ou"))
          @cyclopelgic.save!
        when "copy_to_objective_from_current"
          @objective = @examination.eye_measurements.where(measurement_type: "objective", default_refraction: true).first
          @glass = @examination.glasses.last
          @objective.show = true
          @s_glass = JSON.parse @glass.to_json
          @s_visual_acuity = JSON.parse @glass.visual_acuity.to_json        
          @objective.glass.update(@s_glass.slice( "od_sphere", "od_cylinder", "od_axis", "os_sphere", "os_cylinder", "os_axis", "od_prism", "os_prism", "od_segment_height", "os_segment_height", "od_base_curve", "os_base_curve", "ou_add"))
          @objective.glass.visual_acuity.update(@s_visual_acuity.slice("near_od", "near_os", "near_ou", "distance_od", "distance_os", "distance_ou"))
          @objective.save!
        when "copy_to_subjective_from_current"  
          @subjective = @examination.eye_measurements.where(measurement_type: "subjective", default_refraction: true).first
          @glass = @examination.glasses.last
          @subjective.show = true
          @o_glass = JSON.parse @glass.to_json
          @o_visual_acuity = JSON.parse @glass.visual_acuity.to_json        
          @subjective.glass.update(@o_glass.slice( "od_sphere", "od_cylinder", "od_axis", "os_sphere", "os_cylinder", "os_axis", "od_prism", "os_prism", "od_segment_height", "os_segment_height", "od_base_curve", "os_base_curve", "ou_add"))
          @subjective.glass.visual_acuity.update(@o_visual_acuity.slice("near_od", "near_os", "near_ou", "distance_od", "distance_os", "distance_ou"))
          @subjective.save!
        end
        if params[:any_finalize].present? and params[:any_finalize] == "true"
          render :json => {status: 'Success', msg: "The prescription has successfully been finalized.", partial_tab: params[:submit]}.to_json
        else
          render :json => {status: 'Success', msg: "Exam has been updated successfuly.", partial_tab: params[:submit]}.to_json
        end
    else
      render :json => {status: 'Error', msg: "Error in updating form"}.to_json
    end
  end

  def view
    show_view
    render :partial => "view" 
  end

  def upload_file
    @examination = Examination.find(params[:id])
    if params[:attached_file] != "undefined"
      begin
        @doc = @examination.documents.new
        @doc.docs = params[:attached_file]
        @doc.type_of_file = params[:attached_file].content_type.split('/').first == "image" ? "image" : "file"
        @doc.save!
        render :json => {status: 'Success', msg: "Uploaded successfuly."}.to_json
      rescue Exception => e
        render :json => {status: 'Error', msg: e.message}.to_json
      end
    else
      render :json => {status: 'Error', msg: "Please select a file"}.to_json
    end
  end

  def download_file
    doc = Document.find(params[:doc_id])
    file_name = doc.docs_file_name
    file_type = doc.docs_content_type
    file_path = doc.docs.path
    send_file(
      file_path,
      filename: file_name,
      type: file_type,
      disposition: 'attachment'
    )
  end

  def delete_file
    doc = Document.find(params[:doc_id])
    doc.destroy!
    render :json => {status: 'Success', msg: "Deleted successfuly."}.to_json
  end
    
  def destroy
   if params[:remove_dispensed]
     @examination.status = 'removed'
     @examination.save
     AppMailer.send_dispensed_removal_to_patient(@examination).deliver_later if params[:mail] && !(@examination.try(:joint_resource).try(:patient).try(:email_not_supplied))
   elsif params[:remove_finalized]
     @examination.status = 'removed'
     @examination.save
     @follow_up = @resource.follow_ups.create!(:reason => "finalized order removed!",:due_date => Time.now)
     @follow_up.destroy
   else
     AppMailer.send_appointment_cancellation_to_patient(@examination).deliver_later if params[:mail] && !(@examination.try(:joint_resource).try(:patient).try(:email_not_supplied))
     @examination.destroy
   end
   
   redirect_to :back
  end

  def update_medical_rx
    if params[:med_id].present?
      @med_rx_dates_update = MedicalRx.find(params[:med_id])
      @med_rx_dates_update.update_attributes(issued_date: params[:issue_date],expired_date: params[:expired_date])
    end
    render :json => {status: 'Success', msg: "Updated successfuly."}.to_json
  end

  def remove_medical_rx
    med_rx = MedicalRx.find(params[:medical_rx_id])
    med_rx.destroy!
    render :json => {status: 'Success', msg: "Deleted successfuly."}.to_json
  end

  private

    def examination_params
      params.require(:examination).permit!
    end

    def show_view
      current_organisation.diseases.default_medical.each do |disease|
        (@resource.pmhxes.map(&:disease_id).include? disease.id) ? "" : @resource.pmhxes.build(disease: disease, joint_resource_id: @resource.joint_resource.id)
        (@resource.pohxes.map(&:disease_id).include? disease.id) ? "" : @resource.pohxes.build(disease: disease, joint_resource_id: @resource.joint_resource.id)      
      end
      @bincular=@examination.eye_measurements.where(measurement_type: "bincular")
      @cyclopelgic=@examination.eye_measurements.where(measurement_type: "cyclopelgic")
      @old_exams = @joint_resource.examinations.where("created_at <?", @examination.created_at)
      @previous_exam = @old_exams.last if @old_exams.present?
    end

    def set_examination_and_patient
      @examination=current_organisation.examinations.find(params[:id])
      @joint_resource=@examination.joint_resource
      @resource = @joint_resource.member
      title = @resource.name + ' ' + @examination.exam_date.to_s + ' ' + @examination.store.try(:description)
      set_meta_tags title: title
    end

    def get_resource
      @joint_resource= JointResource.find_by(id: params[:id]) #joint resource id passes in params
    end
end
