##
# DashboardsController is associated to all functionality that showing on doctor Dhashboard.
# All tabs that showing on Doctor Dashboard will be load with ajax.

class Doctor::DashboardsController < Doctor::BaseController
  include ActionView::Helpers::NumberHelper
  before_action :set_seo_attributes, except: [:customer_profile]
  before_action :set_title_profile, only: [:customer_profile]
  include PatientAccountSection
  
  def index
    params[:q] ||= {}
    params[:search_form] ||= ""
    @doctor=current_doctor
    if read_org_appointment(@doctor)
      dashboard_data current_organisation
    elsif read_store_appointment(@doctor)
      dashboard_data_store @doctor.store
    else
      dashboard_data @doctor
    end
    if current_doctor.store.present?
      @contact_lens_distribution_orders = current_doctor.store.contact_lens_distribution_orders.where(soft_delete: false)
    end
    respond_to do |format|
      format.html
      format.js
    end
  end

  def update
    begin
      current_doctor.update!(doctor_params)
      flash[:status]="Success"
      flash[:notice]="Profile successfully updated"
    rescue Exception => e
      flash[:status] = "Error"
      if (e.message == "param is missing or the value is empty: doctor")  
        flash[:notice] = "Please choose avatar"
      else
        flash[:notice] = "Something went wrong due to #{e} !! Try it again"
      end
    end
    respond_to do |format|
      format.json { render :json => {status: flash[:status], msg: flash[:notice]}.to_json }
      format.html {redirect_to root_url}
    end
  end

  def new_patient
  end
  
  def patient_log
    params[:q] ||= {}
    if params[:q][:patient_email_cont].present? && params[:q][:patient_email_cont].include?('n/a')
      @patients = current_organisation.patients.where(email_not_supplied: true).order(updated_at: :desc).map(&:joint_resource)
      @patients = @patients.paginate(:page => params[:page], :per_page => 30)
    else
      @patients_search = current_organisation.joint_resources.ransack(params[:q])
      @patients = @patients_search.result.includes(:patient,:examinations,:favourites,:referrals,:orders).order("patients.updated_at DESC").paginate(:page => params[:page], :per_page => 30)
    end
    @resources = current_organisation.stores.find_each
    @resource = @resources.first
    respond_to do |format|
      format.html
      format.js
    end
  end

  # This action use to show patient profile for doctor
  def customer_profile
    params[:q] ||= {}
    @patient = Patient.find_by(id: params[:patient_id])
    if @patient.present?
      @exams = current_organisation.examinations.where(joint_resource_id: @patient.try(:joint_resource).try(:id)).order('updated_at DESC')
      @search = current_organisation.orders.where(joint_resource_id: @patient.try(:joint_resource).try(:id)).order('order_date DESC').ransack(params[:q])
      @orders =  @search.result.includes(:doctor).paginate(:page => params[:customer_orders_page], :per_page => 10)
      @referrals = @patient.referrals
      @appointments = current_organisation.bookings.with_deleted.where(joint_resource_id: @patient.joint_resource.id).order('start_time DESC')
      @follow_ups_appointments = @appointments.joins(:exam_procedures).where("exam_procedures.name =?", "Follow Up")
      respond_to do |format|
        format.html { render 'customer_profile' and return }
        format.js
      end
    else
      redirect_to :root
    end
  end
  
  def patient_info
    joint_resource = current_organisation.joint_resources.find(params[:jr_id])
    @patient = joint_resource.member
    @address = Address.find_by(id: joint_resource.current_address_id) 
    jr=Hash["Patient id"=>joint_resource.id]
    @all_info = { :Personal => jr.merge(@patient.as_json),:Address => @address.as_json }
    respond_to do |format|
      format.json{ render :json => @all_info.present? ? @all_info.as_json.map{|key,value| [key,value.present? ? value.map{|k,v| [k.titleize,v.to_s]} : [] ] } : {} }
    end
  end
  
  def exam_audits
    @examination = current_organisation.examinations.find(params[:id])
    respond_to do |format|
      format.js{ render :status => 200 }
    end
  end
  
  def order_audits
    @order = current_organisation.orders.find(params[:id])
    respond_to do |format|
      format.js{ render :status => 200 }
    end
  end

  def card_verification
    begin
      @joint_resource = current_organisation.joint_resources.find(params[:jr_id])
      @validation_response = @joint_resource.patient.profile.validate_profile(params['creditcard']['verification_value'])
      if @validation_response.success?
        @joint_resource.patient.profile.update_attributes!(:validated => true)
        flash[:success] = 'success'
        flash[:notice] = "Profile Payment Method Successfully updated !!"
      else
        flash[:success] = 'error'
        flash[:notice] = "Invalid payment/credit card details"
      end
      respond_to do |format|
        format.json { render :json => {status: flash[:success], msg: flash[:notice]}.to_json }
      end
    rescue Exception => e
      render :json => {status: 'Failure', msg: "! Error due to #{e.message}"}.to_json
    end
  end

  def background_update
    current_doctor.update(bgurl: Background.find(params[:bg_id]).avatar.url)
    redirect_to :back
  end

  

  def password_update
    @doctor = current_doctor
    if @doctor.update_with_password(params.require(:edit_doctor).permit(:password, :password_confirmation, :current_password))
      # Sign in the user by passing validation in case their password changed
      sign_in @doctor, :bypass => true
      render :json => {status: 'Success', msg: "Password Successfully updated"}.to_json
    else
      render :json => {status: 'Success', msg: "Error Updating Password due to #{@doctor.errors.full_messages.join(', ')}"}.to_json
    end
  end

  def email_prescription  
    if params[:glass].present?
      @glass_prescription = GlassesPrescription.find(params[:id])
      @exam = @glass_prescription.try(:examination)
    else
      @lens_prescription = current_organisation.try(:lens_prescriptions).find(params[:id])
      @exam = @lens_prescription.try(:examination)
    end
    if (@glass_prescription.present? || @lens_prescription.present?)
      AppMailer.send_prescription_to_patient(@exam.try(:joint_resource).try(:member),@exam).deliver_later unless @exam.try(:joint_resource).try(:patient).try(:email_not_supplied)
      render :json => {status: 'Success', msg: "Email has been sent successfully"}
    else
      render :json => {status: 'Error', msg: "Email not sent !"} unless params[:attachment].present? 
    end
  end

  def confirm_account
    patient = current_organisation.patients.find(params[:id])
    patient.delay.resend_confirmation_instructions
    render :json => {status: 'Success', msg: "Email has been sent successfully"}
  end

  def reconfirm_notify
    if params[:patient_class] == "FamilyMember"
      patient = current_organisation.family_members.find(params[:id])
    else
      patient = current_organisation.patients.find(params[:id])
    end
    booking = Booking.find(params[:booking])
    AppMailer.send_booking_confirmation(booking).deliver_now
    one_click_url=Rails.application.routes.url_helpers.root_url(host: CrmDatabase::Application.config.action_mailer.default_url_options[:host], sudomain: patient.try(:subdomain),  patient_email: patient.try(:email), authentication_token: patient.try(:authentication_token), subdomain: patient.try(:subdomain))
    one_click_url.gsub!(patient.try(:subdomain), patient.try(:subdomain) + ".dev") if Rails.env.include?('staging') && patient.try(:subdomain) != 'dev'
    one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
    notification_message = AppConfig.sms_notification["appointment_create"] % {patient_name: patient.try(:name), doctor_name: booking.doctor.name, appointment_time: booking.start_time.strftime('%m/%d/%Y %I:%M%P'), one_click_login: one_click_url, store_name: booking.store.description , store_address: "#{booking.store.try(:office_address).try(:address1)}, #{booking.store.try(:office_address).try(:address2)}, postal code #{booking.store.try(:office_address).try(:postal_code)}", store_phone_number: "#{number_to_phone(booking.store.phone_number.to_s[2..-1].to_i, area_code: true)}", store_office_homepage: "#{booking.store.url}"}
    TwilioSmsWorker.perform_async({'src'=> booking.organisation.twilio_number, 'dst' => patient.phone, 'text' => notification_message, 'trackable_id'=> booking.id, 'trackable_type'=> 'Booking', 'notification_method'=> booking.joint_resource.patient.notification_method,'org_sid'=> booking.organisation.twilio_acc_sid,'org_auth_token'=> booking.organisation.twilio_auth_token})
    flash[:status]="Success"
    flash[:notice] = "Reconfirmation successfully sent"
    respond_to do |format|
      format.html {redirect_to :back}
      format.json { render :json => {status: flash[:status], msg: flash[:notice]}.to_json }
    end
  end

  #This action is used just to relaod the today's appointments listing table over dashboard under overview tab
  def todays_appointments
    role = current_doctor.role - [""]
    @doctor=current_doctor
    @today_appointment_non_unique=current_organisation.bookings.with_deleted.where(start_time: Time.now.beginning_of_day..Time.now.end_of_day).where.not(joint_resource_id: nil).order(:start_time => :asc)
    @today_appointment_unique=[]
    a = @today_appointment_non_unique.map(&:joint_resource_id).uniq
    a.each{|aa| @today_appointment_unique << @today_appointment_non_unique.where(joint_resource_id: aa).order(:start_time => :asc).last}
    @today_appointment = @today_appointment_unique.sort_by{|x| x.start_time}
    respond_to do |format|
      format.js { render layout: false, content_type: 'text/javascript' }
    end
  end

  def exam_section
    @doctor=current_doctor
    if read_org_appointment(@doctor)
      @recent_exams = current_organisation.examinations.where(:status => [0,1,2,3,4]).order(:updated_at => :desc)
    elsif read_store_appointment(@doctor)
      store = @doctor.store
      @recent_exams = Examination.joins(:doctor).where(doctors: {store_id: store.id}).where(:status => [0,1,2,3,4]).order(:updated_at => :desc)
    else
      @recent_exams = @doctor.examinations.where(:status => [0,1,2,3,4]).order(:updated_at => :desc)
    end
    render  partial: 'doctor/dashboards/dashboard_tabs/exam_tab',locals: { recent_exams: @recent_exams}
  end

  def order_section
    @doctor=current_doctor
    if read_org_appointment(@doctor)
      @recent_orders = current_organisation.orders.where(:status => [0,1,2,3,4]).where("date(updated_at) > ?", 15.days.ago).order(:updated_at => :desc)
      @recent_renewed_orders = current_organisation.favourite_orders.where(:status => [0,1,2]).order(:updated_at => :desc)
    elsif read_store_appointment(@doctor)
      store = @doctor.store
      @recent_orders = Order.joins(:doctor).where(doctors: {store_id: store.id}).where(:status => [0,1,2,3,4]).where("date(orders.updated_at) > ?", 15.days.ago).order(:updated_at => :desc)
      @recent_renewed_orders = FavouriteOrder.joins(:doctor).where(doctors: {store_id: store.id}).where(:status => [0,1,2]).order(:updated_at => :desc)
    else
      @recent_orders = @doctor.orders.where(:status => [0,1,2,3,4]).where("date(updated_at) > ?", 15.days.ago).order(:updated_at => :desc)
      @recent_renewed_orders = @doctor.favourite_orders.where(:status => [0,1,2]).order(:updated_at => :desc)
    end
    @recent_orders = @recent_orders.map{|x| (x.contact_lens_orders.present? || x.eye_glass_orders.present?) ? x : nil}.compact
    render  partial: 'doctor/dashboards/dashboard_tabs/order_tab',locals: { recent_orders: @recent_orders, recent_renewed_orders: @recent_renewed_orders}
  end

  def follow_section
    @doctor=current_doctor
    if read_org_appointment(@doctor)
      @follow_ups = current_organisation.orders.follow.order(:updated_at => :desc)
      @follow_list = FollowUp.where("due_date >= ? AND doctor_id IN (?)", DateTime.now,current_organisation.doctors.map(&:id))
      @renewed_follow_ups = current_organisation.favourite_orders.follow.order(:updated_at => :desc)
    elsif read_store_appointment(@doctor)
      store = @doctor.store
      @follow_ups = Order.joins(:doctor).where(doctors: {store_id: store.id}).follow.order(:updated_at => :desc)
      @follow_list = FollowUp.where("due_date >= ? AND doctor_id IN (?)", DateTime.now,current_organisation.doctors.map(&:id))
      @renewed_follow_ups = FavouriteOrder.joins(:doctor).where(doctors: {store_id: store.id}).follow.order(:updated_at => :desc)
    else
      @follow_ups = @doctor.orders.follow.order(:updated_at => :desc)
      @follow_list = FollowUp.where("due_date >= ? AND doctor_id IN (?)", DateTime.now,current_organisation.doctors.map(&:id))
      @renewed_follow_ups = @doctor.favourite_orders.follow.order(:updated_at => :desc)
    end
    render  partial: 'doctor/dashboards/dashboard_tabs/follow_tab',locals: { follow_ups: @follow_ups, follow_list: @follow_list, renewed_follow_ups: @renewed_follow_ups}
  end

  def dispense_section
    @doctor=current_doctor
    if read_org_appointment(@doctor)
      @to_be_dispensed = current_organisation.orders.ready_for_pick_up.where(:status => [0,1,2,3,4]).order(:updated_at => :desc)
      @re_newed_dispensed = current_organisation.favourite_orders.ready_for_pick_up.order(:updated_at => :desc)
    elsif read_store_appointment(@doctor)
      store = @doctor.store
      @to_be_dispensed = Order.joins(:doctor).where(doctors: {store_id: store.id}).ready_for_pick_up.order(:updated_at => :desc)
      @re_newed_dispensed = FavouriteOrder.joins(:doctor).where(doctors: {store_id: store.id}).ready_for_pick_up.order(:updated_at => :desc)
    else
      @to_be_dispensed = @doctor.orders.ready_for_pick_up.where(:status => [0,1,2,3,4]).order(:updated_at => :desc)
      @re_newed_dispensed = @doctor.favourite_orders.ready_for_pick_up.order(:updated_at => :desc)
    end
    @to_be_ordered = current_organisation.orders.ordered.order(:updated_at => :desc)
    @re_newed_ordered = current_organisation.favourite_orders.ordered.order(:updated_at => :desc)
    render  partial: 'doctor/dashboards/dashboard_tabs/dispense_tab',locals: { to_be_dispensed: @to_be_dispensed, re_newed_dispensed: @re_newed_dispensed }
  end
  
  private
    
    def doctor_params
      params.require(:doctor).permit :first_name, :last_name, :type, :mobile_number,:email,:password,:password_confirmation,:designation,:about,:portfolio,:avatar,
                                      :theme_color,:sign_image,:DOB,home_address_attributes: [:address1,:address2,:city,:postal_code,:addr_type,:state_id,:country_id,:phone],
                                      office_address_attributes: [:address1,:address2,:city,:postal_code,:addr_type,:state_id,:country_id,:phone]
    end

    def dashboard_data current_object
      role = current_doctor.role - [""]
      @recent_orders = current_object.orders.where(:status => [0,1,2,3,4]).where("date(updated_at) > ?", 15.days.ago).order(:updated_at => :desc)
      @recent_orders = @recent_orders.map{|x| (x.contact_lens_orders.present? || x.eye_glass_orders.present?) ? x : nil}.compact
      @recent_renewed_orders = current_object.favourite_orders.where(:status => [0,1,2]).order(:updated_at => :desc)
      @to_be_dispensed = current_object.orders.ready_for_pick_up.where(:status => [0,1,2,3,4]).order(:updated_at => :desc)
      @follow_ups = current_object.orders.follow.order(:updated_at => :desc)
      @follow_list = FollowUp.where("due_date >= ? AND doctor_id IN (?)", DateTime.now,current_organisation.doctors.map(&:id))
      @renewed_follow_ups = current_object.favourite_orders.follow.order(:updated_at => :desc)
      @recent_exams = current_object.examinations.where(:status => [0,1,2,3,4]).order(:updated_at => :desc)
      
      @today_exam=current_object.bookings.where(start_time: Time.now.beginning_of_day..Time.now.end_of_day).where.not(joint_resource_id: nil).count
      @weekly_exam=current_object.bookings.where(start_time: (Time.now.to_date-8).beginning_of_day..(Time.now.to_date-1).end_of_day).count
      @coming_exam=current_object.bookings.where(start_time: Time.now.beginning_of_day..Time.now.end_of_day).where.not(joint_resource_id: nil).count
      @total_exam=current_object.bookings.count
      @comming_search = current_object.bookings.where(start_time: Time.now.beginning_of_day..Time.now.end_of_day).where.not(joint_resource_id: nil).ransack(params[:q])
      @comming_appointment=@comming_search.result.includes(:joint_resource,:exam_procedures,:doctor).paginate(:page => params[:upcoming_appointments_page], :per_page => 10).order(:start_time => :asc)
      @todays_search =current_object.bookings.with_deleted.where(start_time: Time.now.beginning_of_day..Time.now.end_of_day).where.not(joint_resource_id: nil).ransack(params[:q])
      @today_appointment_non_unique=@todays_search.result.includes(:joint_resource,:exam_procedures,:doctor).paginate(:page => params[:todays_appointments_page], :per_page => 20).order(:start_time => :asc)
      @today_appointment_unique=[]
      a = @today_appointment_non_unique.map(&:joint_resource_id).uniq
      a.each{|aa| @today_appointment_unique << @today_appointment_non_unique.where(joint_resource_id: aa).order(:start_time => :asc).last}
      @today_appointment = @today_appointment_unique.sort_by{|x| x.start_time}

      @total_orders=current_object.orders
      
      @today_orders=current_object.orders.where("updated_at::date=?", Date.today)
      @total_eye_glass_orders=current_object.eye_glass_orders
      @today_eye_glass_orders=current_object.eye_glass_orders.where("eye_glass_orders.updated_at::date=?", Date.today)
      @total_contact_lens_orders=current_object.contact_lens_orders
      @today_contact_lens_orders=current_object.contact_lens_orders.where("contact_lens_orders.updated_at::date=?", Date.today)
    end

    def dashboard_data_store store
      role = current_doctor.role - [""]
      @recent_orders = Order.joins(:doctor).where(doctors: {store_id: store.id}).where(:status => [0,1,2,3,4]).where("date(orders.updated_at) > ?", 15.days.ago).order(:updated_at => :desc)
      @recent_orders = @recent_orders.map{|x| (x.contact_lens_orders.present? || x.eye_glass_orders.present?) ? x : nil}.compact
      @recent_renewed_orders = FavouriteOrder.joins(:doctor).where(doctors: {store_id: store.id}).where(:status => [0,1,2]).order(:updated_at => :desc)
      @to_be_dispensed = Order.joins(:doctor).where(doctors: {store_id: store.id}).ready_for_pick_up.order(:updated_at => :desc)
      @follow_ups = Order.joins(:doctor).where(doctors: {store_id: store.id}).follow.order(:updated_at => :desc)
      @follow_list = FollowUp.where("due_date >= ? AND doctor_id IN (?)", DateTime.now,current_organisation.doctors.map(&:id))
      @renewed_follow_ups = FavouriteOrder.joins(:doctor).where(doctors: {store_id: store.id}).follow.order(:updated_at => :desc)
      @recent_exams = Examination.joins(:doctor).where(doctors: {store_id: store.id}).where(:status => [0,1,2,3,4]).order(:updated_at => :desc)
      
      @today_exam=store.bookings.where(start_time: Time.now.beginning_of_day..Time.now.end_of_day).where.not(joint_resource_id: nil).count
      @weekly_exam=store.bookings.where(start_time: (Time.now.to_date-8).beginning_of_day..(Time.now.to_date-1).end_of_day).count
      @coming_exam=store.bookings.where(start_time: Time.now.beginning_of_day..Time.now.end_of_day).where.not(joint_resource_id: nil).count
      @total_exam=store.bookings.count
      
      @comming_search = store.bookings.where(start_time: Time.now.beginning_of_day..Time.now.end_of_day).where.not(joint_resource_id: nil).ransack(params[:q])
      @comming_appointment=@comming_search.result.includes(:joint_resource,:exam_procedures,:doctor).paginate(:page => params[:upcoming_appointments_page], :per_page => 10).order(:start_time => :asc)
      @todays_search = store.bookings.with_deleted.where(start_time: Time.now.beginning_of_day..Time.now.end_of_day).where.not(joint_resource_id: nil).ransack(params[:q])
      @today_appointment_non_unique=@todays_search.result.includes(:joint_resource,:exam_procedures,:doctor).paginate(:page => params[:todays_appointments_page], :per_page => 20).order(:start_time => :asc)
      @today_appointment_unique=[]
      a = @today_appointment_non_unique.map(&:joint_resource_id).uniq
      a.each{|aa| @today_appointment_unique << @today_appointment_non_unique.where(joint_resource_id: aa).order(:start_time => :asc).last}
      @today_appointment = @today_appointment_unique.sort_by{|x| x.start_time}
      
      @total_orders=Order.joins(:doctor).where(doctors: {store_id: store.id})
      @today_orders=Order.joins(:doctor).where(doctors: {store_id: store.id}).where("orders.updated_at::date=?", Date.today)
      @total_eye_glass_orders=EyeGlassOrder.joins(:doctor).where(doctors: {store_id: store.id})
      @today_eye_glass_orders=EyeGlassOrder.joins(:doctor).where(doctors: {store_id: store.id}).where("eye_glass_orders.updated_at::date=?", Date.today)
      @total_contact_lens_orders=ContactLensOrder.joins(:doctor).where(doctors: {store_id: store.id})
      @today_contact_lens_orders=ContactLensOrder.joins(:doctor).where(doctors: {store_id: store.id}).where("contact_lens_orders.updated_at::date=?", Date.today)
    end

    def set_seo_attributes
      @seo = Seo.find_by view_name: 'doctor_dashboard'
      if @seo.present?
        set_meta_tags title: @seo.title,
                description: @seo.description,
                keywords: @seo.keywords
      end
    end

    def set_title_profile
      @patient = Patient.find_by(id: params[:patient_id]) unless @patient.present?
      if @patient.try(:birthday).present?
        title = @patient.try(:name) + ' ' + @patient.try(:birthday).to_s
      elsif current_doctor.store.description.present? && @patient.present?
        title = @patient.try(:name) + ' ' + current_doctor.try(:store).try(:description)
      else
        title = @patient.try(:name)
      end
      set_meta_tags title: title
    end

end
