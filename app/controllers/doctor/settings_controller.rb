##
# Doctor::SettingsController Use to manage Setting => Exam tab functionality
# Like we can create/remove records that we will use in exam form.

class Doctor::SettingsController < Doctor::BaseController
	authorize_resource :class => false

	def index
		@settings = current_organisation.setting
		@diseases_medical =  current_organisation.diseases.medical.order(:name)
		@diseases_oculus = current_organisation.diseases.oculus.order(:name)
		@diseases_allergy = current_organisation.diseases.allergies.order(:name)
		@diseases_surgery = current_organisation.diseases.surgeries.order(:name)
		@diseases_psychology = current_organisation.diseases.psychologies.order(:name)
		@procedures = current_organisation.exam_procedures.all.order(:position)
		@lenses = current_organisation.lens_types_prices.all.order(:brand_name)
		@impressions =  current_organisation.impressions.all.order(:name)
		@recommendations = current_organisation.doctor_recommendations.all.order(:position)
    @prescribing_medications = current_organisation.diseases.prescribing_medication.order(:name)
    @dosages = current_organisation.diseases.dosage.order(:name)
		@doctor = current_doctor
    @exam_cleaning_solution = current_organisation.exam_cleaning_solution.all.order(:name)
    @first_store = current_organisation.stores.first	
    @store_working_schedule = @first_store.present? ? set_working_schedule(@first_store) : "nothing"

		if params[:load_partial] == 'true'
			render partial: '/doctor/settings/exam_form'
		end
	end

  def exam_form_defaults
    @def_values = ExamFormDefault.where(:organisation_id => current_organisation).first
    unless @def_values.present?
      @def_values = ExamFormDefault.create(ctd_def: 'Ortho',ctn_def: '3XP', nra_def: '+2.50', pra_def: '-2.50', npc_def: 'TTN', notes_def: '', hpd_def: 'Ortho', hpn_def: '3XP', vpd_def: 'Ortho', vpn_def: 'Ortho', eoms_def: 'SAFE OD / OS', pupils_def: 'PERRL (-) APD', confrontational_def: 'FTFC OD / OS', color_vision_def: '8/8', steropsis_def: '',prelm_notes_def: '', tear_film_def: 'Clear OU', cornea_def: '+FL large demarcation stain, -stroma involvement', iris_def: 'Flat OU', bulbar_conj_def: 'Clear OU', palpebral_conj_def: 'Clear OU', sclera_def: 'Clear OU', angles_od_def: '', angles_os_def: '', lids_lashes_def: 'Mod MGD OU', ac_def: 'D/Q OU', lens_def: 'Clear OU', visual_field_def: '', od_def: '0.3', vessels_def: '2/3 OU', os_def: '0.3', macula_def: '(+) Foveal Reflex OU', post_pole_def: 'Clear OU', vitreous_def: 'Clear OU', periphery_def: 'Clear OU', reason_field_def: '', dilation_od_def: 'OD 90D Fundus/ 20D fundus lens 360 SD to Ora (No Breaks)', dilation_os: 'OS 90D Fundus/ 20D fundus lens 360 SD to Ora (No Breaks)',dilation_fundus_def: 'Patient refuses dilated fundus examination after explanation of tests',organisation_id: current_organisation.id)
    end
    render partial: '/doctor/settings/exam_form_defaults'
  end

  def set_preliminaries_defaults
    #setting Preliminaries fields default value from settings.
    @def_values = ExamFormDefault.where(:organisation_id => current_organisation).first
    @def_values.update_attributes(exam_form_prel_def_params)
    respond_to do |format|
      format.html {redirect_to settings_path}
      format.js {render template: "/doctor/settings/exam_form_defaults", locals: {:msg => "Exam form values has been successfully updated."}}
    end
  end

  def set_binocular_defaults
    #setting BinocularTesting fields default value from settings.
    @def_values = ExamFormDefault.where(:organisation_id => current_organisation).first
    @def_values.update_attributes(exam_form_binocular_def_params)
    respond_to do |format|
      format.html {redirect_to settings_path}
      format.js {render template: "/doctor/settings/exam_form_defaults", locals: {:msg => "Exam form values has been successfully updated."}}
    end
  end

  def set_external_defaults
    #setting External Exam fields default value from settings.
    @def_values = ExamFormDefault.where(:organisation_id => current_organisation).first
    @def_values.update_attributes(exam_form_external_def_params)
    respond_to do |format|
      format.html {redirect_to settings_path}
      format.js {render template: "/doctor/settings/exam_form_defaults", locals: {:msg => "Exam form values has been successfully updated."}}
    end
  end

  def set_internal_defaults
    #setting Internal Exam fields default value from settings.
    @def_values = ExamFormDefault.where(:organisation_id => current_organisation).first
    @def_values.update_attributes(exam_form_internal_def_params)
    respond_to do |format|
      format.html {redirect_to settings_path}
      format.js {render template: "/doctor/settings/exam_form_defaults", locals: {:msg => "Exam form values has been successfully updated."}}
    end
  end

	def update
		current_organisation.setting.update_attributes(params[:setting].permit!)
		redirect_to settings_path
	end

	def add_disease
		@new_disease = Disease.create(params[:diseases].values)
		@data = @new_disease.collect do |item|
      {
       "id" => item.id.to_s, 
       "label" => item.name,
       "type" => params[:type]
      }
    end
    respond_to do |format|
      format.html { redirect_to settings_path }
      format.js 
    end
	end	

	def delete_disease
		disease = current_organisation.diseases.find(params[:id])
		disease.destroy
		render :nothing => true, :status => 200, :content_type => 'text/html'
	end	

	def add_lens
    @lens_types_price = LensTypesPrice.create(params[:lenses].values)
    @data = @lens_types_price.collect do |item|
      { "id" => item.id.to_s, 
       "label" => item.brand_name,
       "lens_type"=> item.lens_type,
       "select_base_curve_options"=> item.base_curve.split(',').map{|x| "<option value='#{x}'>#{x}</option>"}.join(""),
       "select_diameter_options"=> item.diameters.split(',').map{|x| "<option value='#{x}'>#{x}</option>"}.join(""),
       "sphere" => item.sphere.split(',').map{|x| "<option value='#{x}'>#{x}</option>"}.join(""),
       "cylinder" => item.cylinder.split(',').map{|x| "<option value='#{x}'>#{x}</option>"}.join(""),
       "axis" => item.axis.split(',').map{|x| "<option value='#{x}'>#{x}</option>"}.join(""),
       "add" => item.add.split(',').map{|x| "<option value='#{x}'>#{x}</option>"}.join("")
      }
    end
    respond_to do |format|
      format.html { redirect_to settings_path }
      format.js 
    end
	end	

	def delete_lens
		disease = current_organisation.lens_types_prices.find(params[:id])
		disease.destroy
		render :nothing => true, :status => 200, :content_type => 'text/html'
	end

  def update_procedure_order
    changed = current_organisation.exam_procedures.where(:position => params[:start].to_i + 1).first
    pos1 = changed.position.to_i
    if(params[:start].to_i > params[:current].to_i)
      i = params[:start].to_i
      while(i >= params[:current].to_i + 1)
        temp = current_organisation.exam_procedures.where(:position => i).first
        if temp.present?
          pos2 = temp.position.to_i
          temp.position = pos1;
          temp.save!
          pos1 = pos2
        end
        i = i-1;
      end
    else
      i = pos1 + 1
      while(i <= (params[:current].to_i + 1))
        temp = current_organisation.exam_procedures.where(:position => i).first
        if temp.present?
          pos2 = temp.position.to_i
          temp.position = pos1;
          temp.save!
          pos1 = pos2
        end
        i = i+1;
      end
    end
    changed.position = pos1
    changed.save!
    render :nothing => true, :status => 200
  end

  def update_recommendation_order
  	changed = current_organisation.doctor_recommendations.where(:position => params[:start].to_i + 1).first
    pos1 = changed.position.to_i
    if(params[:start].to_i > params[:current].to_i)
      i = params[:start].to_i
      while(i >= params[:current].to_i + 1)
        temp = current_organisation.doctor_recommendations.where(:position => i).first
        if temp.present?
          pos2 = temp.position.to_i
          temp.position = pos1;
          temp.save!
          pos1 = pos2
        end
        i = i-1;
      end
    else
      i = pos1 + 1
      while(i <= (params[:current].to_i + 1))
        temp = current_organisation.doctor_recommendations.where(:position => i).first
        if temp.present?
          pos2 = temp.position.to_i
          temp.position = pos1;
          temp.save!
          pos1 = pos2
        end
        i = i+1;
      end
    end
    changed.position = pos1
    changed.save!
    render :nothing => true, :status => 200
  end

	def add_procedure
    count = current_organisation.try(:exam_procedures).order(:position).try(:last).try(:position)
    params[:procedures].values.each do |asd|
      count += 1
      asd["position"] = count
    end
		@new_procedure = current_organisation.exam_procedures.create(params[:procedures].values)
		@data = @new_procedure.collect do |item|
      {
        "id" => item.try(:id).to_s, 
       "label" => item.try(:name),
       "code" => item.try(:code), 
       "price" => item.try(:price),
       "value" => item.try(:name)
      }
    end
    respond_to do |format|
      format.html { redirect_to settings_path }
      format.js
    end
	end

	def delete_procedure
		procedure = current_organisation.exam_procedures.find(params[:id])
    pos = procedure.position.to_i + 1
    last_pos = current_organisation.exam_procedures.order(:position).last.position.to_i
    (pos..last_pos).each do |i|
      temp = current_organisation.exam_procedures.where(:position => i).first
      if temp.present?
        temp.position = temp.position.to_i - 1; 
        temp.save!
      end
    end
		procedure.destroy
		render :nothing => true, :status => 200, :content_type => 'text/html'
	end

	def add_impression
		@new_impression = Impression.create(params[:impressions].values)
    @data = @new_impression.collect do |item|
      {
        "id" => item.id.to_s, 
       "label" => item.name,
       "code" => item.code
      }
    end
    respond_to do |format|
      format.html { redirect_to settings_path }
      format.js 
    end
	end
  def add_exam_cleaning_solution
    @new_cleaning_solution = current_organisation.exam_cleaning_solution.create(params[:solutions].values)
    redirect_to settings_path
  end

  def delete_cleaning_solution
    exam_cleaning_solution = current_organisation.exam_cleaning_solution.find(params[:id])
    exam_cleaning_solution.destroy
    render :nothing => true, :status => 200, :content_type => 'text/html'  
  end

	def delete_impression
		impression = current_organisation.impressions.find(params[:id])
		impression.destroy
		render :nothing => true, :status => 200, :content_type => 'text/html'
	end

	def add_recommendation
		count = current_organisation.doctor_recommendations.order(:position).last.position
    params[:doctor_recommendation].values.each do |reco|
      count += 1
      reco["position"] = count
    end
		current_organisation.doctor_recommendations.create(params[:doctor_recommendation].values)
		redirect_to settings_path
	end

	def delete_recommendation
		doctor_recommendation = DoctorRecommendation.find(params[:id])
    rec = doctor_recommendation.position.to_i + 1
    last_rec = DoctorRecommendation.order(:position).last.position.to_i
    (rec..last_rec).each do |i|
      temp1= DoctorRecommendation.where(:position => i).first
      if temp1.present?
        temp1.position = temp1.position.to_i - 1;
        temp1.save!
      end
    end
		doctor_recommendation.destroy
		render :nothing => true, :status => 200, :content_type => 'text/html'
	end
	
	def edit_records
    @settings = current_organisation.setting
		if params[:form_type] == "disease"
			if params[:disease_type] == "medical"
				@diseases =  current_organisation.diseases.medical.order(:name)
			elsif params[:disease_type] == "oculus"
				@diseases = current_organisation.diseases.oculus.order(:name)
			elsif params[:disease_type] == "allergy"
				@diseases = current_organisation.diseases.allergies.order(:name)
			elsif params[:disease_type] == "surgery"
				@diseases = current_organisation.diseases.surgeries.order(:name)
			elsif params[:disease_type] == "prescribing_medication"
        @diseases =  current_organisation.diseases.prescribing_medication.order(:name)
      elsif params[:disease_type] == "dosage"
        @diseases =  current_organisation.diseases.dosage.order(:name)
      else 
				@diseases = current_organisation.diseases.psychologies.order(:name)
			end 
			@partial = "diseases_form"
    elsif params[:form_type] == "contact_lens"
      @lenses = current_organisation.lens_types_prices.all.order(:brand_name)
      @partial = "contact_lenses_form"
    elsif params[:form_type] == "procedure"
    	@procedures = current_organisation.exam_procedures.all
    	@partial = "procedures_form"
    elsif params[:form_type] == "recommendation"
    	@recommendations = current_organisation.doctor_recommendations.all
    	@partial = "recommendations_form"
    elsif params[:form_type] == "impression"
			@impressions =  current_organisation.impressions.all.order(:name)
    	@partial = "impressions_form"
    elsif params[:form_type] == "cleaning_solution"
      @exam_cleaning_solution =  current_organisation.exam_cleaning_solution.all.order(:name)
      @partial = "exam_cleaning_solutions_form"
    end
  end 

  def update_records
		if params[:form_type] == "disease"
    	Disease.update(params[:diseases].keys, params[:diseases].values)
    elsif params[:form_type] == "contact_lens"
    	LensTypesPrice.update(params[:lenses].keys, params[:lenses].values)
    elsif params[:form_type] == "procedure"
    	ExamProcedure.update(params[:procedures].keys, params[:procedures].values)
    elsif params[:form_type] == "impression"
    	Impression.update(params[:impressions].keys, params[:impressions].values)
    elsif params[:form_type] == "recommendation"
    	DoctorRecommendation.update(params[:doctor_recommendations].keys, params[:doctor_recommendations].values)
    elsif params[:form_type] == "cleaning_solution"
      ExamCleaningSolution.update(params[:solutions].keys, params[:solutions].values)
    end
    redirect_to :back
  end

  def set_default_expiry
    @settings = current_organisation.setting
    @settings.update_attributes!(default_expiry: params[:default_expiry_set][:default_expiry])
    redirect_to :back
  end

  def set_default_escribe_expiry
    @settings = current_organisation.setting
    @settings.update_attributes!(default_escribe_expiry_params)
    redirect_to :back
  end 

  def set_defaut_slot_time
    @settings = current_organisation.setting
    @settings.update_attributes!(set_slot_time_params)
    redirect_to :back
  end

	private

    def set_working_schedule(trackable)
      if trackable.try(:working_schedule).present?
        trackable.working_schedule
      else
        trackable.create_working_schedule
      end
    end
	  
		def lens_params
      params.require(:lens_types_price).permit :lens_type, :modality, :brand_name, :price, :diameters, :patient_pays, :base_curve => []
    end

    def default_expiry_params
      params.require(:default_expiry_set).permit :default_expiry
    end

    def default_escribe_expiry_params
      params.require(:default_escribe_expiry).permit :e_scribe_defexpirydate
    end

    def set_slot_time_params
      params.require(:set_calender_slot_time).permit :slot_time
    end
    
    def exam_form_prel_def_params
      params.require(:exam_form_preliminary_defaults).permit :eoms_def,:pupils_def,:steropsis_def,:confrontational_def,:color_vision_def,:prelm_notes_def
    end

    def exam_form_binocular_def_params
      params.require(:exam_form_binocular_defaults).permit :ctd_def,:ctn_def,:nra_def,:pra_def,:npc_def,:notes_def,:hpd_def,:hpn_def,:vpd_def,:vpn_def
    end

    def exam_form_external_def_params
      params.require(:exam_form_external_defaults).permit :tear_film_def,:cornea_def,:iris_def,:bulbar_conj_def,:palpebral_conj_def,:sclera_def,:angles_od_def,:angles_os_def,:lids_lashes_def,:ac_def,:lens_def,:visual_field_def, :cyclopelgic_text, :cyclopelgic1_text, :cyclopelgic2_text, :cyclopelgic3_text
    end

    def exam_form_internal_def_params
      params.require(:exam_form_internal_defaults).permit :od_def,:os_def,:post_pole_def,:periphery_def,:vitreous_def,:vessels_def,:macula_def,:reason_field_def,:dilation_od_def,:dilation_os,:dilation_fundus_def, :tropicomide, :phenylephrine, :fundus, :ophthalmoscopy, :drop3_text, :drop4_text
    end
end