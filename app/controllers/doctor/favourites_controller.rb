class Doctor::FavouritesController < Doctor::BaseController
  before_action :set_favourite_order, only: [:show,:checklist_update,:destroy,:status_update,:dispense]
  include OneClickAndSubscriptionsCommon
  include FavouritesCommon
  before_action :session_check, except: [:dispense ,:destroy,:checklist_update,:show,:calculate_order_total,:create_favourite,:get_1_click,:selected_patient_prescriptions,:email_invoice, :invoice]
  include ActionView::Helpers::NumberHelper

  def show
    @lens_order = @favourite_order.lens_prescription
    @transaction = @favourite_order.transactions.build
    @profile =  @favourite_order.joint_resource.patient.profile.present? ? @favourite_order.joint_resource.patient.profile.get_details : nil
  end

  def destroy
    if params[:remove_dispensed]
      @favourite_order.status = 'follow'
      @favourite_order.save
      AppMailer.send_dispensed_removal_to_patient(@favourite_order).deliver_later if params[:mail] && !(@favourite_order.try(:joint_resource).try(:patient).try(:email_not_supplied))
    elsif params[:remove_finalized]
      @favourite_order.status = 'dispensed'
      @favourite_order.save
      @resource = @favourite_order.joint_resource
      @follow_up = @resource.follow_ups.create!(:reason => "finalized order removed!",:due_date => Time.now,:doctor_id => current_doctor.id)
      @follow_up.destroy
    else
      AppMailer.send_order_cancellation_to_patient(@order).deliver_later if params[:mail] && !(@order.try(:joint_resource).try(:patient).try(:email_not_supplied))
      @favourite_order.destroy
    end
    redirect_to :back
  end
  
  def status_update
    @favourite_order.update_attributes!(favourite_order_params)
    @transaction = @favourite_order.transactions.build
    @profile =  @favourite_order.joint_resource.patient.profile.present? ? @favourite_order.joint_resource.patient.profile.get_details : nil
    respond_to do |format|
      format.js{render layout: false, content_type: 'text/javascript'}
    end and return
  end
   
  def checklist_update
    if @favourite_order.update_attributes!(favourite_order_params)
      render :json => {status: 'Success', msg: "Checklist Updated successfully!"}.to_json
    else
      render :json => {status: 'Error', msg: "Error in updating checklist"}.to_json
    end
  end

  def order_audits
    @order = current_organisation.favourite_orders.find(params[:id])
    respond_to do |format|
      format.js{ render :status => 200 }
    end
  end
  
  def invoice
    @order = current_organisation.favourite_orders.find(params[:id])
    @lens_order = @order.lens_prescription
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "invoice",
               layout: 'layouts/application.pdf.haml'
      end
    end
  end
  
  def dispense
    if ['finalized','ordered'].include? @favourite_order.status
      @favourite_order.update_attributes!(:status => :ready_for_pick_up,:doctor_id => current_doctor.try(:id))
      notification_message = AppConfig.sms_notification["order_ready"] % {patient_name: @favourite_order.joint_resource.name ,store_start_time: @favourite_order.doctor.store.working_schedule.get_schedule[Time.now.wday].first.first , store_close_time: @favourite_order.doctor.store.working_schedule.get_schedule[Time.now.wday].first.last, store_name: @favourite_order.doctor.store.description , store_address: "#{@favourite_order.doctor.store.office_address.address1}, #{@favourite_order.doctor.store.office_address.address2}, postal code #{@favourite_order.doctor.store.office_address.postal_code} " , store_phone_number: "#{number_to_phone(@favourite_order.doctor.store.phone_number.to_s[2..-1].to_i, area_code: true)}", profile_url: root_url(patient_email: @favourite_order.joint_resource.email, authentication_token: @favourite_order.joint_resource.patient.authentication_token)}
      AppMailer.send_renewed_order_dispensed_to_patient(@favourite_order).deliver_later unless @favourite_order.try(:joint_resource).try(:patient).try(:email_not_supplied)
      TwilioSmsWorker.perform_async({'src'=> current_organisation.twilio_number, 'dst' => @favourite_order.joint_resource.phone, 'text' => notification_message, 'notification_method'=> @favourite_order.joint_resource.patient.notification_method,'org_sid'=> current_organisation.twilio_acc_sid,'org_auth_token'=> current_organisation.twilio_auth_token})
    end
    redirect_to manage_patients_by_doctor_index_path
  end
  
  def email_invoice
    @order = current_organisation.favourite_orders.find(params[:id])
    InvoiceMailer.send_invoice_to_patient(@order.try(:joint_resource).try(:member),@order).deliver_later unless @order.try(:joint_resource).try(:patient).try(:email_not_supplied)
    respond_to do |format|
      format.json { render :json => {status: 'Success', msg: "Email has been sent successfully"}.to_json}
    end
  end
  
  private
  
    def session_check
      @patient = params[:patient_id].present? ? current_organisation.patients.find(params[:patient_id]) : current_patient
    end
  
    def set_favourite_order
      @favourite_order = current_organisation.favourite_orders.find(params[:id])
    end
  
end