##
# FamilyMembers concern have all functionality for this controller

class Doctor::FamilyMembersController <  Doctor::BaseController
	skip_before_action :verify_authenticity_token
	include FamilyMembers
	
end