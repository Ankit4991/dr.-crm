##
# Doctor::StoresController use to manage organisation Stores

class Doctor::StoresController <  Doctor::BaseController
  authorize_resource :class => false
  def index
    @store = Store.new
    @store.build_office_address
    @stores = current_organisation.stores 
    @doctor=Doctor.new
    colour = "##{'%06x' % (rand * 0xffffff)}"
    @doctor.theme_color = colour
    @store_doctors=current_organisation.stores.present? ? current_organisation.stores.first.try(:doctors) : "nothing"
  end

  def edit
    @store = current_organisation.stores.find(params[:id])      
  end

  def update
    begin
      @store = current_organisation.stores.find(params[:id])
      @store.update!(store_params)
      flash[:notice] = "Store has been successfuly updated!"
    rescue Exception=>e
      e.to_s.slice!('Validation failed: ') if e.to_s.include?('Validation failed: ')
      e.to_s.slice!('Logo file size ') if e.to_s.include?('Logo file size ')
      flash[:alert] = "#{e}, Try it again"
    end
    respond_to do |format|
      format.html {redirect_to :back}
    end
  end

  def create
    begin
      @store = Store.create!(store_params)
      flash[:notice] = "Store has been successfuly created!"
    rescue Exception=>e
      e.to_s.slice!('Validation failed: ') if e.to_s.include?('Validation failed: ')
      e.to_s.slice!('Logo file size ') if e.to_s.include?('Logo file size ')
      flash[:alert] = "#{e}, Try it again"
    end
    redirect_to :back
  end

  def destroy
    @booking = current_organisation.stores.find(params[:id])  
    @booking.destroy
    respond_to do |format|
      format.html {redirect_to :back}
    end
  end

  def manage_view
    if params[:view_type]=="staff"
      @view_type="staff"
      @view=current_organisation.doctors
    else
      @view_type="store"
      @view=current_organisation.stores
    end
  end
 
  private
    def store_params
      params.require(:store).permit!
    end

end
