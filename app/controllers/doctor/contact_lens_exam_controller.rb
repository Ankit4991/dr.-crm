class Doctor::ContactLensExamController < Doctor::BaseController
  def new
    @contact_lens_exam = ContactLensExam.new
    construct_float_dropdown
  end

  def create
    @contact_lens_exam = ContactLensExam.new(contact_lens_exam_params)
    respond_to do |format|
      if @contact_lens_exam.save
        return render :json => {:success => true}
      else
        return render :json => {:success => false, :message => @contact_lens_exam.errors.join(", ")}
      end
    end
  end

  private
  def construct_float_dropdown
    @float_dropdown = []
    $i = 32
    while $i <= 52
      @float_dropdown.push($i)
      $i += 0.25
    end
  end

  def contact_lens_exam_params
    params.require(:contact_lens_exam).permit(:name,
                                              :dob,
                                              :age,
                                              :od_d_h, 
                                              :od_mm_h, 
                                              :od_axis_h, 
                                              :os_d_h, 
                                              :os_d_h, 
                                              :os_mm_h, 
                                              :os_axis_h,
                                              :od_d_v, 
                                              :od_mm_v, 
                                              :od_axis_v, 
                                              :os_d_v, 
                                              :os_d_v, 
                                              :os_mm_v, 
                                              :os_axis_v)
  end 
end
