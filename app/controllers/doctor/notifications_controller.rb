##
# Doctor::NotificationsController use to show doctor notificataion

class Doctor::NotificationsController < Doctor::BaseController

  def index
    if params[:search].present?
      @notifications=Notification.select("notifications.*").joins("INNER JOIN doctors_notifications ON notifications.id=doctors_notifications.notification_id").where("doctors_notifications.doctor_id IN(?) and notifications.event_type IN ('ContactForm','Log')",current_organisation.doctors.map(&:id)).order("notifications.created_at desc").uniq

      @contact_form_notifications = @notifications.joins(:patient).where("lower(patients.first_name) LIKE ? OR lower(patients.last_name) LIKE ?", ("%#{params[:search]}%").downcase, ("%#{params[:search]}%").downcase).paginate(:page => params[:page], :per_page => 6)
    else  
      @booking_notifications=Notification.select("notifications.*").joins("INNER JOIN doctors_notifications ON notifications.id=doctors_notifications.notification_id").where("doctors_notifications.doctor_id IN(?) AND notifications.event_type= ?",current_organisation.doctors.map(&:id), 'Booking').order("notifications.created_at desc").uniq.paginate(:page => params[:page], :per_page => 10)

      @contact_form_notifications=Notification.select("notifications.*").joins("INNER JOIN doctors_notifications ON notifications.id=doctors_notifications.notification_id").where("doctors_notifications.doctor_id IN(?) and notifications.event_type IN ('ContactForm','Log')",current_organisation.doctors.map(&:id)).order("notifications.created_at desc").uniq.paginate(:page => params[:page], :per_page => 6)
    end 
  end

  def booking_notifications
    @booking_notifications=Notification.select("notifications.*").joins("INNER JOIN doctors_notifications ON notifications.id=doctors_notifications.notification_id").where("doctors_notifications.doctor_id IN(?) AND notifications.event_type= ?",current_organisation.doctors.map(&:id), 'Booking').order("notifications.created_at desc").uniq.paginate(:page => params[:page], :per_page => 10)    
  end

  def contact_form_notifications
    @contact_form_notifications=Notification.select("notifications.*").joins("INNER JOIN doctors_notifications ON notifications.id=doctors_notifications.notification_id").where("doctors_notifications.doctor_id IN(?) and notifications.event_type IN ('ContactForm','Log')",current_organisation.doctors.map(&:id)).order("notifications.created_at desc").uniq.paginate(:page => params[:page], :per_page => 6)
  end
end