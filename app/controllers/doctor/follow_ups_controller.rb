class Doctor::FollowUpsController <  Doctor::BaseController
  authorize_resource :class => false
  before_action :get_resource, only: [:index,:new]
  
  def index
    @follow_ups = @joint_resource.follow_ups.with_deleted.order(:created_at => :asc)
    respond_to do |format|
      format.json
    end
  end
  
  def new
    if !(@joint_resource.follow_ups.present?) || (@joint_resource.follow_ups.order(:due_date).last.due_date < Date.today) || @joint_resource.follow_ups.order(:due_date).last.is_followed?
      @follow_up = @joint_resource.follow_ups.build
      respond_to do |format|
        format.json
      end
    else
      respond_to do |format|
        format.js { render :json => {:status => 'Error', :msg => "Pending Follow Up is allready exists" }}
      end
    end
  end
  
  def create
    last_follow_up = JointResource.find(params[:follow_up][:joint_resource_id]).follow_ups.with_deleted.order(:created_at => :asc).last
    flag = false
    if last_follow_up.present?
      flag = true if last_follow_up.reason == params[:follow_up][:reason] && last_follow_up.created_at > (Time.now - 1.minutes)
    end
    unless flag
      begin
        @jres = JointResource.find(params[:follow_up][:joint_resource_id])
        @latest_order = @jres.orders.order(:updated_at).last if @jres.present?
        @contact_lens_order = @latest_order.contact_lens_orders.order(:updated_at).last if @latest_order.present?
        @contact_lens_order.update(:hold => !@contact_lens_order.hold,doctor: current_doctor) if @contact_lens_order.present?
        @latest_order.update(:status => "follow") if @latest_order.present?
        @follow_up = FollowUp.new(follow_up_params)
        if @follow_up.save
          respond_to do |format|
           format.js { render :json => {:message => "ok"}}
          end
        else
          respond_to do |format|
            format.js { render :json => { :error => @follow_up.errors.full_messages.to_sentence }, :status => :unprocessable_entity}
          end
        end
      rescue Exception => e
        respond_to do |format|
        format.js { render :json => {:message => "Something went wrong due to #{e}"}}
        end
      end
    else
      puts "================================================================================="
      puts "===================================FAIL ADD======================================"
      puts "================================================================================="
      respond_to do |format|
       format.js { render :json => {:status => 200 ,:message => "ok"}}
      end
    end
  end
  
  def edit
    @follow_up = FollowUp.find(params[:id])
  end
  
  def update
    @follow_up = FollowUp.find(params[:id])
    @follow_up.update_attributes!(:outcome => params[:value])
    respond_to do |format|
      format.json { render :json => {:message => "ok",:id => @follow_up.joint_resource.id }}
    end
  end
  
  def show
    @follow_up = FollowUp.find(params[:id])
  end
  
  def contact_days
    respond_to do |format|
      format.json { render :json => { :day => params[:due_date].present? ? (Date.parse(params[:due_date]) - Date.today).to_i : 0} }
    end
  end
  
  def destroy
     @follow_up = FollowUp.find(params[:id])
     @patient = @follow_up.patient
     @follow_up.destroy
     respond_to do |format|
       format.json { render :json => {:message => "ok",:id => @patient.id }}
     end
  end
  
  private
  
  def follow_up_params
    params.require(:follow_up).permit(:reason,:due_date,:joint_resource_id,:doctor_id)
  end
  
  def get_resource
    @joint_resource= JointResource.find_by(id: params[:id]) #joint resource id passes in params
  end
end
