##
# In Doctor namespace we are using FullCalendar plugin for Book an appointment. 
# This cotroller will handle all calls that comes from Full-Calendar

class Doctor::BookingsController < Doctor::BaseController
  include ActionView::Helpers::NumberHelper
  respond_to :html, :xml, :json
  authorize_resource :class => false
  before_action :set_seo_attributes
  before_action :find_doctor, only: [:new,:create, :calendar]

  def index
    if (current_doctor.role & ["super_administrator", "administrator", "staff"]).any?
      if params[:store_id].present?
        store = current_organisation.stores.find_by(id: params[:store_id])
        @bookings = store.bookings.order(:start_time)
      else
        @bookings = current_organisation.bookings.order(:start_time)
      end
    else
      @bookings =  current_organisation.bookings.where("doctor_id = ? AND end_time >= ?", current_doctor.id, Time.now).order(:start_time)
    end
    respond_with @bookings
  end

  def new
    @resources = current_organisation.stores
    @resource = @resources.first 
    @view = "new"
    @booking = Booking.new()
    @doctor = @doctor.present? ? @doctor : (current_doctor.present? ? current_doctor : Doctor.first)
    @fetch_slot_time = current_organisation.setting.slot_time.present? ? current_organisation.setting.slot_time : 20
  end

  def create
    if params[:patient][:email_not_supplied] == "1"
      time = Time.now.to_i
      email = params[:patient][:first_name] + time.to_s + '@' + params[:patient][:last_name] + '.' + params[:patient][:last_name]
      params[:patient][:email] = email
    end
    if params[:patient_existence] == "true"
      begin
        create_booking Patient.new(params[:patient].permit!)
        flash[:notice] = "Booking has been created succesfully."
        flash[:status]="Success"
      rescue Exception=> e    
        flash[:notice] = "Something went wrong due to #{e.message}"
        flash[:status]="Error"
      end
    else
      patient = current_organisation.patients.find_by_email(params[:patient][:email])
      unless patient.present?
        begin
          params[:patient].merge!(password: Devise.friendly_token.first(8))
          patient = Patient.new(params[:patient].permit!)
          patient.build_joint_resource
          patient.unhashed_password = params[:patient][:password]
          patient.skip_confirmation!
          patient.save!
          params[:booking].merge!(joint_resource_id: patient.joint_resource.id)
          create_booking(patient)
          flash[:notice] = "Booking has been created succesfully."
          flash[:status]="Success"
        rescue Exception=> e    
          flash[:notice] = "Something went wrong due to #{e.message}"
          flash[:status]="Error"
        end
      else
        if params[:create_family] == "Yes"
          @family_member = patient.family_members.new
          @family_member.first_name = params[:patient][:first_name]
          @family_member.last_name = params[:patient][:last_name]
          @family_member.birthday = params[:patient][:birthday]
          @family_member.phone = params[:patient][:phone]
          @family_member.vision_insurance_id = params[:patient][:vision_insurance_id]
          @family_member.vision_insurance_id = params[:patient][:medical_insurance_id]
          @family_member.organisation_id = patient.organisation_id
          @family_member.relation = params[:relation]
          @family_member.save!
          params[:booking].merge!(joint_resource_id: @family_member.joint_resource.id)
          create_booking Patient.new(params[:patient].permit!)
          flash[:notice] = "Booking has been created succesfully."
          flash[:status]="Success"
        else
          flash[:notice] = "Patient email is wrong."
          flash[:status]="Error"
        end
      end
    end
    render :json => {status: flash[:status], msg: flash[:notice]}.to_json
  end
  
  def destroy
    begin
      @booking = current_organisation.bookings.find(params[:id])
      @booking.destroyer_type = "Doctor"
      @booking.destroyer_id = current_doctor.id
      @examinations = @booking.examination 
      patient = @booking.joint_resource.patient
      @booking.destroy!
      if params[:exams_delete] && @examinations != nil
        @examinations.destroy!
      end
      if params[:notification]=='true' && @booking.joint_resource.present?
        AppMailer.send_booking_cancellation_to_patient(@booking.id).deliver_later unless @booking.try(:joint_resource).try(:patient).try(:email_not_supplied)
        if patient.present?
          one_click_url=Rails.application.routes.url_helpers.root_url(host: CrmDatabase::Application.config.action_mailer.default_url_options[:host], sudomain: patient.try(:subdomain),  patient_email: patient.try(:email), authentication_token: patient.try(:authentication_token), subdomain: patient.try(:subdomain))
          one_click_url.gsub!(patient.try(:subdomain), patient.try(:subdomain) + ".dev") if Rails.env.include?('staging') && patient.try(:subdomain) != 'dev'
          one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url      
          notification_message = AppConfig.sms_notification["cancel_appointment"] % {patient_name: @booking.joint_resource.try(:name), doctor_name: @booking.doctor.name, appointment_time: @booking.start_time.strftime('%m/%d/%Y %I:%M%P'), one_click_login: one_click_url, store_name: @booking.store.description , store_address: "#{@booking.store.try(:office_address).try(:address1)}, #{@booking.store.try(:office_address).try(:address2)}, postal code #{@booking.store.try(:office_address).try(:postal_code)}", store_phone_number: "#{number_to_phone(@booking.store.phone_number.to_s[2..-1].to_i, area_code: true)}", store_office_homepage: "#{@booking.store.url}"}
          TwilioSmsWorker.perform_async({'src'=> @booking.organisation.twilio_number, 'dst' => patient.phone, 'text' => notification_message, 'trackable_id'=> @booking.id, 'trackable_type'=> 'Booking', 'notification_method'=> patient.notification_method,'org_sid'=> @booking.organisation.twilio_acc_sid,'org_auth_token'=> @booking.organisation.twilio_auth_token})
        end
      end 
      flash[:status]="Success"
      flash[:notice] = "Booking: #{@booking.start_time.strftime('%b %e, %Y %H:%M %p')} to #{@booking.end_time.strftime('%b %e, %Y %H:%M%p')} deleted"
    rescue Exception=> e    
      flash[:notice] = "Something went wrong due to #{e.message}"
      flash[:status]="Error"
    end
    respond_to do |format|
      format.html {redirect_to :back}
      format.json { render :json => {status: flash[:status], msg: flash[:notice]}.to_json }
    end
  end

  def edit
    @booking = current_organisation.bookings.find(params[:id])
  end

  def update
    begin
      unless params[:id] == "undefined"
        if params[:booking][:appointment_status] == "other"
          params[:booking][:appointment_status] = params[:other_status] if params[:other_status] != ""
        end
        @booking = current_organisation.bookings.find(params[:id])
        # Below, future_booking_status_set variable used to set future bookings status to pending if their date is > Date.Today
        future_booking_status_set = DateTime.parse(params[:booking][:start_time]) if params[:booking][:start_time].present?
        if params[:booking][:start_time].present? && future_booking_status_set > Date.today 
          @booking.status = 0
          @booking.check_in = nil
          @booking.update!(params[:booking].permit(:doctor_id, :start_time, :length, :comment, :status, :check_in, :appointment_status, :exam_procedure_ids =>[]))
        else
          @booking.update!(params[:booking].permit(:doctor_id, :start_time, :length, :comment, :appointment_status, :exam_procedure_ids =>[])) 
        end
      end
      flash[:notice] = 'Your booking was updated succesfully'
      flash[:status]="Success"
    rescue Exception=> e    
      flash[:notice] = "Something went wrong due to #{e.message}"
      flash[:status]="Error"
    end 
    respond_to do |format|
      format.html {redirect_to :back}
      format.json { render :json => {status: flash[:status], msg: flash[:notice]}.to_json }
      format.js
    end  
  end

  def multiselect_procedure_select
    @procedures = current_organisation.exam_procedures.all.order(:position)
    json_data = @procedures.collect do |item| 
      { "id" => item.id.to_s, 
       "label" => item.name  
      }
    end
    render json: json_data
  end
  
  def vision_insurance_select
    @vision_insurances = current_organisation.try(:vision_insurances)
    json_data = @vision_insurances.collect do |item|
      { "id" => item.id.to_s,
        "label" => item.name
      }
    end
    render json: json_data
  end

  def medical_insurance_select
    @medical_insurances = current_organisation.try(:medical_insurances)
    if current_organisation.setting.flag_for_medical
      json_data = @medical_insurances.collect do |item|
        { "id" => item.id.to_s,
          "label" => item.name
        }
      end
    else
      json_data = {
        "show" => false
      } 
    end  
    render json: json_data
  end

  def doctor_list_select
    @store = Store.find(params[:store_id])
    @doctors = @store.doctors.doctors_with_role_doctor
    json_data = @doctors.collect do |item|
      { "id" => item.id.to_s,
        "label" => item.name
      }
    end
    render json: json_data
  end
  
  def calendar
    if params[:doctor_id] == "first"
      @bookings = current_organisation.bookings.where("start_time < ? and start_time > ? and store_id = ?", Time.at(params[:end].to_i), Time.at(params[:start].to_i), params[:store_id]).order(:start_time)
    else
      @bookings = current_organisation.bookings.where("start_time < ? and start_time > ? and doctor_id = ? AND store_id = ?", Time.at(params[:end].to_i), Time.at(params[:start].to_i), @doctor.id, params[:store_id]).order(:start_time)
    end
    respond_with @bookings
  end

  # custom_calendar.js get all basic data for calendar from this Action. 
  # On every Calender changes call comes at this action and set basic values.
  def calendar_basic_requirement
    @holidays_array = []
    store=current_organisation.stores.find_by(id: params[:store_id])
    store_holidays = store.holidays
    doctor_schedule = []
    doctor_off_days = []
    if params[:view] == "new" && params[:doctor_id] != "first"
      @doctor = current_organisation.doctors.find_by(id: params[:doctor_id] )
      doctor_schedule = @doctor.try(:working_schedule).try(:get_schedule)
      doctor_off_days = @doctor.try(:working_schedule).try(:off_days)
      doctor_leaves = @doctor.present? ? @doctor.leaves : []
      @holidays = (doctor_leaves + store_holidays)
      @holidays.each_with_index do |leave,index|
        if (leave.date.present? && leave.end_date.present?) && leave.date.to_date != leave.end_date.to_date
          leave.date.to_date.upto(leave.end_date.to_date) { |d|
            @holidays_array << [d.to_date.strftime("%Y-%m-%d"), leave.reason]
          }
        else  
          @holidays_array << [leave.date.strftime("%Y-%m-%d"), leave.reason]
        end
      end
    end
    render :json => {doctor_schedule: doctor_schedule, holidays_array: @holidays_array, doctor_off_days: doctor_off_days}   
  end


  #------------ new booking along with new exams & order log action for doctor--------
  def order_and_exam
    begin
      params[:booking].nil? ? (return render :nothing => true) :(@patient = JointResource.find(params[:booking][:joint_resource_id]).member)
      if @patient.present?
        @booking = current_organisation.bookings.new(booking_params)
        if !@patient.examinations.present? || @patient.examinations.last.status == "finalized"
          examination_and_order
          flash[:notice] = 'Your booking was created successfully'
          flash[:status]="success"
          redirect_to edit_examination_path(@examination.id)
        else
          flash[:alert] = "Last exam has not been finalized yet! Please finalize previous exam, and then proceed to create a new exam."
          redirect_to :back
        end
      end
    rescue Exception => e
      flash[:notice] = "Something went wrong due to #{e} !!"
      flash[:status]="error"
      redirect_to :back
    end
  end

  def get_view
    # ["all", "store", "new"]
    @view = params[:calendar_view]
    if @view == "store" || @view == "new"
      @resources = current_organisation.stores
      @resource = params[:store_id].present? ? @resources.where(id: params[:store_id]).first : @resources.first
    else 
      @resource = current_organisation
    end
    render partial: 'calendar_view'
  end

  def check_in
    begin
      @current_booking = Booking.find(params[:id])
      @current_booking.update_columns(:check_in => Time.now )
      if @current_booking.check_in.between?(@current_booking.start_time,@current_booking.start_time+15.minutes)
        @current_booking.update_columns(:status => 1 )
      elsif @current_booking.check_in.between?(@current_booking.start_time+15.minutes,@current_booking.end_time+6.hours)
        @current_booking.update_columns(:status => 2 )
      elsif @current_booking.check_in.between?(@current_booking.end_time-6.hours,@current_booking.start_time)
        @current_booking.update_columns(:status => 3 )
      else 
        @current_booking.update_columns(:status => 6 )
      end
      flash[:notice] = "Patient is Checked In successfully"
      flash[:status] = "Success"
    rescue Exception => e
      puts "------------------------------------------------------------------------------------------------------"
      puts "Something went wrong due to #{e}"
      puts "------------------------------------------------------------------------------------------------------"
      flash[:notice] = "Something went wrong due to #{e} !!"
      flash[:status] = "Error"
    end
    respond_to do |format|
      format.html {redirect_to :root}
      format.json { render :json => {status: flash[:status], msg: flash[:notice]}.to_json }
    end
  end

  def doctor_list
    @store = Store.find(params[:store_id])
    render  partial: "doctor_list", :locals => {resource: @store}
  end

  def restore_booking
    @booking = Booking.only_deleted.find(params[:id])
    @booking.restore
    @booking.update(:status => 'pending' )
    redirect_to :back
  end

  private
    def booking_params
       params.require(:booking).permit(:doctor_id,:store_id, :joint_resource_id, :start_time, :end_time, :comment, :appointment_status, :exam_procedure_ids=> [])
    end

    def examination_and_order
      ActiveRecord::Base.transaction do
        @booking.save!
        @today_exam = Examination.where(:joint_resource_id => params[:booking][:joint_resource_id]).map(&:exam_date)
        unless @today_exam.include?(Date.today)  
          @last_exam = @booking.joint_resource.examinations.last
          @examination = Examination.new(booking_id: @booking.id, joint_resource_id: @booking.joint_resource_id, doctor_id: @booking.doctor.id, organisation_id: @booking.organisation_id, status: :pending)
          @examination.build_social_history
          @last_exam.present? ? (@examination.build_binocular_testing(@last_exam.binocular_testing.attributes.except("id","examination_id", "created_at", "updated_at"))): @examination.build_binocular_testing  
          @last_exam.present? ? (@examination.build_primlinary_exam(@last_exam.primlinary_exam.attributes.except("id","examination_id", "created_at", "updated_at"))) : @examination.build_primlinary_exam
          @examination.visual_acuities.build(using_glasses: false)
          @examination.visual_acuities.build(using_glasses: true) 
          @last_exam.present? ? (@examination.glasses.build(@last_exam.glasses.last.attributes.except("id","visual_acuity_id","created_at","updated_at","examination_id","eye_glass_order_id","organisation_id","joint_resource_id","selected_finalize"))) : @examination.glasses.build
          @examination.eye_measurements.build(measurement_type: "subjective", default_refraction: true)
          @examination.eye_measurements.build(measurement_type: "objective", default_refraction: true)
          @examination.eye_measurements.build(measurement_type: "bincular_balance", default_refraction: true)
          @examination.eye_measurements.build(measurement_type: "cyclopelgic", default_refraction: true)
          @examination.build_contact_lens_exam.lenses.build(lens_type: "current_lens")
          @last_exam.present? ? (@examination.build_external_exam(@last_exam.external_exam.attributes.except("updated_at","id","examination_id","created_at"))) :  @examination.build_external_exam
          @last_exam.present? ? (@examination.build_internal_exam(@last_exam.internal_exam.attributes.except("updated_at","id","examination_id","created_at"))) :  @examination.build_internal_exam
          @examination.save!
          @booking.exam_procedure_ids.each do |exam_procedure_id|
            @examination.procedure_adjustments.build(exam_procedure_id: exam_procedure_id) 
          end
          if params[:only_exam] == "false"
            @order = @examination.create_order(examination_id: @examination.id,organisation_id: @examination.organisation_id,doctor_id: @booking.doctor.id,joint_resource_id: @examination.joint_resource_id, status: :pending)
          end
        end
        @patient.update(:vision_insurance_id => params[:vision_insurance_id])
      end
    end
    
    def find_doctor
      doctor_id= params[:doctor_id] || (params[:booking].present? ? params[:booking][:doctor_id] : nil)
      if doctor_id.present?
        @doctor = current_organisation.doctors.find_by_id(doctor_id)
      end
    end
    

    def create_booking patient
      @booking = current_organisation.bookings.new(params[:booking].permit(:doctor_id,:store_id, :joint_resource_id, :start_time, :end_time, :comment, :appointment_status, :exam_procedure_ids=> []))
      @booking.save!
      AppMailer.send_survey_link(patient).deliver_later(wait_until: @booking.end_time + 6.hours) if patient.survey_token.present? && !(patient.email_not_supplied)
    end

    def set_seo_attributes
      @seo = Seo.find_by view_name: 'book_appointment'
      if @seo.present?
        set_meta_tags title: @seo.title,
                description: @seo.description,
                keywords: @seo.keywords
      end
    end

end
