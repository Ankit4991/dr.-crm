class Doctor::ContactsController < Doctor::BaseController


	def index
		if params[:search].present?
			q = "%#{params[:search].try(:downcase)}%"
	    	@contacts = Contact.where("lower(company_name) LIKE ?", q).order('company_name ASC')
	  	else
	  		@contacts  = Contact.all.order('company_name ASC')
	  	end
	  	if @contacts.present?
			@contact = @contacts.first
		end
		respond_to do |format| 
			format.html    { render :index }
			format.js  
		end	  
	end

	def new
		@contact = Contact.new
		@contact.build_contact_address
	end

	def create
		begin
			if params[:contact].present?
				params[:contact][:phone_number] = params[:phone_number]
				@contact = Contact.new(params.require(:contact).permit!)
				if @contact.save!
					flash[:status] = "Success"
      		flash[:notice] = "Created successfully!"   
				end
			end			
		rescue Exception => e
			flash[:status] = 'Error'
      flash[:notice] = "Error due to #{e.message}"
		end
		respond_to do |format|
      format.html {redirect_to :back}
      format.json { render :json => {status: flash[:status], msg: flash[:notice]}.to_json }
    end
	end

	def edit
		@contact = Contact.find(params[:id])
	end

	def update
		begin
      @contact = Contact.find(params[:id])
			params[:contact][:phone_number] = params[:phone_number]
      @contact.update(params.require(:contact).permit!)
      flash[:status] = "Success"
      flash[:notice] = "successfully updated"
    rescue Exception=> e
      flash[:status] = 'Error'
      flash[:notice] = "Error due to #{e.message}"
    end
    respond_to do |format|
      format.html {redirect_to "/contacts"}
      format.json { render :json => {status: flash[:status], msg: flash[:notice]}.to_json }
    end
	end


	def destroy
    @contact = Contact.find(params[:id]) 
    if @contact.destroy
    	flash[:status] = "Success"
      flash[:notice] = "Deleted successfully!"
    end    
    respond_to do |format|
      format.html {redirect_to :back}
      format.json { render :json => {status: flash[:status], msg: flash[:notice]}.to_json }
    end
  end


end