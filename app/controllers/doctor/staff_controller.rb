##
# Doctor::StaffController use to manage Staff.

class Doctor::StaffController <  Doctor::BaseController
  authorize_resource :class => false
  skip_before_filter :verify_authenticity_token
  def index
    @store=Store.find(params[:store_id])
    @store_doctors=@store.doctors
  end

  def create
    begin
      params[:doctor][:role].delete("")
      params[:doctor].merge!(password: Devise.friendly_token.first(8))
      @doctor=Doctor.new(params.require(:doctor).permit!)
      if @doctor.bgurl.nil?
        @doctor.bgurl = "/images/Eye_Doctor.jpg"
      end
      @doctor.unhashed_password = params[:doctor][:password]
      @doctor.save!
      flash[:status] = "Success"
      flash[:notice] = "Created successfully!"   
    rescue Exception=> e
      flash[:status] = 'Error'
      flash[:notice] = "Error due to #{e.message}"
    end   
    render :json => {status: flash[:status], msg: flash[:notice]}.to_json
  end

  def edit
    begin
      @doctor=Doctor.find(params[:id])
      @super_admin_count = 0
      @admin_count = 0
      if @doctor.role.include?("administrator")
        @admin_count = current_organisation.doctors.doctors_with_role_admin.count
      end
      if @doctor.role.include?("super_administrator")
        @super_admin_count = current_organisation.doctors.doctors_with_role_super_admin.count
      end
      flash[:status] = "Success"
      flash[:notice] = "staff account successfully updated"
    rescue Exception=> e
      flash[:status] = 'Error'
      flash[:notice] = "Error due to #{e.message}"
    end
  end
  
  def update
    begin
      Doctor.find(params[:id]).update!(params.require(:doctor).permit!)
      redirect_to :back
    rescue Exception=> e
      render :json => {status: 'Error', msg: "Error due to #{e.message}", partial_tab: params[:submit]}.to_json
    end
  end

  def destroy
    begin
      doctor = current_organisation.doctors.find(params[:id])
      doctor.destroy
      flash[:status]="Success"
      flash[:notice] = "Staff Member deleted Successfully"
    rescue Exception=> e    
      flash[:notice] = "Something went wrong due to #{e.message}"
      flash[:status]="Error"
    end
    respond_to do |format|
      format.html {redirect_to :back}
      format.json { render :json => {status: flash[:status], msg: flash[:notice]}.to_json }
    end
  end

end