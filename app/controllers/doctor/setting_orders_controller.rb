##
# Doctor::SettingOrdersController Use to manage Setting => Order tab functionality
# Like we can create/remove records that we will use in Order form

class Doctor::SettingOrdersController < Doctor::BaseController
  authorize_resource :class => false

  def index
    @fetch_setting_orders_id = current_organisation.setting.id   
    @eyeglass_lens_types = current_organisation.eyeglass_types_prices.all
    @eyeglass_lens_materials = current_organisation.lens_materials.all
    @eyeglass_addons = current_organisation.glass_addons_prices.all
    @eyeglass_frames = current_organisation.glass_frames_prices.all

    @doctor = current_doctor    
    render partial: "index"
  end

  def update
    @setting_fields_update = current_organisation.setting
    @setting_fields_update.update!(control_reward_on_off_params)
    redirect_to settings_path
  end
  
  def add_eyeglass_lens_type
    EyeglassTypesPrice.create(params[:eyeglass_types].values)
    redirect_to settings_path
  end

  def delete_eyeglass_lens_type
    eyeglass_type = current_organisation.eyeglass_types_prices.find(params[:id])
    eyeglass_type.destroy
    render :nothing => true, :status => 200, :content_type => 'text/html'
  end

  def add_eyeglass_lens_material
    LensMaterial.create(params[:lens_materials].values)
    redirect_to settings_path
  end

  def delete_eyeglass_lens_material
    eyeglass_material = current_organisation.lens_materials.find(params[:id])
    eyeglass_material.destroy
    render :nothing => true, :status => 200, :content_type => 'text/html'
  end

  def add_eyeglass_addon
    GlassAddonsPrice.create(params[:eyeglass_addons].values)
    redirect_to settings_path
  end

  def delete_eyeglass_addon
    eyeglass_addon = current_organisation.glass_addons_prices.find(params[:id])
    eyeglass_addon.destroy
    render :nothing => true, :status => 200, :content_type => 'text/html'
  end

  def add_eyeglass_frame
    GlassFramesPrice.create(params[:eyeglass_frames].values)
    redirect_to settings_path
  end

  def delete_eyeglass_frame
    eyeglass_addon = current_organisation.glass_frames_prices.find(params[:id])
    eyeglass_addon.destroy
    render :nothing => true, :status => 200, :content_type => 'text/html'
  end

  def edit_records
    if params[:form_type] == "eyeglass_lens_type"      
      @eyeglass_lens_types = current_organisation.eyeglass_types_prices.all
      @partial = "eyeglass_lens_types_form"
    elsif params[:form_type] == "eyeglass_lens_material"
      @eyeglass_lens_materials = current_organisation.lens_materials.all
      @partial = "eyeglass_lens_materials_form"
    elsif params[:form_type] == "eyeglass_addon"
      @eyeglass_addons = current_organisation.glass_addons_prices.all
      @partial = "eyeglass_addons_form"
    elsif params[:form_type] == "eyeglass_frame"
      @eyeglass_frames = current_organisation.glass_frames_prices.all
      @partial = "eyeglass_frames_form"
    end
  end 

  def update_records
    if params[:form_type] == "eyeglass_lens_type"
      current_organisation.eyeglass_types_prices.update(params[:lens_types].keys, params[:lens_types].values)
    elsif params[:form_type] == "eyeglass_lens_material"
      current_organisation.lens_materials.update(params[:lens_materials].keys, params[:lens_materials].values)
    elsif params[:form_type] == "eyeglass_addon"
      current_organisation.glass_addons_prices.update(params[:addons].keys, params[:addons].values)
    elsif params[:form_type] == "eyeglass_frame"
      current_organisation.glass_frames_prices.update(params[:frames].keys, params[:frames].values)
    end
    redirect_to :back
  end 

  private

    def control_reward_on_off_params
      params.require(:control_reward_on_off).permit(:flag_for_reward,:reward_percent_used)
    end
end