class Doctor::ManagePatientsController < Doctor::BaseController
  authorize_resource :class => false
  
  def index
    @pending_appoints = appointments_for_the_day
    @unfinished_exams = current_organisation.examinations.unfexam.order(:updated_at => :desc)
    @unfinished_orders = current_organisation.orders.unforder.order(:updated_at => :desc)
    @to_be_ordered = current_organisation.orders.ordered.order(:updated_at => :desc)
    @re_newed_ordered = current_organisation.favourite_orders.ordered.order(:updated_at => :desc)
    @to_be_dispensed = current_organisation.orders.ready_for_pick_up.order(:updated_at => :desc)
    @re_newed_dispensed = current_organisation.favourite_orders.ready_for_pick_up.order(:updated_at => :desc)
    @follow_ups = current_organisation.orders.follow.order(:updated_at => :desc)
    @follow_list = FollowUp.where("due_date >= ?", DateTime.now)
    @renewed_follow_ups = current_organisation.favourite_orders.follow.order(:updated_at => :desc)
    @insurance_balance = current_organisation.orders.where(remove_from_insurance_due: false)
  end

  def create
    Patient.create!(patient_params)
    redirect_to action: :index
  end

  def new
    @patient = Patient.new
  end
  
  # def exam_procedures
  #   if params[:exam_id].present?
  #     @exam = current_organisation.examinations.find(params[:exam_id])
  #     @exam_procedures = @exam.exam_procedures
  #     @ids = @exam_procedures.map(&:id) if @exam_procedures.present?
  #   else
  #     @order = current_organisation.orders.find(params[:id])
  #     @order_procedures = @order.procedures
  #     @ids = @order_procedures.map(&:id) if @order_procedures.present?
  #   end
  #   @exam_prods = ExamProcedure.all.select(:id,:name).map{ |procedure| {id: procedure.id,text: procedure.name} }.to_json
  #   respond_to do |format|
  #     format.json
  #   end
  # end
  
  # def exam_procedure_update
  #   if params[:exam_id].present?
  #     @exam = current_organisation.examinations.find(params[:exam_id])
  #     @exam.update_attributes!(:exam_procedure_ids => params[:value])
  #   else
  #     @order = current_organisation.orders.find(params[:id])
  #     @order.update_attributes!(:procedure_ids => params[:value])
  #   end
  #   respond_to do |format|
  #     format.json{ render :json => {:message => "Successfully updated!!"} }
  #   end
  # end

  #This action is used just to relaod the today's appointments listing table
  def todays_appointments
    @pending_appoints = appointments_for_the_day
    respond_to do |format|
      format.js
    end
  end
  
  def process_booking
    booking = current_organisation.bookings.find(params[:id])
    if booking.update(processed: true)
      flash[:status] = 'Success'
      flash[:notice] = 'Booking successfully processed !'
    else
      flash[:status] = 'error'
      flash[:notice] = 'Unable to process booking due to #{booking.errors.full_messages.to_sentence}!'
    end
    redirect_to action: :index
  end

  private

  def appointments_for_the_day
    current_organisation.bookings.where(start_time: Time.now.beginning_of_day..Time.now.end_of_day,check_in: nil,status: [0,1,2,3]).where.not(joint_resource_id: nil)
  end

  def patient_params
    params.require(:patient).permit :first_name,
    :last_name,
    :birthday,
    :phone,
    :email,
    :password,
    :password_confirmation
  end
end
