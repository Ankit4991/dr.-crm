##
#  Doctor::SeoController Use to set seo keywords for views

class Doctor::SeoController < Doctor::BaseController
	authorize_resource :class => false
	def index
		@seos = Seo.all
		@remaining_seos = Seo::SeoViewNames - @seos.map(&:view_name)
	end

	def create
		@seo = Seo.where(view_name: params[:seo][:view_name]).first_or_create
		@seo.update(seo_params)
		redirect_to :back
	end

	def update
		@seo= Seo.find(params[:id])
		@seo.update(seo_params)
		respond_to do |format|
      format.html {redirect_to :back}
      format.json { render :json => {status: flash[:status], msg: flash[:notice]}.to_json }
    end
	end

	private
		def seo_params
      params.require(:seo).permit :description, :keywords, :title
    end
end
