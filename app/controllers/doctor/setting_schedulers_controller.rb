##
# SettingSchedulersController Use to manage Setting => Doctor and Office schedule tab functionality.

class Doctor::SettingSchedulersController < Doctor::BaseController
  authorize_resource :class => false
  
  def index
    @doctor = current_doctor
    @first_store = current_organisation.stores.first
    @doctor_closure = @first_store.doctors.order("id").first.try(:leaves)
    @doctor_working_schedule = @first_store.try(:doctors).present? ? set_working_schedule(@first_store.try(:doctors).first) : "nothing"
    @store_working_schedule = @first_store.present? ? set_working_schedule(@first_store) : "nothing"
    @vision_insurances = current_organisation.vision_insurances.flatten
    render partial: "index" 
  end
 
  #Create Doctor and organisation holidays
  def add_holiday
    
    if params[:closure][:trackable_type]=="Doctor"
      if params[:closure][:date].present? && params[:closure][:end_date].present? && params[:closure][:end_date] >= params[:closure][:date]
        sd = Date.parse(params[:closure][:date])
        ed = Date.parse(params[:closure][:end_date])
        booking_exist = false
        booking_date = ''
        sd.upto(ed){ |date|
          doctor = current_organisation.doctors.find_by(id: params[:closure][:trackable_id])
          booking = doctor.bookings.where("Date(end_time) = ?", date.to_date)
          if booking.present?
            booking_exist = true
            booking_date = date
          end
        }
        doctor_holiday = Holiday.where("((Date(date) <= ? and Date(end_date) >= ?) or (Date(date) <= ? and Date(end_date) >= ?)) and trackable_id = ?", params[:closure][:date].to_time.utc,params[:closure][:date],params[:closure][:end_date].to_time.utc,params[:closure][:end_date],params[:closure][:trackable_id])
        if booking_exist
          @status = "Error"
          @notice = "Please first cancel or shift all appointment for #{booking_date}"
        elsif doctor_holiday.present?
          @status = "Alert"
          @notice = "Already On leave."
        else
          if holiday_params["reason"].present?
            holiday = Holiday.create(holiday_params)
            @status = "Success"
            @notice = "Doctor Day Closure Applied Successfully"
          else
            @status = "Error"
            @notice = "Please enter reason to apply."
          end
        end
      else
        @status = "Error"
        @notice = "Please select both the start date and end date correctly"
      end
      @doctor = current_organisation.doctors.find(params[:closure][:trackable_id])
      @doctor_closure = @doctor.leaves
    else
      store=current_organisation.stores.find_by(id: params[:closure][:trackable_id])
      store_holiday = Holiday.where("Date(date) = ?", params[:closure][:date].to_date)
      booking = store.bookings.where("start_time between ? and ? OR end_time between ? and ?", params[:closure][:date].to_date.beginning_of_day,params[:closure][:date].to_date.end_of_day,params[:closure][:date].to_date.beginning_of_day,params[:closure][:date].to_date.end_of_day) if params[:closure][:date].present?
      if booking.present?
        @status = "Error"
        @notice = "Please first cancel or shift all appointment for #{params[:closure][:date]}"
      elsif store_holiday.present?
        @status = "Alert"
        @notice = "Already close on #{params[:closure][:date]}."
      else
        if holiday_params["reason"].present?
          holiday = Holiday.create(holiday_params)
          @status = "Success"
          @notice = "Day Closure Applied Successfully"
        else
          @status = "Error"
          @notice = "Please enter both fields."
        end
      end
    end
  end

  def delete_holiday
    holiday = current_organisation.holidays.find(params[:id])
    holiday.destroy
    render :nothing => true, :status => 200, :content_type => 'text/html'
  end

  #schedule Doctor and organisation working hours
  def working_schedule
    if params[:trackable_type] == "Doctor"
      @tracked = current_organisation.doctors.find(params[:trackable_id])
    else
      @comming_holidays = current_organisation.holidays.comming.where(doctor_id: nil)
      @tracked = current_organisation.stores.find(params[:trackable_id]) # contains store or doctor
      @doctor_closure =@tracked.doctors.first.try(:leaves)
    end
    @trackable=set_working_schedule(@tracked) #contails working schedule for doctor or store
  end

  def update_working_schedule
    working_schedule = WorkingSchedule.find(params[:working_schedule][:id])
    working_schedule.update_attributes!(params[:working_schedule].permit!)
    redirect_to settings_path
  end

  
  def doctor_leave_closure
    @doctor = current_organisation.doctors.find(params[:doctor_id])
    @doctor_closure = @doctor.leaves
  end
    
  private
    def set_working_schedule(trackable)
      if trackable.try(:working_schedule).present?
        trackable.working_schedule
      else
        trackable.create_working_schedule
      end
    end

    def holiday_params
      params.require(:closure).permit(:trackable_id,:trackable_type,:date,:reason,:end_date)
    end
        
end