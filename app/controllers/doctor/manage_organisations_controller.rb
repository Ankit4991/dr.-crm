##
# ManageOrganisationsController we will use for manage organization. 
# Like add Twilio number, Approve organisation by Admin etc.

class Doctor::ManageOrganisationsController < Doctor::BaseController
  authorize_resource :class => false

  def index
    @approvel_pendding_orgs = Organisation.where(approved: false).paginate(:page => params[:pending_page], :per_page => 10)
    @approved_orgs = Organisation.where(approved: true).paginate(:page => params[:approve_page], :per_page => 10)
    authorize! :index, @approvel_pendding_org
  end

  def update
    @organisation = Organisation.find(params[:id])
    if @organisation.update_attributes(org_params) 
      flash[:status] = "Success"
      flash[:notice] = "Organisation details Successfully updated!!"
    else
      flash[:status] = "Error"
      flash[:notice] = "Something went wrong due to #{@organisation.errors.full_messages}"
    end
    if params[:approve_request] == 'true'
      @organisation.owner.password = Devise.friendly_token.first(8)
      @organisation.save!
      @organisation.owner.send_confirmation_instructions
      @organisation.create_setting
      CreateOrganisationBasicRecordsWorker.new.perform @organisation
      AppMailer.send_approve_org_details_to_patient(@organisation).deliver_now
    end
    redirect_to :back
  end

  def remove_pending_org
    @organisation = Organisation.find(params[:id])
    @organisation.destroy!
    redirect_to :back
  end

  private
    def org_params
      params.require(:organisation).permit! 
    end
end