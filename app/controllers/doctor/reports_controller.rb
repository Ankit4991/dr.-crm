##
# Doctor::ReportsController Use to download pateint reports from patient profile by doctor.

class Doctor::ReportsController < Doctor::BaseController
  include PatientReportSection
end