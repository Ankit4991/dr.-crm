##
# ManageExaminationController to control Exam form action like Delete objects from exam and show all differnt tabs data

class Doctor::ManageExaminationController < Doctor::BaseController
  before_action :set_examination_and_patient

	def delete_impression
		@examination = current_organisation.examinations.find(params[:examination_id])
		@impression = @examination.exam_impressions.delete(params[:id])
		redirect_to :back
	end

  def delete_procedure
    @examination = current_organisation.examinations.find(params[:examination_id])
    @procedures = @examination.procedure_adjustments.delete(params[:id])
    redirect_to :back
  end

  def medical_history
    current_organisation.diseases.default_medical.each do |disease|
      (@resource.pmhxes.map(&:disease_id).include? disease.id) ? "" : @resource.pmhxes.build(disease: disease, joint_resource_id: @resource.joint_resource.id)
      (@resource.pohxes.map(&:disease_id).include? disease.id) ? "" : @resource.pohxes.build(disease: disease, joint_resource_id: @resource.joint_resource.id)      
    end
    # current_organisation.diseases.default_allergies.each do |disease|
    #   (@resource.allergies.map(&:disease_id).include? disease.id) ? "" : @resource.allergies.build(disease: disease, joint_resource_id: @resource.joint_resource.id)
    # end
    render partial: "medical_history"
  end

  def primilary_binocular
    render partial: "primilary_binocular"
  end

  def refraction_contact_lens
    render partial: "refraction_contact_lens"
  end

  def external_internal
    render partial: "external_internal"
  end

  def procedure_impression
    render partial: "procedure_impression"
  end

  def finalized_prescriptions
    render partial: "finalized_prescriptions"
  end

  def attached_docs
    render partial: "attached_docs"
  end

  def remove_glass_prescription
    glasses_prescription = @examination.glasses_prescriptions.find_by(id: params["glasses_prescription_id"])
    eye_measurement = glasses_prescription.eye_measurement
    eye_measurement.update!(finalize: false)
    glasses_prescription.delete
    render partial: "finalized_prescriptions"
  end

  def remove_lens_prescription
    lens_prescription = @examination.lens_prescriptions.find_by(id: params["lens_prescription_id"])
    lens = lens_prescription.lens
    lens.update!(finalize: false)
    lens_prescription.delete
    render partial: "finalized_prescriptions"
  end

  def remove_lens
    lens = @examination.contact_lens_exam.lenses.find_by(id: params[:lens_id])
    lens.destroy!
  end

  def remove_glass
    glass = @examination.glasses.find_by(id: params[:glass_id])
    glass.destroy!
  end

  private
    def set_examination_and_patient
      @examination=current_organisation.examinations.find(params[:examination_id])
      @joint_resource=@examination.joint_resource
      @resource = @joint_resource.member
      @old_exams = @joint_resource.examinations.where("created_at <?", @examination.created_at)
      @previous_exam = @old_exams.last if @old_exams.present?
    end
end