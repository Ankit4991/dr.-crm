class Doctor::ContactLensDistributionOrdersController < Doctor::BaseController


	def new
		@examination = Examination.find params[:exam]
		@patient_id = @examination.joint_resource.patient_id
		@lens = Lens.find_by(id: params[:lens_id])
		@contact_lens_distributon_order = ContactLensDistributionOrder.new
		respond_to do |format| 
			format.html    { render :index }
			format.js  
		end
	end


	def create
		begin
			if params[:contact_lens_distribution_order].present?
				@contact_lens_distribution_order = ContactLensDistributionOrder.new(params.require(:contact_lens_distribution_order).permit!)
				if @contact_lens_distribution_order.save!
					flash[:status] = "Success"
					flash[:notice] = "Confirmed!"   
				end
			end
		rescue Exception => e
			flash[:status] = 'Error'
			flash[:notice] = "Error due to #{e.message}"
		end
		render :json => {status: flash[:status], msg: flash[:notice]}.to_json
	end

	def edit
		@lens = Lens.find_by(id: params[:lens_id])
		@contact_lens_distributon_order = ContactLensDistributionOrder.find(params[:id])
	end


	def update
		begin
			@contact_lens_distribution_order = ContactLensDistributionOrder.find params[:id]
			@contact_lens_distribution_order.update(params.require(:contact_lens_distribution_order).permit!)
			flash[:status] = "Success"
			flash[:notice] = "Confirmed!"
		rescue Exception => e
			flash[:status] = 'Error'
			flash[:notice] = "Error due to #{e.message}"
		end
		render :json => {status: flash[:status], msg: flash[:notice]}.to_json
	end

	def destroy
		@contact_lens_distribution_order = ContactLensDistributionOrder.find(params[:id]) 
		if @contact_lens_distribution_order.destroy
			@contact_lens_distribution_orders = ContactLensDistributionOrder.all
			flash[:status] = "Success"
			flash[:notice] = "Deleted successfully!"
		end    
		respond_to do |format|
			format.html {redirect_to :back}
			format.json { render :json => {status: flash[:status], msg: flash[:notice]}.to_json }
			format.js
		end
		
	end

	def order_contact
		if params[:id].present?
			@contact = Contact.find params[:id]
		end
		respond_to do |format| 
			format.html
			format.js  
		end
	end

	def send_report
		if params[:contact_emails].present?
			@acount_numbers = []
			acount = params[:contact_acounts].split(",")
			params[:contact_acounts].split(",").each_with_index do |value, index|
				if index.even?
					@acount_numbers	<< [acount[index], acount[index+1]]
				end
			end
			@store = current_doctor.store
			@contact_lens_distribution_orders = @store.contact_lens_distribution_orders.where(soft_delete: false)
			contact_emails = params[:contact_emails].split(",").uniq
			AppMailer.send_order_to_contact(contact_emails, current_doctor, "You have recevied a new contact lens order from #{@store.description}", @acount_numbers).deliver_later

			AppMailer.send_order_to_contact(@store.contact_email, current_doctor, "You have just placed a contact lens order", @acount_numbers).deliver_later
				
			@store.contact_lens_distribution_orders.update_all(soft_delete: true)
			
			@contact_lens_distribution_orders = @store.contact_lens_distribution_orders.where(soft_delete: false)
			respond_to do |format| 
				format.html 
				format.js
			end
		end
	end

	def lens_pdf
		begin
			@contact_lens_distribution_orders = current_doctor.store.contact_lens_distribution_orders
			@doctor = current_doctor
			generate_pdf('/doctor/dashboards/dashboard_partial/contact_lens_pdf.html','trial_lens_order.pdf')
		rescue Exception => e
			flash[:status] = 'Error'
			flash[:notice] = "Error due to #{e.message}"
		end
	end

	private
		def generate_pdf(template_pth,file_name)
        pdf = WickedPdf.new.pdf_from_string(
                                render_to_string(
                                  template: template_pth,
                                  layout: 'layouts/application.pdf.haml'))
        send_data(pdf,
                  filename: file_name,
                  type: 'application/pdf',
                  disposition: 'attachment')
      end

end
