class Doctor::OrdersController <  Doctor::BaseController
  before_action :get_resource, only: [:new]
  before_action :set_order_and_patient, only: [:edit, :update, :destroy, :update_checklist, :checklist, :create_glass_suborder, :create_contact_lens_suborder, :reload_procedures]
  before_action :organisation_setting
  include OrderChecklist
  include OrderFinalizedPrescriptions
  include OrderItemsPrices
  include OrderLogActions
  include SubordersActions
  include ActionView::Helpers::NumberHelper
  
  def index
   @orders = current_organisation.orders
  end
  
  def new
    today_order = Order.where("Date(created_at) = ? and joint_resource_id = ?", Date.today,@joint_resource.id)
    unless today_order.present?
      @patient = @joint_resource.member
      @order = @joint_resource.orders.build
      @order.doctor = current_doctor
      @order.joint_resource_id=@joint_resource.id
      if params[:changecall].present?
        case params[:changecall]
        when "pending"
          @order.status = "unforder"
        end
      end
      if params[:examination_id].present?
        @order.examination_id = params[:examination_id]
        @order.total = @order.examination.procedure_adjustments.inject(0) { |sum, a| sum + a.exam_procedure.price }
        # @order.examination.booking.exam_procedures.each do |proc|
        #   # @exam_procedure = current_organisation.exam_procedures.where(name: proc.name).first
        #   @procedures = @order.examination.procedure_adjustments.create(order_id: @order.id,examination_id: params[:examination_id],exam_procedure_id: @exam_procedure.id, price: @exam_procedure.price)
        # end
      end
      @home_address = @joint_resource.home_address if @joint_resource.home_address.present?
      #@order.contact_lens_orders.build(lenses: [Lens.new])
      #@order.eye_glass_orders.build(glasses: [Glass.new])
      @order.save!
      redirect_to edit_order_path(@order.id)
    else
      redirect_to edit_order_path(today_order.first)
    end
  end
  
  def create
    @order = Order.new(order_params)
    @order.save
    AppMailer.send_order_confirmation_to_patient(@order).deliver_now unless @order.try(:joint_resource).try(:patient).try(:email_not_supplied)
    if @order.errors.any?
      flash[:status] = "Error"
      flash[:notice] = "Something went wrong due to #{@order.errors.full_messages}"
    else
      flash[:status] = "Success"
      flash[:notice] = "Order Successfully created!!"
    end
    redirect_to  edit_order_path(@order.id)
  end
  
  def edit
    @examination = @order.examination if @order.examination.present?
    @patient = @joint_resource.member
    @transaction = @order.transactions.build
    @profile = @joint_resource.profile.present? ? @joint_resource.profile.get_details : nil
    @home_address = @joint_resource.home_address
    if params[:changecall].present? && @order.status.include?('pending')
      case params[:changecall]
      when "pending_order"
        @order.status = "unforder"
      end
      @order.save
    end
    @exam_copay = @examination.present? ? @examination.procedure_adjustments.sum(:price) : @order.procedure_adjustments.sum(:price)
    #if(@order.total == 0.0)
    #  @order.total = @exam_copay
    #end
    @all_orders = @joint_resource.orders
    @contact_lens_orders = @order.contact_lens_orders
    @eye_glass_orders = @order.eye_glass_orders
    @doctors = current_organisation.doctors.select{|doc| doc.role.include?('staff') || doc.role.include?('doctor')}
  end
  
  #Deprecated method

  # def update
  #   if params[:contacts_suborder_update].present? || params[:glasses_suborder_update].present?
  #     suborder_update
  #   else
  #     @order.update_attributes!(order_params)
  #     @exam = @order.examination if @order.examination.present?
  #     patient = @order.joint_resource.patient
  #     if @order.status == "finalized"
  #       if @exam.present?
  #         @exam.status = @order.status
  #         @exam.save
  #       end
  #       AppMailer.send_order_confirmation_to_patient(@order).deliver_later
  #       one_click_url = root_url(patient_email: patient.email, authentication_token: patient.authentication_token)
  #       notification_message = AppConfig.sms_notification["invoice_ready"] % {patient_name: @order.joint_resource.name ,one_click_login: one_click_url, store_name: @order.doctor.store.description , store_address: "#{@order.doctor.store.office_address.address1}, #{@order.doctor.store.office_address.address2}, postal code #{@order.doctor.store.office_address.postal_code} " , store_phone_number: "#{@order.doctor.store.phone_number}"}
  #       TwilioSmsWorker.perform_async({'src'=> current_organisation.twilio_number, 'dst' => @order.joint_resource.phone, 'text' => notification_message, 'notification_method'=> patient.notification_method})
  #     end
  #     if @order.errors.any?
  #       flash[:status] = "Error"
  #       flash[:notice] = "Something went wrong due to #{@order.errors.full_messages}"
  #     else
  #       flash[:status] = "Success"
  #       flash[:notice] = "Order Successfully updated!!"
  #     end
  #   end
  #   @order.reload
  #   @transaction = @order.transactions.build
  #   @profile = @order.joint_resource.patient.profile.present? ? @order.joint_resource.patient.profile.get_details : nil
  #   respond_to do |format|
  #     format.html {redirect_to :back}
  #     format.js{render layout: false, content_type: 'text/javascript'}
  #   end and return
  # end

  def update
    @order.update_attributes!(order_params) if params[:order].present?
    all_glass_orders_total = @order.eye_glass_orders.where(:hold => false).sum(:total)
    all_lens_orders_total = @order.contact_lens_orders.where("total > 0 AND hold = false").sum(:total)
    exam_copay = @order.exam_copay
    @order.update_attributes(:total => (all_glass_orders_total + all_lens_orders_total + exam_copay).round(2),doctor: @order.doctor)
    render :json => {order_total: '%.2f' % @order.total, balance_due: '%.2f' % @order.balance_due,order_refund: '%.2f' % @order.refund,order_reward: '%.2f' % @order.try(:joint_resource).try(:reward_point), status: 'Success', msg: "Order has been updated successfuly."}.to_json
  end

  def destroy
    @order = current_organisation.orders.find(params[:id])
    if params[:remove_dispensed]
      @order.status = 'dispensed'
      @order.save
      @order.examination.update_attributes!(:status => 'removed') if @order.examination.present?
      @follow_up = @joint_resource.follow_ups.create!(:reason => "finalized order removed!",:due_date => Time.now)
      @follow_up.destroy
      AppMailer.send_dispensed_removal_to_patient(@order).deliver_later if params[:mail] && !(@order.try(:joint_resource).try(:patient).try(:email_not_supplied))
      flash[:notice] = "Order is Dispensed Successfully."
      flash[:status]="Success"
    elsif params[:remove_finalized]
      @order.status = 'dispensed'
      @order.save
      @order.examination.update_attributes!(:status => 'removed') if @order.examination.present?
      @follow_up = @joint_resource.follow_ups.create!(:reason => "finalized order removed!",:due_date => Time.now)
      @follow_up.destroy
      @follow_list = FollowUp.where("due_date >= ?", DateTime.now).last
      if @follow_list.present?
        @follow_list.destroy
      end
    else
      AppMailer.send_order_cancellation_to_patient(@order).deliver_later if params[:mail] && !(@order.try(:joint_resource).try(:patient).try(:email_not_supplied))
      @order.destroy
    end
    respond_to do |format|
      format.html {redirect_to :back}
      format.json {redirect_to :back, status: flash[:status], msg: flash[:notice]}.to_json
      format.js {redirect_to :back unless params[:remove_finalized]}
    end
  end

  # Deplecated
  # def add_procedures
  #   @examination = current_organisation.examinations.find(params[:id])
  #   @procedures = @examination.update_attributes(examination_params)
  #   @order = @examination.order
  #   @order.reload
  #   render :json => {order_total: @order.total, balance_due: @order.balance_due,order_refund: @order.refund,order_id: @order.id}.to_json
  # end

  def add_procedures
    @examination = current_organisation.try(:examinations).find(params[:id])
    @exam_procedures = current_organisation.try(:exam_procedures).where(name: params[:procedure_name]).first
    @order = @examination.try(:order)
    @procedures = @examination.try(:procedure_adjustments).create(order_id: @order.try(:id),examination_id: params[:id],exam_procedure_id: @exam_procedures.try(:id), price: @exam_procedures.try(:price))
    render :json => {status: 'Success', msg: "Procedures Added.",order_total: '%.2f' % @order.try(:total), balance_due: '%.2f' % @order.try(:balance_due),order_refund: '%.2f' % @order.try(:refund),order_id: @order.try(:id),order_reward: '%.2f' % @order.try(:joint_resource).try(:reward_point)}.to_json
  end

  def delete_procedures
    @examination = current_organisation.examinations.find(params[:examination_id])
    @order = @examination.order
    begin
      @procedures = @examination.procedure_adjustments.find(params[:id]) if @examination.procedure_adjustments.find(params[:id]).present?
      @procedures.destroy if @procedures.present?
      status = "Success"
      notice = "Procedures removed"
      @order.reload
      render :json => {status: status , msg: notice,order_total: '%.2f' % @order.total, balance_due: '%.2f' % @order.balance_due,order_refund: '%.2f' % @order.refund,order_id: @order.id,order_reward: '%.2f' % @order.try(:joint_resource).try(:reward_point)}.to_json
    rescue Exception => e
      status = "Error"
      notice = "#{e}"
      @order.reload
      render :json => {status: status , msg: notice ,order_total: '%.2f' % @order.total, balance_due: '%.2f' % @order.balance_due,order_refund: '%.2f' % @order.refund,order_id: @order.id,order_reward: '%.2f' % @order.try(:joint_resource).try(:reward_point)}.to_json
      
    end
  end

  def reload_procedures
    respond_to do |format|
      format.js { render :file => 'doctor/orders/order_partial/procedure_adjustment/procedure_adjustment_table.js.erb'}
    end
  end
  
  def organisation_setting
    @setting = current_organisation.setting
  end

  private

    def examination_params
      params.require(:examination).permit!
    end

    def suborder_update
      params[:order][:contact_lens_orders_attributes].each do |index,lens_attributes|
        if lens_attributes[:id] == params[:contacts_suborder_update]
          lens_order = ContactLensOrder.find(lens_attributes[:id])
          lens_order.update_attributes!(lens_attributes.except(:_destroy,:id).permit!)
          @order.reload
        end
      end
      params[:order][:eye_glass_orders_attributes].each do |index,glass_attributes|
        if glass_attributes[:id] == params[:glasses_suborder_update]
          glass_order = EyeGlassOrder.find(glass_attributes[:id])
          glass_order.update_attributes!(glass_attributes.except(:_destroy,:id).permit!)
          @order.reload
        end
      end
    end
    
    def order_params
      params.require(:order).permit!
    end
    def get_resource
      @joint_resource= JointResource.find_by(id: params[:id]) #joint resource id passes in params
    end
    def set_order_and_patient
      @order=current_organisation.orders.find(params[:id])
      @joint_resource=@order.joint_resource
      @resource = @joint_resource.member
      title = @resource.try(:name).to_s + ' ' + @order.updated_at.try(:to_date).to_s + ' ' + @order.try(:store).try(:description).to_s
      set_meta_tags title: title
    end
    # def contact_lens_exam_params
      # params.require(:contact_lens_exam).permit!
    # end
    def eye_measurement_params
      params.require(:eye_measurements).permit!
    end
    def eyeglass_suborder_params
      params.require(:eye_glass_order).permit!
    end
    
    def contact_lens_suborder_params
      params.require(:contact_lens_order).permit!
    end
    
end
