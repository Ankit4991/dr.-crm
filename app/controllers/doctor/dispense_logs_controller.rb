class Doctor::DispenseLogsController <  Doctor::BaseController
  authorize_resource :class => false

  def create
    begin
      @dispense_log = DispenseLog.new(dispense_log_params)
      @dispense_log.save!
      respond_to do |format|
       format.js { render :nothing => true}
      end
    rescue Exception => e
      respond_to do |format|
       format.js { render :json => {:message => "Something went due to #{e}"}}
      end
    end
  end
  
  def show
    @dispense_log = current_organisation.dispense_logs.find(params[:id])
  end
  
  #To show all dispense logs in modal box
  def all_logs
    if params[:dispensable_type] == "Order"
      @order = current_organisation.orders.find(params[:dispensable_id])
      @dispense_logs = @order.dispense_logs
    elsif params[:dispensable_type] == "FavouriteOrder"
      @order = current_organisation.favourite_orders.find(params[:dispensable_id])
      @dispense_logs = @order.dispense_logs
    end
    render :partial => 'dispense_logs',:locals => {:dispense_logs => @dispense_logs}
  end
  
  def add_new
    if params[:dispensable_type] == "Order"
      @order = current_organisation.orders.find(params[:dispensable_id])
      @dispense_log = @order.dispense_logs.build
    elsif params[:dispensable_type] == "FavouriteOrder"
      @order = current_organisation.favourite_orders.find(params[:dispensable_id])
      @dispense_log = @order.dispense_logs.build
    end
    render :partial => 'add_log',:locals => {:dispense_log => @dispense_log}
  end
  
  #To create a dispense log along with sending an email to patient about order ready to deliver
  def email_order_ready
    if params[:dispensable_type] == "Order"
      @order = current_organisation.orders.find(params[:dispensable_id])
      @dispense_log = @order.dispense_logs.build(:note => "Order is ready to be picked up",:logged_at => Time.zone.now,:emailed => true)
      @dispense_log.save!
      day_start_time = @order.try(:doctor).try(:working_schedule).try("#{Date.today.strftime('%A').downcase}_start_time")
      AppMailer.send_order_dispensed_to_patient(@order).deliver_later(wait_until: Time.at(Time.now.to_i + (Time.now.to_i % 1.day))) unless @order.try(:joint_resource).try(:patient).try(:email_not_supplied)
    elsif params[:dispensable_type] == "FavouriteOrder"
      @order = current_organisation.favourite_orders.find(params[:dispensable_id])
      @dispense_log = @order.dispense_logs.build(:note => "Order is ready to be picked up",:logged_at => Time.zone.now,:emailed => true)
      @dispense_log.save!
      AppMailer.send_renewed_order_dispensed_to_patient(@order).deliver_later unless @order.try(:joint_resource).try(:patient).try(:email_not_supplied)
    end
    respond_to do |format|
      format.js { render :json => {} }
    end
  end
  
  private
  
  def dispense_log_params
     params.require(:dispense_log).permit(:note,:log_date_time,:logged_at_date,:logged_at_time,:dispensable_id,:dispensable_type)
  end
end
