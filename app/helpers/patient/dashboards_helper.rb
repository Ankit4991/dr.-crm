module Patient::DashboardsHelper
 def medical_history(grp)
   grp.map{|phx| "#{phx.disease.name}: #{phx.notes}" if phx.disease.groups.include? "medical" or phx.disease.groups.include? "allergy" }
 end
 def social_history(grp)
   grp.map{|neuro| neuro.name if neuro.groups.include? "psychology"}
 end
 # def state_keys
 #   State.get_yaml.values.map{|k| k.values.reverse }
 # end
 
 def lens_prescription(lens_order_id)
   contact_lens_order = current_organisation.contact_lens_orders.find(lens_order_id)
   lens = contact_lens_order.lenses.first if contact_lens_order.lenses.present?
   return lens
 end
end