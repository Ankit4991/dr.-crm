module Patient::FavouritesHelper
  def eye_lens_price(type)
    @lenstype = current_organisation.lens_types_prices.find_by_brand_name(type)
    if @lenstype.present?
      return @lenstype.price
    else
      return 0.0
    end
  end
  
  def adjustments
    return (@order.total - subtotal).round(2)
  end
  
  def subtotal
    return (@order.lens_orders['lens_od_unit_price'].to_f * @order.lens_orders['lens_od_quantity'].to_f) + (@order.lens_orders['lens_os_quantity'].to_f * @order.lens_orders['lens_os_unit_price'].to_f)
  end
end
