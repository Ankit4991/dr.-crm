module Patient::BookingsNewHelper  
  def get_data
    if @patient.email.count('+') > 0
      array_after_split = @patient.email.split('+')
      first_part = array_after_split.first
      second_part = array_after_split.second.split('@').second
    else
      array_after_split = @patient.email.split('@')
      first_part = array_after_split.first
      second_part = array_after_split.second
    end
    return (first_part+','+second_part)
  end

end
