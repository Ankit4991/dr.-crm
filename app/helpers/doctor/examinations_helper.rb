module Doctor::ExaminationsHelper
	def subtest_show_hide eye_measurement
		glass = eye_measurement.try(:glass)
		if (glass.present? && ((eye_measurement.measurement_type == "objective") || (eye_measurement.measurement_type == "subjective") || (eye_measurement.measurement_type == "bincular_balance") || (eye_measurement.measurement_type == "cyclopelgic"))) && (glass.od_cylinder.present? || (glass.od_sphere !="pl") || (glass.os_sphere !="pl") || glass.os_cylinder.present? || glass.try(:visual_acuity).try(:distance_od).present? || glass.try(:visual_acuity).try(:distance_os).present? || glass.try(:visual_acuity).try(:distance_ou).present? || glass.ou_add.present?)
			return true
		else
			return false
		end
	end

	def keratometry_show_hide cl_exam 
		cl_exam.od_d_h.present? || cl_exam.od_mm_h.present? || cl_exam.od_axis_h.present? || cl_exam.os_d_h.present? || cl_exam.os_mm_h.present? || cl_exam.os_axis_h.present? || cl_exam.od_d_v.present? || cl_exam.od_mm_v.present? || cl_exam.od_axis_v.present? || cl_exam.os_d_v.present? || cl_exam.os_mm_v.present? || cl_exam.os_axis_v.present?
	end
end