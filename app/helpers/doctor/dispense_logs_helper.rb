module Doctor::DispenseLogsHelper
  def note_color(dispense_log)
    dispense_log.emailed ? 'green':'orange'
  end
end
