module Doctor::OrdersHelper
  
  def lab_order_prescription(property)
    @eye_glass_order.glasses.first.present? ? @eye_glass_order.glasses.first[property] : ""
  end
  
  def lab_order_lens_types
    @eye_glass_order.eyeglass_types_price.present? ? @eye_glass_order.eyeglass_types_price.glasstype : ""
  end
  
  def lab_order_pupillary_distance(property)
    @eye_glass_order.glasses.first.present? ? @eye_glass_order.glasses.first.visual_acuity[property] : ""
  end

  def lab_order_addon_details
    addons = []
    unless @eye_glass_order.addon_details.blank?
      @eye_glass_order.addon_details.each do |detail|
        addons += [[detail.glass_addons_price.try(:addon),detail.color,detail.checkmark]]
      end
    end
    return addons
  end
  
  def lenses_prices
    @order.lenses.map{ |lens| [lens.od_type,lens.contact_lens_order.od_price,lens.os_type,lens.contact_lens_order.os_price] } if @order.lenses.present?
  end
  
  def order_status(order)
    case order.status
    when "pending"
      return "Pending"
    when "unforder"
      return "Unfinished Order"
    when "ordered"
      return "To Be Ordered"
    when "ready_for_pick_up"
      return "Ready For Pick Up"
    when "finalized"
      return "Finalized"
    when "dispensed"
      return "dispensed"
    else
      return "Not Identified"
    end
  end

  def order_transaction_title(order_transaction)
    #order_transaction.transaction_type.include?('refund') ? 'Refund Requested': 'Payment Requested'
    if order_transaction.transaction_type.to_sym == :refund
      return 'Refund Requested transaction details'
    elsif order_transaction.transaction_type.to_sym == :auth_capture && order_transaction.referenced?
      return 'Payment has been successfully received but refunded now'
    else
      return 'Payment Requested transaction'
    end
  end

  def available_contact_lens_types
    current_organisation.lens_types_prices.all.map(&:brand_name)
  end
  
end
