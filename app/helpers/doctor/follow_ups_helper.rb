module Doctor::FollowUpsHelper
  def status_check(follow_up)
    unless follow_up.deleted_at.present?
      if follow_up.present? && follow_up.due_date.present?
        due = Date.parse(follow_up.due_date.strftime('%Y-%m-%d'))
        outcome = follow_up.outcome
        if follow_up.created_at < follow_up.updated_at && due < follow_up.updated_at && follow_up.outcome.present?
         return 'blue';
        elsif follow_up.created_at < follow_up.updated_at && due >= follow_up.updated_at && follow_up.outcome.present?
         return 'green';
        elsif follow_up.outcome.blank? && due < Date.today
         return 'red';
        else
         return 'yellow';
        end
      end
    else
      return 'grey'
    end
  end

  def contact_days(follow_up)
    days = Date.parse(follow_up.due_date.to_s) - Date.today
    return days
  end
end
