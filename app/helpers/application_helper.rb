module ApplicationHelper
  include Doctor::ExaminationsHelper

  def full_title(page_title)
    if doctor_signed_in?
      if current_doctor.try(:store).try(:description).present? 
        base_title = "#{current_doctor.store.description}"
      else
        base_title = "Eyetrust Vision"
      end
    elsif patient_signed_in?
      base_title = "Eyetrust Vision"
    end
    if page_title.empty?
      base_title
    else
      "#{base_title} | #{page_title}"
    end
  end

  def display_user_name
    if doctor_signed_in?
      current_doctor.name
    elsif patient_signed_in?
      current_patient.name
    end
  end 

  # Set sidebar active class
  def current_page(path)
    'active' if current_page? path
  end 

  def exam_simple_checkbox_add(field_name, f, options, &block)
    parent   = "<div class=\"#{options[:class]}\" checkboxAdd>"
    checkbox = f.input field_name, 
                as: :boolean, 
                input_html: { checkboxAddChanger: '' , :class => 'icheck-me parent-toggle', "data-color" => "blue", "data-skin" => "line", "data-text" => options[:checkbox_label] || field_name.humanize},
                label: false
                # label: options[:checkbox_label] || field_name.humanize
    contents = content_tag options['contents_parent_tag'] || :div, capture(:contents, &block), checkboxAddContent: ''
    # ActiveSupport::SafeBuffer.new parent + checkbox + contents + '</div>'
    (parent + checkbox +  contents + '</div>').html_safe
  end

  def simple_checkbox_add(field_name, f, options, &block)
    parent   = "<div class=\"#{options[:class]}\" checkboxAdd>"
    checkbox = f.input field_name, 
                as: :boolean, 
                input_html: { checkboxAddChanger: ''},
                label: options[:checkbox_label] || field_name.humanize
    contents = content_tag options['contents_parent_tag'] || :div, capture(:contents, &block), checkboxAddContent: ''
    # ActiveSupport::SafeBuffer.new parent + checkbox + contents + '</div>'
    (parent + checkbox +  contents + '</div>').html_safe
  end

  def range_number_conversion(num)
    num = num.to_f
    number_with_precision(num, precision: 2)
    if num > 0
      "+#{num}"
    elsif num == 0.0
      "pl"
    else
      num
    end
  end

  def class_name obj
    obj.class.name
  end

  def user_profile_image
    if doctor_signed_in?
      image_url = current_doctor.avatar? ? current_doctor.avatar.url : current_doctor.gender == 'male' ? "/images/male.jpg" : "/images/female1.jpg"

    elsif patient_signed_in?
      image_url = current_patient.avatar? ? current_patient.avatar.url : current_patient.gender == 'male' ? "/images/male.jpg" : "/images/female-icon1.jpg"

    end

  end

  def get_card_image card_name
    case card_name
      when 'visa'
        "/images/visa.png"
      when 'discover'
        "/images/discover.png"
      when 'master'
        "/images/master.png"
      when 'american_express'
        "/images/amex.png"      
      else
    end
  end

  def filter_lens_fields type, field
    if(((field == 'add')  && (type == 'sphere' ||  type == 'toric')) || ((field == 'cylinder' || field == 'axis')  && (type == 'sphere' ||  type == 'multifocal')))
      'hidden'
    end    
  end

  def read_store_appointment doctor
    doctor.role.include?("staff") ? true : false
  end

  def read_org_appointment doctor
    (doctor.role.include?("administrator") || doctor.role.include?("super_administrator")) ? true : false
  end

  def notification_url notification
    if notification.event_type == "ContactForm"
      if notification.try(:event).try(:notification_method) == 1
        "#contact-form-email-modal#{notification.id}"
      elsif notification.try(:event).try(:notification_method) == 2
        "#contact-form-sms-modal#{notification.id}"
      else
        "#contact-form-both-modal#{notification.id}"
      end
    elsif notification.event_type == "Log"
      if notification.event.try(:trackable_type) == "ContactForm"
        if notification.try(:event).try(:trackable).try(:notification_method) == 1
          "#contact-form-email-modal#{notification.id}"
        elsif notification.try(:event).try(:trackable).try(:notification_method) == 2
          "#contact-form-sms-modal#{notification.id}"
        else
          "#contact-form-both-modal#{notification.id}"
        end
      elsif notification.event.try(:trackable_type) == "TwilioWebhook"
        "#reply-sms-modal#{notification.id}"
      end
    end  
  end

#Helper methods for Invoice Reports all benefits.

  def over_all_gl_disc(order)
    @sum = 0.0
    if order.eye_glass_orders.present?
      order.eye_glass_orders.each do |ee|
       @sum = @sum + ee.try(:discount).try(:to_f)
      end
    end
    return @sum.to_s   
  end 

  def over_all_gl_frame_allowance(order)
    @sum = 0.0
    if order.eye_glass_orders.present?  
      order.eye_glass_orders.each do |ee|
       @sum = @sum + ee.try(:frame_allowance).try(:to_f)
      end
    end
    return @sum.to_s
  end

  def over_all_gl_material_copay(order)
    @sum = 0.0
    if order.eye_glass_orders.present?  
      order.eye_glass_orders.each do |ee|
       @sum = @sum + ee.try(:material_copay).try(:to_f)
      end
    end
    return @sum.to_s
  end

  def over_all_gl_overage_discount(order)
    @sum = 0.0
    if order.eye_glass_orders.present?   
      order.eye_glass_orders.each do |ee|
       @sum = @sum + ee.try(:overage_discount).try(:to_f)
      end
    end
    return @sum.to_s
  end

  def over_all_cl_material_copay(order)
    @sum = 0.0
    if order.contact_lens_orders.present?  
      order.contact_lens_orders.each do |ee|
       @sum = @sum + ee.try(:material_copay).try(:to_f)
      end
    end
    return @sum.to_s
  end

  def over_all_cl_fit(order)
    @sum = 0.0
    if order.contact_lens_orders.present?  
      order.contact_lens_orders.each do |ee|
       @sum = @sum + ee.try(:cl_fit).try(:to_f)
      end
    end
    return @sum.to_s
  end

  def over_all_cl_allowance(order)
    @sum = 0.0
    if order.contact_lens_orders.present?  
      order.contact_lens_orders.each do |ee|
       @sum = @sum + ee.try(:allowance).try(:to_f)
      end
    end
    return @sum.to_s
  end

  def cl_tot(order)
    @total = 0.00
    if order.contact_lens_orders.present?
      order.contact_lens_orders.each do |tot|
        @total = @total.to_f + tot.try(:total).try(:to_f)
      end
    end
    @total = number_to_currency(@total)
    if @total.to_s == "$0.00"
      return true
    else
      return false
    end
  end

  def eye_tot(order)
    @total = 0.00
    if order.eye_glass_orders.present?
      order.eye_glass_orders.each do |tot|
        @total = @total + tot.try(:total).try(:to_f)
      end
    end
    @total = number_to_currency(@total)
    if @total.to_s == "$0.00"
      return true
    else
      return false
    end 
  end

  def about_us_data data_name
    case data_name
      when 'save_time'
        "Our goal is to save your time in the integration process. Our team has spend years to find the easiest way to integrate the system into your office. We want you to focus on saving your time and ensure the implementation exceeds your expectations. We stand behind our guarantee to be the most seamless platform your staff, patients, and doctors have ever used!"   
      when 'make_more_revenue'
        "Our integrated platform allows your patients to feel confident in your practice. Our targeted email campaigns will specifically target patients on that they like to order, and when they are due to order. Patients will order from the comfort of their house or via their smart phone. The gained trust from your patients will only turn into save your time and make more money!"
      when 'free_storage'
        "Our EHR is flexible, adaptable, and is always changing to fit your business needs. Our cloud server has a 99.99% guaranteed up time, and you will never loss a record again. We are the only company that gives you full access to our 100+ TB cloud server, and you will have limitless free storage. Weather you are the only doctor in the office, or have multiple stores we have you covered."
      when 'customer_service'
        "We want to make sure you always have tech support, and there is never an unanswered question. We have trained technical support agents at your fingertips all day, ever day, whenever you need it. Your success is our number one priority, and we'll ensure that any issue you might have we'll solve it in a timely manner."
      when 'opto_platform'
        "Our focus when we created the iTrust optometry App was to be the only all-in-1 software, and address all needs of an optometry practice. Our competitions will nickel and dime business owners for any small additional to their platform without guaranteeing success. We took a different mindset, that we focus on you as a person and business owner. Together we will achieve higher levels of success."
      when 'satisfaction_guarantee'
        "We are confident that you will love our product so we 100% stand behind our application at all costs. Our guarantee will ensure that you are always completely satisfied will our products and services. For any reason this doesn't hold true, we will make it right 100% of the time."
      when 'internet_platform'
        "We are devoted to give you fast answers when you need them. You can relax knowing that we are working hard managing your business so you don’t have to. Our company will give you more power over the competition and more control over your patients. We pledge to always provide you expert service when you need it. Our promise to you is not give up until you’re completely satisfied."
    end
  end
  
end
