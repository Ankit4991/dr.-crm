class ExaminationDecorator < Draper::Decorator
  delegate_all

  def last_exams_date
    unless object.last_exam
      return ' - '
    end
    h.time_ago_in_words(object.last_exam.created_at) + ' ago'
  end
end
