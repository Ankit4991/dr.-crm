##
# Follow up model values is used for patient contacts history.There can be multiple follow ups for every Patient.
# Doctor will enter an amount of days that the contact will occur in. From this the doctor can note when the patient will need to be contacted.

class FollowUp < ActiveRecord::Base
  belongs_to :joint_resource
  validates_presence_of :reason, :due_date
  
  acts_as_paranoid
  scope :updated_today,lambda { where("updated_at >= ? AND updated_at <= ?", Time.zone.now.midnight, Time.zone.now.end_of_day )}

  def is_followed?
  	return true if outcome.present?
  	false
  end
end
