class MedicalInsurance < ActiveRecord::Base
	belongs_to :organisation
	before_create :add_organisation_id
	validates :name, :code, presence: true 

	private
    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end
end
