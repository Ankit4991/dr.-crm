##
# LensPrescription is final result that Doctor get after his analysis and experiments. 
# Lens also knows as "Final Lens Prescription" . This is also a part of Exam form.

class LensPrescription < ActiveRecord::Base
  before_create :add_organisation_id ,:set_modality_period
  audited :associated_with => :examination
  before_create :update_issued_expiry_date
  belongs_to :lens
  belongs_to :examination
  belongs_to :organisation
  accepts_nested_attributes_for :lens, reject_if: :all_blank
  has_and_belongs_to_many :exam_cleaning_solution

  
  def prescription_cost
    if self.lens.present?
      od_price = self.lens.try(:od_lens_type_price).try(:price).to_f
      os_price = self.lens.try(:os_lens_type_price).try(:price).to_f
      return od_price + os_price
    else
      return 0.0
    end
  end
  
  def lens_od_modality_period
    self.lens.od_lens_type_price.try(:modality) || 12
  end
  
  def lens_os_modality_period
    self.lens.os_lens_type_price.try(:modality) || 12
  end
  
  def in_use?
    self.created_at.to_date >= (Date.today - 1.year) ? true : false
  end
  
  private
    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end
    
    def set_modality_period
      self.modality_period =  [lens_od_modality_period,lens_os_modality_period].max
    end

    def update_issued_expiry_date
      if self.lens.finalize == true && self.issued_date.nil? && self.expired_date.nil?
        self.issued_date = Time.now
        if self.examination.organisation.setting.default_expiry.to_i.year.present?
          self.expired_date = Time.now + self.examination.organisation.setting.default_expiry.to_i.year
        else
          self.expired_date =  Time.now + 1.year
        end
      end
    end
end
