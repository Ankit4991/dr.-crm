##
# Referral is associated to exam form. Doctor can referr to any patient for other Doctor.
# That will contain the information of exam form that done by the doctor. 

class Referral < ActiveRecord::Base
	belongs_to :examination
  has_one :joint_resource, through: :examination
  delegate :doctor, to: :examination, prefix: true, :allow_nil => true
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  def valid_email?
    VALID_EMAIL_REGEX.match(self.cc_to)
  end
end
