##
# EyeglassTypesPrice store glass type with price and we will use in glass prescription as a glass type.

class EyeglassTypesPrice < ActiveRecord::Base
  belongs_to :organisation

  before_create :add_organisation_id
  
  validates :glasstype, :presence => true
  validates :price, :presence => true
  
  private
    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end
end
