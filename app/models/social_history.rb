##
# SocialHistory is part of Exam form

class SocialHistory < ActiveRecord::Base
	belongs_to :examination
	has_and_belongs_to_many :neurological_psych_status, :class_name => 'Disease'
end