class AddonDetail < ActiveRecord::Base
  
 belongs_to :glass_addons_price
 belongs_to :eye_glass_order
 belongs_to :organisation

 before_create :add_organisation_id
 
 def is_empty?
    whitelist = %w[ color glass_addons_price_id ]
    self.attributes.all? do |attr, val|
      whitelist.exclude?(attr) || val.nil? || val == ""
    end
 end
 
 private
   def add_organisation_id
    self.organisation_id=Organisation.current.try(:id) unless self.organisation_id.present?
   end
 
end
