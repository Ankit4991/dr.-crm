##
# EyeMeasurement model is associate to Glass and GlassPrescription.

class EyeMeasurement < ActiveRecord::Base
	after_save :create_final_glass_prescription
  belongs_to :glass
  belongs_to :examination
  accepts_nested_attributes_for :glass 

  def create_final_glass_prescription
  	if finalize && self.examination.present? && self.examination.glasses_prescriptions.empty?
  	  glasses_prescription = examination.glasses_prescriptions.create(eye_measurement_id: id)
      glass_data = JSON.parse self.glass.to_json
      glass = Glass.create!(glass_data.slice( "od_sphere", "od_cylinder", "od_axis", "os_sphere", "os_cylinder", "os_axis", "od_prism", "os_prism", "od_segment_height", "os_segment_height", "od_base_curve", "os_base_curve", "ou_add"))
      glasses_prescription.glass = glass
      glasses_prescription.save!
    end
  end 
end
