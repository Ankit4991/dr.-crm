class MedicalRx < ActiveRecord::Base
  belongs_to :examination
  before_create :update_issued_expiry_date

  private
  	def update_issued_expiry_date
      if self.issued_date.nil? && self.expired_date.nil?
        self.issued_date = Time.now
      	if self.examination.organisation.setting.e_scribe_defexpirydate.present?
          months =['0.25','0.50','0.75']
          month = self.examination.organisation.setting.e_scribe_defexpirydate
          if (months.include? month)
            if self.examination.organisation.setting.e_scribe_defexpirydate == "0.25"
              self.expired_date = Time.now + 3.months
            elsif self.examination.organisation.setting.e_scribe_defexpirydate == "0.50"
              self.expired_date = Time.now + 6.months
            else
              self.expired_date = Time.now + 9.months
            end
          else  
            self.expired_date = Time.now + self.examination.organisation.setting.e_scribe_defexpirydate.to_i.year
          end
        else
          self.expired_date =  Time.now + 1.year
        end
      end
    end

end