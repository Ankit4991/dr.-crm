##
# CheckPayment model values as part of order(for 'Order' or 'FavouriteOrder' named models) payments as check mode.
# There can be multiple check payment transactions for an order and every transaction refund option has its changes audits.

class CheckPayment < ActiveRecord::Base
  
  before_create :add_organisation_id
  belongs_to :organisation
  belongs_to :doctor
  belongs_to :orderable, polymorphic: :true,dependent: :destroy

  validates :amount, :presence => true, :numericality => { :greater_than => 0 }
  validates :check_number, :presence => true, uniqueness: true

  enum status: [:paid, :refund]

  private
     def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end
end
