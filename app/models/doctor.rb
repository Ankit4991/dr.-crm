##
# Doctor is a Clinic user. That may have role staff, manager, doctor.
# Doctor will be associate to store.

class Doctor < ActiveRecord::Base
  
  devise :database_authenticatable, :recoverable, :rememberable,
         :trackable, :validatable,:confirmable, request_keys: [:subdomain]
  attr_accessor :unhashed_password

  before_create :add_organisation_id_and_theme_color
  before_create :add_organisation_subdomain
  after_create :create_schedule
  # after_create :send_confirmation_instructions

  scope :doctors_with_role_admin, -> { where("'administrator' = ANY(role)") }
  scope :doctors_with_role_doctor, -> { where("'doctor' = ANY(role)") }
  scope :doctors_with_role_super_admin, -> { where("'super_administrator' = ANY(role)") }
         
  belongs_to :organisation
  belongs_to :store  

  has_many :examinations, dependent: :destroy
  has_many :orders, dependent: :destroy
  has_many :favourite_orders
  has_many :ContactLensExam, dependent: :destroy
  has_many :leaves, class_name: "Holiday", as: :trackable, dependent: :destroy
  has_many :bookings, dependent: :destroy
  has_many :backgrounds
  has_many :eye_glass_orders, through: "orders"
  has_many :contact_lens_orders, through: "orders"
  has_many :cash_payments, through: 'orders'
  has_many :other_payments, through: 'orders'
  has_many :check_payments, through: 'orders'
  has_many :contact_lens_distribution_orders

  has_one  :working_schedule, :as => :trackable,  dependent: :destroy
  has_one :home_address,-> { where(addr_type: 'home') },class_name: 'Address'
  has_one :office_address,-> { where(addr_type: 'office') }, class_name: 'Address'  

  has_attached_file :avatar, :styles => { :medium => ["300x300>",:png], :thumb => ["100x100>",:png], :original => ["100%",:png] },
    url: "/uploads/doctors/:id/:style/:basename.:extension",
    path: ":rails_root/public/uploads/doctors/:id/:style/:basename.:extension"

  has_attached_file :sign_image, :styles => { :medium => ["300x300>",:png], :thumb => ["100x100>",:png], :original => ["100%",:png] },
    url: "/uploads/doctors/:id/:style/:basename.:extension",
    path: ":rails_root/public/uploads/doctors/:id/:style/:basename.:extension"

  has_and_belongs_to_many :notifications, dependent: :destroy
  
  validates :first_name, presence: true
  validates :theme_color, presence: true #, uniqueness: true
  #validates :mobile_number, presence: true, format: { with: /(\+\d[0-9]{10,15})+\z/, message: "invalid format!" }


  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  validates_with AttachmentSizeValidator, :attributes => :avatar, :less_than => 1.megabytes

  accepts_nested_attributes_for :home_address, allow_destroy: true
  accepts_nested_attributes_for :office_address,  allow_destroy: true

  before_create :check_email_with_subdomain

  acts_as_paranoid

  def name
    "#{first_name.sub(first_name.chr,first_name.chr.upcase)} #{last_name.sub(last_name.chr,last_name.chr.upcase)}" if first_name.present? && last_name.present?
  end 

  ransacker :full_name do |parent|
    Arel::Nodes::InfixOperation.new('||',Arel::Nodes::InfixOperation.new('||',parent.table[:first_name], Arel::Nodes.build_quoted(' ')),parent.table[:last_name])
  end
   
  #this is for devise login with subdomain
  def self.find_for_authentication(warden_conditions)
    warden_conditions[:email].downcase!
    subdomain = warden_conditions[:subdomain].present? ? (warden_conditions[:subdomain].include?('.') ? warden_conditions[:subdomain].split('.')[0] : warden_conditions[:subdomain]) : ''
    where(:email => warden_conditions[:email], :subdomain => subdomain).first
  end

  def self.from_google_omniauth(access_token,org_id)
    @org = Organisation.find(org_id)
    data = access_token.info
    @doctor = @org.doctors.where(:email => data["email"]).first
    unless @doctor
      @doctor = @org.doctors.new
      @doctor.first_name = data["first_name"]
      @doctor.last_name = data["last_name"]
      @doctor.email = data["email"]
      @doctor.password = Devise.friendly_token[0,20]
      @doctor.avatar = open(data["image"])
      @doctor.save!
      @doctor.confirm!
    end
    @doctor
  end

  private
    def create_schedule
      self.create_working_schedule
    end
    
    def add_organisation_subdomain
      self.subdomain = Organisation.current.try(:domain_name) || "brickellvision"  unless self.subdomain.present?
    end
    
    def add_organisation_id_and_theme_color      
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
      self.theme_color = "##{'%06x' % (rand * 0xffffff)}"
    end

    def check_email_with_subdomain
      count = Doctor.where(email: self.email,subdomain: self.subdomain).count
      if count > 0
        raise "This email is already taken by a staff member, please use a different email."
      end
    end

end
