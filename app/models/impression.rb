##
# ImpressionModel has some records that we used in ExamImpression

class Impression < ActiveRecord::Base
	before_create :add_organisation_id
  belongs_to :organisation
  
  validates :name, :presence => true
  validates :code, :presence => true
  
  private
    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end
end