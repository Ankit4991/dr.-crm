class Document < ActiveRecord::Base
  belongs_to :examination
  belongs_to :setting
  belongs_to :patient
  belongs_to :family_member
  has_attached_file :docs, styles: lambda { |a| (a.instance.docs_content_type =~ %r(image) ? {thumb: ["200x150#", :png], original: ["100%",:png]}  : (a.instance.docs_content_type == 'application/pdf' ? {thumb: ["200x150#", :png]}  : {})) },
    url: "/uploads/documents/:id/:style/:basename.:extension",
    path: ":rails_root/public/uploads/documents/:id/:style/:basename.:extension"

  validates_attachment_content_type :docs, :content_type => [/\Aimage\/.*\Z/,"text/html", "application/pdf" ], :message => 'Please use an image, html or PDF file'
  validates_with AttachmentSizeValidator, :attributes => :docs, :less_than => 10.megabytes

  after_validation :clean_paperclip_errors

  private
    def clean_paperclip_errors
      errors.delete(:docs)
    end
end
