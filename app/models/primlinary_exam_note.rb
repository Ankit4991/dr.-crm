class PrimlinaryExamNote < ActiveRecord::Base
  belongs_to :primlinary_exam

  before_create :set_defaults #before creating object of this model associated set default value of fields from settings exam form defaults. 

  def set_defaults
  	def_values = ExamFormDefault.where(:organisation_id => Organisation.current.try(:id)).first
	self.notes = def_values.prelm_notes_def
  end
end
