##
# InternalExam is a sub section of Exam form

class InternalExam < ActiveRecord::Base
	belongs_to :examination

	before_create :set_defaults #before creating object of this model associated set default value of fields from settings exam form defaults. 

	def set_defaults
		def_values = ExamFormDefault.where(:organisation_id => Organisation.current.try(:id)).first
		self.od = def_values.od_def
		self.os = def_values.os_def
		self.post_pole = def_values.post_pole_def
		self.periphery = def_values.periphery_def
		self.vitreous = def_values.vitreous_def
		self.vessels = def_values.vessels_def
		self.macula = def_values.macula_def
		self.reason_field = def_values.reason_field_def
		self.dilation_od = def_values.dilation_od_def
		self.dilation_os = def_values.dilation_os
	end
end