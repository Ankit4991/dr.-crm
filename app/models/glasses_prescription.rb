##
# GlassesPrescription is final result that Doctor get after his analysis and experiment on exam form. 
# GlassesPrescription also knows as "Final Glass Prescription" . This is also a part of exam form.

class GlassesPrescription < ActiveRecord::Base
  before_save :update_issued_expiry_date
  belongs_to :eye_measurement
  belongs_to :glass
  accepts_nested_attributes_for :eye_measurement, reject_if: :all_blank
  accepts_nested_attributes_for :glass, reject_if: :all_blank
  has_and_belongs_to_many :doctor_recommendations
  belongs_to :examination 
  
  private
    def update_issued_expiry_date
      if self.eye_measurement.finalize == true && self.issued_date.nil? && self.expired_date.nil?
        self.issued_date = Time.now
        if self.examination.organisation.setting.default_expiry.to_i.year.present?
          self.expired_date = Time.now + self.examination.organisation.setting.default_expiry.to_i.year
        else
          self.expired_date = Time.now + 1.year
        end
      end
    end
end
