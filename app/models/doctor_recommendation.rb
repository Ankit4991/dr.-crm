##
# DoctorRecommendation Model has some static records that Doctor can recommend to Patient in his Exam form.

class DoctorRecommendation < ActiveRecord::Base
  belongs_to :glasses_prescription
  belongs_to :organisation
end
