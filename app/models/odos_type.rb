class OdosType < ActiveRecord::Base
  enum dominated_eye: {
    od: 1,
    os: 2
  }
end
