##
# App is SAAS base. SO any Eyecare organisation that want to use this app can request.
# All data will flow coressponding to Organisation

class Organisation < ActiveRecord::Base
  SIZE = [ "Less than 10 employees", "11 to 50 employees", "51 to 250 employees", "251 to 1000 employees", "1001 to 10000 employees", "10000+ employees"]
  before_validation :downcase_subdomain

  belongs_to :owner, class_name: Doctor
  
  has_many :patients #please dont add dependent destroy
  has_many :family_members #please dont add dependent destroy
  has_many :stores, dependent: :destroy
  has_many :doctors, dependent: :destroy
  has_many :diseases, dependent: :destroy
  has_many :exam_procedures, dependent: :destroy
  has_many :impressions, dependent: :destroy
  has_many :doctor_recommendations, dependent: :destroy
  has_many :exam_cleaning_solution, dependent: :destroy
  has_many :lens_types_prices, dependent: :destroy
  has_many :eyeglass_types_prices, dependent: :destroy
  has_many :glass_frames_prices, dependent: :destroy
  has_many :glass_addons_prices, dependent: :destroy
  has_many :lens_materials, dependent: :destroy
  has_many :orders, dependent: :destroy
  has_many :bookings, dependent: :destroy
  has_many :eye_glass_orders
  has_many :contact_lens_orders
  has_many :surveys,  dependent: :destroy
  has_many :examinations, dependent: :destroy
  has_many :contact_lens_exams
  has_many :lens_prescriptions
  has_many :addon_details
  has_many :favourite_orders
  has_many :holidays, dependent: :destroy
  has_many :dispense_logs, dependent: :destroy
  has_many :bookings,dependent: :destroy
  has_many :lenses, class_name: "Lens"
  has_many :glasses, dependent: :destroy
  has_many :contact_forms, dependent: :destroy
  has_many :logs, dependent: :destroy
  has_many :twilio_webhooks, dependent: :destroy
  has_many :joint_resources, dependent: :destroy
  has_many :order_subscriptions, dependent: :destroy
  has_many :order_transactions,dependent: :destroy
  has_many :mailgun_webhooks,dependent: :destroy
  has_many :cash_payments,dependent: :destroy
  has_many :check_payments, dependent: :destroy
  has_many :contact_lens_distribution_order
  has_many :other_payments,dependent: :destroy

  has_one  :setting, dependent: :destroy

  #has_one :address, dependent: :destroy
  has_one :address,-> { where(addr_type: 'org_address') },class_name: 'Address', dependent: :destroy
  
  has_many :vision_insurances, dependent: :destroy
  has_many :medical_insurances, dependent: :destroy
  
  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "150x150>", :thumbnail => "30*30" },
    url: "/uploads/organisations/:id/:style/:basename.:extension",
    path: ":rails_root/public/uploads/organisations/:id/:style/:basename.:extension"
    
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  validates_with AttachmentSizeValidator, :attributes => :avatar, :less_than => 1.megabytes
  
  validates_presence_of :domain_name
  validates_presence_of :company_name
  validates_format_of :domain_name, :with => /^[a-z\d]+([-_][a-z\d]+)*$/i, :message => 'The subdomain can only contain alphanumeric characters and dashes.', :allow_blank => true, :multiline => true
  validates_uniqueness_of :domain_name, :case_sensitive => false
  validates_exclusion_of :domain_name, :in => %w( support blog www billing help api ), :message => "The subdomain <strong>{{value}}</strong> is reserved and unavailable."

  accepts_nested_attributes_for :owner
  accepts_nested_attributes_for :address, allow_destroy: true

  has_one :exam_form_default
  after_create :set_exam_form_default
  
  def self.current
    Thread.current[:organisation]
  end

  def self.current=(organisation)
    Thread.current[:organisation] = organisation
  end

  def self.from_google_omniauth(access_token)
    data = access_token.info
    @org = Organisation.where(domain_name: data["first_name"])
    if @org.present?
      @doctor = @org.doctors.where(:email => data["email"]).first
      unless @doctor
        @doctor = @org.doctors.new
        @doctor.first_name = data["first_name"]
        @doctor.last_name = data["last_name"]
        @doctor.email = data["email"]
        @doctor.password = Devise.friendly_token[0,20]
        @doctor.avatar = open(data["image"])
        @doctor.save!
        @doctor.confirm!
      end
    else
      @doctor = Doctor.new
      @doctor.first_name = data["first_name"]
      @doctor.last_name = data["last_name"]
      @doctor.email = data["email"]
      @doctor.password = Devise.friendly_token[0,20]
      @doctor.avatar = open(data["image"])
      @doctor.subdomain = data["first_name"]
      @doctor.role = ['administrator']
      @doctor.save!
      @doctor.confirm!
      @org = Organisation.new
      @org.domain_name = data["first_name"]
      @org.company_name = data["first_name"]
      @org.email = data["email"]
      @org.owner_id = @doctor.id
      @org.admins = [@doctor.id]
      @org.build_address(country_id: Organisation.first.address.country_id, state_id: Organisation.first.address.state_id)
      @org.save!
      @doctor.organisation_id = @org.id
      @doctor.save!
      AppMailer.send_org_create_to_admin(@org).deliver_now
    end
    @doctor
  end

  def set_exam_form_default
    check_existence = ExamFormDefault.where(:organisation_id => self.id).first
    if check_existence.nil? 
      ExamFormDefault.create(ctd_def: 'Ortho',ctn_def: '3XP', nra_def: '+2.50', pra_def: '-2.50', npc_def: 'TTN', notes_def: '', hpd_def: 'Ortho', hpn_def: '3XP', vpd_def: 'Ortho', vpn_def: 'Ortho', eoms_def: 'SAFE OD / OS', pupils_def: 'PERRL (-) APD', confrontational_def: 'FTFC OD / OS', color_vision_def: '8/8', steropsis_def: '',prelm_notes_def: '', tear_film_def: 'Clear OU', cornea_def: '+FL large demarcation stain, -stroma involvement', iris_def: 'Flat OU', bulbar_conj_def: 'Clear OU', palpebral_conj_def: 'Clear OU', sclera_def: 'Clear OU', angles_od_def: '', angles_os_def: '', lids_lashes_def: 'Mod MGD OU', ac_def: 'D/Q OU', lens_def: 'Clear OU', visual_field_def: '', od_def: '0.3', vessels_def: '2/3 OU', os_def: '0.3', macula_def: '(+) Foveal Reflex OU', post_pole_def: 'Clear OU', vitreous_def: 'Clear OU', periphery_def: 'Clear OU', reason_field_def: '', dilation_od_def: 'OD 90D Fundus/ 20D fundus lens 360 SD to Ora (No Breaks)', dilation_os: 'OS 90D Fundus/ 20D fundus lens 360 SD to Ora (No Breaks)', dilation_fundus_def: 'Patient refuses dilated fundus examination after explanation of tests',organisation_id: self.id)
    end 
  end

  protected

    def downcase_subdomain
      self.domain_name.downcase! if attribute_present?("domain_name")
    end
end
