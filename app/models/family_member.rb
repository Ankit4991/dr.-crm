##
# FamilyMember is associated to Patient. And Patient can have many family member. Family member can have same feature(Exam form, order, reports) like patient. 
# But family member dont have own account. Patient can login and access his family member reports and other information.

class FamilyMember <  ActiveRecord::Base
  include CommonPatientAndFamily
  after_create :create_joint_resource
  before_create :add_organisation_id
  after_destroy :remove_associated_audits
  
  belongs_to :patient
  belongs_to :vision_insurance
  has_one :joint_resource,-> { where("family_member_id IS NOT NULL") },  dependent: :destroy
  has_many :documents, dependent: :destroy

  validates :first_name, presence: true
  has_attached_file :avatar, :styles => { :medium => ["300x300>",:png], :thumb => ["100x100>",:png], :original => ["100%",:png] },
    url: "/uploads/family_members/:id/:style/:basename.:extension",
    path: ":rails_root/public/uploads/family_members/:id/:style/:basename.:extension"
  
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  validates_with AttachmentSizeValidator, :attributes => :avatar, :less_than => 1.megabytes
  delegate :family_members,to: :patient
  delegate :about,to:  :patient
  audited
  
  ransacker :family_full_name do |parent|
    Arel::Nodes::InfixOperation.new('||',Arel::Nodes::InfixOperation.new('||',parent.table[:first_name], Arel::Nodes.build_quoted(' ')),parent.table[:last_name])
  end

  ransacker :updated_at, type: :date do
    Arel.sql('family_members.updated_at::date')   # filter only the date
  end

  def email
    self.patient.email
  end

  def create_joint_resource
    JointResource.create!(patient_id: self.patient_id, family_member_id: self.id)
  end
  
  def all_finalized_orders
    self.joint_resource.orders.finalized
  end
  
  def all_removed_orders
    self.joint_resource.orders.dispensed
  end
  
  def prev_audits
    self.audits.map{|audit| [audit.auditable_type,audit.created_at,audit.audited_changes,audit.try(:user).try(:name),audit.try(:user).try(:class).try(:name)]}
  end

  def all_resources
    JointResource.where(family_member_id: self.id)
  end

  def examinations
    Organisation.find(organisation_id).examinations.where(joint_resource_id: all_resources.map(&:id))
  end

  def all_prescriptions
    examinations.map{ |exam| exam if exam.glasses_prescriptions.present? || exam.lens_prescriptions.present? }.compact
  end

  def all_orders_reports
    Order.joins(:joint_resource).where("joint_resources.family_member_id =?",self.id).order(:updated_at => :desc)
  end

  def med_rx
    @med_rx_count = 0
    examinations.each do |ex| 
      if ex.medical_rxes.present?
        @med_rx_count = @med_rx_count + ex.medical_rxes.count
      end
    end
    return @med_rx_count
  end

  def total_report_count
    referrals.count + all_orders_reports.count + all_prescriptions.count + med_rx
  end

  private

	  def add_organisation_id
	      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
	  end

    def remove_associated_audits
      self.audits.map{|adt| adt.delete} if self.audits.present?
    end
end
