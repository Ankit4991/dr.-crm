#model will be used to keep record of twilio and contact_form email
class Log < ActiveRecord::Base
  before_create :add_organisation_id
  after_create :create_notifications

  belongs_to :organisation
  belongs_to :trackable, polymorphic: :true, dependent: :destroy
 
  has_one :notification, :as => :event 
  delegate :name, to: :trackable
  alias_attribute :comment, :message
  ## validations
  validates :trackable_id, :trackable_type, presence: true
  validates :trackable_type, inclusion: { in: %w(ContactForm Booking TwilioWebhook), message: "%{value} is not a valid trackable_type" }
  
  private
    def add_organisation_id
      self.organisation_id = self.trackable.organisation_id
    end

    def create_notifications
      if action == "recive"
        doctors = self.organisation.doctors.map(&:id)

        twilio_webhook = TwilioWebhook.find self.trackable_id
        @patient_id = Patient.where(phone: twilio_webhook.from).try(:first).try(:id)

        self.create_notification!("doctor_ids"=>doctors, status: false, patient_id: @patient_id)
        self.notification.doctors_notifications.map{|x| x.update(status: false)}
      end
    end
end
