##
# Lens value are use in Exam Form at various section(Contact Lens, Final Lens Prescription)
# Lens value are also use in Order Form at various section(Contact Lens Order have many lenses, Final Lens Prescription Lens can be prepopulated to a contact lens order lens)
# This model value changes is recorded as its audits.

class Lens < ActiveRecord::Base
	self.table_name="lens"
	after_save :create_final_lens_prescription  ,:set_modality_period
  before_create :add_organisation_id

  belongs_to :contact_lens_exam
  belongs_to :visual_acuity
  belongs_to :contact_lens_order
  belongs_to :organisation
  belongs_to :od_lens_type_price, class_name: "LensTypesPrice", foreign_key: "od_lens_types_price_id"
  belongs_to :os_lens_type_price, class_name: "LensTypesPrice", foreign_key: "os_lens_types_price_id"
  accepts_nested_attributes_for :visual_acuity
  audited :associated_with => :contact_lens_exam
  has_one :contact_lens_distribution_order, dependent: :destroy
  
  def create_final_lens_prescription
  	if self.contact_lens_exam.present? && self.contact_lens_exam.examination.present?
  	  examination= self.contact_lens_exam.examination
  	  if finalize && examination.lens_prescriptions.empty?
        finalize_lens = self.dup
        finalize_lens.contact_lens_exam_id = nil
        finalize_lens.save
        examination.lens_prescriptions.create(lens_id: finalize_lens.id)
      end
    end
  end
  
  def is_empty?
    whitelist = %w[ od_type os_type]
    self.attributes.all? do |attr, val|
      whitelist.exclude?(attr) || val.nil? || val == ""
    end
  end

  private
    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end
    
    def set_modality_period
      lens_prescription = LensPrescription.find_by_lens_id(self.id)
      if lens_prescription.present?
        lens_prescription.modality_period =  [lens_prescription.lens_od_modality_period,lens_prescription.lens_os_modality_period].max
        lens_prescription.save
      end
    end
end
