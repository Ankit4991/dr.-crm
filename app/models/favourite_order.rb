##
# FavouriteOrder model is use to store Patient Renewed Contact Lens Orders or One-click buyed orders.
# Payments of these orders are made using profile card only(as it is created only at the patient side).

class FavouriteOrder < ActiveRecord::Base
  
  include CommonOrderAndFavouriteOrder
  
  audited only: [:status]
  enum status: [:ordered,:ready_for_pick_up,:finalized,:follow,:dispensed]
  enum shippment_method: [:free_shipping,:pick_up_at_office]

  before_create :add_organisation_id
  after_create :send_confirmation, :set_uuid
  after_destroy :remove_associated_audits

  belongs_to :doctor
  belongs_to :organisation
  belongs_to :joint_resource
  belongs_to :lens_prescription
  belongs_to :order_subscription
  
  has_many :transactions,as: :orderable,dependent: :destroy, :class_name => "OrderTransaction"
  
  has_many :dispense_logs,as: :dispensable,foreign_key: :dispensable_id,dependent: :destroy
  
  validate :save_object?, :on => :create

  store_accessor :lens_orders, :lens_od_quantity,:lens_os_quantity,:lens_od_unit_price,:lens_os_unit_price

  def adjustments
    (subtotal - self.total).round(2)
  end

  def subtotal
    (self.lens_orders['lens_od_unit_price'].to_f * self.lens_orders['lens_od_quantity'].to_f) + (self.lens_orders['lens_os_quantity'].to_f * self.lens_orders['lens_os_unit_price'].to_f)
  end

  def last_dispensed_on
    status_audit = self.audits.order('created_at').detect{ |adt| adt.audited_changes['status'] !=nil and adt.audited_changes['status'].kind_of?(Array) and adt.audited_changes['status'].include?('ready_for_pick_up') }
    return status_audit.try(:created_at)
  end

  private
    
    def remove_associated_audits
      self.audits.map{|adt| adt.delete} if self.audits.present?
    end
    
    def save_object?
      unless self.joint_resource.blank? || !self.class.exists?(:joint_resource_id => self.joint_resource.id)
        unless ['dispensed'].include? self.joint_resource.favourites.last.status
          errors[:attribute] << "Last lens order not delivered yet !!"
          return false
        end
      end
    end

    def send_confirmation
      patient_reward_update
      AppMailer.send_renewed_order_confirmation_to_patient(self).deliver_later unless self.try(:joint_resource).try(:patient).try(:email_not_supplied)
    end

    def patient_reward_update
      patient = self.joint_resource.patient
      remain_reward = (patient.reward_point.to_f - self.reward_points_used)
      patient.update_attributes!(reward_point: remain_reward)
    end

    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end

    def set_uuid
      self.uuid = "REO#{self.id}"
    end
end
