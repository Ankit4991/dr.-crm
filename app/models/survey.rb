##
# When patient comes first time in clinic, we sent a survey email. And after that patient give rating and feedback.

class Survey < ActiveRecord::Base
  attr_accessor :survey_token
  before_create :add_organisation_id
  belongs_to :patient
  belongs_to :organisation
  validates :patient_id, :presence => {:message => 'Patient cannot be blank!'}

  def overall_rating
    ((self.professionalism + self.work_quality + self.schedule_adherence + self.communication + self.cleanliness + self.staff)/6.0).round(2)
  end
  
  private
    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end
end
