##
# PrimlinaryExam model is a part of Exam form.

class PrimlinaryExam < ActiveRecord::Base
	belongs_to :examination
	has_many :primlinary_exam_notes, dependent: :destroy

	accepts_nested_attributes_for :primlinary_exam_notes

	before_create :set_defaults #before creating object of this model associated set default value of fields from settings exam form defaults. 

	def set_defaults
		def_values = ExamFormDefault.where(:organisation_id => Organisation.current.try(:id)).first
		self.eoms = def_values.eoms_def
		self.pupils = def_values.pupils_def
		self.confrontational = def_values.confrontational_def
		self.color_vision = def_values.color_vision_def
		self.steropsis = def_values.steropsis_def
	end

end
