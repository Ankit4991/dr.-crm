##
# CashPayment model values as part of order(for 'Order' or 'FavouriteOrder' named models) payments as cash mode.
# There can be multiple cash payment transactions for an order and every transaction refund option has its changes audits.

class CashPayment < ActiveRecord::Base
  
  before_create :add_organisation_id
  belongs_to :organisation
  belongs_to :doctor
  belongs_to :orderable, polymorphic: :true,dependent: :destroy

  validates :amount, :presence => true, :numericality =>  { greater_than_or_equal_to: 0 }
  enum status: [:paid, :refund]

  private
     def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end
end
