##
# CornealDisease Model values are part of Exam form => External Exam form.

class CornealDisease < ActiveRecord::Base
	belongs_to :external_exam
	has_and_belongs_to_many :diseases
end
