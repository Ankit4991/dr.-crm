##
# Ability use to set app Authorization

class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= Doctor.new # guest user (not logged in)

    if user.role.include?("doctor")
      can :manage, [:doctor,:booking]
      cannot :manage, [:doctor,:manage_organisation]
      cannot :manage, [:doctor,:seo]
      cannot :manage, [:doctor,:setting]
      cannot :manage, [:doctor,:store]
      cannot :manage, [:doctor,:manage_patient]
      cannot :manage, [:doctor,:campaign]
      cannot :manage, [:doctor,:analytic]
      cannot :manage, [:doctor,:follow_up]
      cannot :manage, [:doctor,:dispense_log]
      cannot :manage, [:doctor,:store]
    end

    if user.role.include?("staff")
      can :manage, :all
      cannot [:index, :update, :edit], [:doctor,:manage_organisation]
      cannot :manage, [:doctor,:seo]
    end

    if user.role.include?("administrator")
      can :manage, :all
      cannot [:index], [:doctor,:manage_organisation]      
      cannot :manage, [:doctor,:seo]      
    end

    if user.role.include?("super_administrator")
      can :manage, :all
    end
    
  end
end
