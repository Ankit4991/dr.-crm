##
# ExamProcedure has some records that have HABTM with Examination.
# ExamProcedure added in Examination form define the cost of examination form.

class ExamProcedure < ActiveRecord::Base
	audited :associated_with => :examination
	belongs_to :order
	belongs_to :organisation
	belongs_to :examination
	before_create :add_organisation_id
	
	validates :name, :presence => true
	validates :code, :presence => true
	validates :price, :presence => true
  
	def code_with_name
		"#{code} #{name}"
	end

  private
    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end

end
