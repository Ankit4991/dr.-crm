##
# DispenseLog model values is used when an order is ready to be dispensed.There can be multiple dispense logs for an order.
# Doctor can send notification to patient either manually or automated mail after every multiple of 2 days(like 2,4,6 and so on)

class DispenseLog < ActiveRecord::Base
  include DateTimeAttribute
  date_time_attribute :logged_at

  belongs_to :dispensable, polymorphic: :true,dependent: :destroy
  belongs_to :organisation

  before_create :add_organisation_id

  validates :dispensable_id, :dispensable_type, presence: true               # can be for Order model or FavouriteOrder model
  validates :note, :presence => {:message => 'Note cannot be blank, log not saved'}
  
  private
    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end
  
end
