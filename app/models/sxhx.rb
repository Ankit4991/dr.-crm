class Sxhx < MedicalHistory
  belongs_to :patient
  belongs_to :family_member
  audited :associated_with => :patient, only: [:disease_id,:notes]
end
