##
# Booking Model is responsible to store Booking(Appointment)

class Booking < ActiveRecord::Base
  include Bookable
  include ActionView::Helpers::NumberHelper
  audited only: [:start_time, :store_id, :doctor_id]
  attr_accessor :audits_count_before_update
  
  acts_as_paranoid  #Used to track deleted bookings datetime and you need to query bookings.with_deleted for all canceled bookings
  
  validates :doctor_id, presence: true 

  has_and_belongs_to_many :exam_procedures  
  has_many :mailgun_webhook, :as => :trackable,  dependent: :destroy
  has_one :notification, :as => :event
  has_one :examination
  belongs_to :destroyer, polymorphic: :true
  belongs_to :organisation
  belongs_to :joint_resource
  belongs_to :store
  belongs_to :doctor

  before_create :add_organisation_id
  before_create :set_end_time  #, :set_status
  after_create :create_notifications
  after_destroy :change_delete_value_notification, :change_exam_order_status
  after_save :send_sms_email_notification
  before_save :set_audit_count
  after_real_destroy :remove_associated_audits
         
  
  accepts_nested_attributes_for :notification
  enum status: [:pending, :on_time, :late, :early, :no_show, :finalized, :nil]

  scope :account_future_bookings_find_by_patient_id, -> (patient_id) { joins(:joint_resource).where(joint_resources:{patient_id: patient_id}).where("start_time>?", Time.now) }
  #Below scope for fetching Today's Appointments excluding canceled bookings.
  scope :fetch_today_bookings, -> { where("deleted_at IS NULL") }

  private
    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end

    def create_notifications
      doctors=(self.organisation.admins<<self.doctor.id.to_s).uniq
      self.create_notification!("doctor_ids"=>doctors, patient_id: self.joint_resource.patient.id, status: false)
      self.notification.doctors_notifications.map{|x| x.update(status: false)}
    end

    def change_delete_value_notification
      self.notification.update(is_deleted: true, status: false) if self.notification.present?
      self.notification.doctors_notifications.map{|x| x.update(status: false)} if self.notification.present? && self.notification.doctors_notifications.present?
    end
    
    def change_exam_order_status
      examination = self.examination
      if examination.present?
        examination.status = 'removed'
        examination.save
      end
    end

    #Appointment Create/update sms and email notification will be sent through here
    def send_sms_email_notification
      unless self.really_destroyed?
        patient = self.try(:joint_resource).try(:patient)
        if patient.present?
          one_click_url=Rails.application.routes.url_helpers.root_url(host: CrmDatabase::Application.config.action_mailer.default_url_options[:host], sudomain: patient.try(:subdomain),  patient_email: patient.try(:email), authentication_token: patient.try(:authentication_token), subdomain: patient.try(:subdomain))
          one_click_url.gsub!(patient.try(:subdomain), patient.try(:subdomain) + ".dev") if Rails.env.include?('staging') && patient.try(:subdomain) != 'dev'
          one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url

          ret = Sidekiq::ScheduledSet.new
          jobs = ret.map{|a| a if a.klass == "ActiveJob::QueueAdapters::SidekiqAdapter::JobWrapper"}.compact! || []
          jobsms = ret.map{|a| a if a.klass == "TwilioSmsWorker"}.compact! || []
          jobs.each do |job|
            if job.args.first["arguments"][3] == self.id
              job.delete
            end
          end
          jobsms.each do |job|
            if job.args[0]["trackable_id"] == self.id
              job.delete
            end
          end
          AppMailer.send_booking_confirmation(self.id).deliver_later(wait: 1.minute) unless patient.try(:email_not_supplied)
          if patient.home_address == nil 
            notification_message = AppConfig.sms_notification["appointment_create"] % {patient_name: self.joint_resource.try(:name), doctor_name: self.doctor.name, appointment_time: self.start_time.strftime('%m/%d/%Y %I:%M%P'), one_click_login: one_click_url, store_name: self.store.description , store_address: "#{self.store.try(:office_address).try(:address1)}, #{self.store.try(:office_address).try(:address2)}, postal code #{self.store.try(:office_address).try(:postal_code)}", store_phone_number: "#{number_to_phone(self.store.phone_number.to_s[2..-1].to_i, area_code: true)}", store_office_homepage: "#{self.store.url}"}
          else
            notification_message = AppConfig.sms_notification["appointment_create_sign_up"] % {patient_name: self.joint_resource.try(:name), doctor_name: self.doctor.name, appointment_time: self.start_time.strftime('%m/%d/%Y %I:%M%P'), one_click_login: one_click_url, store_name: self.store.description , store_address: "#{self.store.try(:office_address).try(:address1)}, #{self.store.try(:office_address).try(:address2)}, postal code #{self.store.try(:office_address).try(:postal_code)}", store_phone_number: "#{number_to_phone(self.store.phone_number.to_s[2..-1].to_i, area_code: true)}", store_office_homepage: "#{self.store.url}"}
          end
          TwilioSmsWorker.perform_async({'src'=> self.organisation.twilio_number, 'dst' => patient.phone, 'text' => notification_message, 'trackable_id'=> self.id, 'trackable_type'=> 'Booking', 'notification_method'=> patient.notification_method,'org_sid'=> self.organisation.twilio_acc_sid,'org_auth_token'=> self.organisation.twilio_auth_token})
        end
      end
    end

    def set_audit_count
      self.audits_count_before_update = self.audits.count
    end

    def set_end_time
      unless self.end_time.present?
        self.end_time = self.start_time + 30.minutes
      end
    end

    def remove_associated_audits
      self.audits.map{|adt| adt.delete}
    end
    # def set_status
    #   unless self.status.present?
    #     self.status = "coming"
    #   end
    # end


end
