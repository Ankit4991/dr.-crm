class ProcedureAdjustment < ActiveRecord::Base
  belongs_to :examination
  belongs_to :order
  belongs_to :exam_procedure
  after_save :update_price
  before_save :set_default_price_and_order
  after_destroy :update_price

  delegate :name, to: :exam_procedure, :allow_nil => true
  delegate :code, to: :exam_procedure, :allow_nil => true
  delegate :price,:prefix => 'actual', to: :exam_procedure, :allow_nil => true


  private

    def set_default_price_and_order
      unless self.examination.try(:order).blank?
        self.order = self.examination.try(:order)
      end
      self.price ||= self.exam_procedure.try(:price)
    end

    def update_price
      if self.order.present?
        all_glass_orders_total = self.order.eye_glass_orders.where(:hold => false).sum(:total)
        all_lens_orders_total = self.order.contact_lens_orders.where("total > 0 AND hold = false").sum(:total)
        exam_copay = self.order.exam_copay
        self.order.update_columns(:total => (all_glass_orders_total + all_lens_orders_total + exam_copay).round(2),:updated_at => Time.now.utc)
      end
    end
end
