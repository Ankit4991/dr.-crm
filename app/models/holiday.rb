##
# Holiday is polymorphic type. That belongs to Doctor leave and Clinic Day clousre.

class Holiday < ActiveRecord::Base
  before_create :add_organisation_id
  
  belongs_to :doctor
  belongs_to :trackable, polymorphic: :true
  belongs_to :organisation
  validates :date, presence: true 
  validates :trackable_id, :trackable_type, presence: true
  validates :trackable_type, inclusion: { in: %w(Doctor Store), message: "%{value} is not a valid trackable_type" }
  scope :comming,lambda { where("date >= ? ",Time.now )}
  
  private
    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end
end
