##
# LensTypesPrice store lens type with price and we will use in Lens prescription as a lens type.

class LensTypesPrice < ActiveRecord::Base
  belongs_to :organisation
  before_create :add_organisation_id
  after_save :update_lens_prescriptions
  
  validates :brand_name, :presence => true
  validates :price, :presence => true
  
  private
    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end
    
    #This callback is used to trigger update callback for each lens prescription having this lens types price 
    def update_lens_prescriptions
      # lens_prescriptions_in_use = self.organisation.lens_prescriptions.where('created_at::date >= ?',Date.today - 1.year).flatten
      # lens_prescriptions_in_use.each do |lens_prescription|
      #   lens_prescription.lens.save
      # end
      LensPriceUpdatesWorker.new.perform self
    end
end
