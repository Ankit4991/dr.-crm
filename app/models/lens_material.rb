class LensMaterial < ActiveRecord::Base
   belongs_to :eye_glass_order
   before_create :add_organisation_id
   
   validates :name, :presence => true
   validates :price, :presence => true
   
   def material_prices
    "#{name} &nbsp;&nbsp;&nbsp;~~> $#{price}".html_safe
   end
  
  private
    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end
end
