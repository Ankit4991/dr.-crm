##
# ExternalExam valuse are use in Examination form. This will contain  one tab(section) info of Examination Form. 

class ExternalExam < ActiveRecord::Base
	belongs_to :examination
	has_one :gonioscopy, dependent: :destroy
	has_one :corneal_disease, dependent: :destroy
	has_many :gats
	has_many :ncts
  
	accepts_nested_attributes_for :gonioscopy, reject_if: :all_blank
	accepts_nested_attributes_for :corneal_disease, reject_if: :all_blank
	accepts_nested_attributes_for :gats
	accepts_nested_attributes_for :ncts

	before_create :set_defaults #before creating object of this model associated set default value of fields from settings exam form defaults. 

	def set_defaults
		def_values = ExamFormDefault.where(:organisation_id => Organisation.current.try(:id)).first
		self.tear_film = def_values.tear_film_def
		self.cornea = def_values.cornea_def
		self.iris = def_values.iris_def
		self.bulbar_conj = def_values.bulbar_conj_def
		self.palpebral_conj = def_values.palpebral_conj_def
		self.sclera = def_values.sclera_def
		self.angles_od = def_values.angles_od_def
		self.angles_os = def_values.angles_os_def
		self.lids_lashes = def_values.lids_lashes_def
		self.ac = def_values.ac_def
		self.lens = def_values.lens_def
		self.visual_field = def_values.visual_field_def
	end
end
