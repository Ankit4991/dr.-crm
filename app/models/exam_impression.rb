##
# ExamImpression model Values are parts of Exam Form => Procedure Impression/plan

class ExamImpression < ActiveRecord::Base
	attr_accessor :checked
	belongs_to :impression
	belongs_to :examination
end
