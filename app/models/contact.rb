class Contact < ActiveRecord::Base
	belongs_to :store

	belongs_to :contact_address, class_name: "Address"
	
	accepts_nested_attributes_for :contact_address,  allow_destroy: true
	has_attached_file :profile_image, :styles => { :medium => ["300x300>",:png], :thumb => ["100x100>",:png], :original => ["100%",:png] },
    url: "/uploads/contacts/:id/:style/:basename.:extension",
    path: ":rails_root/public/uploads/contacts/:id/:style/:basename.:extension"
    validates_attachment_content_type :profile_image, :content_type => /\Aimage\/.*\Z/

end
