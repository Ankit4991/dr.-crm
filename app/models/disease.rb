##
# Disease Model have list of diseases, that use in Patient Medical History.

class Disease < ActiveRecord::Base
  serialize group: Array

  before_create :add_organisation_id  
            
  belongs_to :corneal_disease
  belongs_to :organisation

  has_and_belongs_to_many :social_history

  validates :name, :presence => true

  scope :default_medical, -> { where("'medical' = ANY(groups) and default_active = true" ) }
  scope :default_oculus, -> { where("'oculus' = ANY(groups) and default_active = true") }
  scope :default_allergies, -> { where("'allergy' = ANY(groups) and default_active = true") }
  scope :default_psychologies, -> { where("'psychology' = ANY(groups) and default_active = true") }
  scope :default_surgeries, -> { where("'surgery' = ANY(groups) and default_active = true") }
  scope :default_corneas, -> { where("'corneal' = ANY(groups) and default_active = true") }

  scope :medical, -> { where("'medical' = ANY(groups)") }
  scope :oculus, -> { where("'oculus' = ANY(groups)") }
  scope :allergies, -> { where("'allergy' = ANY(groups)") }
  scope :psychologies, -> { where("'psychology' = ANY(groups)") }
  scope :surgeries, -> { where("'surgery' = ANY(groups)") }  
  scope :prescribing_medication, -> { where("'prescribing_medication' = ANY(groups)") }
  scope :dosage, -> { where("'dosage' = ANY(groups)") }

  private
    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end
  
end

