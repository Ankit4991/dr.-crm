module CommonSuborder
  extend ActiveSupport::Concern

  included do

    after_save :update_order_total
    after_destroy :update_order_total 
    
    def subtotal
      (total.to_f + overall_discount.to_f).round(2)
    end
    
    private
      def update_order_total
        all_glass_orders_total = self.order.eye_glass_orders.where(:hold => false).sum(:total)
        all_lens_orders_total = self.order.contact_lens_orders.where("total > 0 AND hold = false").sum(:total)
        exam_copay = self.order.exam_copay
        self.order.update_attributes(:total => (all_glass_orders_total + all_lens_orders_total + exam_copay).round(2),doctor: self.doctor)
      end
  end
end