module CommonOrderAndFavouriteOrder
  extend ActiveSupport::Concern

  included do 

    has_many :cash_payments, as: :orderable, dependent: :destroy
    has_many :check_payments, as: :orderable, dependent: :destroy
    has_many :other_payments, as: :orderable, dependent: :destroy
    
    def paid?
      return self.balance_due <= 0 
    end
  
    def balance_due
      due = self.total_paid <= self.total ? (self.total - self.total_paid).round(2) : 0.0
      self.update_column('reward_taken',false) if due > 0.0 and self.class == Order
      return due
    end

    def refund
      return self.total_paid >= self.total ? (self.total_paid - self.total).round(2) : 0.0 
    end
  
    def total_paid
      total_paid = 0.0
      total_paid += cash_payment_received
      total_paid += check_payment_received
      total_paid += other_payment_received
      total_paid += self.transactions.paid?.sum(:amount)
      total_paid += self.reward_points_used.to_f
      return total_paid
    end

    def total_refunded
      data = self.cash_payments.where(:status => 1).present? ? self.cash_payments.where(:status => 1).sum(:amount) : 0.0
      data += self.check_payments.where(:status => 1).present? ? self.check_payments.where(:status => 1).sum(:amount) : 0.0 
      data += self.transactions.refunded?.sum(:amount)
      return data
    end

    #counting only those cash payment records having paid status
    def cash_payment_received
      self.cash_payments.where(:status => 0).present? ? self.cash_payments.where(:status => 0).sum(:amount) : 0.0
    end
 
    #counting only those check payment records having paid status
    def check_payment_received
      self.check_payments.where(:status => 0).present? ? self.check_payments.where(:status => 0).sum(:amount) : 0.0
    end

    def other_payment_received
      self.other_payments.where(:status => 0).present? ? self.other_payments.where(:status => 0).sum(:other_amount) : 0.0
    end

  
  end
end