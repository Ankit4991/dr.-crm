##
# CommonPatientAndFamily have all common code off Patient and Family Members Models

module CommonPatientAndFamily
  extend ActiveSupport::Concern

  included do 
    has_many :pmhxes, :dependent => :delete_all
    has_many :fmhxes, :dependent => :delete_all
    has_many :pohxes, :dependent => :delete_all
    has_many :medications,:dependent => :delete_all
    has_many :fohxes, :dependent => :delete_all
    has_many :sxhxes, :dependent => :delete_all
    has_many :allergies, :dependent => :delete_all
    #has_many :orders
    belongs_to :vision_insurance
    belongs_to :current_glass, class_name: Glass
    has_many :follow_ups
    
    has_many :referrals, through: :joint_resource #to be run with patient only as it gives all refers associated to patient and family member
    
    delegate :name, to: :vision_insurance, prefix: true, :allow_nil => true
    delegate :code, to: :vision_insurance, prefix: true, :allow_nil => true

    after_initialize :init_created_at

    validates :vision_insurance_id, presence: true
    
    accepts_nested_attributes_for :pmhxes, reject_if: proc { |attributes| 
      removeable_object = !ActiveRecord::ConnectionAdapters::Column::TRUE_VALUES.include?(attributes['checked'])
      if removeable_object && attributes['id'].present?
        Pmhx.find(attributes['id']).destroy
      end
      removeable_object
    }
    accepts_nested_attributes_for :medications, reject_if: proc { |attributes| 
      removeable_object = !ActiveRecord::ConnectionAdapters::Column::TRUE_VALUES.include?(attributes['checked'])
      if removeable_object && attributes['id'].present?
        obj =  Medication.find(attributes['id']).destroy
      end
      removeable_object
    }
    accepts_nested_attributes_for :fmhxes, reject_if: proc { |attributes| 
      removeable_object = !ActiveRecord::ConnectionAdapters::Column::TRUE_VALUES.include?(attributes['checked'])
      if removeable_object && attributes['id'].present?
        Fmhx.find(attributes['id']).destroy
      end
      removeable_object
    }
    accepts_nested_attributes_for :pohxes, reject_if: proc { |attributes| 
      removeable_object = !ActiveRecord::ConnectionAdapters::Column::TRUE_VALUES.include?(attributes['checked'])
      if removeable_object && attributes['id'].present?
        Pohx.find(attributes['id']).destroy
      end
      removeable_object
    }
    accepts_nested_attributes_for :fohxes, reject_if: proc { |attributes| 
      removeable_object = !ActiveRecord::ConnectionAdapters::Column::TRUE_VALUES.include?(attributes['checked'])
      if removeable_object && attributes['id'].present?
        Fohx.find(attributes['id']).destroy
      end
      removeable_object
    }
    accepts_nested_attributes_for :sxhxes, reject_if: proc { |attributes| 
      removeable_object = !ActiveRecord::ConnectionAdapters::Column::TRUE_VALUES.include?(attributes['checked'])
      if removeable_object && attributes['id'].present?
        Sxhx.find(attributes['id']).destroy
      end
      removeable_object
    }
    accepts_nested_attributes_for :allergies, reject_if: proc { |attributes| 
      removeable_object = !ActiveRecord::ConnectionAdapters::Column::TRUE_VALUES.include?(attributes['checked'])
      if removeable_object && attributes['id'].present?
        Allergy.find(attributes['id']).destroy
      end
      removeable_object
    }

    def get_exams
      self.joint_resource.examinations
    end
    
    def get_orders
      self.joint_resource.orders
    end

    def name
      "#{first_name.sub(first_name.chr,first_name.chr.upcase)} #{last_name.sub(last_name.chr,last_name.chr.upcase)}" if first_name.present? && last_name.present?
    end

    def get_bookings
      self.joint_resource.bookings
    end

    def get_glasses
      self.joint_resource.glasses
    end
    
    #to get referrals for family_member and patient seperately
    def get_referrals
      self.joint_resource.referrals
    end
    
    def age
      begin
        now = Time.now.utc.to_date
        now.year - birthday.year - ((now.month > birthday.month || (now.month == birthday.month && now.day >= birthday.day)) ? 0 : 1)
      rescue
        '-'
      end
    end

    def init_created_at
      created_at = Time.now
    end

  end

end

