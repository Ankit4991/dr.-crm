##
# Bookable Module use to check, is any Booking valid or not. All validation are set in this module.

module Bookable
  extend ActiveSupport::Concern

  included do
    belongs_to :doctor

    validates :start_time, presence: true 
    validates :length, presence: true, numericality: { greater_than: 0 }
    #validate :start_date_cannot_be_in_the_past, on: :create
    validate :overlaps
    before_validation :already_exist, on: :create
    before_validation :calculate_end_time
  

    scope :end_during, ->(new_start_time, new_end_time) do
      if (!new_start_time.nil?) && (!new_end_time.nil?)
        where('end_time > ? AND end_time < ?', new_start_time, new_end_time)
      else
        return nil
      end
    end

    scope :start_during, ->(new_start_time, new_end_time) do
      if (!new_start_time.nil?) && (!new_end_time.nil?)
        where('start_time > ? AND start_time < ?', new_start_time, new_end_time)
      else
        return nil
      end
    end

    scope :happening_during, ->(new_start_time, new_end_time) do
      if (!new_start_time.nil?) && (!new_end_time.nil?)
        where('start_time > ? AND end_time < ?', new_start_time, new_end_time)
      else
        return nil
      end 
    end

    scope :enveloping, ->(new_start_time, new_end_time) do
      if (!new_start_time.nil?) && (!new_end_time.nil?)
        where('start_time < ? AND end_time > ?', new_start_time, new_end_time)
      else
        return nil
      end
    end

    scope :identical, ->(new_start_time, new_end_time) do
      if (!new_start_time.nil?) && (!new_end_time.nil?)
        where('start_time = ? AND end_time = ?', new_start_time, new_end_time)
      else
        return nil
      end
    end
  end

  def overlaps
    overlapping_bookings = [ 
      doctor.bookings.end_during(start_time, end_time),
      doctor.bookings.start_during(start_time, end_time),
      doctor.bookings.happening_during(start_time, end_time),
      doctor.bookings.enveloping(start_time, end_time),
      doctor.bookings.identical(start_time, end_time)
    ].flatten
    overlapping_bookings.delete self
    if overlapping_bookings.count >= 2
      errors.add(:base, 'There are already two bookings at the scheduled time, please place the booking at a different time.')
    end
  end

  def start_date_cannot_be_in_the_past
    if start_time && start_time < DateTime.now + (5.minutes)
      errors.add(:start_time, 'must be at least 5 minutes from present time')
    end
  end

  def calculate_end_time
    start_time = validate_start_time
    length = validate_length
    if start_time && length
      self.end_time = start_time + (length.minutes - 60)
    end
  end

  def already_exist
    if self.joint_resource.present? && self.joint_resource.bookings.where("start_time > ? ", DateTime.now + (30.minutes)).present?
      bookings= self.joint_resource.bookings.where("start_time > ? ", DateTime.now + (30.minutes))
      errors.add(:already_scheduled, "Your already scheduled slot at #{bookings.last.start_time}")
      return nil
    end
  end

  def late?
    (self.start_time + 10.minutes < Time.zone.now) && (self.end_time > Time.zone.now)
  end

  def as_json(options = {})
    medical_insurance = self.organisation.setting.flag_for_medical ? self.try(:joint_resource).try(:member).try(:medical_insurance).try(:name)  : 'hide'
   {  
    :id => self.id,
    :comment => self.comment,
    :procedure_ids => self.exam_procedure_ids,
    :title => " ",
    :jr_id => self.try(:joint_resource).try(:id),
    :family_member_id => self.try(:joint_resource).try(:family_member_id) || '',
    :patient_id => self.try(:joint_resource).try(:patient).try(:id),
    :patient_name => self.try(:joint_resource).try(:member).try(:name),
    :patient_email => self.try(:joint_resource).try(:email),
    :patient_phone =>  self.try(:joint_resource).try(:member).try(:phone),
    :patient_age =>  self.try(:joint_resource).try(:member).try(:age),
    :patient_dob =>  self.try(:joint_resource).try(:member).try(:birthday).try(:strftime, '%m-%d-%y'),
    :patient_vision_insurance => self.try(:joint_resource).try(:member).try(:vision_insurance).try(:name),
    :patient_medical_insurance => medical_insurance,
    :store_name => self.try(:store).try(:description),
    :doctor_id => self.try(:doctor).try(:id),
    :doctor_name => self.try(:doctor).try(:name),
    :doctor_theme_color => self.try(:doctor).try(:theme_color),
    :doctor_working_shcedule => self.try(:doctor).try(:working_schedule).try(:get_schedule),
    :start => self.start_time,  
    :end => self.end_time + 60,  
    :recurring => false, 
    :allDay => false,
    :appointment_status => self.appointment_status,
    :store_doctor => self.try(:store).doctors.doctors_with_role_doctor.map{|doctor, dr_options=""| dr_options += "<option value= '#{doctor.id}'> #{doctor.name} </option>"}.join('')
   } 

  end  

  private

    def validate_start_time
      if !self.start_time.nil?
        start_time = self.start_time
      else
        return nil
      end
    end

    def validate_length
      if !self.length.nil?
        length = self.length.to_i
      else
        return nil
      end
    end

end