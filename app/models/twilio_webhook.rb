##
# Twillio store the sendding and reciving SMS data. 

class TwilioWebhook < ActiveRecord::Base
  attr_accessor :name #Because log model have name delegate for other trackable 
  before_create :add_organisation_id
  belongs_to :organisation
  has_many  :logs, :as => :trackable

  private
    def add_organisation_id
      self.organisation_id = Organisation.find_by(twilio_number: to).id 
    end
end
