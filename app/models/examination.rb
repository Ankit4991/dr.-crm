##
# Examination model is use to store Patient Exam form. Examination is main model for Examination form.
# There are many sub model that are part of patient exam form.

class Examination < ActiveRecord::Base
  before_create :add_organisation_id

  belongs_to :doctor
  belongs_to :organisation
  belongs_to :current_glass, class_name: Glass
  before_create :update_exam_date
  after_destroy :remove_associated_audits
  after_create :create_page_in_cms
 
  has_many :visual_acuities, dependent: :destroy
  has_many :glasses#, through: :patient
  has_many :glasses_prescriptions
  has_many :lens_prescriptions, class_name: "LensPrescription"
  has_many :referrals, dependent: :destroy
  has_many :medical_rxes, dependent: :destroy
  has_many :eye_measurements, -> { where(default_refraction: true).order('created_at') }, dependent: :destroy
  has_many :exam_impressions, dependent: :destroy
  #has_and_belongs_to_many :exam_procedures
  has_many :procedure_adjustments
  has_many :documents, dependent: :destroy

  has_one  :social_history, dependent: :destroy
  has_one  :primlinary_exam, dependent: :destroy
  has_one  :binocular_testing, dependent: :destroy
  has_one  :external_exam, dependent: :destroy
  has_one  :internal_exam, dependent: :destroy
  has_one  :contact_lens_exam, dependent: :destroy
  has_one  :order

  belongs_to :booking
  belongs_to :joint_resource

  accepts_nested_attributes_for :joint_resource
  accepts_nested_attributes_for :eye_measurements

  accepts_nested_attributes_for :glasses
  accepts_nested_attributes_for :current_glass, reject_if: :all_blank
  accepts_nested_attributes_for :glasses_prescriptions, reject_if: :all_blank, limit: 4
  accepts_nested_attributes_for :lens_prescriptions, reject_if: :all_blank, limit: 4

  accepts_nested_attributes_for :social_history
  accepts_nested_attributes_for :primlinary_exam
  accepts_nested_attributes_for :binocular_testing
  accepts_nested_attributes_for :external_exam
  accepts_nested_attributes_for :internal_exam
  accepts_nested_attributes_for :contact_lens_exam
  accepts_nested_attributes_for :exam_impressions
  accepts_nested_attributes_for :procedure_adjustments
  accepts_nested_attributes_for :documents

  accepts_nested_attributes_for :visual_acuities

  delegate :store, to: :doctor, :allow_nil => true  
  

  # after_initialize :setup_accessor, :init_created_at
  scope :updated_today,lambda { where("updated_at >= ? AND updated_at <= ?", Time.zone.now.midnight, Time.zone.now.end_of_day )}
  enum status: [:pending, :unfexam,:unforder,:ordered,:dispensed,:finalized,:removed]
  has_associated_audits
  validate :save_object?, :on => :create
  
  
  def prev_audits
    self.associated_audits.map{|assoc| [assoc.auditable_type,assoc.created_at,assoc.audited_changes,assoc.try(:user).try(:name),assoc.try(:user).try(:class).try(:name)]}
  end

  private

    def save_object?
      unless self.joint_resource.blank? || !self.class.exists?(:joint_resource_id => self.joint_resource.id)
        unless ['finalized','removed'].include? self.joint_resource.examinations.last.status
          errors[:attribute] << "Last Exam not delivered yet !!"
          return false
        end
      end
    end

    def remove_associated_audits
      self.associated_audits.map{|adt| adt.delete} if self.associated_audits.present?
    end

    def init_created_at
      created_at = Time.now
    end

    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end
    
    def update_exam_date
      self.exam_date = Date.today
    end

    def create_page_in_cms
      exam_date = self.created_at.strftime("%b %d,%Y")
      store_name = self.store.try(:description)
      store_city = self.store.try(:office_address).try(:city)
      exam_producer = self.doctor.name
      first_name = self.joint_resource.try(:member).try(:first_name)
      last_name = self.joint_resource.try(:member).try(:last_name)
      booking_date = self.booking.start_time.strftime("%b %d,%Y")
      exam_procedures = self.booking.try(:exam_procedures).try(:first).try(:name)
      provider_name = self.doctor.name
      json_data= JSON.parse((RestClient.post "http://brickellvision.com/pages", {"page":{"exam_date": exam_date,"store_name": store_name, "store_city": store_city, "exam_producer": exam_producer, "first_name": first_name, "last_name": last_name,"booking_date": booking_date, "exam_procedures": exam_procedures,"provider_name": provider_name}}.to_json, {content_type: :json, accept: :json}), :symbolize_names => true)
    end
end
