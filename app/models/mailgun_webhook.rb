##
# MailgunWebhook Store all Email data (click, opened, Bounce etc)

class MailgunWebhook < ActiveRecord::Base
  #don't change these campaign name because lots of the thing are heppened in Js and server side on the basis of these keyword
  #campaign = ["appointment", "survey", "order", "follow_up", "contact_form", "patient_account"]
  belongs_to :trackable, polymorphic: :true, dependent: :destroy
  validates :trackable_id, :trackable_type, presence: true
  #validates :trackable_type, inclusion: { in: %w(Doctor Store), message: "%{value} is not a valid trackable_type" }
end
