##
# Patient model has devise and it is a our App end user.

class Patient <  ActiveRecord::Base
  #If you want to delete any patient Plaese find do like @patient.joint_resource.destroy
  #because all dependent destroy are dependent upon JointResource Model

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  include CommonPatientAndFamily
  devise :database_authenticatable, :registerable, :recoverable, :rememberable,
         :trackable, :validatable, :confirmable, request_keys: [:subdomain]
  attr_accessor :unhashed_password
  attr_accessor :old_email

  before_create :add_organisation_subdomain_and_id_and_token
  before_update :check_email_update
  after_destroy :remove_associated_audits

  belongs_to :organisation

  audited except: [:current_address_id, :encrypted_password, :reset_password_token, :reset_password_sent_at, :remember_created_at, :sign_in_count, :current_sign_in_at, :last_sign_in_at, :current_sign_in_ip, :last_sign_in_ip, :confirmation_token, :confirmed_at, :confirmation_sent_at, :unconfirmed_email, :history_info, :survey_token, :avatar_file_name, :avatar_content_type, :avatar_file_size, :avatar_updated_at, :bgurl, :subdomain, :authentication_token], on: [:update,:create]

  has_many :backgrounds,:dependent => :destroy
  has_many :family_members,:dependent => :destroy
  has_many :notifications,:dependent => :destroy
  has_many :contact_forms,:dependent => :destroy
  has_many  :mailgun_webhook, :as => :trackable,  dependent: :destroy
  has_many :documents, dependent: :destroy

  has_one :home_address,-> { where(addr_type: 'home') },class_name: 'Address',:dependent => :destroy
  has_one :office_address,-> { where(addr_type: 'office') }, class_name: 'Address',:dependent => :destroy
  has_one :survey, dependent: :destroy
  has_one :profile, class_name: PatientProfile, dependent: :destroy
  has_one :relative, dependent: :destroy
  has_one :joint_resource,-> { where("family_member_id IS NULL") }, dependent: :destroy
  has_many :contact_lens_distribution_order
  belongs_to :medical_insurance
  delegate :phone, to: :relative, prefix: true, :allow_nil => true
    
  accepts_nested_attributes_for :home_address, allow_destroy: true
  accepts_nested_attributes_for :office_address,  allow_destroy: true
  accepts_nested_attributes_for :relative,  allow_destroy: true
  accepts_nested_attributes_for :joint_resource, allow_destroy: true

  validates :first_name, presence: true
  #validates :phone, presence: true, format: { with: /(\+\d[0-9]{10,15})+\z/, message: "number invalid format!" }
  
  #ransack_alias :joint_resource_name, :joint_resource_first_name_or_joint_resource_last_name
  ransack_alias :doctor_name, :doctor_first_name_or_doctor_last_name

  has_attached_file :avatar, :styles => { :medium => ["300x300>",:png], :thumb => ["150x150>",:png], :thumbnail => ["30*30",:png], original: ["100%",:png] },
    url: "/uploads/patients/:id/:style/:basename.:extension",
    path: ":rails_root/public/uploads/patients/:id/:style/:basename.:extension"
  
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  validates_with AttachmentSizeValidator, :attributes => :avatar, :less_than => 1.megabytes

  scope :updated_today,lambda { where("updated_at >= ? AND updated_at <= ?", Time.zone.now.midnight, Time.zone.now.end_of_day )}
  
  has_associated_audits


  ransacker :updated_at, type: :date do
    Arel.sql('patients.updated_at::date')   # filter only the date
  end

  #this is for devise login with subdomain
  def self.find_for_authentication(warden_conditions)
    warden_conditions[:email].downcase!
    subdomain = warden_conditions[:subdomain].present? ? (warden_conditions[:subdomain].include?('.') ? warden_conditions[:subdomain].split('.')[0] : warden_conditions[:subdomain]) : ''
    where(:email => warden_conditions[:email], :subdomain => subdomain).first
  end

  ransacker :full_name do |parent|
    Arel::Nodes::InfixOperation.new('||',Arel::Nodes::InfixOperation.new('||',parent.table[:first_name], Arel::Nodes.build_quoted(' ')),parent.table[:last_name])
  end

  def email
    if !self.changed_attributes[:email].present? && email_not_supplied && self.id.present?
      return "N/A"
    end
    return self.read_attribute(:email)
  end
  
  def nested_audits
    self.associated_audits.map{|assoc| [assoc.auditable.class.name,assoc.created_at,assoc.audited_changes,assoc.try(:user).try(:name)]}
  end

  def prev_audits
    self.audits.map{|assoc| [assoc.auditable_type,assoc.created_at,assoc.audited_changes,assoc.try(:user).try(:name),assoc.try(:user).try(:class).try(:name)]}
  end

  def all_resources
    JointResource.where(patient_id: self.id)
  end
   
  def all_finalized_orders
    self.all_resources.map{|jr| jr.orders.finalized.present? ? jr.orders.finalized : []}
  end
  
  def all_dispensed_orders
    self.all_resources.map{|jr| jr.orders.ready_for_pick_up.present? ? jr.orders.ready_for_pick_up : []}
  end
  
  def all_renewed_orders
    self.all_resources.map{|jr| jr.favourites.where('status !=?',4).present? ? jr.favourites.where('status !=?',4) : []}
  end
  
  def all_removed_orders
    self.all_resources.map{|jr| jr.orders.dispensed.present? ? jr.orders.dispensed.order(:updated_at => :desc) : []}
  end
  
  def all_orders_reports
    Order.joins(:joint_resource).where("joint_resources.patient_id =? AND orders.status in ('0','1','2','3','4')",self.id).order(:order_date => :desc)
  end

  def all_order_subscriptions
    self.all_resources.map{|jr| jr.subscriptions.any? ? jr.subscriptions : []}
  end
  
  #to create profile for existing patient
  def create_profile(options)
    ActiveMerchant::Billing::Base.mode = AppConfig.authorize_net_mode.to_sym
      patient_profile = {
          :profile => {
            :description => self.subdomain,
            :email =>  self.email
        }
      }
    response = cim_gateway.create_customer_profile(patient_profile)          #patient profile via unconfirmed email
    raise "#{response.message}" unless response.success? && response.params['customer_profile_id'].present?
    profile = self.create_profile!(:customer_profile_id => response.params['customer_profile_id'],:first_name => options[:first_name],:last_name => options[:last_name])
    profile.create_payment_profile(options)
  end

  def examinations
    Organisation.find(organisation_id).examinations.where(joint_resource_id: joint_resource.id)
  end

  def all_prescriptions
    examinations.map{ |exam| exam if exam.glasses_prescriptions.present? || exam.lens_prescriptions.present? }.compact
  end

  def orders
    Organisation.find(organisation_id).orders.where("joint_resource_id =? ",joint_resource.id)
  end

  def med_rx
    @med_rx_count = 0
    examinations.each do |ex| 
      if ex.medical_rxes.present?
        @med_rx_count = @med_rx_count + ex.medical_rxes.count
      end
    end
    return @med_rx_count
  end

  def total_report_count
    referrals.count + orders.count + all_prescriptions.count + med_rx
  end

  def self.from_google_omniauth(access_token,org_id)
    @org = Organisation.find(org_id)
    data = access_token.info
    @patient = @org.patients.where(:email => data["email"]).first
    unless @patient
      @patient = @org.patients.new
      @joint_resource = @patient.build_joint_resource 
      @patient.first_name = data["first_name"]
      @patient.last_name = data["last_name"]
      @patient.email = data["email"]
      @patient.password = Devise.friendly_token[0,20]
      @patient.vision_insurance_id = @org.vision_insurances.last.id
      @patient.medical_insurance_id = @org.medical_insurances.last.id
      @patient.avatar = open(data["image"])
      @patient.save!
      @patient.confirm!
    end
    @patient
  end
  
  private
    def add_organisation_subdomain_and_id_and_token
      self.subdomain = Organisation.current.try(:domain_name) || "brickellvision"
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
      self.authentication_token= Devise.friendly_token
      self.survey_token = Devise.friendly_token.first(12)
    end

    def check_email_update
      if email_changed?
        future_bookings = Booking.account_future_bookings_find_by_patient_id(self.id)
        if confirmed?
          self.old_email =  changed_attributes[:email]
          AppMailer.send_email_changed_confirmation(self,self.email).deliver_later 
          AppMailer.send_bookings_details_confirmation(future_bookings).deliver_now if future_bookings.present? && !(self.email_not_supplied)
        elsif !changed_attributes[:confirmation_sent_at].present?
          send_confirmation_instructions
        end
      end
    end

    def remove_associated_audits
      self.associated_audits.map{|adt| adt.delete} if self.associated_audits.present?
      self.audits.map{|adt| adt.delete} if self.audits.present?
    end
    
    def cim_gateway
      ActiveMerchant::Billing::AuthorizeNetCimGateway.new(
        :login    => ENV["AUTHORIZE_NET_LOGIN"],
        :password => ENV["AUTHORIZE_NET_KEY"])
    end
    
end
