##
# One organisation can have many stores. Store has own staff and working hours.

class Store < ActiveRecord::Base
  before_create :add_organisation_id
  after_create :create_schedule

  belongs_to :organisation
  belongs_to :manager, class_name: "Doctor" 
  belongs_to :office_address, class_name: "Address" 
  
  has_many :bookings
  has_many :doctors
  has_many :contacts
  has_many :holidays, as: :trackable, dependent: :destroy
  has_one  :working_schedule, :as => :trackable,  dependent: :destroy
  has_many :contact_lens_distribution_orders

  validates :description, presence: true
  validates :phone_number, presence: true, format: { with: /(\+\d[0-9]{10,15})+\z/, message: "invalid format!" }


  has_attached_file :logo, :styles => { :medium => ["300x300>",:png], :thumb => ["150x150>",:png], :thumbnail => ["30*30",:png], original: ["100%",:png] },
    url: "/uploads/stores/:id/:style/:basename.:extension",
    path: ":rails_root/public/uploads/stores/:id/:style/:basename.:extension"

  validates_attachment_content_type :logo, :content_type => /\Aimage\/.*\Z/
  validates_with AttachmentSizeValidator, :attributes => :logo, :less_than => 1.megabytes, :message => 'Please use an image smaller than 1mb'

  accepts_nested_attributes_for :office_address,  allow_destroy: true
  accepts_nested_attributes_for :doctors,  allow_destroy: true, reject_if:proc { |attributes|
    attributes.doctor_id.blank?
  }
  
  after_validation :clean_paperclip_errors
  
  def open_today?
    holidays.where("date::date=CURRENT_DATE").blank? && working_schedule.working_day?(Date.today)
  end
  
  private
    def create_schedule
      self.create_working_schedule
    end
    
    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end

    def clean_paperclip_errors
      errors.delete(:logo)
    end
end
