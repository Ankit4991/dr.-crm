##
# Joint resource is a common Model between Patient and FamilyMember . 
# This is a model that help to identify patient(family member or patient).
# We create mostaly object(Examination, Booking, Order) coressponding to JointResource.

class JointResource < ActiveRecord::Base
  before_create :add_organisation_id
  before_destroy :remove_patient,:destroy_with_soft_records

  belongs_to :patient
  belongs_to :family_member
  belongs_to :organisation
  belongs_to :current_glass, class_name: Glass
  
  has_many :examinations, dependent: :destroy
  has_many :glasses, dependent: :destroy
  has_many :orders, dependent: :destroy
  has_many :subscriptions, :class_name => "OrderSubscription",:dependent => :destroy
  has_many :favourites,:class_name => "FavouriteOrder",:dependent => :destroy
  has_many :exam_orders,:through => :examinations,class_name: 'Order'
  has_many :bookings, dependent: :destroy
  has_many :follow_ups, dependent: :destroy
  has_many :referrals, through: :examinations,:dependent => :destroy

  delegate :email,:phone,:home_address,:current_address_id,:views_count,:reward_point,:sign_in_count,:intrests,:profile,:birthday, to: :patient

  accepts_nested_attributes_for :patient
  accepts_nested_attributes_for :family_member
  include ActionView::Helpers::NumberHelper


  def member
    if self.family_member.present?
      self.family_member
    else
      self.patient
    end
  end

   def name
    if self.family_member.present?
      self.family_member.name
    else
      self.patient.name
    end
  end

  def patient_joint_resource
    self.patient.joint_resource
  end

  def is_family_member?
  	self.family_member.present?
  end

  def doctor_suggestion_list
    if self.member.birthday.present?
      #fmo = (self.member.class.to_s == "FamilyMember" ? ", FMO: #{self.patient.name}" : "")
      "#{self.member.name}, #{self.member.birthday.strftime("%m/%d/%Y") } - #{self.member.age} y/o, #{number_to_phone(self.member.phone.to_s[2..-1].to_i, area_code: true)}"
    else
      "#{self.member.name}, #{number_to_phone(self.member.phone.to_s[2..-1].to_i, area_code: true)}"
    end
  end

  def last_assigned_doctor
    self.try(:examinations).try(:last).try(:doctor) || self.try(:orders).try(:last).try(:doctor) || self.try(:bookings).try(:last).try(:doctor)
  end

  def lens_prescriptions_in_use
    self.examinations.map{ |exam| exam.lens_prescriptions.where('expired_date::date >= ?',Date.today).order(:updated_at => :desc)}.flatten
  end
  
  def glass_prescriptions_in_use
    self.examinations.map{ |exam| exam.glasses_prescriptions.where('expired_date::date >= ?',Date.today).order(:updated_at => :desc) }.flatten
  end

  def previous_holded_order
    JointResource.joins(:orders).where('orders.status !=6 and orders.joint_resource_id=?',self.id) 
  end

  private

  def remove_patient
    if self.family_member.present?
      self.update(patient_id: nil)
    else
      JointResource.where("patient_id=? AND family_member_id IS NOT NULL", self.patient.id).to_a.each do|jr|
        jr.update(patient_id: nil)
        jr.destroy
      end
    end
  end
  
  def destroy_with_soft_records
    self.bookings.with_deleted.each do |booking|
      booking.update(joint_resource_id: nil)
      booking.really_destroy!
    end
    self.follow_ups.with_deleted.each do |follow_up|
      follow_up.update(joint_resource_id: nil)
      follow_up.really_destroy!
    end
  end

  def add_organisation_id
    self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
  end
end
