##
# Order model is use to store Patient Order form.
# This is the main root model for an order as it further may have many submodels (contact lens orders or eye glass orders) that are part of it.
class Order < ActiveRecord::Base
  
  include CommonOrderAndFavouriteOrder
  
  audited only: [:status],on: [:update,:create]
  enum status: [:pending, :unforder,:ordered,:finalized,:ready_for_pick_up,:follow,:dispensed]

  after_save :reward_to_patient
  before_create :add_organisation_id, :update_order_date 
  after_destroy :remove_associated_audits
  after_create :set_uuid

  belongs_to :examination
  belongs_to :doctor
  belongs_to :organisation
  belongs_to :joint_resource

  has_many :transactions,as: :orderable,dependent: :destroy, :class_name => "OrderTransaction"
  # has_many :cash_payments, as: :orderable, dependent: :destroy
  # has_many :check_payments, as: :orderable, dependent: :destroy

  #has_many :exam_procedures,:through => :examination
  has_many :procedure_adjustments, :through => :examination

  has_many :contact_lens_orders, dependent: :destroy
  has_many :lenses,:through => :contact_lens_orders
  has_many :eye_glass_orders, dependent: :destroy
  has_many :addon_details,:through => :eye_glass_orders
  has_many :dispense_logs,as: :dispensable,foreign_key: :dispensable_id,dependent: :destroy

  has_and_belongs_to_many :procedures,class_name: 'ExamProcedure'
  has_associated_audits

  delegate :store, to: :doctor, :allow_nil => true    
  
  accepts_nested_attributes_for :contact_lens_orders,:allow_destroy => true
  accepts_nested_attributes_for :eye_glass_orders,:allow_destroy => true,reject_if: :all_blank
  accepts_nested_attributes_for :procedure_adjustments
  
  scope :updated_today,lambda { where("updated_at >= ? AND updated_at <= ?", Time.zone.now.midnight, Time.zone.now.end_of_day )}
  

  #validates_presence_of :check_number, :if => :check_amount?

  def last_dispensed_on
    status_audit = self.audits.order('created_at').detect{ |adt| adt.audited_changes['status'] !=nil and adt.audited_changes['status'].kind_of?(Array) and adt.audited_changes['status'].include?('ready_for_pick_up') }
    return status_audit.try(:created_at)
  end

  # Adjustments has been shown over invoices
  def adjustments
    return (self.total - subtotal).round(2)
  end

  def subtotal
    exam_copay = self.procedure_adjustments.sum(:price)
    return (eye_glass_suborders_subtotal.to_f + contact_lens_suborders_subtotal.to_f + exam_copay.to_f).round(2)
  end

  def overall_discount
    (self.eye_glass_orders.where(:hold => false).to_a.sum(&:overall_discount) +  self.contact_lens_orders.where(:hold => false).to_a.sum(&:overall_discount)).round(2)
  end

  def eye_glass_suborders_subtotal
    self.eye_glass_orders.where(:hold => false).to_a.sum(&:subtotal)
  end

  def contact_lens_suborders_subtotal
    self.contact_lens_orders.where(:hold => false).to_a.sum(&:subtotal)
  end

  # Exam Copay is the total sum of all exam procedures prices corresponding to this order through its examination form if any.
  def exam_copay
    self.examination.present? ? self.examination.try(:procedure_adjustments).sum(:price) : self.try(:procedure_adjustments).sum(:price)
  end

  def reward_to_patient
    if self.status.include?("finalized") && self.paid?
      unless self.reward_taken
        patient = self.joint_resource.patient
        rewarded_value = (self.total_paid.to_f - self.reward_points_used.to_f - self.earned_point_on_val.to_f)
        if self.joint_resource.orders.last.organisation.setting.flag_for_reward == true
          earned_rewards = rewarded_value*self.joint_resource.orders.last.organisation.setting.reward_percent_used.to_f/100
          patient.reward_point = patient.reward_point + earned_rewards
          patient.save
        else
          earned_rewards = rewarded_value * 0.0
          patient.reward_point = patient.reward_point + earned_rewards
          patient.save
        end
        self.update_column('reward_taken',true)
        self.update_column('earned_point_on_val',(self.total_paid.to_f - self.reward_points_used.to_f))
        AppMailer.send_earned_reward_points_to_patient(self,earned_rewards).deliver_later if ((earned_rewards > 0) && !patient.try(:email_not_supplied))
      end
    end
  end

  #This method is user to retrieve all changes made to this order (along with all nested models changes)
  def prev_audits
    self.associated_audits.map{|assoc| [assoc.auditable_type,assoc.created_at,assoc.audited_changes,assoc.try(:user).try(:name),assoc.try(:user).try(:class).try(:name)]}
  end

  # def all_lens_presriptions
  #   self.contact_lens_orders.map{|lens_order| lens_order.lenses }
  # end

  def hold_lenses
    self.contact_lens_orders.map(&:hold).count(true)
  end
  
  def hold_glasses
    self.eye_glass_orders.map(&:hold).count(true)
  end
  
  def all_finalized_lenses
    self.contact_lens_orders.map{|lens_order| lens_order.lenses.where(:finalize => true) }
  end
  
  private

    def remove_associated_audits
      self.associated_audits.map{|adt| adt.delete} if self.associated_audits.present?
      self.audits.map{|adt| adt.delete} if self.audits.present?
    end

    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end

    def update_order_date
      self.order_date = Date.today
    end

    def set_uuid
      self.uuid = "NEO#{self.id}"
      self.save!
    end

end
