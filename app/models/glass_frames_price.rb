class GlassFramesPrice < ActiveRecord::Base
  before_create :add_organisation_id
  belongs_to :organisation
  
  validates :frame, :presence => true
  validates :price, :presence => true
  
  def frames_prices
    "#{frame} &nbsp;&nbsp;&nbsp;&nbsp; $#{price}".html_safe
  end
  
  private
    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end
end
