##
# Medical History is a disease history . This is polymorphic model and inherited by many other model.

class MedicalHistory < ActiveRecord::Base
  belongs_to :disease
  attr_accessor :checked
  Relation = ['great grandparent', 'grandparent', 'parent', 'parent father', 'parent mother', 'sibling', 'sibling sister', 'sibling brother', 'child', 'child daughter', 'child son', 'others', 'unknown']
end
