class OrderTransaction < ActiveRecord::Base
  attr_accessor :valid_card
  PROCESSING, FAILED, SUCCESS = 1, 2, 3

  before_create :add_organisation_id
  belongs_to :organisation
  belongs_to :orderable, polymorphic: :true,dependent: :destroy
  
  validates :valid_card, :inclusion => {:in => [true], :message => 'Invalid Credit Card'}
  validates :amount, :presence => true, :numericality => { :greater_than => 0 }
  enum transaction_type: [:auth_capture, :refund]
  scope :paid? , lambda { where(:status => SUCCESS, :transaction_type => 0)}
  scope :refunded?,  lambda { where(:status => SUCCESS, :transaction_type => 1)}

  def process_payment(creditcard, options)
    ActiveMerchant::Billing::Base.mode = AppConfig.authorize_net_mode.to_sym
    self.status = PROCESSING
    response = gateway.purchase(amount * 100, creditcard, options)
    if response.success?
      self.response_id = response.params["transaction_id"]
      self.status = SUCCESS
    else
      self.status = FAILED
    end
    return self
  rescue Exception => e
    self.status = FAILED
    return self
  end
  
  #payment method - using registered profile card
  def process_payment_using_payment_profile(transaction)
    ActiveMerchant::Billing::Base.mode = AppConfig.authorize_net_mode.to_sym
    self.status = PROCESSING
    response = cim_gateway.create_customer_profile_transaction(transaction)
    if response.success?
      self.response_id = response.params["direct_response"]["transaction_id"]
      self.status = SUCCESS
      order = self.orderable
      transaction_time = Time.now.strftime('%m/%d/%Y at %I:%M%p').to_s
      AppMailer.send_auto_receipt_to_patient(order,response.params['direct_response'],transaction_time).deliver_later unless order.try(:joint_resource).try(:patient).try(:email_not_supplied)
      InvoiceMailer.send_invoice_to_patient(order.try(:joint_resource).try(:member),order).deliver_later unless order.try(:joint_resource).try(:patient).try(:email_not_supplied)
    else
      self.status = FAILED
    end
    return self
  rescue Exception => e
    self.status = FAILED
    return self
  end

  def process_refund_using_payment_profile(transaction)
    ActiveMerchant::Billing::Base.mode = AppConfig.authorize_net_mode.to_sym
    self.status = PROCESSING
    response = cim_gateway.create_customer_profile_transaction_for_refund(transaction)
    self.reference_trans_id = transaction[:transaction][:trans_id] if transaction[:transaction][:trans_id].present?
    if response.success?
      self.response_id = response.params["direct_response"]["transaction_id"]
      self.status = SUCCESS
    else
      self.status = FAILED
    end
    return self
  rescue Exception => e
    self.status = FAILED
    return self
  end

  def referenced?
    OrderTransaction.where(reference_trans_id: self.response_id,status: 3).any?
  end
 
  def success?
    self.status == SUCCESS
  end
 
  private
  
    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end
    
    def gateway
      ActiveMerchant::Billing::AuthorizeNetGateway.new(
        :login    => ENV["AUTHORIZE_NET_LOGIN"],
        :password => ENV["AUTHORIZE_NET_KEY"])
    end
    
    def cim_gateway
      ActiveMerchant::Billing::AuthorizeNetCimGateway.new(
        :login    => ENV["AUTHORIZE_NET_LOGIN"],
        :password => ENV["AUTHORIZE_NET_KEY"])
    end
   
end