class ContactLensDistributionOrder < ActiveRecord::Base
	belongs_to :lens
	belongs_to :organisation
	belongs_to :doctor
	belongs_to :store
	belongs_to :patient 
end
