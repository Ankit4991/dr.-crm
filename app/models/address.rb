##
# Address Model is a polymorphic type. that can be associate to many Model like Patient, Organisation, Doctor
class Address < ActiveRecord::Base
  
  belongs_to :patient
  belongs_to :doctor
  belongs_to :organisation
  belongs_to :contact
  audited associated_with: :patient

  after_save :current_address
  before_create :add_organisation_id

  

  def country_name
    Carmen::Country.coded(self.try(:country_id)).try(:name)
  end

  def state_name
    Carmen::Country.coded(self.try(:country_id)).try(:subregions).try(:coded,self.try(:state_id)).try(:name)
  end

  def full
    full_address = "#{self.address1} #{self.address2}, #{self.city} #{self.state_name} #{self.postal_code}, Email: #{self.email}".html_safe
    return full_address.split(',').join(',').gsub(',',"<br>").html_safe
  end

  def full_addr_for_reports
    full_address = "#{self.address1} #{self.address2}, #{self.city} #{self.state_name} #{self.postal_code}".html_safe
    return full_address.split(',').join(',').gsub(',',"<br>").html_safe
  end  
  
  def current_address
    if self.patient.present? ||self.doctor.present?
      resource = self.patient||self.doctor
      resource.current_address_id = self.id
      resource.save
    end
  end
  
  def prev_audits
    self.audits.map{|audit| [audit.auditable_type,audit.created_at,audit.audited_changes,audit.try(:user).try(:name),audit.try(:user).try(:class).try(:name)]}
  end

  private
    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end
end
