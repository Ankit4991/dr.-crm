##
# Notification keep record of message and appointment notification.

class Notification < ActiveRecord::Base
  #status field in notification is only used patient that tells if patient has viewed the notification
  belongs_to :patient
  belongs_to :event, polymorphic: :true, dependent: :destroy
  has_many :doctors_notifications
  has_and_belongs_to_many :doctors
  accepts_nested_attributes_for :doctors
end
