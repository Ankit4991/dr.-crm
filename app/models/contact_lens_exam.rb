##
# ContactLensExam model values are part of exam form. 

class ContactLensExam < ActiveRecord::Base
  require 'json'
  enum dominated: {
    left: 1,
    right: 2
  }

  audited :associated_with => :examination
  has_associated_audits

  before_create :add_organisation_id

  has_many :lenses, class_name: "Lens"

  belongs_to :examination
  belongs_to :organisation
  
  accepts_nested_attributes_for :lenses, reject_if: :all_blank
  
  store_accessor :contact_lens_evaluation,:od_d_h,:od_d_v, :od_mm_h, :od_mm_v, :od_axis_h,:od_axis_v,:os_d_h, :os_d_v, :os_mm_h, :os_mm_v, :os_axis_h,:os_axis_v,:dominated_eye_od, :dominated_eye_os
  
  private
    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end

end
