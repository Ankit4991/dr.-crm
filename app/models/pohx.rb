##
# Pohx Model is just a class that is inherited from Medical History.
# and Associated to Medical History table in db.

class Pohx < MedicalHistory
  belongs_to :patient
  belongs_to :family_member
  audited :associated_with => :patient, only: [:disease_id,:notes]
end
