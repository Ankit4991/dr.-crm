class OtherPayment < ActiveRecord::Base

	before_create :add_organisation_id
	belongs_to :organisation
	belongs_to :doctor
	belongs_to :orderable, polymorphic: :true,dependent: :destroy

	validates :other_amount, :presence => true, :numericality =>  { greater_than_or_equal_to: 0 }
  	enum status: [:paid, :refund]
	private
     def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end
end
