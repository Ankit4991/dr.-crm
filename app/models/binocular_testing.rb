##
# BinocularTesting Model values are parts of exam form.

class BinocularTesting < ActiveRecord::Base
	belongs_to :examination

	before_create :set_defaults #before creating object of this model associated set default value of fields from settings exam form defaults. 
	def set_defaults
		def_values = ExamFormDefault.where(:organisation_id => Organisation.current.try(:id)).first
		self.ctd = def_values.ctd_def
		self.ctn = def_values.ctn_def
		self.nra = def_values.nra_def
		self.pra = def_values.pra_def
		self.npc = def_values.npc_def
		self.notes = def_values.notes_def
		self.hpd = def_values.hpd_def
		self.hpn = def_values.hpn_def
		self.vpd = def_values.vpd_def
		self.vpn = def_values.vpn_def
	end
end
