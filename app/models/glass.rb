##
# Glass value are use in Exam Form at various section(Refraction, Current Glass, Final Glass Prescription).

class Glass < ActiveRecord::Base
  before_create :add_organisation_id
  belongs_to :examination
  belongs_to :visual_acuity
  belongs_to :eye_glass_order
  belongs_to :organisation
  belongs_to :joint_resource

  has_one :eye_measurement
  has_one :glasses_prescription, dependent: :destroy
  delegate :examination, to: :patient, :allow_nil => true

  accepts_nested_attributes_for :visual_acuity
  
  private
    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end

end
