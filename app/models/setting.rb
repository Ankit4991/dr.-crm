##
# Setting is sore some organisation specific date Like Ranges for various Exam select field. 

class Setting < ActiveRecord::Base
  belongs_to :organisation

  before_create :add_organisation_id

  has_many :documents, dependent: :destroy
  
  
  private
    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end
end
