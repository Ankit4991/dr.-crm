##
# EyeGlassOrder model values are part of order form.There can be multiple eye glass orders for an order.
# This model value changes is recorded as its audits.

class EyeGlassOrder < ActiveRecord::Base
  
  include CommonSuborder

  before_create :add_organisation_id

  belongs_to :order
  belongs_to :organisation
  belongs_to :eyeglass_types_price
  belongs_to :glass_frames_price
  belongs_to :lens_material
  belongs_to :doctor

  has_many :glasses, dependent: :destroy
  has_many :addon_details, dependent: :destroy

  accepts_nested_attributes_for :glasses,reject_if: :all_blank
  accepts_nested_attributes_for :addon_details,:allow_destroy => true,reject_if: :all_blank
  
  audited :associated_with => :order
 
  acts_as_paranoid
  
  def checklist
    {
      :od_checkmark => self.od_checkmark,
      :os_checkmark => self.os_checkmark,
      :order_checkmark => self.order_checkmark,
      :lens_material_checkmark => self.lens_material_checkmark,
      :eye_glass_type_checkmark => self.eye_glass_type_checkmark,
      :frame_checkmark => self.frame_checkmark
    }
  end
  
  def is_empty_frame_details?
    whitelist = %w[ model eye_size bridge glass_b glass_ed color material temple comments]
    self.attributes.all? do |attr, val|
      whitelist.exclude?(attr) || val.nil? || val == ""
    end
  end

  def overall_discount
    (frame_allowance + (overage_discount/100 * total) + (discount/100 * total)).round(2)
  end

  def calculate_total
    suborder = self
    glass_type_price = suborder.glass_type_price || suborder.try(:eyeglass_types_price).try(:price).to_f
    glass_lens_material = suborder.lens_material_price || suborder.try(:lens_material).try(:price).to_f
    unless frame_price.present?
      frame_x = (suborder.try(:glass_frames_price).try(:price).to_f > suborder.frame_allowance.to_f) ? (suborder.try(:glass_frames_price).try(:price).to_f - suborder.frame_allowance.to_f) : suborder.try(:glass_frames_price).try(:price).to_f        # here we are initializing frame total price based on allowance 
    else
      frame_x = (suborder.frame_price > suborder.frame_allowance.to_f) ? (suborder.frame_price - suborder.frame_allowance.to_f) : suborder.frame_price        # here we are initializing frame total price based on allowance 
    end
    suborder.frame_price = suborder.overage_discount.to_f.between?(0,100) ? (frame_x - (suborder.overage_discount.to_f  * (frame_x/100))) : (suborder.overage_discount = 0)
    suborder.overage_discount,suborder.frame_price = 0,0 if (frame_x - (suborder.overage_discount.to_f  * (frame_x/100))) <= 0
    glass_frame_price = suborder.frame_price || suborder.try(:glass_frames_price).try(:price).to_f               #first checking, if glass order frame price is having any value then place it as it is and if not then place actualy frame price value
    addons_price = suborder.addons_price || suborder.try(:addon_details).map{|add_on| add_on.try(:glass_addons_price).try(:price).to_f}.sum
    subtotal = glass_type_price + addons_price + glass_frame_price + suborder.material_copay.to_f + glass_lens_material
    total =  subtotal - (  suborder.discount.to_f * (subtotal/100))
    self.total = total
    self.save
    self.total
  end

  private
    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end

end
