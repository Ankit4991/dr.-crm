class PatientProfile < ActiveRecord::Base
  belongs_to :patient
  
  audited on: [:update,:create]
  
  before_destroy :remove_gateway_profile
  after_destroy :remove_associated_audits

  def get_details
    ActiveMerchant::Billing::Base.mode = AppConfig.authorize_net_mode.to_sym
    Rails.cache.fetch("#{cache_key}/patient_profile", expires_in: 12.hours) do
      cim_gateway.get_customer_profile(:customer_profile_id => self.customer_profile_id)
    end
  end
  
  def update_details(options)
    ActiveMerchant::Billing::Base.mode = AppConfig.authorize_net_mode.to_sym
    cim_gateway.update_customer_payment_profile(
      :customer_profile_id => self.customer_profile_id,
      :payment_profile => {
        :customer_payment_profile_id => self.customer_payment_profile_id,
        :payment => {
          :credit_card => ActiveMerchant::Billing::CreditCard.new(options)
        }
      }
    )
    update_card_brand_and_date options
    validate_card(options)
    patient.profile.update_attributes!(:validated => true)
  end
  
  def validate_profile(options)
    ActiveMerchant::Billing::Base.mode = AppConfig.authorize_net_mode.to_sym
    cim_gateway.validate_customer_payment_profile(
      :customer_profile_id => self.customer_profile_id,
      :customer_payment_profile_id => self.customer_payment_profile_id,
      :customer_address_id => self.customer_shipping_address_id,
      :card_code => options['verification_value'],
      :validation_mode => AppConfig.authorize_net_mode.to_sym
    )
  end
  
  #to create payment profile
  def create_payment_profile(options)
    ActiveMerchant::Billing::Base.mode = AppConfig.authorize_net_mode.to_sym
    payment_profile = {
      :customer_profile_id => self.customer_profile_id,
      :payment_profile => {
        :payment => {
          :credit_card => ActiveMerchant::Billing::CreditCard.new(
            number:              options[:number],
            verification_value:  options[:verification_value],
            month:               options[:month],
            year:                options[:year],
            first_name:          options[:first_name],
            last_name:           options[:last_name]
          )
        },
        :bill_to => {
          :first_name => options[:first_name],
          :last_name => options[:last_name],
          :address => options[:address],
          :state => options[:state],
          :city => options[:city],
          :zip => options[:zip_code],
          :country => options[:country]
        }
      }
    }
    response = cim_gateway.create_customer_payment_profile(payment_profile) 
    if response.params['customer_payment_profile_id'].present?
      self.update_attributes!(:customer_payment_profile_id => response.params['customer_payment_profile_id'])
    end
    update_card_brand_and_date options
    create_shipping_address(options)
    patient.profile.update_attributes!(:validated => true) if validate_card(options)
  end

  def create_shipping_address(options)
    ActiveMerchant::Billing::Base.mode = AppConfig.authorize_net_mode.to_sym
    shipping_address = {
      :customer_profile_id => self.customer_profile_id,
      :address => {
        :first_name => options[:first_name],
        :last_name => options[:last_name],
        :address => options[:address],
        :state => options[:state],
        :city => options[:city],
        :zip => options[:zip_code],
        :country => options[:country]
      }
    }
    response = cim_gateway.create_customer_shipping_address(shipping_address)
    if response.params['customer_address_id'].present?
      self.update_attributes!(:customer_shipping_address_id => response.params['customer_address_id'])
    end
  end
  
  def prev_audits
    self.audits.map{|audit| [audit.auditable_type,audit.created_at,audit.audited_changes,audit.try(:user).try(:name),audit.try(:user).try(:class).try(:name)]}
  end

  def card_trancated_pan
    "#{self.get_details.params['profile']['payment_profiles']['payment']['credit_card']['card_number']}"
  end

  private
    
    def cim_gateway
      ActiveMerchant::Billing::AuthorizeNetCimGateway.new(
        :login    => ENV["AUTHORIZE_NET_LOGIN"],
        :password => ENV["AUTHORIZE_NET_KEY"])
    end

    def remove_associated_audits
      self.audits.map{|adt| adt.delete}
    end
    
    def update_card_brand_and_date options
      self.update_attributes!(brand_name: ActiveMerchant::Billing::CreditCard.new(number:options[:number],verification_value: options[:verification_value],month:options[:month],year:options[:year],first_name:options[:first_name],last_name:options[:last_name]).brand, card_expiry_date: (Date.parse "1-#{options["month"]}-#{options["year"]}"))
    end

    def validate_card(options)
      validation_response = validate_profile(options)
      unless validation_response.success?
        self.destroy!
        self.errors[:base] << validation_response.message
        puts "+++++++++++++++++++++++++++++++++++++++++++FAIL VALIDATION #{validation_response.message}+++++++++++++++++++++++++++++"
        raise validation_response.message
        return false
      end
      return true
    end

    def remove_gateway_profile
      ActiveMerchant::Billing::Base.mode = AppConfig.authorize_net_mode.to_sym
      payment_profile = {
        :customer_profile_id => self.customer_profile_id
      }
      cim_gateway.delete_customer_profile payment_profile
    end

end
