##
# VisualAcuity is part of Exam Form => glass

class VisualAcuity < ActiveRecord::Base
  belongs_to :examination
  has_one :glass
  audited :associated_with => :examination
  
  def is_empty?
    whitelist = %w[ id created_at updated_at using_glasses]
    self.attributes.all? do |attr, val|
      whitelist.include?(attr) || val.nil? || val == ""
    end
  end

  def as_json(options={})
    super(options).reject { |k, v| v.nil? || v == ""}
  end

  def distance_pupillary?
    whitelist = %w[ distance_od distance_os distance_ou]
    whitelist.all? do |attr|
      self.as_json.keys.include?(attr)
    end 
  end

  def intermediate_pupillary?
    whitelist = %w[ intermediate_od intermediate_os intermediate_ou]
    whitelist.all? do |attr|
      self.as_json.keys.include?(attr)
    end
  end

  def near_pupillary?
    whitelist = %w[ near_od near_os near_ou]
    whitelist.all? do |attr|
      self.as_json.keys.include?(attr)
    end
  end

end
