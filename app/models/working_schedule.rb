##
# Working schedule polymorphic type and keep info of patient or store working hours.

class WorkingSchedule < ActiveRecord::Base
  belongs_to :trackable, polymorphic: :true, dependent: :destroy
  ## validations
  validates :trackable_id, :trackable_type, presence: true
  validates :trackable_type, inclusion: { in: %w(Doctor Store), message: "%{value} is not a valid trackable_type" }
  
	def get_schedule
		[[[self.sunday_start_time, self.sunday_end_time]],[[self.monday_start_time, self.monday_end_time]],[[self.tuesday_start_time, self.tuesday_end_time]],[[self.wednesday_start_time, self.wednesday_end_time]],[[self.thursday_start_time, self.thursday_end_time]],[[self.friday_start_time, self.friday_end_time]],[[self.saturday_start_time, self.saturday_end_time]]]
	end

	def off_days
		off_days = []
		if(monday_start_time == monday_end_time)
			off_days << "fc-mon"
		end
		if(tuesday_start_time==tuesday_end_time)
			off_days << "fc-tue"
		end
		if(wednesday_start_time == wednesday_end_time)
			off_days << "fc-wed"
		end
		if(thursday_start_time == thursday_end_time)
			off_days << "fc-thu"
		end
		if(friday_start_time == friday_end_time)
			off_days << "fc-fri"
		end
		if(saturday_start_time == saturday_end_time)
			off_days << "fc-sat"
		end
		if(sunday_start_time == sunday_end_time)
			off_days << "fc-sun"
		end
		off_days
	end
	
	def working_day?(day)
    return self.send("#{day.strftime('%A').downcase}_start_time") != self.send("#{day.strftime('%A').downcase}_end_time")
  end

  def remaining_month_days
  	return (Date.today..Date.today.end_of_month).to_a
  end
end
