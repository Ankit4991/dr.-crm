##
# Seo model store seo data(keywords, title) for seo required pages.

class Seo < ActiveRecord::Base
	belongs_to :organisation
	SeoViewNames = ['home', 'about_us', 'contact_us', 'book_appointment', 'features', 'pricing', 'testimonials', 'sign_up', 'login', 'patient_dashboard', 'doctor_dashboard']
	
end
