class OrderSubscription < ActiveRecord::Base
  
  before_create :add_organisation_id, :set_lenses_quantity
  after_destroy :remove_associated_audits

  enum status: [:inactive, :active]
  enum shippment_method: [:free_shipping,:pick_up_at_office]
  
  belongs_to :joint_resource
  belongs_to :organisation
  belongs_to :lens_prescription
  has_many :favourite_orders
  audited on: [:update,:create]

  def prev_audits
    self.audits.map{|audit| [audit.auditable_type,audit.created_at,audit.audited_changes,audit.try(:user).try(:name),audit.try(:user).try(:class).try(:name)]}
  end
  
  private
   
    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end

    def remove_associated_audits
      self.audits.map{|adt| adt.delete} if self.audits.present?
    end

    
    def set_lenses_quantity
      self.od_lens_quantity = (self.lens_prescription.modality_period / self.lens_prescription.lens_od_modality_period)
      self.os_lens_quantity = (self.lens_prescription.modality_period / self.lens_prescription.lens_os_modality_period)
    end
end
