##
# Background Model is responsible to save Background Images, 
# Doctor and Patient can upload background images.

class Background < ActiveRecord::Base
  belongs_to :doctor
  belongs_to :patient
  
  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" },
    url: "/uploads/backgrounds/:id/:style/:basename.:extension",
    path: ":rails_root/public/uploads/backgrounds/:id/:style/:basename.:extension"
  

  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  validates_with AttachmentSizeValidator, :attributes => :avatar, :less_than => 1.megabytes, :message => "File Size should not be more then 1mb !"
end
