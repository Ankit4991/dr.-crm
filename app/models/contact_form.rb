##
# ContactForm Model use to store contact form queries.

class ContactForm < ActiveRecord::Base
  before_create :add_patient
  before_create :add_organisation_id
  after_create :create_notifications

  belongs_to :patient
  belongs_to :organisation
  
  has_one :notification, :as => :event 
  has_many  :logs, :as => :trackable

  validates :email, :comment,  presence: true

  private
    def create_notifications
      doctors = self.organisation.doctors.map(&:id)
      self.create_notification!("doctor_ids"=>doctors, status: false)
      self.notification.doctors_notifications.map{|x| x.update(status: false)}
    end

    def add_patient
      patient = Patient.find_by(email: self.email)
      if self.existing && patient.present?
        self.patient_id = patient.id
      end
    end

    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end
end
