##
# ContactLensOrder model values are part of order form. There can be multiple contact lens orders for an order.
# This model value changes is recorded as its audits.

class ContactLensOrder < ActiveRecord::Base

  include CommonSuborder
  
  audited :associated_with => :order
  before_create :add_organisation_id,:set_default_options
  
  belongs_to :order
  belongs_to :organisation
  belongs_to :od_lens_type_price, class_name: "LensTypesPrice", foreign_key: "od_lens_types_price_id"
  belongs_to :os_lens_type_price, class_name: "LensTypesPrice", foreign_key: "os_lens_types_price_id"
  belongs_to :lens_prescription
  belongs_to :doctor
  
  has_many :lenses, class_name: "Lens"
  accepts_nested_attributes_for :lenses,reject_if: :all_blank
  
  acts_as_paranoid
  
  def checklist
    {
      :od_checkmark => self.od_checkmark,
      :os_checkmark => self.os_checkmark,
      :order_checkmark => self.order_checkmark
    }
  end

  def overall_discount
    (cl_fit.to_f + allowance.to_f + (discount.to_f * total.to_f)).round(2)
  end

  def is_empty_lens_details?
    self.lenses.first.is_empty?
  end

  #used as remaining option
  def allowance_cl_fit_balance
     allowance.to_f - cl_fit.to_f;
  end

  def discount_option
    (discount.to_f > 0 && discount.to_f < 1) ? discount.to_f : (discount.to_f/100)
  end

  def calculated_total
    if od_lens_type_price.present? && os_lens_type_price.present?
      od_qnt_value = applicable_od_patient_pays * od_qnt.to_i
      os_qnt_value = applicable_os_patient_pays * os_qnt.to_i
      total =  od_qnt_value + os_qnt_value - (allowance_cl_fit_balance < 0 ? 0 : allowance_cl_fit_balance )
      total = (total > 0) ? (total + material_copay.to_f) : material_copay
    elsif od_lens_type_price.present?
      od_qnt_value = applicable_od_patient_pays * od_qnt.to_i
      total =  od_qnt_value - (allowance_cl_fit_balance < 0 ? 0 : allowance_cl_fit_balance )
      total = (total > 0) ? (total + material_copay.to_f) : material_copay
    elsif os_lens_type_price.present?
      os_qnt_value = applicable_os_patient_pays * os_qnt.to_i
      total = os_qnt_value - (allowance_cl_fit_balance < 0 ? 0 : allowance_cl_fit_balance )
      total = (total > 0) ? (total + material_copay.to_f) : material_copay
    else
      total = "0.00".to_f
    end
  end

  def od_lens_modality
    self.lenses.first.try(:od_lens_type_price).try(:modality) || 1
  end

  def os_lens_modality
    self.lenses.first.try(:os_lens_type_price).try(:modality) || 1
  end
  
  def applicable_od_patient_pays
    od_price.to_f - (od_price.to_f * discount_option)
  end

  def applicable_os_patient_pays
    os_price.to_f - (os_price.to_f * discount_option)
  end

  private
    def add_organisation_id
      self.organisation_id = Organisation.current.try(:id) || 1 unless self.organisation_id.present?
    end
    def set_default_options
      if self.lens_prescription.present?
        order_lens = self.lens_prescription.lens
        self.od_price = order_lens.try(:od_lens_type_price).try(:price)
        self.os_price = order_lens.try(:os_lens_type_price).try(:price)
        self.od_qnt,self.os_qnt = 0,0
        self.total = self.calculated_total
      end
    end
end
