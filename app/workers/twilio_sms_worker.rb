# Resque Job for sending SMS through Twilio
class TwilioSmsWorker
  include Sidekiq::Worker
  include TwilioMessage
  sidekiq_options :queue => :sms, :retry => false

  # Send a message with Twilio
  def perform(*args)
    TwilioMessage.send_message args[0]
  end
end