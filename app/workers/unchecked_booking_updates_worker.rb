class UncheckedBookingUpdatesWorker
  
  include Sidekiq::Worker
  include Sidetiq::Schedulable
  sidekiq_options :retry => 1

  # start from midnight
  recurrence do
    hourly.minute_of_hour(0, 15, 30, 45)
  end 

  def perform
    puts "************ Sidetiq worker called ***************************  #{Time.zone.now.strftime("%m/%d/%Y at %I:%M%p")} ***************************************"
    Organisation.all.each do |org|
      begin
        Time.zone = org.time_zone
        time_in = Time.zone.now-2.hours               #if this rake is breaking over server (by checking cron logs) ,then you can replace the time.now-n.days to remove missed appointment examinations as it might raise validation fails error for 'last examination not delivered yet!' 
        time_out = Time.zone.now-1.hour
        passed_bookings = org.bookings.where(end_time: time_in..time_out,check_in: nil,status: 0)
        passed_bookings.each do |booking|
          booking.update_columns(:status => 4)        # no show
          puts "-----------------##{booking.id} Booking with no examination ; booking status updated to No Show"
        end
      rescue 
        puts "---------------------------------- UncheckedBookingUpdatesWorker ---------------------------------"
      end
    end
  end

end