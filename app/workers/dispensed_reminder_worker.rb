class DispensedReminderWorker
  
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  sidekiq_options :retry => 1
 
  recurrence { daily }

  def perform
    begin
      puts "Dispensed Worker started-------------------------"
      Order.ready_for_pick_up.each do |order|
        if order.try(:doctor).try(:store).present?
          unless !order.try(:doctor).try(:store).try(:open_today?) && (order.dispensed_delay_count % 2 != 0)
            if order.try(:doctor).present? && order.try(:doctor).try(:working_schedule).present?
              day_start_time = order.try(:doctor).try(:working_schedule).try("#{Date.today.strftime('%A').downcase}_start_time")
              AppMailer.delay_until(Time.utc(Time.now.year,Time.now.month,Time.now.day,day_start_time.try(:hour),day_start_time.try(:min))).send_order_dispensed_to_patient(order) unless order.try(:joint_resource).try(:patient).try(:email_not_supplied)
            else
              AppMailer.delay.send_order_dispensed_to_patient(order) unless order.try(:joint_resource).try(:patient).try(:email_not_supplied)
            end
            dispense_log = order.dispense_logs.build(:note => "Order is ready to be picked up",:logged_at => Time.zone.now,:emailed => true)
            dispense_log.save!
          end
          puts "-------------------- Order Dispensed Count : #{order.dispensed_delay_count}----------------------"
          order.increment!(:dispensed_delay_count,1)
        end
      end
      FavouriteOrder.ready_for_pick_up.each do |order|
        if order.try(:doctor).try(:store).present?
          unless !order.try(:doctor).try(:store).try(:open_today?) && (order.dispensed_delay_count % 2 != 0)
            if order.try(:doctor).present? && order.try(:doctor).try(:working_schedule).present?
              day_start_time = order.try(:doctor).try(:working_schedule).try("#{Date.today.strftime('%A').downcase}_start_time")
              AppMailer.delay_until(Time.utc(Time.now.year,Time.now.month,Time.now.day,day_start_time.try(:hour),day_start_time.try(:min))).send_renewed_order_dispensed_to_patient(order) unless order.try(:joint_resource).try(:patient).try(:email_not_supplied)
            else
              AppMailer.delay.send_renewed_order_dispensed_to_patient(order) unless order.try(:joint_resource).try(:patient).try(:email_not_supplied)
            end
            dispense_log = order.dispense_logs.build(:note => "Order is ready to be picked up",:logged_at => Time.zone.now,:emailed => true)
            dispense_log.save!
          end
          puts "---------------------------------- Order Dispensed Count : #{order.dispensed_delay_count} ---------------------------------"
          order.increment!(:dispensed_delay_count,1)
        end
      end
    rescue
      puts "---------------------------------- DispensedReminderWorker ---------------------------------"
    end
  end
end