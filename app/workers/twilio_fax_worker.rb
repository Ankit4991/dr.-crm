# Resque Job for Fax through Twilio
class TwilioFaxWorker
  include Sidekiq::Worker
  include TwilioFax
  sidekiq_options :queue => :fax, :retry => false

  # Send a message with Twilio
  def perform(*args)
    TwilioFax.send_fax args[0]
  end
end
  

