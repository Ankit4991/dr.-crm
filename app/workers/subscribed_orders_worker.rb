class SubscribedOrdersWorker
   
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  sidekiq_options :retry => 1
 
  recurrence { daily }

  def perform
    begin
      todays_order_subscriptions = OrderSubscription.where("end_time <=? and active=?",Date.today.next_week,true).order(:updated_at => :desc).flatten
      todays_order_subscriptions.each do |subscription|
        begin
          if subscription.lens_prescription.in_use?
            subscription_od_lens_type = LensTypesPrice.find_by_brand_name(subscription.try(:lens_prescription).try(:lens).try(:od_type))
            subscription_os_lens_type = LensTypesPrice.find_by_brand_name(subscription.try(:lens_prescription).try(:lens).try(:os_type))
            subtotal = (subscription_od_lens_type.try(:price) * subscription.try(:od_lens_quantity) + subscription_os_lens_type.try(:price) * subscription.try(:os_lens_quantity)).round(2)
            total = subtotal - (subtotal * 0.15)
            subscribed_order = subscription.try(:favourite_orders).create!(:lens_orders => { :lens_od_quantity => subscription.try(:od_lens_quantity),:lens_os_quantity => subscription.try(:os_lens_quantity),:lens_od_unit_price => subscription_od_lens_type.try(:price),:lens_os_unit_price => subscription_os_lens_type.try(:price)},:lens_prescription_id => subscription.try(:lens_prescription_id),:joint_resource_id => subscription.try(:joint_resource_id),:shippment_method => subscription.try(:shippment_method) ,:organisation_id => subscription.try(:organisation_id),:total => total ,:discount => '0.15')
            subscription.end_time += subscription.try(:interval).try(:months)
            subscription.save
            AppMailer.delay.send_renewed_order_confirmation_to_patient(subscribed_order) unless subscribed_order.try(:joint_resource).try(:patient).try(:email_not_supplied)
          else
            subscription.active = false
            subscription.save
            AppMailer.delay.send_failed_subscription_to_patient(subscription) unless subscription.try(:joint_resource).try(:patient).try(:email_not_supplied)
          end
        rescue Exception => e
          puts "Last subscription error due to #{e}"
        end
      end
    rescue
      puts "---------------------------------- SubscribedOrdersWorker ---------------------------------"
    end
  end
end