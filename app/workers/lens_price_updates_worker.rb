class LensPriceUpdatesWorker
  
  def perform lens_type_price 
    begin
      #update all lens prescription dependant lens for modalities stuff
      lens_prescriptions_in_use = lens_type_price.try(:organisation).try(:lens_prescriptions).where('created_at::date >= ?',Date.today - 1.year).flatten
      lens_prescriptions_in_use.each do |lens_prescription|
        lens_prescription.lens.save
      end
     
      #update all Contact lens order having the lens type either for OD eye  or OS eye and its order status hasn't been remove yet
        #contact_lens_orders = lens_type_price.organisation.contact_lens_orders.joins(:order).where("(od_lens_types_price_id =? OR os_lens_types_price_id =?) AND orders.status < ?",lens_type_price.id,lens_type_price.id,5)
      
      lenses = lens_type_price.try(:organisation).try(:lenses).where("(od_lens_types_price_id=? OR os_lens_types_price_id=?) AND contact_lens_order_id IS NOT NULL",lens_type_price.id,lens_type_price.id)
      contact_lens_orders = lenses.map{|lens| unless ['dispensed'].include? lens.try(:contact_lens_order).try(:order).try(:status)
                                                 lens.try(:contact_lens_order)
                                              end 
                                      }

      contact_lens_orders.compact.each do |lens_order|
        if lens_order.od_lens_type_price == lens_type_price
          lens_order.od_price = lens_type_price.price
          lens_order.total = lens_order.calculated_total
          lens_order.save
        elsif lens_order.os_lens_type_price == lens_type_price
          lens_order.os_price = lens_type_price.price
          lens_order.total = lens_order.calculated_total
          lens_order.save
        end
      end
    rescue
      puts "---------------------------------- LensPriceUpdatesWorker ---------------------------------"
    end
  end
  
end