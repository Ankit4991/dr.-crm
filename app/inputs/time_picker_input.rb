class TimePickerInput < SimpleForm::Inputs::StringInput
  def input(wrapper_options)
    set_html_options
    set_value_html_option

    template.content_tag :div, class: 'input-group timepicker' do
      input = super(wrapper_options) # leave StringInput do the real rendering
      input + input_button
    end
  end

  def input_html_classes
    super.push 'timepicker'   # 'form-control'
  end

  private

  def input_button
    template.content_tag :span, class: 'input-group-btn' do
      template.content_tag :button, class: 'btn btn-default', type: 'button' do
        template.content_tag :span, '', class: 'fa fa-clock-o'
      end
    end
  end

  def set_html_options
    input_html_options[:type] = 'text'
    input_html_options[:data] ||= {}
    input_html_options[:data].merge!(date_options: date_options)
  end

  def set_value_html_option
    return unless value.present?
    input_html_options[:value] ||= I18n.localize(value, format: display_pattern)
  end

  def value
    object.send(attribute_name) if object.respond_to? attribute_name
  end 

  def display_pattern
    I18n.t('timepicker.dformat', default: '%I:%M %p')
  end

  def picker_pattern
    I18n.t('timepicker.pformat', default: 'HH:mm')
  end

  def date_options_base
    {
        locale: I18n.locale.to_s,
        format: picker_pattern
    }
  end

  def date_options
    date_options_base
  end

end

