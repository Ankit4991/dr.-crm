/*--------------------------Methods call from ui page like onclick start-----------------*/  
  function externalInternalClick() {
    setTimeout(
      function() {
        icheck(); 
        ComponentsPickers.init();        
    }, 4000);
  }
  // method used in html file
  function examFormSubmit(){
    if($(".exam-form" ).length > 0)
    {
      $.post($(".exam-form" ).attr("action"), $(".exam-form" ).serialize())
        .done(function( data ) {
          $('#'+data.partial_tab).trigger('click');
            jQuery.gritter.add({ title: data.status, text: data.msg });
        });
    }
  }

  function examFormSubmitEnter(){
    $(".exam-form").on("keypress", function (e) {
      if (e.keyCode == 13) {
        return false;
        }
    });
    $('.os_copy').click(function(){
      $(this).parent().parent().find('.sphere_od').val($(this).parent().parent().next().find('.sphere_os').val())
      $(this).parent().parent().find('.cylinder_od').val($(this).parent().parent().next().find('.cylinder_os').val())
      $(this).parent().parent().find('.axis_od').val($(this).parent().parent().next().find('.axis_os').val())
      $(this).parent().parent().parent().next().find('.distance_od').val($(this).parent().parent().parent().next().find('.distance_os').val())
      $(this).parent().parent().parent().next().find('.near_od').val($(this).parent().parent().parent().next().find('.near_os').val())
    })

    $('.od_copy').click(function(){
      $(this).parent().parent().find('.sphere_os').val($(this).parent().parent().prev().find('.sphere_od').val())
      $(this).parent().parent().find('.cylinder_os').val($(this).parent().parent().prev().find('.cylinder_od').val())
      $(this).parent().parent().find('.axis_os').val($(this).parent().parent().prev().find('.axis_od').val())
      $(this).parent().parent().parent().next().find('.distance_os').val($(this).parent().parent().parent().next().find('.distance_od').val())
      $(this).parent().parent().parent().next().find('.near_os').val($(this).parent().parent().parent().next().find('.near_od').val())
    })
  }

  $(window).bind('beforeunload', function(){
    examFormSubmit();
  });
  
  var contact_ids = [];
  var contact_email = [];
  var fax = [];
  $(document).on( "railsAutocomplete.select", '.contact-autocomplete', function(event, respose_data){
    var exam_id = $(".contact-autocomplete").attr('id');
    var id = respose_data.item.id;
    var email = respose_data.item.email;
    var contact_fax = respose_data.item.fax;
    $.ajax({
      type: "get",
      dataType: 'script',
      url:  "/examinations/"+ exam_id +"/referrals/find_contact",
      data: {id: respose_data.item.id},
      success: function(response) {
        contact_ids.push(id);
        contact_email.push(email);
        fax.push(contact_fax);
        $('#contact_ids').val(contact_ids);
        $('#refferal_cc').val(contact_email);
        $('#fax').val(fax);
        $(".clear_input").val("");
      }
    });
  });
  $(document).on('click','.cross',function() {
    var id = $(this).next().val();
    var email = $(this).next().attr('name');
    contact_email.splice(contact_email.indexOf(email), 1);
    contact_ids.splice(contact_ids.indexOf(id), 1);
    $('#contact_ids').val(contact_ids);
    $('#refferal_cc').val(contact_email);
    $(this).parent().remove();
  });
/*--------------------------Methods call from ui page like onclick start-----------------*/  


/* -------method will cal on page change like on edit or after live update Start-----*/
  function examinationsFormPage(){
    $('body').removeClass("modal-open");
    $loading = $('.spinner').hide();

    // $('.exam-form').sisyphus();
    $('.disable_input :input').prop('disabled', true);
    $(".glass_section a.add_fields").data("association-insertion-method", 'append').data("association-insertion-node", '.glass_append');
    $(".lens-section a.add_fields").data("association-insertion-method", 'append').data("association-insertion-node", '.lens-append');
    
    referrals();
    disease_toggle();  
   
    var availableGlassTypes = ["Single Vision","Progessive","Bifocal","Trifocal"]
    $(".eye-glass-type" ).autocomplete({
      source: availableGlassTypes
    });  

    function referrals(){
      $("#referral-btn").bind('ajax:complete', function () {
        $loading.hide();   
        $("#referral-modal").modal();
        $("#old-referral-modal").modal('hide');
      });

      $("#old-referral-btn").bind('ajax:complete', function () {
        $loading.hide();
        $("#old-referral-modal").modal();
      });

      $("#print-btn").bind('ajax:complete', function () {
        $loading.hide();
        $("#print-modal").modal();
      });
    }
    // load exam when select exam from top dropdown
    $(document).on('change',"#exam_drop",function(e) {
      window.location = "/examinations/"+$("#exam_drop option:selected").attr('value')+'/edit';
    });

    // delete upload doc under exam attached doc tab
    $(document).on('click','.image_delete',function(){
      $(this).parent().remove();
    })
    
    /* ....................patient follow ups .............................*/
  }
/* -------method will cal on page change like on edit or after live update End-----*/


/* ---------method for exam form Live updation Start---------------------*/
  function examLiveEvent(examination_id, current_doctor_id,myFirebaseRef){
    var current_examination = examination_id;
    var current_doctor = current_doctor_id;

    $(document).on('change',".provider_change",function(event) { 
      $.post($(".exam-form" ).attr("action"), $(".exam-form" ).serialize())
        .done(function( data ) {
          jQuery.gritter.add({ title: data.status, text: data.msg });
        });  
      $('#overview_tab').trigger('click');
      $element_id = $(this).attr('id');
      $element_val = $(this).val();    
      myFirebaseRef.child($element_id).transaction(function(data){ return $element_val });
      
      console.log('current-doctor'+current_doctor);
      var new_user_hash = {};
      new_user_hash[current_doctor] = $.now();
      myFirebaseRef.child("users").transaction(function(data){ return $.extend(data,new_user_hash) });
    });
    
    // most of firebase events --------------start--------
    
    myFirebaseRef.on('child_changed',function(childSnapshot, prevChildKey) {
      onFirebaseAction(childSnapshot, prevChildKey)
    });

    myFirebaseRef.on("child_added", function(childSnapshot, prevChildKey) {
      onFirebaseAction(childSnapshot, prevChildKey);
      //$('.spinner').hide();
    });


    // Action that will update all online user and make data synchronization  
    
    function onFirebaseAction(childSnapshot, prevChildKey)
    { 
      if(childSnapshot.key() == "select_exam" || childSnapshot.key() == "selected_exam" || childSnapshot.key() == "exam_drop" )
      {
      }
      else
      {
        console.log("CHILD CHANGED DATA  => "+childSnapshot.key()+":"+childSnapshot.val()+" previous key"+prevChildKey);
        
        if(childSnapshot.key() == "users"){
          $.ajax({ 
                  type: "GET", 
                  url: "/orders/online_users?ids="+Object.keys(childSnapshot.val()),
                  success: function(data){ 
                }
          });
        }

        console.log("CHILD ADDED DATA  => "+childSnapshot.key()+":"+childSnapshot.val()+" previous key"+prevChildKey);
        $('#'+childSnapshot.key()).val(childSnapshot.val());
        if(childSnapshot.key() == 'update-contact-lens')
        {
          updateContactLens(childSnapshot.val());
        }  


        if($('#'+childSnapshot.key()).length > 0 && $('#'+childSnapshot.key()).hasClass('tab_reload')){
          var url = $('#'+childSnapshot.key()).attr('data-href');
          $('#tab_show').load( url, function( response, status, xhr ) {         
            icheck();      
            examinationsFormPage();
            initializePlugins();
          });
        }
      }
    }
    
    //Firebase event generate on these click actions --------start-------
    $(document).on('click', ".exam_tabs a", function(event){
      event.preventDefault();
      $('.spinner').show();
      url = $(this).attr('data-href');
      var data = $('.exam-form').serializeArray();   
      formSubmit(data);
      $('#tab_show').load( url, function( response, status, xhr ) {   
        $('.spinner').hide();
        icheck();      
        examinationsFormPage();
        initializePlugins();      
      });
      if (event.currentTarget.id == "overview_tab")
      {
        $('#print_overview').html('');
      }
    })

    $(document).on('click', ".attached-doc-btn", function(event){
      event.preventDefault();
      var path = $(this).attr('data-href');
      var page = $(this).attr('data-page');
      var form_data = new FormData();
      var file_data = $("#attached_file").prop("files")[0];
      form_data.append("attached_file", file_data);
      $.ajax({
        url: path,
        type: 'patch',
        data: form_data,
        contentType: false,
        processData: false, 
        beforeSend: function() {
          $("#progress-bar").width('0%');
        },
        success: function(data){
          if (data.status == "Success")
          {
            $('#doc_badge').text(parseInt($('#doc_badge').text()) + 1);
          }
          jQuery.gritter.add({ title: data.status, text: data.msg });
          myFirebaseRef.child(page).transaction(function(data){ return $.now()});
        },
        xhr: function(){
            // get the native XmlHttpRequest object
            var xhr = $.ajaxSettings.xhr() ;
            // set the onprogress event handler
            xhr.upload.onprogress = function (event){ 
              $("#progress-bar").width(event.loaded/event.total*100 + '%');
              $("#progress-bar").html('<div id="progress-status">' + event.loaded/event.total*100 +' %</div>')
            }
            // return the customized object
            return xhr ;
        }
      });
    })

    $(document).on('click', ".add-new-btn, .delete-btn, .copy-to-objective, .copy-to-subjective", function(event){
      event.preventDefault();
      event.stopImmediatePropagation();
      $('.spinner').show();
      var page = $(this).attr('data-page');
      var data = $('.exam-form').serializeArray();
      data.push({name: 'commit', value: $(this).attr('data-value')});    
      $('.spinner').show();      
      formSubmit(data);
      myFirebaseRef.child(page).transaction(function(data){ return $.now()}); 
      
    })

    $(document).on('click', ".procedure-or-impression-delete-btn", function(event){
      event.preventDefault();
      event.stopImmediatePropagation();
      $('.spinner').show();
      var page = $(this).attr('data-page');
      var data = $('.exam-form').serializeArray();
      data.push({name: 'commit', value: $(this).attr('data-value')});    
      $('.spinner').show();      
      //formSubmit(data);
      myFirebaseRef.child(page).transaction(function(data){ return $.now()}); 
      
    })
    //Firebase event generate on these actions --------end-------

    //Firebase event generate on these Autocomplete actions --------start-------
    $(document).on( "railsAutocomplete.select", '.procedure_autocomplete_field', function(event, data){
      if (data.item.id == "new_procedure")
      {
        $('#procedure_modal').modal();
        $('#procedures_1_name').val(data.item.value);
      }
      else
      {
        $('.spinner').show();
        if(data.item.id!="")
        {
          $(this).before('<input type="text" class="hidden" value="'+data.item.id+'" name="examination[procedure_adjustments_attributes]['+Date.now()+'][exam_procedure_id]"/>');

          $('.spinner').show();
          data = $(".exam-form" ).serialize();
          formSubmit(data);     
          myFirebaseRef.child("procedure_impression_plan_page").transaction(function(data){ return $.now()});    
        }
        this.value="";
      }
    });
    
    $(document).on( "railsAutocomplete.select", '.impression_autocomplete_field', function(event, data){
      if (data.item.id == "new_imp")
      {
        $('#Impression_modal').modal();
        $('#impressions_1_name').val(data.item.value);

      }
      else
      {
        $('.spinner').show();
        var time = (new Date).getTime();
        if(data.item.id!="")
        {
          $(this).before('<input class="hidden" type="radio" value="OU" checked="checked" name="examination[exam_impressions_attributes]['+time+'][eye]"><input class="hidden" type="radio" value="OS" name="examination[exam_impressions_attributes]['+time+'][eye]" ><input class="hidden" type="radio" value="OD" name="examination[exam_impressions_attributes]['+time+'][eye]" ><input class="hidden" type="text" value="" name="examination[exam_impressions_attributes]['+time+'][plan]"><input type="hidden" value="'+data.item.id+'" name="examination[exam_impressions_attributes]['+time+'][impression_id]">')

          $('.spinner').show();
          data = $(".exam-form" ).serialize();
          formSubmit(data);       
          myFirebaseRef.child("procedure_impression_plan_page").transaction(function(data){ return $.now()});
          
        }
        this.value="";
      }
    });
    
    $(document).on( "railsAutocomplete.select", '.disease_autocomplete_field', function(event, data){
      if (data.item.id == "pmhxes")
      {
        $('#medical_history_modal').modal();
        $('#disease_pmhxes').val(data.item.value); 
      }
      else if(data.item.id == "pohxes")
      {
        $('#oculus_history_modal').modal();
        $('#disease_pohxes').val(data.item.value);
      }
      else if(data.item.id == "medications")
      {
        $('#psychology_history_modal').modal();
        $('#disease_medications').val(data.item.value);
      }
      else if(data.item.id == "sxhxes")
      {
        $('#surgery_history_modal').modal();
        $('#disease_surgery').val(data.item.value);
      }
      else if(data.item.id == "allergies")
      {
        $('#allergy_history_modal').modal();
        $('#disease_allergy').val(data.item.value);
      }
      else
      {
        $('.spinner').show();
        var data_type=$(this).attr("data-type");
        var time = (new Date).getTime();
        var type_parmas = $(this).attr("data-type-params");
        if(data.item.id!="")
        {
          $(this).before('<input  class="hidden" type="checkbox" value="1" name="examination[joint_resource_attributes]['+type_parmas+'_attributes]['+data_type+'_attributes]['+time+'][checked]"  checked="checked"><input type="hidden" value="'+data.item.id+'" name="examination[joint_resource_attributes]['+type_parmas+'_attributes]['+data_type+'_attributes]['+time+'][disease_id]"><input class="hidden" type="text" name="examination[joint_resource_attributes]['+type_parmas+'_attributes]['+data_type+'_attributes]['+time+'][notes]">')

          $('.spinner').show();
          data = $(".exam-form" ).serialize();
          formSubmit(data);
          myFirebaseRef.child("medical_social_history_page").transaction(function(data){ return $.now()});
          
        }
        this.value="";
      }
    });
      
    $(document).on( "railsAutocomplete.select", '.family_disease_autocomplete_field', function(event, data){
      if (data.item.id == "fmhxes")
      {
        $('#family_medical_history_modal').modal();
        $('#disease_fmhxes').val(data.item.value);
      }
      else if (data.item.id == "fohxes")
      {
        $('#family_oculus_history_modal').modal();
        $('#disease_fohxes').val(data.item.value);
      }
      else{
        $('.spinner').show();
        var data_type=$(this).attr("data-type");

        var relation_value = $(this).parent().find('select').val()
        var time = (new Date).getTime();
        var type_parmas = $(this).attr("data-type-params");
        if(data.item.id!="")
        {
          $(this).before('<input  type="text" value='+relation_value+' name="examination[joint_resource_attributes]['+type_parmas+'_attributes]['+data_type+'_attributes]['+time+'][relation]" class="hidden"><input type="checkbox" value="1" name="examination[joint_resource_attributes]['+type_parmas+'_attributes]['+data_type+'_attributes]['+time+'][checked]" checked="checked" class= "hidden"><input type="hidden" value="'+data.item.id+'" name="examination[joint_resource_attributes]['+type_parmas+'_attributes]['+data_type+'_attributes]['+time+'][disease_id]">');

          $('.spinner').show();
          data = $(".exam-form" ).serialize();
          formSubmit(data);        
          myFirebaseRef.child("medical_social_history_page").transaction(function(data){ return $.now()});
  
        }
        this.value="";
      }
    });
      
    $(document).on( "railsAutocomplete.select", '.add_new_family_disease_autocomplete_field', function(event, data){
      if (data.item.id == "fmhxes")
      {
        $('#family_medical_history_modal').modal();
        $('#disease_fmhxes').val(data.item.value);
      }
      else if (data.item.id == "fohxes")
      {
        $('#family_oculus_history_modal').modal();
        $('#disease_fohxes').val(data.item.value);
      }
      else{
        var data_type=$(this).attr("data-type");
        var time = (new Date).getTime();
        var type_parmas = $(this).attr("data-type-params");
        if(data.item.id!="")
        {
          $(this).before('<input type="checkbox" value="1" name="examination[joint_resource_attributes]['+type_parmas+'_attributes]['+data_type+'_attributes]['+time+'][checked]" checked="checked" class="hidden"><input type="hidden" value="'+data.item.id+'" name="examination[joint_resource_attributes]['+type_parmas+'_attributes]['+data_type+'_attributes]['+time+'][disease_id]" class="hidden">');

          $('.spinner').show();
          data = $(".exam-form" ).serialize();
          formSubmit(data); 
          myFirebaseRef.child("medical_social_history_page").transaction(function(data){ return $.now()});
        }
        this.value="";
      }
    });

    $(document).on( "railsAutocomplete.select", '.contact-type-autocomplete', function(event, respose_data){
      if (respose_data.item.label == "Add New")
      {
        $('#contact_lens_type_modal').modal();
      }
      else
      {
        $(this).parent().next().find('.hidden').val(respose_data.item.id);
        data = $(".exam-form" ).serialize();
        formSubmit(data);
        $element_id = $(this).attr('id');
        $element_val = $(this).val();    
        myFirebaseRef.child($element_id).transaction(function(data){ return $element_val });
        respose_data['item']['element_id'] = $(this).attr('id');      
      
        myFirebaseRef.child('update-contact-lens').transaction(function(data){ return respose_data.item});     
      }
    }); 
    //Firebase event generate on these Autocomplete actions --------end-------

    $(document).on('change', '.relation_select', function(){
      var relation_value = $(this).val();
      $(this).parent().parent().parent().parent().find('.relation_field').each(function( index ) {
        $( this ).val(relation_value); 
      });
    });

    //Firebase event generate on these Icheck actions
    $(document).on('ifToggled','.exam-form input', function(){
      $('.spinner').show();
      if($(this).hasClass('parent-toggle'))
      {
        $('.spinner').hide();
      }
      else
      {
        data = $(".exam-form" ).serialize();
        formSubmit(data);       
        var pageID = $(".tab_reload").attr('id')
        myFirebaseRef.child(pageID).transaction(function(data){ return $.now()}); 
        
      }
    })  

    function updateContactLens(item){
      var elementId = '#'+item.element_id;
      $(elementId).parent().next().find('.hidden').val(item.id);
      $(elementId).parent().parent().parent().next().find('.base_curve select').html(item.select_base_curve_options)
      $(elementId).parent().parent().parent().next().find('.diameter select').html(item.select_diameter_options)
      $(elementId).parent().parent().parent().next().find('.axis select').html(item.axis)
      if(item.element_id.split('_')[8] == "os"){
        var sphere_value = $(elementId).parent().parent().parent().next().find('.os_sphere select').val()
        $(elementId).parent().parent().parent().next().find('.os_sphere select').html(item.sphere)
        $(elementId).parent().parent().parent().next().find('.os_sphere select').val(sphere_value)
        $(elementId).parent().parent().parent().next().find('.os_cylinder select').html(item.cylinder)
        $(elementId).parent().parent().parent().next().find('.os_add select').html(item.add)
        $(elementId).parent().parent().parent().next().find('.os_axis input').html(item.axis)
      }
      else if(item.element_id.split('_')[8] == "od"){
        var sphere_value = $(elementId).parent().parent().parent().next().find('.od_sphere select').val()
        $(elementId).parent().parent().parent().next().find('.od_sphere select').html(item.sphere)
        $(elementId).parent().parent().parent().next().find('.od_sphere select').val(sphere_value)
        $(elementId).parent().parent().parent().next().find('.od_cylinder select').html(item.cylinder)
        $(elementId).parent().parent().parent().next().find('.od_add select').html(item.add)
        $(elementId).parent().parent().parent().next().find('.od_axis input').html(item.axis)
      }

      if(item.lens_type == "multifocal"){
        $(elementId).parent().parent().parent().next().find('.axis').addClass('hidden');
        $(elementId).parent().parent().parent().next().find('.cylinder').addClass('hidden');
        $(elementId).parent().parent().parent().next().find('.add').removeClass('hidden');
      }
      else if(item.lens_type == "sphere")
      {
        $(elementId).parent().parent().parent().next().find('.axis').addClass('hidden');
        $(elementId).parent().parent().parent().next().find('.cylinder').addClass('hidden');
        $(elementId).parent().parent().parent().next().find('.add').addClass('hidden');
      }
      else if(item.lens_type == "toric")
      {
        $(elementId).parent().parent().parent().next().find('.axis').removeClass('hidden');
        $(elementId).parent().parent().parent().next().find('.cylinder').removeClass('hidden');
        $(elementId).parent().parent().parent().next().find('.add').addClass('hidden');
      }
      else
      {
        $(elementId).parent().parent().parent().next().find('.axis').removeClass('hidden');
        $(elementId).parent().parent().parent().next().find('.cylinder').removeClass('hidden');
        $(elementId).parent().parent().parent().next().find('.add').removeClass('hidden');
      }     
    }

    
    function formSubmit(data)
    {
      $.ajax({
        url: $(".exam-form" ).attr("action"),
        type: 'POST',
        data: data,
        async: false
      });
    } 
    

    $(document).on('click','.followups',function(){
      $loading.show();
      var patient_id = $(this).data('id');
      var patient_name  = $(this).data('name');
      console.log(' followups  data -- patient_id :'+patient_id+'   -- patient_name : '+patient_name);
      $.ajax({ 
         type: "GET",
         url: "/follow_ups",
         data: 'id='+patient_id,
         dataType: "JSON",
         async: false
         }).success(function(data){
             $('#followup-modal').find('.modal-header').html("<h3><i class='fa fa-user fa-2x'></i>&nbsp;&nbsp;"+patient_name+"&nbsp;Follow up Log</h3>");
             $('#followup-modal').find('.modal-body').html(data.html);
             $('#followup-modal').find('.modal-footer').html("<a class='btn btn-default' data-dismiss='modal'>Close</a> <a href=# data-id="+patient_id+" data-name="+patient_name+" class='add_new_follow_up btn btn-primary'>Add New</a>");
             $('#followup-modal').modal('show');
             $loading.hide();
        });
    });
    
    $(document).on('click','.add_new_follow_up',function(){
      $loading.show();
      var patient_id = $(this).data('id');
      var patient_name  = $(this).data('name');
      console.log(' followups  data -- patient_id :'+patient_id+'   -- patient_name : '+patient_name);
      $.ajax({ 
         type: "GET",
         url: "/follow_ups/new",
         data: 'id='+patient_id,
         dataType: "JSON",
         async: true
         }).success(function(data){
            if(data.status == "Error"){
              $('#followup-modal').modal('hide');
              jQuery.gritter.add({ title: data.status, text: data.msg });
            } else { 
              $('#followup-modal').find('.modal-header').html("<h3><i class='fa fa-user fa-2x'></i>&nbsp;&nbsp;"+patient_name+"&nbsp;Follow up Log</h3>");
              $('#followup-modal').find('.modal-body').html(data.html);
              $('#followup-modal').find('.modal-footer').html("<a class='btn btn-default' data-dismiss='modal'>Close</a> <a href=# data-id="+patient_id+" data-name="+patient_name+" class='save_follow_up btn btn-primary'>Save</a>");
              $('#followup-modal').modal('show');
              $loading.hide();
            }
        });
    });
    $(document).on('click','.save_follow_up',function(event){
      $(this).attr("disabled", true);
      var myform = $('#new_follow_up').serialize();
      var patient_id = $(this).data('id');
      console.log('patient id :'+ patient_id);
      var patient_name  = $(this).data('name');
      $.ajax({ 
         type: "POST",
         url: "/follow_ups",
         data: myform,
         dataType: "JSON"
         }).success(function(data){
           var element;
           $('.followups').each(function(){ if($(this).data('id')== patient_id ){ element=$(this) } });
           console.log('element is'+element);
           if( typeof element != "undefined" ) { element.trigger('click');}
        }).error(function(data){
           $('#error_explanation').html(data.responseJSON.error);
           $('#error_explanation').show();
           return false;
        });
    });
  }

/* ---------method for exam form Live updation Start---------------------*/

/* ----method used for finalising lens if any of the od or os is present----start----*/
function finalise_lens(){  
  $(".fin_btn").on('click',function(){
    if (($(this).parent().parent().parent().find(".od_lens_fill").val() == "") && ($(this).parent().parent().parent().find(".os_lens_fill").val() == "")) {
      return true;
    }
    else{
      $('#any_finalize').val(true); 
      $(this).prev('input').attr('value', true); 
      examFormSubmit(); 
      return false;
    }
  });
}
/* ----method used for finalising lens if any of the od or os is present----end----*/

function overview_tab_styling() {
  $('.hr_exam_form').hover(function(){
    $(this).prev().css('border','1px solid #000');},
    function(){
    $(this).prev().css('border','1px solid #eee');
  })
  $(".store_descrip").css({ "margin-left": "106px", "left": "-12%" }); 
}

$(document).on('click','.clear_current_lens',function(){
  $(".lens").first().find('input').val(' ');
  $(".lens").first().find('select').val(' ');
  $(".lens").first().find('svg').remove();
  return false;
});
$(document).on('click','.send_report_btn',function(){
  $('#old-referral-modal').modal('hide'); 
  $('#referral-modal').modal('hide');
})

