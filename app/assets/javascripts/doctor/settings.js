/* Index page js --------start----------*/
function settingsWorkingSchedule(){
  $('#doctor_working_schedule_form input:checkbox').change(function(){
    check = this;
    chackbox_change(this, "#doctor_working_schedule_form");

  });
  $('#store_working_schedule_form input:checkbox').change(function(){
    check = this;
    chackbox_change(this, "#store_working_schedule_form");

  });
  function chackbox_change(check, form_parent_id)
  {
    a=check.id;
    if($(check).is(':checked')){
      $(form_parent_id+' #working_schedule_'+a+'_start_time').val("00:00");
      $(form_parent_id+' #working_schedule_'+a+'_end_time').val("00:00");
      $(form_parent_id+' #working_schedule_'+a+'_start_time').prop("readonly", true);
      $(form_parent_id+' #working_schedule_'+a+'_end_time').prop("readonly", true);
    }
    else{
      $(form_parent_id+' #working_schedule_'+a+'_start_time').prop("readonly", false);
      $(form_parent_id+' #working_schedule_'+a+'_end_time').prop("readonly", false);
    }
  }


 
  onload_checked("#store_working_schedule_form");
  onload_checked("#doctor_working_schedule_form");
    
  
  function onload_checked(form_parent_id)
  {
    var week = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']
    week.forEach(function(day_name){
      start= $(form_parent_id+' #working_schedule_'+day_name+'_start_time').val();
      end= $(form_parent_id+' #working_schedule_'+day_name+'_end_time').val();
      if(start == '12:00 AM' && end == '12:00 AM'){
        $(form_parent_id+' #'+day_name).prop('checked', 'checked');
      }
      if($(form_parent_id+' #'+day_name).is(':checked'))
      {
        $(form_parent_id+' #working_schedule_'+day_name+'_start_time').prop("readonly", true);
        $(form_parent_id+' #working_schedule_'+day_name+'_end_time').prop("readonly", true);
      }

    });
  }
}

function settingsIndexPage(){
  $('.add-object').click(function(){
    
    var object_coppied = $(this).parent().parent().find(".object").first().clone();
    object_coppied.children().find('.closed').show().removeClass('hidden');
    object_coppied.children().find('.select2-container').remove()
    var time = (new Date).getTime();
    $(object_coppied).find(':input').each(function(){
      if($(this).attr("name") != undefined)
      {
        $(this).attr("name", $(this).attr("name").replace(/\d+/, time));
      }   
      $(this).attr("id", $(this).attr("id").replace(/\d+/, time));
    })
    $(this).parent().parent().find(".modal-body").append(object_coppied);
    initializeTagMultiselect();
    return false;
  });

  $(document).on('click', '.closed', function(){
    $(this).parent().parent().remove();
    return false;
  });

  function change_slider(start_id, end_id){
    if(a[0]==0){
      $('#'+start_id).val('pl');
    }
    else{
      $('#'+start_id).val((a[0] > 0)?('+'+a[0]):a[0]);
    }
    if(a[1]==0){
      $('#'+end_id).val('pl');
    }
    else{
      $('#'+end_id).val((a[1] > 0)?('+'+a[1]):a[1]);
    }

  }

  $("#sphere_eyeglass_slider").slider({});
  $("#cylinder_eyeglass_slider").slider({});
  $("#add_eyeglass_slider").slider({});

  $('#sphere_eyeglass_slider').on('slide', function (ev) {
    a = $('#sphere_eyeglass_slider').val().split(",")
    change_slider('setting_glass_sphere_start', 'setting_glass_sphere_end');
  });

  $('#cylinder_eyeglass_slider').on('slide', function (ev) {
    a = $('#cylinder_eyeglass_slider').val().split(",")
    change_slider('setting_glass_cylinder_start', 'setting_glass_cylinder_end');
  });

  $('#add_eyeglass_slider').on('slide', function (ev) {
    a = $('#add_eyeglass_slider').val().split(",")
    change_slider('setting_glass_add_start', 'setting_glass_add_end');
  });

  $('.recommendation_table').sortable({
    start: function(event, ui) {
      var start_pos = ui.item.index();
      ui.item.data('start_pos', start_pos);
    },
    change: function(event, ui) {
      var start_pos = ui.item.data('start_pos');
      var index = ui.placeholder.index();
      ui.item.data('cur_pos', index);
      if (start_pos < index) {
          $('#sortable li:nth-child(' + index + ')').addClass('highlights');
      } else {
          $('#sortable li:eq(' + (index + 1) + ')').addClass('highlights');
      }
    },
    update: function(event, ui) {
      var start_pos = ui.item.data('start_pos');
      var cur_pos = ui.item.data('cur_pos')
      if (start_pos < cur_pos)
      {
        cur_pos = cur_pos - 1;
      }
      var path = '/settings/update_recommendation_order';
      $.ajax({
        url: path,
        type: 'get',
        data: "start=" + start_pos + "&current=" + cur_pos
      });
    }
  });

  $('.procedure_table').sortable({
    start: function(event, ui) {
      var start_pos = ui.item.index();
      ui.item.data('start_pos', start_pos);
    },
    change: function(event, ui) {
      var start_pos = ui.item.data('start_pos');
      var index = ui.placeholder.index();
      ui.item.data('cur_pos', index);
      if (start_pos < index) {
          $('#sortable li:nth-child(' + index + ')').addClass('highlights');
      } else {
          $('#sortable li:eq(' + (index + 1) + ')').addClass('highlights');
      }
    },
    update: function(event, ui) {
      var start_pos = ui.item.data('start_pos');
      var cur_pos = ui.item.data('cur_pos')
      if (start_pos < cur_pos)
      {
        cur_pos = cur_pos - 1;
      }
      var path = '/settings/update_procedure_order';
      $.ajax({
        url: path,
        type: 'get',
        data: "start=" + start_pos + "&current=" + cur_pos
      });
    }
  });

  $(document).on('change','.lens_type_selection',function(){
    value = $(this).val();
    if(value == "multifocal"){
      $(this).parent().parent().find('.lenses_cylinder').addClass('hidden');
      $(this).parent().parent().find('.lenses_axis').addClass('hidden');
      $(this).parent().parent().find('.lenses_add').removeClass('hidden');
    }
    else if(value == "sphere")
    {
      $(this).parent().parent().find('.lenses_cylinder').addClass('hidden');
      $(this).parent().parent().find('.lenses_axis').addClass('hidden');
      $(this).parent().parent().find('.lenses_add').addClass('hidden');
    }
    else if(value == "toric")
    {
      $(this).parent().parent().find('.lenses_cylinder').removeClass('hidden');
      $(this).parent().parent().find('.lenses_axis').removeClass('hidden');
      $(this).parent().parent().find('.lenses_add').addClass('hidden');
    }
    else
    {
      $(this).parent().parent().find('.lenses_cylinder').removeClass('hidden');
      $(this).parent().parent().find('.lenses_axis').removeClass('hidden');
      $(this).parent().parent().find('.lenses_add').removeClass('hidden');
    }
  })

  $(document).on('click', "#settings_tabs a", function(event){
    event.preventDefault();
    $('.spinner').show();
    url = $(this).attr('data-href');
    $('#tab_show').load( url, function( response, status, xhr ) {   
      $('.spinner').hide();
      settingsIndexPage();   
      ComponentsPickers.init(); 
    }); 
  }) 
}


/* Index page js --------end----------*/

/*---Settings Order's Form Tab Js for Reward Used ReadOnly based on radio button Yes or No.-----start-------*/

function reward_used_readonly () {
  $('.control_reward_on_off_flag_for_reward input:radio').click(function() {
    if ($(this).val() === 'true') {
      $("#control_reward_on_off_reward_percent_used").prop("readonly", false);
      $("#control_reward_on_off_reward_percent_used").css('background-color' , 'transparent');
    } else if ($(this).val() === 'false') {
      $("#control_reward_on_off_reward_percent_used").prop("readonly", true);
      $("#control_reward_on_off_reward_percent_used").css('background-color' , '#DEDEDE');
    } 
 });
}

/*---Settings Order's Form Tab Js for Reward Used ReadOnly based on radio button Yes or No.-----end-------*/

$(document).on('ready',function(){
  $(document).on('click',".edit_vision",function(){
    var visionId = $(this).data('id');
    var name = $(this).data('name');
    var code = $(this).data('code');
    $(".modal-body #vision_insurance_vision_id").val( visionId );
    $(".modal-body #vision_insurance_name").val( name );
    $(".modal-body #vision_insurance_code").val( code );
    $('#edit-modal').modal('show');
  });
});

$(document).on('ready',function(){
  $(document).on('click',".edit_medical",function(){
    var medicalId = $(this).data('id');
    var name = $(this).data('name');
    var code = $(this).data('code');
    $(".modal-body #medical_insurance_medical_id").val( medicalId );
    $(".modal-body #medical_insurance_name").val( name );
    $(".modal-body #medical_insurance_code").val( code );
    $('#edit-modal-medical').modal('show');
  });
});

function ranges_modal_box_readonly() {
  $(document).on('click','.edit_lens_range',function(){
    $( ".lens_range_edit input").removeAttr("disabled");
    $( ".lens_range_edit select").removeAttr("disabled");
    $(".lens_range_slider").show();
    $(".set_lens_range").removeAttr("disabled");
    return false;
  })
  $(document).on('click','.edit_glass_range',function(){
    $( ".eyeglass_range_edit input").removeAttr("disabled");
    $( ".eyeglass_range_edit select").removeAttr("disabled");
    $(".eyeglass_range_slider").show();
    $(".set_glass_range").removeAttr("disabled");
    return false;
  })
  $( ".lens_range_edit input").attr("disabled","disabled");
  $( ".lens_range_edit select").attr("disabled","disabled");
  $( ".eyeglass_range_edit input").attr("disabled","disabled");
  $( ".eyeglass_range_edit select").attr("disabled","disabled");
  $(".lens_range_slider").hide();
  $(".eyeglass_range_slider").hide();
  $(".set_lens_range").attr('disabled','disabled');
  $(".set_glass_range").attr('disabled','disabled');
}