//Analytic page js ------------start-------------------------

function campaignsAnalyticPage(){
  $(document).on("click", ".pagination a", function() {
    event.preventDefault();
    var url=$(this).parent().attr("data-url"),
    campaign=$(this).parent().attr("data-type");
    ($(this).closest("#summary_section")).load($(this).attr('href'));
  });

  $(document).on("click", ".view_by", function() {
    event.preventDefault();
    var campaign=$(this).attr("data-filter"), view_type=$(this).attr("data-view");
    $("#graph_body"+campaign).load('/campaigns/graph_view?campaign='+campaign+'&view_type='+view_type);
  });

  $(document).on("click", ".view_summary", function () {
    $('.spinner').show();
    var target_action = $(this).attr("data-target"),
        filter_data_target = $(this).attr("data-filter"),
        target_parent = $(this).attr("data-parent"),
        url;
    url = '/campaigns/'+target_action+'?campaign='+filter_data_target;
    $(target_parent+' #summary_section').load(url, function( response, status, xhr ) {
      $('.spinner').hide();
      $('html, body').animate({scrollTop: '+=550px'}, 800);
      $('.dropdown-toggle').dropdown();
    });
    
  });

  $(document).on("click", ".campaign_tabs", function () {
    $('.spinner').show();
    var filter_data_target = $(this).attr("data-target"),
        target_wrapper = $(this).attr("href"),
        url;
    $(".tab-pane").removeClass("active");
    $(target_wrapper).addClass('active');
    if(filter_data_target!="overall")
    {
      url = '/campaigns/analytic?campaign='+filter_data_target;
      $(target_wrapper).load(url, function( response, status, xhr ) {
        $('.spinner').hide();
        $('.dropdown-toggle').dropdown();
      });
    }
    else
    {
      $('.spinner').hide();
    }
  });
}


//Analytic page js ------------end-------------------------




//Index page js ------------start-------------------------

function campaignsIndexPage(){
  $(".numeric").rating('create', {showClear: false, showCaption: false});
  url = '/contact_forms';
  $('.contact_form').load(url, function( response, status, xhr ) {
    $('.spinner').hide();
  });

  url = '/twilio_webhooks';
  $('.sms').load(url, function( response, status, xhr ) {
    $('.spinner').hide();
  });
  $(document).on("click", ".pagination a", function() {
    event.preventDefault();
    var url=$(this).attr("href"),
        campaign=$(this).parent().attr("data-type");
    $(this).href='';
    if (campaign == "twilio")
    {
      ($(this).closest("#tab_twilio_webhooks")).load(url);
    }
    else if (campaign == "contact_forms")
    {
      ($(this).closest("#tab_contact_form")).load(url);
    }
    else if (campaign == "done")
    {
      url = $(this).parent().attr("data-url");
      ($(this).closest("#portlet_tab1")).load(url+ "?campaign=" + campaign+'&page='+$(this).attr("href").slice(-1));
    }
  });
}

function dtable_patient_survey_table(){
  $(document).ready(function() {
    $('#sample_editable_2').dataTable({
      "sPaginationType": "full_numbers",
      bSort: false,
      bFilter: true,
      bJQueryUI: true,
      "fnDrawCallback":function(){
        if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1) {
          $('.dataTables_paginate').css("display", "block");  
          $('.dataTables_length').css("display", "block");
          $('.dataTables_filter').css("display", "block");  
        } 
        else {
          $('.dataTables_paginate').css("display", "none");
          $('.dataTables_length').css("display", "none");
        }
      }
    });
  });
}

//Index page js ------------end-------------------------