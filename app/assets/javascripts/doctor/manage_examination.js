function procedure_impression_tab(){
	
	$(document).ready(function(){
      url = $('#overview_tab').attr('data-href')
      $('#print_overview').load( url, function( response, status, xhr ) {   
        $('.spinner').hide();
        icheck();      
        examinationsFormPage();      
        initializePlugins();
      });
  	})
}
$(document).on('click', ".bp_time_button", function() {
  var date = new Date();
  var t=moment(date).format("h:mm:ss a")
  $(".bp_row").find('input').attr('value', t);
});

function medical_rx() {
  $('#medical_print').on('click',function(){
    $(".cl_presc").hide();
    $(".glass_presc").hide();
    $(".email_presc_btn").hide();
    $(".final_presc_tab_download_btn").hide();
    $('.page-header, .procedure, .impression, .portlet-title, .exam_tabs, .del').hide();
    $(this).hide();
    window.print();
    $('.page-header, .procedure, .impression, .portlet-title, .exam_tabs, .del').show();
    $(this).show();
    $(".cl_presc").show();
    $(".glass_presc").show();
    $(".email_presc_btn").show();
    $(".final_presc_tab_download_btn").show();
  })

  $(document).on('click','.remove_med_rx',function(){
    $(this).parent().parent().parent().parent().prev().remove();
    $(this).parent().parent().parent().parent().remove();
    if ($("div.delete_data").length == 0){
      $(".portlet box, .medical_rx").remove();
    }
  })
}

  function medical_rx_date_picker(){
    $(".medical_rxs").datepicker();
  }
  function update_med_rx_dates(){
      $('.medical_rxs').on('change', function(){ 
      var examination_id = $(".exam_id").text();
      var medical_rx_id = $(this).parent().parent().parent().find("div:hidden").text().replace(/\s+/g, "");
      if ($(this).hasClass('med_rxissuedate')){
        var medical_rx_issuedate = $(this).val();
        var medical_rx_expdate = $(this).parent().parent().children().find('input')[1].value
      }
      else
      {
        var medical_rx_expdate = $(this).val();
        var medical_rx_issuedate = $(this).parent().parent().children().find('input')[0].value
      }
      $.ajax({ 
       type: "PUT", 
       url: "/examinations/"+examination_id+"/update_medical_rx", 
       data: { med_id: medical_rx_id , issue_date: medical_rx_issuedate , expired_date: medical_rx_expdate},
       success: function(data){
       }
      });
      return false;
    });
  }