// index page --------start-----------------

function managePatientsIndexPage(myFirebaseRef,formatted_date,doctorDashboardFirebaseRef){
  $("#DefaultView_unfexam").html("<a class='defview_unfexam btn btn-sm default'><i class='fa fa-refresh'></i>&nbsp;&nbsp;Default View</a>");
  $("#ShowToday_unfexam").html("<a class='stday_unfexam btn btn-sm default'><i class='fa fa-clock-o'></i>&nbsp;&nbsp;Show Today</a>");
  $("#DefaultView_unforder").html("<a class='defview_unforder btn btn-sm default'><i class='fa fa-refresh'></i>&nbsp;&nbsp;Default View</a>");
  $("#ShowToday_unforder").html("<a class='stday_unforder btn btn-sm default'><i class='fa fa-clock-o'></i>&nbsp;&nbsp;Show Today</a>");
  $("#DefaultView_ordered").html("<a class='defview_ordered btn btn-sm default'><i class='fa fa-refresh'></i>&nbsp;&nbsp;Default View</a>");
  $("#ShowToday_ordered").html("<a class='stday_ordered btn btn-sm default'><i class='fa fa-clock-o'></i>&nbsp;&nbsp;Show Today</a>");
  $("#DefaultView_dispensed").html("<a class='defview_dispensed btn btn-sm default'><i class='fa fa-refresh'></i>&nbsp;&nbsp;Default View</a>");
  $("#ShowToday_dispensed").html("<a class='stday_dispensed btn btn-sm default'><i class='fa fa-clock-o'></i>&nbsp;&nbsp;Show Today</a>");
  $("#DefaultView_followup").html("<a class='defview_followup btn btn-sm default'><i class='fa fa-refresh'></i>&nbsp;&nbsp;Default View</a>");
  $("#ShowToday_followup").html("<a class='stday_followup btn btn-sm default'><i class='fa fa-clock-o'></i>&nbsp;&nbsp;Show Today</a>");
  
  $('.defview').hide();
  
  $(".defview_unfexam").on("click",function(){
      $("#yadcf-filter--unfexam-from-date-4").val("");
      $("#yadcf-filter--unfexam-to-date-4").val("");
      yadcf.exFilterExternallyTriggered($('#unfexam').dataTable());
      $('#DefaultView_unfexam').toggle('slide');
  });
  $(".stday_unfexam").on("click",function(){
    $("#yadcf-filter--unfexam-from-date-4").val(formatted_date);
    $("#yadcf-filter--unfexam-to-date-4").val(formatted_date);
    yadcf.exFilterExternallyTriggered($('#unfexam').dataTable());
    $('#DefaultView_unfexam').toggle('slide');
  });
  
  $(".defview_unforder").on("click",function(){
    $("#yadcf-filter--unforder-from-date-4").val("");
    $("#yadcf-filter--unforder-to-date-4").val("");
    yadcf.exFilterExternallyTriggered($('#unforder').dataTable());
    $('#DefaultView_unforder').toggle('slide');
  });
  
  $(".stday_unforder").on("click",function(){
    $("#yadcf-filter--unforder-from-date-4").val(formatted_date);
    $("#yadcf-filter--unforder-to-date-4").val(formatted_date);
    yadcf.exFilterExternallyTriggered($('#unforder').dataTable());
    $('#DefaultView_unforder').toggle('slide');
  });
  
  $(".defview_ordered").on("click",function(){
     $("#yadcf-filter--ordered-from-date-4").val("");
     $("#yadcf-filter--ordered-to-date-4").val("");
     yadcf.exFilterExternallyTriggered($('#ordered').dataTable());
     $('#DefaultView_ordered').toggle('slide');
  });
  $(".stday_ordered").on("click",function(){
    $("#yadcf-filter--ordered-from-date-4").val(formatted_date);
    $("#yadcf-filter--ordered-to-date-4").val(formatted_date);
    yadcf.exFilterExternallyTriggered($('#ordered').dataTable());
    $('#DefaultView_ordered').toggle('slide');
  });
  
  $(".defview_dispensed").on("click",function(){
     $("#yadcf-filter--dispensed-from-date-4").val("");
     $("#yadcf-filter--dispensed-to-date-4").val("");
     yadcf.exFilterExternallyTriggered($('#dispensed').dataTable());
     $('#DefaultView_dispensed').toggle('slide');
  });
  $(".stday_dispensed").on("click",function(){
    $("#yadcf-filter--dispensed-from-date-4").val(formatted_date);
    $("#yadcf-filter--dispensed-to-date-4").val(formatted_date);
    yadcf.exFilterExternallyTriggered($('#dispensed').dataTable());
    $('#DefaultView_dispensed').toggle('slide');
  });
  
  $(".defview_followup").on("click",function(){
     $("#yadcf-filter--followup-from-date-4").val("");
     $("#yadcf-filter--followup-to-date-4").val("");
     yadcf.exFilterExternallyTriggered($('#followup').dataTable());
     $('#DefaultView_followup').toggle('slide');
  });
  $(".stday_followup").on("click",function(){
    $("#yadcf-filter--followup-from-date-4").val(formatted_date);
    $("#yadcf-filter--followup-to-date-4").val(formatted_date);
    yadcf.exFilterExternallyTriggered($('#followup').dataTable());
    $('#DefaultView_followup').toggle('slide');
  });
  
  $(document).on('click','#deleteNotify',function(e){
    var href = $('#emailcancelid').attr('href');
    if($('#deleteNotify').is(':checked'))
      $('#emailcancelid').attr('href', href+'?mail=true');
    else
      $('#emailcancelid').attr('href', href.split("?")[0]);
  });
  
  $(document).on('click',"#booking_delete_confirm",function(e) {
    e.preventDefault();
    var href = $('#emailcancelid').attr('href');
    $('#emailcancelid').attr('href', href.replace('pony', $(this).data('id')));
    $('#modalconfirm').data('id', $(this).data('id')).modal('show');
  });
  
  // pending order delete confirm
  $(document).on('click',"#order_delete_confirm",function(e) {
    e.preventDefault();
    var href = $('.order_email_cancel_id').attr('href');
    $('.order_email_cancel_id').attr('href', href.replace('pony', $(this).data('id')));
    $('#order_delete_confirm_modal').modal('show');
  });
  
  // pending order delete confirm email notify
  $(document).on('click','#order_delete_notify',function(e){
    var href = $('.order_email_cancel_id').attr('href');
    if($('#order_delete_notify').is(':checked'))
      $('.order_email_cancel_id').attr('href', href+'?mail=true');
    else
      $('.order_email_cancel_id').attr('href', href.split("?")[0]);
  });
  
  $(document).on('click','#removeNotify',function(e){
    var href = $('#emailremoveid').attr('href');
    var mailcheck = '&mail=true'
    if($('#removeNotify').is(':checked'))
      $('#emailremoveid').attr('href', href+mailcheck);
    else
      $('#emailremoveid').attr('href', href.replace(mailcheck,''));
  });

  $(document).on('click','#emailremoveid',function(e){
    $('.spinner').show();
  }); 
  
  $(document).on('click',"#removeconfirm",function(e) {
    e.preventDefault();
    if(typeof $(this).data('renewed') != 'undefined')
      $('#emailremoveid').attr('href', '/favourites/'+$(this).data('id')+'?remove_dispensed=true');
    else
      $('#emailremoveid').attr('href', '/orders/'+$(this).data('id')+'?remove_dispensed=true');
    $('#modalremoveconfirm').data('id', $(this).data('id')).modal('show');
  });
  
  $(document).on('click',"#dispenselog",function(e) {
    e.stopImmediatePropagation();
    dispenseLogs($(this).data('order'),$(this).data('type'),"all_logs");
    $('#dispensemodal').find('.modal-title').html("<h3><i class='fa fa-suitcase'></i>&nbsp;&nbsp;"+$(this).data('name')+"&nbsp;&nbsp;Dispense Log</h3>")
    $('#dispensemodal').find('.modal-footer').html("<button class='btn btn-primary add-log' data-order='"+$(this).data('order')+"' data-type='"+$(this).data('type')+"' data-name='"+$(this).data('name')+"' type='button' ><i class='fa fa-plus'></i>&nbsp;&nbsp;Add Note</button>&nbsp;&nbsp;<button class='btn btn-primary email-order-ready' data-order='"+$(this).data('order')+"' data-type='"+$(this).data('type')+"' data-name='"+$(this).data('name')+"' type='button' ><i class='fa fa-send'></i>&nbsp;&nbsp;Re: Email Order is ready for pick up</button><button class='btn btn-default' data-dismiss='modal' type='button'>Close</button>");
    $('#dispensemodal').modal('show');
  });
  
  $(document).on('click',".add-log",function(e) {
    dispenseLogs($(this).data('order'),$(this).data('type'),"add_new");
    $('#dispensemodal').find('.modal-title').html("<h3><i class='fa fa-suitcase'></i>&nbsp;&nbsp;"+$(this).data('name')+"&nbsp;&nbsp;Dispense Log</h3>")
    $('#dispensemodal').find('.modal-footer').html("<button class='btn btn-primary' id='savelog' data-order='"+$(this).data('order')+"' data-type='"+$(this).data('type')+"' data-name='"+$(this).data('name')+"' type='button'>Save</button>&nbsp;&nbsp;<button class='btn btn-default' data-dismiss='modal' type='button'>Close</button>");
    $('#dispensemodal').modal('show');
  });	
  
  $(document).on('click',".email-order-ready",function(e) {
    e.stopImmediatePropagation();
    dispenseLogs($(this).data('order'),$(this).data('type'),"email_order_ready");
    dispenseLogs($(this).data('order'),$(this).data('type'),"all_logs");
    $('#dispensemodal').find('.modal-title').html("<h3><i class='fa fa-suitcase'></i>&nbsp;&nbsp;"+$(this).data('name')+"&nbsp;&nbsp;Dispense Log</h3>")
    $('#dispensemodal').find('.modal-footer').html("<button class='btn btn-primary add-log' data-order='"+$(this).data('order')+"' data-type='"+$(this).data('type')+"' data-name='"+$(this).data('name')+"' type='button' ><i class='fa fa-plus'></i>&nbsp;&nbsp;Add Note</button>&nbsp;&nbsp;<button class='btn btn-primary email-order-ready' data-order='"+$(this).data('order')+"' data-type='"+$(this).data('type')+"' data-name='"+$(this).data('name')+"'  type='button' ><i class='fa fa-send'></i> &nbsp;&nbsp;Re: Email Order is ready for pick up</button><button class='btn btn-default' data-dismiss='modal' type='button'>Close</button>");
    $('#dispensemodal').modal('show');
  });
  
  $(document).on('click',"#savelog",function(e){
    var myform = $('#new_dispense_log').serialize();
    $.ajax({
                url: '/dispense_logs',
                data: myform,
                cache: false,
                type: 'POST',
                success: function(result) {
                    $('#dispensemodal').find('.modal-body').html(result);
                }
    });
    dispenseLogs($(this).data('order'),$(this).data('type'),"all_logs");
    $('#dispensemodal').find('.modal-footer').html("<button class='btn btn-primary add-log' data-order='"+$(this).data('order')+"' data-type='"+$(this).data('type')+"' data-name='"+$(this).data('name')+"' type='button' ><i class='fa fa-plus'></i>&nbsp;&nbsp;Add Note</button>&nbsp;&nbsp;<button class='btn btn-primary email-order-ready' data-order='"+$(this).data('order')+"'  data-type='"+$(this).data('type')+"' data-name='"+$(this).data('name')+"'  type='button' >Re: Email Order is ready for pick up</button><button class='btn btn-default' data-dismiss='modal' type='button'>Close</button>");
  });
  
  function dispenseLogs(data_id,data_type,action_path){ 
    $.ajax({ 
       type: "GET",
       url: '/dispense_logs/'+action_path,
       data: 'dispensable_id='+data_id+'&dispensable_type='+data_type,
       dataType: "text",
       async: false
       }).success(function(data){
                  $('#dispensemodal').find('.modal-body').html(data);
    });
  }
  
  
  $(document).on('change','.text_filter',function(e){
    e.stopPropagation();
    $('#DefaultView_'+this.parentNode.parentNode.parentNode.parentNode.parentElement.id).show();
  });
  
  $(document).on('ajax:complete','.dispense_link',function(e){
    location.reload();
  });
  
  // follow up popup actions
  $(document).on('click','.followups',function(){
    var patient_id = $(this).data('id');
    var patient_name  = $(this).data('name');
    $.ajax({ 
           type: "GET",
           url: "/follow_ups",
           data: 'id='+patient_id,
           dataType: "JSON",
           async: false
           }).success(function(data){
               $('#followup-modal').find('.modal-header').html("<h3><i class='fa fa-user'></i>&nbsp;&nbsp;"+patient_name+"&nbsp;Follow up Log</h3>");
               $('#followup-modal').find('.modal-body').html(data.html);
               $('#followup-modal').find('.modal-footer').html("<a class='btn btn-default' data-dismiss='modal'>Close</a> <a href=# data-id="+patient_id+" data-name="+patient_name+" class='add_new_follow_up btn btn-primary'>Add New</a>");
               $('#followup-modal').modal('show');
          });
  });
  
  $(document).on('click','.add_new_follow_up',function(){
    var patient_id = $(this).data('id');
    var patient_name  = $(this).data('name');
    $.ajax({ 
           type: "GET",
           url: "/follow_ups/new",
           data: 'id='+patient_id,
           dataType: "JSON",
           async: true
           }).success(function(data){
              if(data.status == "Error"){
                $('#followup-modal').modal('hide');
                jQuery.gritter.add({ title: data.status, text: data.msg });
              } else {
                $('#followup-modal').find('.modal-header').html("<h3><i class='fa fa-user fa-2x'></i>&nbsp;&nbsp;"+patient_name+"&nbsp;Follow up Log</h3>");
                $('#followup-modal').find('.modal-body').html(data.html);
                $('#followup-modal').find('.modal-footer').html("<a class='btn btn-default' data-dismiss='modal'>Close</a> <a href=# data-id="+patient_id+" data-name="+patient_name+" class='save_follow_up btn btn-primary'>Save</a>");
                $('#followup-modal').modal('show');
              }
          });
  });
  
  $(document).on('click','.save_follow_up',function(e){
    // e.stopPropagation();
    var myform = $('#new_follow_up').serialize();
    var patient_id = $(this).data('id');
    var patient_name  = $(this).data('name');
    $.ajax({ 
           type: "POST",
           url: "/follow_ups",
           data: myform,
           dataType: "JSON"
           }).success(function(data){
             var element;
             $('.followups').each(function(){ if($(this).data('id')== patient_id ){ element=$(this) } });
             if( typeof element != "undefined" ) { element.trigger('click');}
             $('#error_explanation').hide();
          }).error(function(data){
             $('#error_explanation').html(data.responseJSON.error);
             $('#error_explanation').show();
             return false;
          });
  });
  
  // To provide options for unfinished orders to add exam procedures directly before opening its order
  

  // $(document).on('click','.open-order',function(){
  //   $('#exam-procedure-modal').modal('show');
  //   var exam_id = $(this).data('exam-id');
  //   var order_id = $(this).data('id');
  //   var patient_name  = $(this).data('name');
  //   var href = $(this).attr('href');
  //   $.ajax({ 
  //          type: "GET",
  //          url: "/manage_patients/exam_procedures",
  //          data: 'exam_id='+exam_id+"&id="+order_id,
  //          dataType: "JSON",
  //          async: false
  //          }).success(function(data){
  //              $('#exam-procedure-modal').find('.modal-header').html("<h4><i class='fa fa-edit'></i>&nbsp;&nbsp;"+patient_name+"&nbsp;Exam Procedures</h4>");
  //              $('#exam-procedure-modal').find('.modal-body').html(data.html);
  //              $('#exam-procedure-modal').find('.modal-footer').html("<a href="+href+" class='btn blue fa fa-chevron-right' data-no-turbolink='true'>proceed to order</a>");
  //              $('#exam-procedure-modal').modal('show');
  //         });
  //   return false;
  // });
  $(document).on('click','.booking_checkin',function(event){
    event.preventDefault();
    var booking_id = $(this).data('booking-id')
    $.ajax({ 
              type: "GET",
              dataType: 'JSON',
              url: $(this).data('url'),
              success: function(data){ 
                 //location.reload()
              }
    });

    var new_user_hash = {};    
    //user_updates_count.push(parseInt(current_doctor)) ;
    new_user_hash[booking_id] = $.now();
    myFirebaseRef.child('booking').transaction(function(data){ return $.extend(data, new_user_hash)  });
    location.reload()
  });
  

  // var callback_change_firebase = function(childSnapshot,event){
  //   debugger
  //   console.log("dashboard change call")
  //   if(childSnapshot.key() == "booking"){
  //     $.ajax({ 
  //             type: "GET", 
  //             url: "/manage_patients/todays_appointments",
  //             success: function(data){
  //           }
  //     });
  //   }
  // }

  // var callback_added_firebase = function(childSnapshot,event){
  //   console.log('child added')
  //   debugger

  //   if(childSnapshot.key() == "booking"){
  //     $.ajax({ 
  //             type: "GET", 
  //             url: "/manage_patients/todays_appointments",
  //             success: function(data){
  //           }
  //     });
  //   }
  // }

  //var initial_load = true
  
  doctorDashboardFirebaseRef.on('child_changed', function(childSnapshot,event){
  
    //if(initial_load === false){
      $.ajax({ 
              type: "GET", 
              dataType: "script",
              url: "/manage_patients/todays_appointments",
              success: function(data){
            }
      });
    //}
    //initial_load = false
  });
  
  doctorDashboardFirebaseRef.on("child_added", function(childSnapshot,event){
    
    //if(initial_load === false){
      $.ajax({ 
              type: "GET", 
              dataType: "script",
              url: "/manage_patients/todays_appointments",
              success: function(data){
              }
      });
    //}
    //initial_load = false
  });
 
  all_datatables();

  $(document).on('change','#yadcf-filter--unfexam-from-date-4,#yadcf-filter--unfexam-to-date-4,#yadcf-filter--unforder-from-date-4,#yadcf-filter--unforder-to-date-4,#yadcf-filter--ordered-from-date-4,#yadcf-filter--ordered-to-date-4,#yadcf-filter--dispensed-from-date-4,#yadcf-filter--dispensed-to-date-4,#yadcf-filter--followup-from-date-4,#yadcf-filter--followup-to-date-4',function(){
    table_id = $(this).parents('.dataTable').attr('id');
    yadcf.exFilterExternallyTriggered($('#'+table_id).dataTable());
  });
  
  $('#insurance_balance_table').dataTable({
            "bDestroy": true
        }).fnDestroy();
  //init for column filter changes
  yadcfAddBootstrapClass();
  $('#insurance_balance_table').dataTable({
    bSort: false,
    bFilter: true,
    bJQueryUI: true
  });
}

// index page ----------end------------

// -------------helper methods --------------------------------------------------------------

  var datepickerDefaults = {
                  showTodayButton: true,
                  showClear: true
              };
    
  function table_initialize_with(tableid){
      $('#'+tableid).dataTable({
              bSort: false,
              bFilter: true,
              bJQueryUI: true,
              "sDom": 'R<C><"#ShowToday_'+tableid+'"><"#DefaultView_'+tableid+'">H<"clear"><"ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix"l>t<"ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix"ip><"clear">',
              bDestroy: true,
              "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                  // Bind click event
                  $('td:gt(5)',nRow).click(function(){ console.log('td with header actions not bind to event');
                         $.ajax({ 
                                type: "GET", 
                                url: '/doctor/patient/info?jr_id='+aData[6],
                                dataType: 'script',
                                success: function(data){ 
                                  arrinfo = $.parseJSON(data);
                                  var patientid = aData[6];
                                  htmlData = function(){ var list = '<div>';
                                               $.each(arrinfo,function(key,valarry){ 
                                                  var infolist = "<div class='logtb' style='margin-left: 20%;'>";
                                                  list += '<h5><b>'+valarry[0]+': </b>';
                                                  $.each(valarry[1],function(q,w){ 
                                                    //if(valarry[0]=='Personal'){ patientid = valarry[1][0][1];}
                                                    infolist += '<h5>'+w[0]+':<span style=\'margin-right: 10%;float: right;\'>'+w[1]+'</span></h5>';
                                                  });
                                                  list+= infolist+'</div></h5>'; 
                                               }); 
                                               list += '</div>';
                                               return list;
                                             };
                                }
                              });
                      return false;
                  });
                  return nRow;
              },
              "fnDrawCallback": function (oSettings) {
                                  var pgr = $(oSettings.nTableWrapper).find('.dataTables_paginate')
                                  if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
                                        $("#"+tableid+"_length").hide();
                                        pgr.hide();
                                  } else {
                                        $("#"+tableid+"_length").show();
                                        pgr.show();
                                  }
                                    $(".badge-danger").addClass("badge");
                                    $(".fa-shopping-cart").addClass("fa");
                                    $(".fa-openid").addClass("fa");
                                },
              "order": [[ 4, "desc" ]],
              aoColumnDefs:  [ { sTitle: "Name" ,aTargets: [0]}, { sTitle: "Birthdate", aTargets: [1]}, { sTitle: "Email" ,aTargets: [2]},{ sTitle: "Phone" ,aTargets: [3]},{ sTitle: "Latest Entry" ,aTargets: [4]},{ sTitle: "Actions" ,aTargets: [5]},{ sTitle: "Id" ,aTargets: [6]} ],
              aoColumns: [ { aTargets: [0]}, { aTargets: [1]}, { aTargets: [2]},{ aTargets: [3]},{ aTargets: [4]},{ aTargets: [5]} ,{ aTargets: [6]}],
              sPaginationType: "full_numbers"
            }).yadcf([
                {column_number : 0,filter_type: "text",filter_reset_button_text: false},
                {column_number : 1, filter_type: "text",filter_reset_button_text: false},
                {column_number : 2, filter_type: "text",filter_reset_button_text: false},
                {column_number : 3, filter_type: "text",filter_reset_button_text: false},
                {column_number : 4, filter_type: "range_date",datepicker_type: 'jquery-ui',date_format: 'mm/dd/yyyy', filter_reset_button_text: false,filter_plugin_options: datepickerDefaults}],{cumulative_filtering : true }, { externally_triggered: true});
  }
    
  function all_datatables(){
      var tableids = ['unfexam','unforder','ordered','dispensed','followup']
      $.each(tableids,function(key,vals){
          table_initialize_with(vals);
      });
  }
 
    
  function yadcfAddBootstrapClass() {
    var filterWrapper = $('.yadcf-filter-wrapper'),
        filterInput = $('input.yadcf-filter, input.yadcf-filter-range, input.yadcf-filter-date'),
        filterSelect = $('select.yadcf-filter'),
        filterReset = $('input.yadcf-filter-reset-button');

    if ($('.table-filter').hasClass('js-table-filter-group')) {
        filterWrapper.addClass('input-group');
        filterInput.addClass('form-control');
        //filterSelect.addClass('form-control');
        filterReset.addClass('btn btn-default').wrap("<span class='input-group-btn'></span>");
    } else {
        filterInput.addClass('form-control');
        filterSelect.addClass('form-control');
        filterReset.addClass('btn btn-default').wrap("<span></span>");
    }
  }

/*--Used inside _follow_up_list.html.haml--start--*/
  function finalise_rvm() {
    $(document).on('click',"#finalized_remove",function(e) {
      e.preventDefault();
      if(typeof $(this).data('renewed') != 'undefined')
        $('#follow_up_remove_id').attr('href','/favourites/'+$(this).data('id')+'?remove_finalized=true');
      else
        $('#follow_up_remove_id').attr('href', '/orders/'+$(this).data('id')+'?remove_finalized=true');
      $('#modalfollowupremove').data('id', $(this).data('id')).modal('show');
    });
  }
/*--Used inside _follow_up_list.html.haml--end--*/
function insurance_rvm() {
    $(document).on('click',"#insurance_remove",function(e) {
      e.preventDefault();
      $('#modalinsuranceremove').modal('show');
    });
  }

/*--Used inside _todays_pending_appointment.html.haml--start--*/
  function tod_pending_appointment(){
    $(document).ready(function(){ 

      var datepickerDefaults = {
        showTodayButton: true,
        showClear: true
      };
      
      function table_initialize_with1(tableid){
        $('#'+tableid).dataTable({
          bSort: false,
          bFilter: true,
          bJQueryUI: true,
          "sDom": 'R<C><"#ShowToday_'+tableid+'"><"#DefaultView_'+tableid+'">H<"clear"><"ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix"l>t<"ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix"ip><"clear">',
          bDestroy: true,
          "fnRowCallback": function (nRow, aData, iDisplayIndex) {
              // Bind click event
              $('td:gt(5)',nRow).click(function(){ console.log('td with header actions not bind to event');
                     $.ajax({ 
                            type: "GET", 
                            url: '/doctor/patient/info?jr_id='+aData[6],
                            dataType: 'script',
                            success: function(data){ 
                              arrinfo = $.parseJSON(data);
                              var patientid = aData[6];
                              htmlData = function(){ var list = '<div>';
                                           $.each(arrinfo,function(key,valarry){ 
                                              var infolist = "<div class='logtb' style='margin-left: 20%;'>";
                                              list += '<h5><b>'+valarry[0]+': </b>';
                                              $.each(valarry[1],function(q,w){ 
                                                //if(valarry[0]=='Personal'){ patientid = valarry[1][0][1];}
                                                infolist += '<h5>'+w[0]+':<span style=\'margin-right: 10%;float: right;\'>'+w[1]+'</span></h5>';
                                              });
                                              list+= infolist+'</div></h5>'; 
                                           }); 
                                           list += '</div>';
                                           return list;
                                         };
                            }
                          });
                  return false;
              });
              return nRow;
          },
          "fnDrawCallback": function (oSettings) {
                              var pgr = $(oSettings.nTableWrapper).find('.dataTables_paginate')
                              if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
                                    $("#"+tableid+"_length").hide();
                                    pgr.hide();
                              } else {
                                    $("#"+tableid+"_length").show();
                                    pgr.show();
                              }
                                $(".badge-danger").addClass("badge");
                                $(".fa-shopping-cart").addClass("fa");
                                $(".fa-openid").addClass("fa");
                            },
          "order": [[ 4, "desc" ]],
          aoColumnDefs:  [ { sTitle: "Name" ,aTargets: [0]}, { sTitle: "Birthdate", aTargets: [1]}, { sTitle: "Email" ,aTargets: [2]},{ sTitle: "Phone" ,aTargets: [3]},{ sTitle: "Latest Entry" ,aTargets: [4]},{ sTitle: "Actions" ,aTargets: [5]},{ sTitle: "Id" ,aTargets: [6]} ],
          aoColumns: [ { aTargets: [0]}, { aTargets: [1]}, { aTargets: [2]},{ aTargets: [3]},{ aTargets: [4]},{ aTargets: [5]} ,{ aTargets: [6]}],
          sPaginationType: "full_numbers"
        }).yadcf([
            {column_number : 0,filter_type: "text",filter_reset_button_text: false},
            {column_number : 1, filter_type: "text",filter_reset_button_text: false},
            {column_number : 2, filter_type: "text",filter_reset_button_text: false},
            {column_number : 3, filter_type: "text",filter_reset_button_text: false},
            {column_number : 4, filter_type: "range_date",datepicker_type: 'jquery-ui',date_format: 'mm/dd/yyyy', filter_reset_button_text: false,filter_plugin_options: datepickerDefaults}],{cumulative_filtering : true }, { externally_triggered: true});
      }
      table_initialize_with1('pending')
      
      yadcfAddBootstrapClass();
    });
  }
/*--Used inside _todays_pending_appointment.html.haml--end--*/