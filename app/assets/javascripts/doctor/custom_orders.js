// Order edit page js ----------------------------start ------------------

function ordersEditPage(myFirebaseRef,current_order,current_doctor){
  $(document).on('click','.lens_manual_change',function(event){
    // event.preventDefault();
    var save_option = $("#contacts_prescription_manual_change_"+$(this).data('suborder')).css('display') 
    if(save_option == "block"){
      $("#lens_save_manual_change_"+$(this).data('suborder')).show();
    }else{
      $("#lens_save_manual_change_"+$(this).data('suborder')).hide();
    }
    $('#lens_edit_manual_change_'+$(this).data('suborder')+",#contacts_prescription_manual_change_"+$(this).data('suborder')).toggle('slow');
  });
  
  $(document).on('click','.glass_manual_change',function(event){
    // event.preventDefault();
    var save_option = $("#glasses_prescription_manual_change_"+$(this).data('suborder')).css('display')
    if(save_option == "block"){
      $("#glass_save_manual_change_"+$(this).data('suborder')).show()
      $('.other_prescriptation').css( "pointer-events","none" )
    }else{
      $("#glass_save_manual_change_"+$(this).data('suborder')).hide()
      $('.other_prescriptation').css( "pointer-events","auto" )
    }
    $('#glass_edit_manual_change_'+$(this).data('suborder')+",#glasses_prescription_manual_change_"+$(this).data('suborder')).toggle('slow');
  });

  var new_user_hash = {};    
  //user_updates_count.push(parseInt(current_doctor)) ;
  new_user_hash[current_doctor] = $.now();
  myFirebaseRef.child("users").transaction(function(data){ return $.extend(data,new_user_hash) });

  $(document).on('click','.lens_save',function(event){
    lens_val = $('.lens_order_total').val();
    if (parseInt(lens_val) < 0)    
    {      
      $('.lens_order_total').val("0.00");   
    }
    var form = $(this).closest('form');
    var validator = form.validate();
    console.log("Lens form validation status: "+validator.numberOfInvalids());
    if(validator.numberOfInvalids() ==  0){
      var new_form_hash = {}
      new_form_hash['lens_save'] = $.now();
      myFirebaseRef.child(form.attr('id')).transaction(function(data){ return $.extend(data, new_form_hash)  });
    }
    else{
      return false;
    }
  });

  $(document).on('click','.lens_delete',function(event){
    var form = $(this).closest('form');
    var new_form_hash = {}
    new_form_hash['lens_delete'] = $.now();
    myFirebaseRef.child(form.attr('id')).transaction(function(data){ return $.extend(data, new_form_hash)  });
  });

  $(document).on('click','.glass_save',function(event){
    var form = $(this).closest('form');
    var validator = form.validate();
    console.log("Glass form validation status: "+validator.numberOfInvalids());
    if(validator.numberOfInvalids() ==  0){
      var new_form_hash = {}
      new_form_hash['glass_save'] = $.now();
      myFirebaseRef.child(form.attr('id')).transaction(function(data){ return $.extend(data, new_form_hash)  });
    }
    else{
      return false;
    }
  });

  $(document).on('click','.glass_delete',function(event){
    var form = $(this).closest('form');
    var new_form_hash = {}
    new_form_hash['glass_delete'] = $.now();
    myFirebaseRef.child(form.attr('id')).transaction(function(data){ return $.extend(data, new_form_hash)  });
  });
  
  $(document).on('change','.lens_total_calc',function(e){
    $("#lens_save_manual_change_"+$(this).data('suborder')).show();
    lens_total($(this));
  });

  function lens_total(element){
    var form = element.closest('form');
    var suborder_id = element.data('suborder')
    $.ajax({ 
             type: "PUT", 
             url: "/orders/"+suborder_id+"/calculate_lens_subtotal",
             data: form.serialize(),
             success: function(data){
               form.find('#contact_lens_order_od_patient_pays').val(data['od_patient_pays']);
               form.find('#contact_lens_order_os_patient_pays').val(data['os_patient_pays']);
               form.find('#od_patient_pays').html("$"+data['od_patient_pays'])
               form.find('#os_patient_pays').html("$"+data['os_patient_pays'])
               form.find('#contact_lens_order_od_qnt').val(data['od_qnt']);
               form.find('#doctor_select_od').html(data['od_qnt'])
               form.find('#contact_lens_order_os_qnt').val(data['os_qnt']);
               form.find('#doctor_select_os').html(data['os_qnt'])
               form.find('#contact_lens_order_total').val(data['total']);
               form.find('.cl_fit_and_allowance_remaining').html("$ "+data['remaining']);
               form.find('#contact_lens_order_remaining').val(data['remaining']);
               form.find('#contact_lens_order_discount').html(data['discount'])
             }
    });
  }

  $(document).on('change','.glass_total_calc',function(e){
    $("#glass_save_manual_change_"+$(this).data('suborder')).show();
    var form = $(this).closest('form');
    var suborder_id = $(this).data('suborder')
    $.ajax({ 
             type: "PUT", 
             url: "/orders/"+suborder_id+"/calculate_glass_subtotal",
             data: form.serialize(),
             success: function(data){
                form.find('#eye_glass_order_glass_type_price').val(data['eyeglass_types_price']);
                form.find('#eye_glass_order_lens_material_price').val(data['lens_material']);
                form.find('#eye_glass_order_frame_price').val(data['glass_frames_price']);
                form.find('#eye_glass_order_addons_price').val(data['order_addons_price']);
                form.find('#eye_glass_order_frame_allowance').val(data['frame_allowance']);
                form.find('#eye_glass_order_overage_discount').val(data['overage_discount']);
                form.find('#eye_glass_order_total').val(data['total']);
              }
    });
  });

  $(document).on('click','.glass_total_calc_for_addons',function(e){
    var form = $(this).closest('form');
    var suborder_id = $(this).data('suborder')
    $.ajax({ 
             type: "PUT", 
             url: "/orders/"+suborder_id+"/calculate_glass_subtotal",
             data: form.serialize(),
             success: function(data){
                form.find('#eye_glass_order_glass_type_price').val(data['eyeglass_types_price']);
                form.find('#eye_glass_order_lens_material_price').val(data['lens_material']);
                form.find('#eye_glass_order_frame_price').val(data['glass_frames_price']);
                form.find('#eye_glass_order_addons_price').val(data['order_addons_price']);
                form.find('#eye_glass_order_frame_allowance').val(data['frame_allowance']);
                form.find('#eye_glass_order_overage_discount').val(data['overage_discount']);
                form.find('#eye_glass_order_total').val(data['total']);
              }
    });
  });


   $(document).on('change',".multi-select-addons",function(){
    var element_id = $(this).attr('id');
    var form = $(this).closest('form');
    form.find('#eye_glass_order_addons_price').val('');
    $.ajax({ 
             type: "GET", 
             url: "/orders/lens_addons_price",
             data: { id: form.find('#'+element_id).val() },
             success: function(data){
              form.find("#"+element_id).parent().parent().parent().find(".addon_static_price").html(data['price']);               // if(data['price'] != null){
               //   var addon_total_element = $("#"+element_id.replace(/(_[^_]*_[^_]*_[^_]*_[^_]*_[^_]*_[^_]*_[^_]*_[^_]*)$/,'_addons_price'));
               //   var addtotal = addon_total_element.val();
               //   if(isNaN(addtotal) == true){ addtotal = 0 ;}
               //   var addonstot = 0;
               //   $("#"+element_id).parent().parent().parent().find(".addon_static_price").data('price',data['price']);
               //   $("#"+element_id).parent().parent().parent().find(".addon_static_price").html(data['price']);
               //   $("#"+element_id).parent().parent().parent().parent().parent().find(".addon_static_price").each(function(){  
               //     addonstot = parseFloat(addonstot) + parseFloat($(this).data('price'));
               //   });
               //   if(isNaN(addonstot) == true){ addonstot = 0 ;}
               //   addon_total_element.val(addonstot);
               // $('.glass_total_calc').trigger('change');
               // }
             }
    });
    form.find('#eye_glass_order_addons_price').trigger('change');
    form.ajaxSubmit({
        dataType: 'json'
    });
  });

  $(document).on('click','.pupillary_distance_distance',function(event){
    var form = $(this).closest('form');
    form.find('.pd_d').toggleClass('hidden');
  });
  $(document).on('click','.pupillary_distance_intermediate',function(event){
    var form = $(this).closest('form');
    form.find('.pd_i').toggleClass('hidden');
  });
  $(document).on('click','.pupillary_distance_near',function(event){
    var form = $(this).closest('form');
    form.find('.pd_n').toggleClass('hidden');
  });

  // submitting eye glass order on changing addon color -- need to do as it causing empty exiting addons color while adding new addon 
  $(document).on('change','.addon_color',function(){
    var element_id = $(this).attr('id');
    var form = $(this).closest('form');
    form.ajaxSubmit({
        dataType: 'json'
    });
  });
   
  $(document).on('change',".glasses-type",function(){
    var element_id = $(this).attr('id');
    var form = $(this).closest('form')
    $.ajax({ 
             type: "GET", 
             url: "/orders/glass_price",
             data: 'glasstype='+form.find('#'+element_id).val(),
             dataType: 'JSON',
             success: function(data){
               if(data['price'] != null)
                 //$("#"+element_id.replace(/(_[^_]*_[^_]*_[^_]*_[^_]*)$/,'_glass_type_price')).val(data['price']);
                 form.find('#eye_glass_order_glass_type_price').val(data['price']);
                 form.find('#eye_glass_order_glass_type_price').trigger('change');
             }
          });
  });
  $(document).on('change',".lens-material",function(){
    var element_id = $(this).attr('id');
    var form = $(this).closest('form')
    $.ajax({ 
             type: "GET", 
             url: "/orders/lens_material_price",
             data: { material: form.find('#'+element_id).val() },
             dataType: 'JSON',
             success: function(data){
               if(data['price'] != null){
                 // $("#"+element_id.replace(/(_[^_]*)$/,'_price')).val(data['price']);
                 form.find('#eye_glass_order_lens_material_price').val(data['price']);
                 form.find('#eye_glass_order_lens_material_price').trigger('change');
               }
             }
    });
    //$('.glass_total_calc').trigger('change');
  });
  $(document).on('change',".glass-frame",function(){
    var element_id = $(this).attr('id');
    var form = $(this).closest('form')
    $.ajax({ 
             type: "GET", 
             url: "/orders/glass_frame_price",
             data: { frame: form.find('#'+element_id).val() },
             dataType: 'JSON',
             success: function(data){
               if(data['price'] != null){
                 // $("#"+element_id.replace(/(_[^_]*_[^_]*_[^_]*_[^_]*)$/,'_frame_price')).val(data['price']);
                 form.find('#eye_glass_order_frame_price').val(data['price']);
                 form.find('#eye_glass_order_frame_price').trigger('change');
               }
               // $('.glass_total_calc').trigger('change');
             }
    });
  });

  
  // exam procedure events over order form
  $(document).on('change','.procedure_adjustment',function(event){
    var form = $(this).closest('form');
    //form.submit();
    form.ajaxSubmit({
      dataType: 'json',
      async: false
    });
    
    myFirebaseRef.on('child_changed', callback_change_firebase);

    myFirebaseRef.on("child_added", callback_added_firebase);
    
    myFirebaseRef.child("procedure_adjustment_change").transaction(function(data){ return $.now()});
  });

  $(document).on('ajax:complete','#procedure_adjustments_form',function(status,data) {
      $('#total_all').html('$ '+data.responseJSON.order_total)
      $('#order_balance_due').html(' $ '+data.responseJSON.balance_due)
      $('#order_refund').html(' $ '+data.responseJSON.order_refund)
      $('#order_reward').html(' $ '+data.responseJSON.order_reward)
  });

  $(document).on('ajax:complete','.procedure-delete-btn',function(status,data){
    var form = $(this).closest('form');
    $(this).parent().parent().next().remove();
    $(this).parent().parent().remove();
    form.submit();
    $.ajax({
      url: "/orders/"+data.responseJSON.order_id+"/reload_procedures",
      type: 'GET',
      async: false
    });
    myFirebaseRef.child("procedure_adjustment_change").transaction(function(data){ return $.now()});
  });

  $(document).on( "railsAutocomplete.select", '.procedure_autocomplete_field', function(event, data){
    if (data.item.id == "new_procedure")
    {
      $('#procedure_modal').modal();
      $('#procedures_1_name').val(data.item.value);
    }
    else
    {
      $('.spinner').show();
      if(data.item.id!="")
      {
        $(this).before('<input type="text" class="hidden" value="'+data.item.id+'" name="examination[procedure_adjustments_attributes]['+Date.now()+'][exam_procedure_id]"/>');

        $('.spinner').show();
        form = $(".order-form" ).closest('form');
        //form.submit()
        form.ajaxSubmit({
          dataType: 'json'
        });
        myFirebaseRef.on('child_changed', callback_change_firebase);
        myFirebaseRef.on("child_added", callback_added_firebase);
        myFirebaseRef.child("procedure_adjustment_change").transaction(function(data){ return $.now()});  
        // myFirebaseRef.child("procedure_impression_plan_page").transaction(function(data){ return $.now()});    
      }
      this.value="";
    }
  });


  $(document).on('ajax:complete', ".order-form",function (status,data) {
    status.preventDefault();
    console.log('data: '+data.statusText+'   status: '+status);
    $('#total_all').html('$ '+data.responseJSON.order_total)
    $('#order_balance_due').html(' $ '+data.responseJSON.balance_due)
    $('#order_refund').html(' $ '+data.responseJSON.order_refund) 
    $('#order_reward').html(' $ '+data.responseJSON.order_reward)
    
    // $.ajax({
    //   url: "/orders/"+data.responseJSON.order_id+"/reload_procedures",
    //   type: 'GET',
    //   async: false
    // });
    myFirebaseRef.on('child_changed', callback_change_firebase);
    myFirebaseRef.on("child_added", callback_added_firebase);
  });
  // $(document).on('click','.proceed_to_checklist',function(){
  //   $('#checklist_modal').modal('show');
  //   var patient_name = $(this).data('name');
  //   $.ajax({ 
  //          type: "GET",
  //          url: "/orders/checklist",
  //          data: "id="+current_order,
  //          dataType: "Js",
  //          }).success(function(data){
  //             
  //              $('#checklist_modal').find('.modal-title').html("<h3><i class='fa fa-user'></i>&nbsp;&nbsp;"+patient_name+"&nbsp;&nbsp;Order Checklist</h3>")
  //              $('#checklist_modal').find('.modal-body').html(data.html);
  //              $('#checklist_modal').modal('show');
  //         });
  //   return false;
  // });
  $(document).on('click','.checklist_save_option',function(event){
    event.preventDefault();
    event.stopImmediatePropagation();
    $('.checklist_update').submit();
  });

  /*--------------------Firebase specific events ----------------------------- */

  $(document).on('change',".lens_suborder_form :input, .lens_suborder_form select",function(event) { 
    element_id = $(this).attr('id');
    var form  = $(this).closest('form');
    element_val = $(this).val();
    suborder_changes(element_id,element_val,form);
  });

  $(document).on('change',".glass_suborder_form :input, .glass_suborder_form select",function(event) { 
    element_id = $(this).attr('id');
    var form  = $(this).closest('form');
    element_val = $(this).val();
    suborder_changes(element_id,element_val,form);
  });
  
  function suborder_changes($element_id,$element_val,form){
    var new_form_hash = {}
    new_form_hash[$element_id] = $element_val
    myFirebaseRef.child(form.attr('id')).transaction(function(data){ return $.extend(data, new_form_hash)  });
    console.log('current-doctor'+current_doctor);

    // if(jQuery.inArray(parseInt(current_doctor),user_updates_count) < 0){
    //   var new_user_hash = {};
      
    //   //user_updates_count.push(parseInt(current_doctor)) ;
    //   new_user_hash[current_doctor] = $.now();
    //   myFirebaseRef.child("users").transaction(function(data){ return $.extend(data,new_user_hash) });
    // }
  }

  function online_users(){
    var user_updates_count = []
    myFirebaseRef.child('users').once('value',function(snapht){
      snapht.forEach(function(snap){
        console.log(snap)
        user_updates_count.push(parseInt(snap.key()));
      });
    });

    $.ajax({ 
              type: "GET", 
              url: "/orders/online_users?ids="+user_updates_count,
              success: function(data){ 
              }
    });
  }
  
  $(document).on('click',".hold_glass",function(event) { 
    $element_id = $(this).attr('id');
    var form  = $(this).closest('form');
    var new_form_hash = {}
    new_form_hash[$element_id] = $.now();
    myFirebaseRef.child(form.attr('id')).transaction(function(data){ return $.extend(data, new_form_hash)  });
  });

  $(document).on('click',".hold_lens",function(event) { 
    $element_id = $(this).attr('id');
    var form  = $(this).closest('form');
    var new_form_hash = {}
    new_form_hash[$element_id] = $.now();
    myFirebaseRef.child(form.attr('id')).transaction(function(data){ return $.extend(data, new_form_hash)  });
    if($('.lens_save').is(':hidden') == false) 
    {      
      $('.lens_save').trigger('click');    
    }
  });

  $(document).on('click','.skip_card_verify',function(e){
      $("#registered-payment-method").modal();
  });

  $(".card_verify").bind('ajax:success',function(data,response, xhr){
      console.log('card verify response data'+data);
      console.log(" card verify on complete bind "+response.status);
      if(response.status == "success"){
        location.reload();
      }
  });

  
  var callback_change_firebase = function(childSnapshot,event){
      if(childSnapshot.key() == "procedure_adjustment_change"){
        console.log("procedure_adjustment_change  parent is"+childSnapshot.ref().parent().key())
        $.ajax({
          url: "/orders/"+childSnapshot.ref().parent().key()+"/reload_procedures",
          type: 'GET',
          async: false
        });
      }

      
      childSnapshot.ref().on('child_changed',function(innerChildSnapshot,prevChildSnap){
         console.log("CHILD CHANGED DATA  => "+innerChildSnapshot.key()+":"+innerChildSnapshot.val());

        if(childSnapshot.key() != "users"){

          var form = $('#'+childSnapshot.key()).closest('form');
          
          if(form.find('#'+innerChildSnapshot.key()).length > 0){
            form.find('#'+innerChildSnapshot.key()).val(innerChildSnapshot.val());
          }

          if($('#'+childSnapshot.key()).length < 1 && childSnapshot.key() != "users"){
            if(childSnapshot.key().indexOf('eye_glass_order') > 0)
            {
              var glass_suborder_id = childSnapshot.key().match(/\d+/)[0];
              $.ajax({ 
                    type: "GET", 
                    url: '/orders/'+glass_suborder_id+'/show_glass_suborder',
                    success: function(data){ 
                  }
              });
            }
            else if(childSnapshot.key().indexOf('contact_lens_order') > 0){
              var lens_suborder_id = childSnapshot.key().match(/\d+/)[0];
               $.ajax({ 
                    type: "GET", 
                    url: '/orders/'+lens_suborder_id+'/show_lens_suborder',
                    success: function(data){ 
                  }
               });
            }
          }

          if( innerChildSnapshot.key().indexOf('lens_save') >= 0){
            var lens_suborder_id = childSnapshot.key().match(/\d+/)[0];
            $.ajax({ 
                    type: "GET", 
                    url: '/orders/'+lens_suborder_id+'/reload_updated_lens',
                    success: function(data){ 
                    }
            });
          }
          else if ( innerChildSnapshot.key().indexOf('glass_save') >= 0) {
            var glass_suborder_id = childSnapshot.key().match(/\d+/)[0];
            $.ajax({ 
                    type: "GET", 
                    url: '/orders/'+glass_suborder_id+'/reload_updated_glass',
                    success: function(data){ 
                    }
            });
          }
          else if(form.find('#'+innerChildSnapshot.key()).hasClass('contact-lens-type')){
            form.find('#'+innerChildSnapshot.key()).data("ui-autocomplete")._trigger("change");
          } 
          else if(form.find('#'+innerChildSnapshot.key()).hasClass('multi-select-addons')){
            form.find('#'+innerChildSnapshot.key()).trigger("change");
          }
          else if(form.find('#'+innerChildSnapshot.key()).hasClass('glasses-type')){
            form.find('#'+innerChildSnapshot.key()).trigger("change");
          }
          else if(form.find('#'+innerChildSnapshot.key()).hasClass('glass-frame')){
            form.find('#'+innerChildSnapshot.key()).trigger("change");
          }
          else if(form.find('#'+innerChildSnapshot.key()).hasClass('lens-material')){
            form.find('#'+innerChildSnapshot.key()).trigger("change");
          }
          
          if(form.find('#'+innerChildSnapshot.key()).hasClass('hold_lens') && childSnapshot.key() != "users"){
            var lens_suborder_id = innerChildSnapshot.key().match(/\d+/)[0];
            $.ajax({ 
                    type: "GET", 
                    url: '/orders/'+lens_suborder_id+'/holded_lens_suborder',
                    success: function(data){ 
                  }
            });
          }
          if(form.find('#'+innerChildSnapshot.key()).hasClass('hold_glass') && childSnapshot.key() != "users"){
            var glass_suborder_id = innerChildSnapshot.key().match(/\d+/)[0];
            $.ajax({ 
                    type: "GET", 
                    url: '/orders/'+glass_suborder_id+'/holded_glass_suborder',
                    success: function(data){ 
                  }
            });
          }
          if(form.find('#'+innerChildSnapshot.key()).hasClass('lens_total_calc') && childSnapshot.key() != "users"){
            lens_total(form.find('#'+innerChildSnapshot.key()));
          } 
          

        }
        online_users()
        // else if(childSnapshot.key() == "users"){
        //   //event.stopImmediatePropagation();
        //  
        //   //user_updates_count = Object.keys(childSnapshot.val())
        //   $.ajax({ 
        //           type: "GET", 
        //           url: "/orders/online_users?ids="+Object.keys(childSnapshot.val()),
        //           success: function(data){ 
        //         }
        //   });
        // }
      });
    
  }

  var callback_added_firebase = function(outerChildSnapshot,event){

      if(outerChildSnapshot.key() == "procedure_adjustment_change"){
        console.log("procedure_adjustment_change  parent is"+outerChildSnapshot.ref().parent().key())
        $.ajax({
          url: "/orders/"+outerChildSnapshot.ref().parent().key()+"/reload_procedures",
          type: 'GET',
          async: false
        });
      }

      outerChildSnapshot.ref().on('child_added',function(childSnapshot,prevChildSnap){
        console.log("CHILD ADDED DATA  => "+childSnapshot.key()+":"+childSnapshot.val());
        if(outerChildSnapshot.key() != "users"){
          
          // retrieve the closest form first as id are same on multiple forms
          var form = $('#'+outerChildSnapshot.key()).closest('form');
          if($('#'+outerChildSnapshot.key()).length < 1 && outerChildSnapshot.key() != "users"){
            if(outerChildSnapshot.key().indexOf('eye_glass_order') > 0)
            {
              var glass_suborder_id = outerChildSnapshot.key().match(/\d+/)[0];
              $.ajax({ 
                    type: "GET", 
                    url: '/orders/'+glass_suborder_id+'/show_glass_suborder',
                    success: function(data){ 
                  }
              });
            }
            else if(outerChildSnapshot.key().indexOf('contact_lens_order') > 0){
              var lens_suborder_id = outerChildSnapshot.key().match(/\d+/)[0];
               $.ajax({ 
                    type: "GET", 
                    url: '/orders/'+lens_suborder_id+'/show_lens_suborder',
                    success: function(data){ 
                  }
               });
            }
          }
          
          if(form.find('#'+childSnapshot.key()).length > 0){
            form.find('#'+childSnapshot.key()).val(childSnapshot.val());
          }

          if( childSnapshot.key().indexOf('lens_save') >= 0){
            var lens_suborder_id = outerChildSnapshot.key().match(/\d+/)[0];
            $.ajax({ 
                    type: "GET", 
                    url: '/orders/'+lens_suborder_id+'/reload_updated_lens',
                    success: function(data){ 
                    }
            });
          }
          else if( childSnapshot.key().indexOf('lens_delete') >= 0){
            var lens_suborder_id = outerChildSnapshot.key().match(/\d+/)[0];
            $.ajax({ 
                    type: "GET", 
                    url: '/orders/'+lens_suborder_id+'/reload_removed_lens',
                    success: function(data){ 
                    }
            });
          }
          else if ( childSnapshot.key().indexOf('glass_save') >= 0) {
            var glass_suborder_id = outerChildSnapshot.key().match(/\d+/)[0];
            $.ajax({ 
                    type: "GET", 
                    url: '/orders/'+glass_suborder_id+'/reload_updated_glass',
                    success: function(data){ 
                    }
            });
          }
          else if( childSnapshot.key().indexOf('glass_delete') >= 0){
            var glass_suborder_id = outerChildSnapshot.key().match(/\d+/)[0];
            $.ajax({ 
                    type: "GET", 
                    url: '/orders/'+glass_suborder_id+'/reload_removed_glass',
                    success: function(data){ 
                    }
            });
          }
          else if(form.find('#'+childSnapshot.key()).hasClass('contact-lens-type')){
            form.find('#'+childSnapshot.key()).data("ui-autocomplete")._trigger("change");
          }
          else if(form.find('#'+childSnapshot.key()).hasClass('multi-select-addons')){
            form.find('#'+childSnapshot.key()).trigger("change");
          }
          else if(form.find('#'+childSnapshot.key()).hasClass('glasses-type')){
            form.find('#'+childSnapshot.key()).trigger("change");
          }
          else if(form.find('#'+childSnapshot.key()).hasClass('glass-frame')){
            form.find('#'+childSnapshot.key()).trigger("change");
          }
          else if(form.find('#'+childSnapshot.key()).hasClass('lens-material')){
            form.find('#'+childSnapshot.key()).trigger("change");
          }

          if(form.find('#'+childSnapshot.key()).hasClass('hold_lens') && outerChildSnapshot.key() != "users"){
            var lens_suborder_id = childSnapshot.key().match(/\d+/)[0];
            $.ajax({ 
                    type: "GET", 
                    url: '/orders/'+lens_suborder_id+'/holded_lens_suborder',
                    success: function(data){ 
                  }
            });
          }
          if(form.find('#'+childSnapshot.key()).hasClass('hold_glass') && outerChildSnapshot.key() != "users"){
            var glass_suborder_id = childSnapshot.key().match(/\d+/)[0];
            $.ajax({ 
                    type: "GET", 
                    url: '/orders/'+glass_suborder_id+'/holded_glass_suborder',
                    success: function(data){ 
                  }
            });
          }
          if(form.find('#'+childSnapshot.key()).hasClass('lens_total_calc') && outerChildSnapshot.key() != "users"){
            lens_total(form.find('#'+childSnapshot.key()));
          }
          if(childSnapshot.key() == "procedure_adjustment_change"){
            console.log("procedure_adjustment_change  parent is")
          }
        }
        online_users()
        // else if(outerChildSnapshot.key() == "users"){
        //   // event.stopImmediatePropagation();
        //  
        //   //user_updates_count = Object.keys(outerChildSnapshot.val());
        //   $.ajax({ 
        //           type: "GET", 
        //           url: "/orders/online_users?ids="+Object.keys(outerChildSnapshot.val()),
        //           success: function(data){ 
        //         }
        //   });
        // }
      });
  }

  //myFirebaseRef.on('child_changed', callback_change_firebase);

  //myFirebaseRef.on("child_added", callback_added_firebase);

  myFirebaseRef.on('child_removed', function(dataSnapshot) {
    console.log('All changes removed');
    // code to handle new value.
  });
  
  // Js form validation rules

  jQuery.validator.addClassRules({
    lens_order_total: {
      required: true
    },
    price_inp: {
      min: 0,
      required: true
    }
  });
  //lensorderFormValidation();
  InitializeLensFormsValidator();
  InitializeGlassFormsValidator();

  $(window).on('beforeunload', function(){
    myFirebaseRef.remove();
  }); 
  lens_order_discount(myFirebaseRef,current_doctor);
}

/* ---------------------------------On Page load end --------------------------------- */



$(document).on('ajax:complete', ".order_confirm_btn",function (status,data) {
  status.preventDefault();
  console.log('data: '+data.statusText+'   status: '+status);
  if (data.responseJSON.status == "Error"){
    $('#registered-payment-method').modal('show');
    $('.error_payment_div').show().text(data.responseJSON.msg);
  }
  else{
    $('#registered-payment-method').modal('hide');
    if (data.responseJSON.order_total)
    {
      $('#total_all').html('$ '+data.responseJSON.order_total)
      $('#order_balance_due').html(' $ '+data.responseJSON.balance_due)
      $('#order_refund').html(' $ '+data.responseJSON.order_refund)
      $('#order_reward').html(' $ '+data.responseJSON.order_reward)
      if ((data.responseJSON.order_total != 0) && (data.responseJSON.balance_due == 0) && ($('.email_invoice').length == 0))
      {
        $('.order_action').append('<li><a class="email_invoice" data-remote="true" data-type="json" href="/orders/'+ data.responseJSON.order_id + '/email_invoice"><i class="fa fa-send"></i>Email Invoice</a></li>')
      }
      else
      {
        $('.email_invoice').remove();
      }
    }
  }
});

$(document).on('ajax:complete','.refund_transaction',function(status,data){
  status.preventDefault();
  console.log('data: '+data.statusText+' status: '+status);
  $('#registered-payment-method').modal('hide');
  $('#order_audits_modal').modal('hide');
  $('#total_all').html('$ '+data.responseJSON.order_total)
  $('#order_balance_due').html(' $ '+data.responseJSON.balance_due)
  $('#order_refund').html(' $ '+data.responseJSON.order_refund)
  $('#order_reward').html(' $ '+data.responseJSON.order_reward)
  if ((data.responseJSON.order_total != 0) && (data.responseJSON.balance_due == 0) && ($('.email_invoice').length == 0))
  {
    $('.order_action').append('<li><a class="email_invoice" data-remote="true" data-type="json" href="/orders/'+ data.responseJSON.order_id + '/email_invoice"><i class="fa fa-send"></i>Email Invoice</a></li>')
  }
  else
  {
    $('.email_invoice').remove();
  }
});

$(document).on('ajax:complete','.cash_check_payment',function(status,data){
  status.preventDefault();
  console.log('data: '+data.statusText+' status: '+status);
  $('#registered-payment-method').modal('hide');
  $('#order_audits_modal').modal('hide');
  $('#total_all').html('$ '+data.responseJSON.order_total)
  $('#order_balance_due').html(' $ '+data.responseJSON.balance_due)
  $('#order_refund').html(' $ '+data.responseJSON.order_refund)
  $('#order_reward').html(' $ '+data.responseJSON.order_reward)
  if ((data.responseJSON.order_total != 0) && (data.responseJSON.balance_due == 0) && ($('.email_invoice').length == 0))
  {
    $('.order_action').append('<li><a class="email_invoice" data-remote="true" data-type="json" href="/orders/'+ data.responseJSON.order_id + '/email_invoice"><i class="fa fa-send"></i>Email Invoice</a></li>')
  }
  else
  {
    $('.email_invoice').remove();
  }
});
/* ------------------------ Helper methods -------------start---------------*/

function cardVerifyValidation(){
    var form = $('.card_verify');
    var error = $('.alert-danger', form);
    var success = $('.alert-success', form);
    form.validate({
        doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true,
        rules: {
                 //account
                 'creditcard[verification_value]': {
                    minlength: 3,
                    required: true
                  }
        },
        messages: { // custom messages for radio buttons and checkboxes
                    'creditcard[verification_value]': {
                        required: "Please enter a number",
                        minlength: "Please enter minimum three digits CVV/security code"
                    }
        },
        invalidHandler: function (event, validator) { //display error alert on form submit
            success.hide();
            error.show();
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            
            label
                .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
        }
    });
}

// contact suborders discount option toggling
function lens_order_discount(myFirebaseRef,current_doctor){
  $(document).on('ifChanged',"input[type='checkbox'].icheck-me" ,function(event) {
    //$(this).closest('.row').find('.toggle_discount').attr('disabled',! $(this).hasClass('checked'));
    var form = $(this).closest('form');
    //form.find('');
    if(event.target.checked === true){
      form.find('#contact_lens_order_discount').val('15.00');
    }
    else{
      //$(this).closest('.row').find('.toggle_discount').val("0.15")
      form.find('#contact_lens_order_discount').val('0.00');
    }
    form.find('#contact_lens_order_discount').trigger('change');
    firebase_contacts_discount(form.find('#contact_lens_order_discount'));
  });
  function firebase_contacts_discount(element){
    var form = element.closest('form');
    if(typeof myFirebaseRef != "undefined" ){
      
      var new_form_hash = {}
      new_form_hash[element.attr('id')] = element.val()
      myFirebaseRef.child(form.attr('id')).transaction(function(data){ return $.extend(data, new_form_hash)  });
      var new_user_hash = {};
      new_user_hash[current_doctor] = $.now();
      myFirebaseRef.child("users").transaction(function(data){ return $.extend(data,new_user_hash) });
    }
  }
}

// This helper method is user to prepolupate cylinder,axis and add property per eye lens over contact lens suborders for an order form

function prepopulate_contact_lens_types_prices(availableTags,myFirebaseRef,current_doctor){
  var cloned_od_cylinder,cloned_od_axis,cloned_od_add;
  var cloned_os_cylinder,cloned_os_axis,cloned_os_add;
  $(document).on('focus','input.contact-lens_type',function(){
    console.log("focus on input: "+$(this).val());
  });
  $(".contact-lens-type").autocomplete({
    source: availableTags,
    change: function (event) {
      var element_id = $(this).attr('id');
      var form = $(this).closest('form');
      var eyetp = $(this).data('eyetp');
      var element_value = $(this).val();
      if(typeof myFirebaseRef != "undefined" ){
        var new_user_hash = {};
        new_user_hash[current_doctor] = $.now();
        //myFirebaseRef.child(element_id).transaction(function(data){ return element_value });
        var new_form_hash = {}
        new_form_hash[element_id] = element_value
        myFirebaseRef.child(form.attr('id')).transaction(function(data){ return $.extend(data, new_form_hash)  });
        myFirebaseRef.child("users").transaction(function(data){ return $.extend(data,new_user_hash) });
      }
      if(eyetp == "od"){
        if($("#"+element_id.replace('type','cylinder')).length == 0){ 
          // cloned_od_cylinder.appendTo($('#'+element_id).parents(".lenses").find('.'+eyetp+'_type_fields'));
          cloned_od_cylinder.appendTo(form.find('.'+eyetp+'_type_fields'))
        }
        if($("#"+element_id.replace('type','axis')).length == 0){ 
          // cloned_od_axis.appendTo($('#'+element_id).parents(".lenses").find('.'+eyetp+'_type_fields'));
          cloned_od_axis.appendTo(form.find('.'+eyetp+'_type_fields'));
        }
        if($("#"+element_id.replace('type','add')).length == 0){ 
          // cloned_od_add.appendTo($('#'+element_id).parents(".lenses").find('.'+eyetp+'_type_fields'));
          cloned_od_add.appendTo(form.find('.'+eyetp+'_type_fields'));
        }
        cloned_od_cylinder = $("#"+element_id.replace('type','cylinder')).parents('.col-md-2').clone();
        cloned_od_axis  = $("#"+element_id.replace('type','axis')).parents('.col-md-2').clone();
        cloned_od_add  = $("#"+element_id.replace('type','add')).parents('.col-md-2').clone();
      }
      else if(eyetp == "os"){
        if($("#"+element_id.replace('type','cylinder')).length == 0){ 
          // cloned_os_cylinder.appendTo($('#'+element_id).parents(".lenses").find('.'+eyetp+'_type_fields'));
          cloned_os_cylinder.appendTo(form.find('.'+eyetp+'_type_fields'));
        }
        if($("#"+element_id.replace('type','axis')).length == 0){ 
          // cloned_os_axis.appendTo($('#'+element_id).parents(".lenses").find('.'+eyetp+'_type_fields'));
          cloned_os_axis.appendTo(form.find('.'+eyetp+'_type_fields'));
        }
        if($("#"+element_id.replace('type','add')).length == 0){ 
          // cloned_os_add.appendTo($('#'+element_id).parents(".lenses").find('.'+eyetp+'_type_fields'));
          cloned_os_add.appendTo(form.find('.'+eyetp+'_type_fields'));
        }
        cloned_os_cylinder = $("#"+element_id.replace('type','cylinder')).parents('.col-md-2').clone();
        cloned_os_axis  = $("#"+element_id.replace('type','axis')).parents('.col-md-2').clone();
        cloned_os_add  = $("#"+element_id.replace('type','add')).parents('.col-md-2').clone();
      }
      $.ajax({ 
        type: "GET", 
        url: "/orders/lens_prices_base_curves",
        data: 'brand='+form.find('#'+element_id).val(),
        dataType: 'JSON',
        success: function(data){ 
          $.each(data,function(type,vals){
            var base_curve = form.find("#"+element_id.replace('type','base_curve'));
            var diameter = form.find("#"+element_id.replace('type','diameter'));
            var sphere = form.find("#"+element_id.replace('type','cylinder'));
            var cylinder = form.find("#"+element_id.replace('type','cylinder'));
            var axis = form.find("#"+element_id.replace('type','axis'));
            var add = form.find("#"+element_id.replace('type','add'));
            //var price  = $("#"+element_id.replace('lenses_attributes_0_'+eyetp+'_type',eyetp+'_price'));
            var price  = form.find("#contact_lens_order_"+eyetp+'_price');
            form.find("#contact_lens_order_"+eyetp+'_patient_pays').val("");
            if(typeof vals[0] != 'undefined'){
              // $('#'+element_id).parents('.lens_options').find('.'+eyetp+'_details').removeClass('hidden');
              form.find('.'+eyetp+'_details').removeClass('hidden');
              if(vals[0]['dia']!= null){ 
                diameter.empty();
                $.each(vals[0]['dia'].split(','),function(indx,dia_option){
                  diameter.append("<option value='"+dia_option+"'>"+dia_option+"</option>");
                });
              }
              if(vals[0]['bc'] != null){
                base_curve.empty();
                $.each(vals[0]['bc'].split(','),function(indx,bc_option){
                  base_curve.append("<option value='"+bc_option+"'>"+bc_option+"</option>"); 
                });                   
              }
              if(vals[0]['add']!= null){ 
                add.empty();
                $.each(vals[0]['add'].split(','),function(indx,add_option){
                  add.append("<option value='"+add_option+"'>"+add_option+"</option>");
                });
              }
              if(vals[0]['sphere']!= null){ 
                sphere.empty();
                $.each(vals[0]['sphere'].split(','),function(indx,sphere_option){
                  sphere.append("<option value='"+sphere_option+"'>"+sphere_option+"</option>");
                });
              }
              if(vals[0]['cylinder']!= null){ 
                cylinder.empty();
                $.each(vals[0]['cylinder'].split(','),function(indx,cylinder_option){
                  cylinder.append("<option value='"+cylinder_option+"'>"+cylinder_option+"</option>");
                });
              }
              if(vals[0]['axis']!= null){ 
                axis.empty();
                $.each(vals[0]['axis'].split(','),function(indx,axis_option){
                  axis.append("<option value='"+axis_option+"'>"+axis_option+"</option>");
                });
              }
              switch(vals[0]['type']){
                case "sphere":
                  cylinder.parents('.col-md-2').remove();
                  axis.parents('.col-md-2').remove();
                  add.parents('.col-md-2').remove();
                  break;
                case "toric":
                  add.parents('.col-md-2').remove();
                  break;
                case "multifocal":
                  cylinder.parents('.col-md-2').remove();
                  axis.parents('.col-md-2').remove();
                  break;
                default: 
                  break;
              }
              if(vals[0]['price'] != null){
                price.val(vals[0]['price'])
              }
              if(vals[0]['id'] != null){
                // $('#'+element_id.replace(/([^_]*_[^_]*_[^_]*[^_]*_[^_]*_[^_]*)$/,eyetp+"_lens_types_price_id")).val(vals[0]['id']);
                form.find('#contact_lens_order_'+eyetp+"_lens_types_price_id").val(vals[0]['id']);
                form.find('#'+element_id.replace('type','lens_types_price_id')).val(vals[0]['id']);
                console.log('element_id-----'+element_id+'; id---------'+vals[0]['id']+';-----lens_order-----'+element_id.replace(/_([^_]*_[^_]*_[^_]*_[^_]*_[^_]*)$/,"_"+eyetp+"_lens_types_price_id"));
              }
            }
            form.find("#contact_lens_order_"+eyetp+'_patient_pays').trigger("change");
          });         
        }
      });
    }
  });
}


function prepopulate_contact_lens_type1(availableTags)
{

  $(".contact-lens-type").autocomplete({
    source: availableTags,
    change: function (event) {
    
      var element_id = $(this).attr('id');
      var form = $(this).closest('form');
      var eyetp = $(this).data('eyetp');
      var element_value = $(this).val();
      if(typeof myFirebaseRef != "undefined" ){
        var new_user_hash = {};
        new_user_hash[current_doctor] = $.now();
        //myFirebaseRef.child(element_id).transaction(function(data){ return element_value });
        var new_form_hash = {}
        new_form_hash[element_id] = element_value
        myFirebaseRef.child(form.attr('id')).transaction(function(data){ return $.extend(data, new_form_hash)  });
        myFirebaseRef.child("users").transaction(function(data){ return $.extend(data,new_user_hash) });
      }
      if(eyetp == "od"){
        if($("#"+element_id.replace('type','cylinder')).length == 0){ 
          // cloned_od_cylinder.appendTo($('#'+element_id).parents(".lenses").find('.'+eyetp+'_type_fields'));
          cloned_od_cylinder.appendTo(form.find('.'+eyetp+'_type_fields'))
        }
        if($("#"+element_id.replace('type','axis')).length == 0){ 
          // cloned_od_axis.appendTo($('#'+element_id).parents(".lenses").find('.'+eyetp+'_type_fields'));
          cloned_od_axis.appendTo(form.find('.'+eyetp+'_type_fields'));
        }
        if($("#"+element_id.replace('type','add')).length == 0){ 
          // cloned_od_add.appendTo($('#'+element_id).parents(".lenses").find('.'+eyetp+'_type_fields'));
          cloned_od_add.appendTo(form.find('.'+eyetp+'_type_fields'));
        }
        cloned_od_cylinder = $("#"+element_id.replace('type','cylinder')).parents('.col-md-2').clone();
        cloned_od_axis  = $("#"+element_id.replace('type','axis')).parents('.col-md-2').clone();
        cloned_od_add  = $("#"+element_id.replace('type','add')).parents('.col-md-2').clone();
      }
      else if(eyetp == "os"){
        if($("#"+element_id.replace('type','cylinder')).length == 0){ 
          // cloned_os_cylinder.appendTo($('#'+element_id).parents(".lenses").find('.'+eyetp+'_type_fields'));
          cloned_os_cylinder.appendTo(form.find('.'+eyetp+'_type_fields'));
        }
        if($("#"+element_id.replace('type','axis')).length == 0){ 
          // cloned_os_axis.appendTo($('#'+element_id).parents(".lenses").find('.'+eyetp+'_type_fields'));
          cloned_os_axis.appendTo(form.find('.'+eyetp+'_type_fields'));
        }
        if($("#"+element_id.replace('type','add')).length == 0){ 
          // cloned_os_add.appendTo($('#'+element_id).parents(".lenses").find('.'+eyetp+'_type_fields'));
          cloned_os_add.appendTo(form.find('.'+eyetp+'_type_fields'));
        }
        cloned_os_cylinder = $("#"+element_id.replace('type','cylinder')).parents('.col-md-2').clone();
        cloned_os_axis  = $("#"+element_id.replace('type','axis')).parents('.col-md-2').clone();
        cloned_os_add  = $("#"+element_id.replace('type','add')).parents('.col-md-2').clone();
      }
      $.ajax({ 
        type: "GET", 
        url: "/orders/lens_prices_base_curves",
        data: 'brand='+form.find('#'+element_id).val(),
        dataType: 'JSON',
        success: function(data){ 
          $.each(data,function(type,vals){
            var base_curve = form.find("#"+element_id.replace('type','base_curve'));
            var diameter = form.find("#"+element_id.replace('type','diameter'));
            var sphere = form.find("#"+element_id.replace('type','cylinder'));
            var cylinder = form.find("#"+element_id.replace('type','cylinder'));
            var axis = form.find("#"+element_id.replace('type','axis'));
            var add = form.find("#"+element_id.replace('type','add'));
            //var price  = $("#"+element_id.replace('lenses_attributes_0_'+eyetp+'_type',eyetp+'_price'));
            var price  = form.find("#contact_lens_order_"+eyetp+'_price');
            form.find("#contact_lens_order_"+eyetp+'_patient_pays').val("");
            if(typeof vals[0] != 'undefined'){
              // $('#'+element_id).parents('.lens_options').find('.'+eyetp+'_details').removeClass('hidden');
              form.find('.'+eyetp+'_details').removeClass('hidden');
              if(vals[0]['dia']!= null){ 
                diameter.empty();
                $.each(vals[0]['dia'].split(','),function(indx,dia_option){
                  diameter.append("<option value='"+dia_option+"'>"+dia_option+"</option>");
                });
              }
              if(vals[0]['bc'] != null){
                base_curve.empty();
                $.each(vals[0]['bc'].split(','),function(indx,bc_option){
                  base_curve.append("<option value='"+bc_option+"'>"+bc_option+"</option>"); 
                });                   
              }
              if(vals[0]['add']!= null){ 
                add.empty();
                $.each(vals[0]['add'].split(','),function(indx,add_option){
                  add.append("<option value='"+add_option+"'>"+add_option+"</option>");
                });
              }
              if(vals[0]['sphere']!= null){ 
                sphere.empty();
                $.each(vals[0]['sphere'].split(','),function(indx,sphere_option){
                  sphere.append("<option value='"+sphere_option+"'>"+sphere_option+"</option>");
                });
              }
              if(vals[0]['cylinder']!= null){ 
                cylinder.empty();
                $.each(vals[0]['cylinder'].split(','),function(indx,cylinder_option){
                  cylinder.append("<option value='"+cylinder_option+"'>"+cylinder_option+"</option>");
                });
              }
              if(vals[0]['axis']!= null){ 
                axis.empty();
                $.each(vals[0]['axis'].split(','),function(indx,axis_option){
                  axis.append("<option value='"+axis_option+"'>"+axis_option+"</option>");
                });
              }
              switch(vals[0]['type']){
                case "sphere":
                  cylinder.parents('.col-md-2').remove();
                  axis.parents('.col-md-2').remove();
                  add.parents('.col-md-2').remove();
                  break;
                case "toric":
                  add.parents('.col-md-2').remove();
                  break;
                case "multifocal":
                  cylinder.parents('.col-md-2').remove();
                  axis.parents('.col-md-2').remove();
                  break;
                default: 
                  break;
              }
              if(vals[0]['price'] != null){
                price.val(vals[0]['price'])
              }
              if(vals[0]['id'] != null){
                // $('#'+element_id.replace(/([^_]*_[^_]*_[^_]*[^_]*_[^_]*_[^_]*)$/,eyetp+"_lens_types_price_id")).val(vals[0]['id']);
                form.find('#contact_lens_order_'+eyetp+"_lens_types_price_id").val(vals[0]['id']);
                form.find('#'+element_id.replace('type','lens_types_price_id')).val(vals[0]['id']);
                console.log('element_id-----'+element_id+'; id---------'+vals[0]['id']+';-----lens_order-----'+element_id.replace(/_([^_]*_[^_]*_[^_]*_[^_]*_[^_]*)$/,"_"+eyetp+"_lens_types_price_id"));
              }
            }
            form.find("#contact_lens_order_"+eyetp+'_patient_pays').trigger("change");
          });         
        }
      });
    }
  });

}

//prepopulate_contact_lens_type1();


// load order when select order from top dropdown
function loadOrder() {
  window.location = "/orders/"+$("select#select_order option:selected").attr('value')+'/edit';
}
/* ------------------------ Helper methods -------------end -----------------*/