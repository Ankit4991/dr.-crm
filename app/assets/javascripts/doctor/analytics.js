//index page js ------------start-------------------------
function analyticsIndexPage(){    
  $('.year').show().removeClass('hidden');
  $("#day_view").click(function(){
    $('.day').show().removeClass('hidden');
    $('.week').hide();
    $('.month').hide();
    $('.year').hide();
  });
  $("#week_view").click(function(){
    $('.week').show().removeClass('hidden');
    $('.day').hide();
    $('.month').hide();
    $('.year').hide();
  });
  $("#month_view").click(function(){
    $('.month').show().removeClass('hidden');
    $('.day').hide();
    $('.week').hide();
    $('.year').hide();
  });
  $("#year_view").click(function(){
    $('.year').show().removeClass('hidden');
    $('.month').hide();
    $('.week').hide();
    $('.day').hide();
  });
}
//Analytic page js ------------End-------------------------