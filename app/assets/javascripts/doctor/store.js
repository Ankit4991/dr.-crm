function size_limit(){
  $('.store_form').submit(function(e) {
    var fileInput = $(this).find("#store_logo");
    if(fileInput.get(0).files.length){
      var fileSize = fileInput.get(0).files[0].size; // in bytes
      var maxSize = 1024000;
      if(fileSize>maxSize){
        alert('Please use an image smaller than 1mb, and try again');
        return false;
      }
    }
  });
}

function reload_on_update(){
  $('.reportform').on('ajax:success', function(e, data, status, xhr){
     window.location.href='/stores#tab_3-3'
     window.location.reload();
  });
}

$(document).ready(function() {
  Metronic.init();
  ComponentsPickers.init();
  telInput();

  $(".store_edit_btn").bind('ajax:complete', function () {
    $('.spinner').hide();
    $("#store_edit_modal").modal();
  });
});

function staff_form(){
  $("#new_doctor").bind('ajax:complete', function (status, data) {
    if(data.responseJSON.status == "Success") 
    {  
      $(this).find("input").each(function(i){        
        if (this.type!="submit")
          this.value="";
      });
    }
  })

  $('.multi-select-roles').multiselect({
    enableFiltering: true,
    includeSelectAllOption: true,
    maxHeight: '300',
    buttonWidth: '100%',
    templates: {
      filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default multiselect-clear-filter" type="button"><b>X<b></button></span>',
      saveBtn: '<span class="input-group-btn"><button type="button" class="btn btn-default"><b>√<b></button></span>'
    }
  });
  $('.dropdown-toggle').dropdown();

  $(".doctor_edit_btn").bind('ajax:complete', function () {
    $('.spinner').hide();
    $("#doctor_edit_modal").modal();
  })
}

function doctor_save_btn()
{
  $('#doctor_edit_modal').modal('hide');
}