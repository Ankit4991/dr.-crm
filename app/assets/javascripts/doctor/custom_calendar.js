$(document).ready(function(){
  calendar();
});

$(document).on( "railsAutocomplete.select", '.event_form .procedure_autocomplete_field', function(event,data) {
  if(data.item.id!="")
  {
    $(this).before('<div><label id = "label_id"><input  type="hidden" value="'+data.item.id+'" name="exam_procedure_ids[]" >'+data.item.label+'</label><a href="#" id ="ab" onclick="$(this).parent().remove()">x</a></div>');
  }
  this.value="";
});

$(document).on('click', ".fc-button", function(){
  $('.popover').remove();
}); 

$(document).bind('ajax:complete', ".event_form", function(evt, data, status, xhr){
  $('.popover').remove();
  $('#calendar').fullCalendar( 'refetchEvents' );
});

$(document).on('click','.patient_existance', function() {
   $('.patient_existance:input').prop('checked', false); 
  if($(this).hasClass('patient_already_exists')){
    $(this).prop('checked', true);
    $(this).parents('.popover').find('#patient_name').show();
    $(this).parents('.popover .popover-content').find('input[name$="patient[password]"]').hide();
    $('#patient_email').attr('readonly', true);
    $('#patient_email_not_supplied').attr('disabled', 'disabled');
    $(this).parents('.popover .popover-content').find('input[name$="patient[email]"]').val("");
    $('#patient_email_not_supplied').prop("checked", false);
  }else{
    $(this).prop('checked', true);
    $(this).parents('.popover').find('#patient_name').hide();
    $(this).parents('.popover .popover-content').find('input[name$="patient[password]"]').show();
    $(this).parents('.popover').find('#patient_name').val("");
    $(this).parents('.popover .popover-content').find('input[name$="patient[email]"]').val("");
    $(this).parents('.popover .popover-content').find('input[name$="patient[first_name]"]').val("");
    $(this).parents('.popover .popover-content').find('input[name$="patient[last_name]"]').val("");
    $(this).parents('.popover .popover-content').find('input[name$="patient[birthday]"]').val("");
    //$(this).parents('.popover .popover-content').find('input[name$="patient[vision_insurance_id]"]').val("");
    $(this).parents('.popover .popover-content').find('input[name$="patient[phone]"]').val("");
    $(this).parents('.popover .popover-content').find('#patient_actions').remove();
    $('#patient_email').removeAttr('readonly');
    $('#patient_email_not_supplied').removeAttr('disabled','disabled');
  }
});

$(document).on( "railsAutocomplete.select", '#patient_name', function(event,ui) {
   var exam_url = "/examinations/new?patient="+ui.item.id;
   $(this).parents('.popover .popover-content').find('input[name$="patient[email]"]').val(ui.item.email);
   $(this).parents('.popover .popover-content').find('input[name$="patient[first_name]"]').val(ui.item.first_name);
   $(this).parents('.popover .popover-content').find('input[name$="patient[last_name]"]').val(ui.item.last_name);
   if(ui.item.vision_insurance_id != null){
     $( ".vision-insurance-options" ).find( 'option[value="' + ui.item.vision_insurance_id + '"]' ).prop( "selected", true );
   }
   $(this).parents('.popover .popover-content').find('input[name$="patient[birthday]"]').val(ui.item.birthday);
   $(this).parents('.popover .popover-content').find('input[name$="patient[phone]"]').val(ui.item.phone);
   $(this).parents('.popover .popover-content').find('input[name$="patient[phone]"]').prev().find('input').val(ui.item.phone);
   code = ui.item.phone.slice(-14,-10).split('+')[1]
   $('.country-list li').removeClass('highlight active');
   $('.country-list').find("li[data-dial-code=" + code +"]").addClass('highlight active')
   cc2 = $('.country-list').find("li[data-dial-code=" + code +"]").attr('data-country-code')
   cc1 = $('.selected-flag .iti-flag').attr('class').split('iti-flag ')[1]
   $('.selected-flag .iti-flag').removeClass(cc1);
   $('.selected-flag .iti-flag').addClass(cc2);
   $(this).parents('.popover .popover-content').find('input[name$="booking[joint_resource_id]"]').val(ui.item.id);
   /*$(this).parents('.popover .popover-content').find('button').after(('<div id="patient_actions"><a href='+exam_url+' class="pull-left btn btn-primary btn-sm">Exam</a><a href="#" class="pull-left btn btn-success  btn-sm">Order</a><a href="#" class="pull-left btn btn-success  btn-sm">Profile</a></div>'));*/
});

var calendar = function(view_name){
    $('.popover').remove();//just remove all previous popover
    var doctor_id = $("input[type='radio'][name='doctor_id']:checked").val();
    var store_id = $("#store_id").val()
    var calendar_views = 'month,agendaWeek,agendaDay';
    var doctorAvailablePeriods = [];
    var $holiday = [];
    var $holidays_array = []
    var $doctor_off_days = []
    var slot_times = parseInt($("#fetch_slot").val());
    var writeonly = true;
    var event_source_url= "";
    var day_event_more_total; 
    if (view_name === undefined)
    { 
      view_name = "new";
    }
    if(view_name == "new" && (doctor_id === undefined || doctor_id === null))
    {
      event_source_url = "#"
    }
    else if(view_name == "new" || typeof view_name === "undefined")
    { 

      event_source_url = ' /bookings/calendar?doctor_id='+doctor_id+'&store_id='+store_id; 
    }
    else if(view_name == "store")
    {
      writeonly = false;
      calendar_views= 'year,month,agendaWeek,agendaDay';
      event_source_url = ' /bookings?store_id='+store_id; 
    }
    else{ 
      writeonly = false;
      calendar_views= 'year,month,agendaWeek,agendaDay';
      event_source_url = ' /bookings'; 
    }
    // page is now ready, initialize the calendar...

    // use to get doctor schedule
    

    if(!(doctor_id === undefined || doctor_id === null) && !(store_id === undefined || store_id === null))
    {
      var url;
      if(!(store_id === undefined || store_id === null))
      {
        url= '/bookings/calendar_basic_requirement?doctor_id='+doctor_id+'&store_id='+store_id+'&view='+view_name;
        
      }
      else
      {
        url= '/bookings/calendar_basic_requirement?doctor_id='+doctor_id+'&view='+view_name;
      }
      $.ajax(
        url,
        { 'type': 'get',
          dataType: 'json',
          success: function(data) {
            doctorAvailablePeriods = data.doctor_schedule;
            // $holiday = data.holidays;
            $holidays_array = data.holidays_array
            $doctor_off_days = data.doctor_off_days
          }
        }

      );
    }

    var current_resource = function(){
      return window.location.href.match(/resources\/(\d+)\/bookings/)[1];
    };

    var today_or_later = function(start){
      var today = new Date();
      if(start < today) {
        return false;
      } else {
        return true;
      };
    };

    function display_holidays(view, i, holidays)
    {      
      holidayMoment = moment(holidays[i][0],'YYYY-MM-DD');
      reason = holidays[i][1];
      if (view.name == 'month') {
        $("td[data-date=" + holidayMoment.format('YYYY-MM-DD')+ "]").css({'background-color':'lightgray', 'text-align': 'center', 'text-transform':'capitalize'});
        $("td[data-date=" + holidayMoment.format('YYYY-MM-DD')+ "]").find('.fc-day-content').html(reason);
      } else if (view.name =='agendaWeek') {
        var classNames = $("th:contains(' " + holidayMoment.format('M/D') + "')").attr("class");
        if (classNames != null) {
          var classNamesArray = classNames.split(" ");
          for(var i = 0; i < classNamesArray.length; i++) {
            if(classNamesArray[i].indexOf('fc-col') > -1) {
              $("td." + classNamesArray[i]).css("background-color", "lightgray");
              break;
            }
          }
        }
      } else if (view.name == 'agendaDay') {
        if(holidayMoment.format('YYYY-MM-DD') == moment($('#calendar').fullCalendar('getDate')).format('YYYY-MM-DD')) {
          $("td.fc-col0").css("background-color", "lightgray");
        };
      }
    }

    function holiday_render(view)
    {
      var holidays = [];
      holidays = $holidays_array || [];
      var doctor_off_days = [];
      doctor_off_days = $doctor_off_days || [];
      var holidayMoment;
      var reason;
      for(var i = 0; i < holidays.length; i++) {        
        setTimeout(display_holidays(view, i, holidays), 3000+(i*100));
      }

      for(var j=0; j < doctor_off_days.length; j++)
      {
        $('td.'+doctor_off_days[j]).css("background-color", "lightgray");
      }
    }

    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: calendar_views
      }, 
            
      eventSources: [{
        url: event_source_url,  
      }],

      selectable: {
        month: false,
        agenda: true
      } ,
      slotMinutes: slot_times,
    
    editable:  writeonly,
    eventStartEditable:  writeonly, 
    eventDurationEditable: writeonly,

    eventDrop: function(booking) {
      var length = (booking.end-booking.start)/(3600000)*60;

        function updateEvent(booking) {
              $.ajax(
                '/bookings/'+booking.id+'?doctor_id='+$("input[name=doctor_id]:radio:checked").val(),
                { 'type': 'PATCH',
                  dataType: 'json',
                  data: { booking: { 
                           start_time: "" + booking.start,
                           length: length
                         } },
                  success: function(data) {
                    jQuery.gritter.add({ title: data.status, text: data.msg });
                  }
                }

              );
          };

        updateEvent(booking);

      }
    ,

    eventResize: function(booking) {
      var length = (booking.end-booking.start)/(3600000)*60;

        function updateEvent(booking) {
              $.ajax(
                '/bookings/'+booking.id+'?doctor_id='+$("input[name=doctor_id]:radio:checked").val(),
                { 'type': 'PATCH',
                  dataType: 'json',
                  data: { booking: { 
                           start_time: "" + booking.start,
                           length: length
                         } },
                  success: function(data) {
                    jQuery.gritter.add({ title: data.status, text: data.msg });
                  }
                }
              );
          };

        updateEvent(booking);

      }
    ,

    dayClick: function(date, allDay, jsEvent, view) {
      // console.log(view.name);
      if (view.name === "month") { 
        $('#calendar').fullCalendar('gotoDate', date);
        $('#calendar').fullCalendar('changeView', 'agendaDay');
      }
    }
    ,
    
    eventRender: function(booking, element, view) { 
    	
      
      //if($("#doctor_id").length > 0)
        //doctorAvailablePeriods=booking.doctor_working_shcedule
      
      $(element).removeClass('MaxHght');
      if (view.name == 'month') {
        $(element).addClass('MaxHght');
        var year = booking.start.getFullYear(), month = booking.start.getMonth() + 1, date = booking.start.getDate();
        var result = year + '-' + (month < 10 ? '0' + month : month) + '-' + (date < 10 ? '0' + date : date);
        $(element).addClass(result);
        var ele = $('td[data-date="' + result + '"]'),count=$('.' + result).length;
        $(ele).find('.viewMore').remove();
        if ( count < 4)
          day_event_more_total = 0;
        if ( count > 4) {
          day_event_more_total = day_event_more_total+1                                   
          $(ele).find('.fc-day-content').after('<a class="viewMore"> More('+day_event_more_total+') </a>');
          $('.' + result + ':gt(3)').remove(); 
        } 
       
      }
      element.css('background-color', booking.doctor_theme_color);
      birthdate = booking.patient_dob != null ? booking.patient_dob : 'birthday'
      appointment_status = booking.appointment_status != null ? booking.appointment_status.replace('_',' ') : ''
      /*----N/A for Vision Insurance we are showing inside My Calender when we click on week View--start--*/
      if (booking.patient_vision_insurance == null){
        booking.patient_vision_insurance = 'N/A'
      }
      var dob_tag;
      if (booking.patient_dob == null){
        dob_tag= "";
      }
      else{
        dob_tag ='<p>DOB : '+booking.patient_dob+'</p>';
      }
    /**
     *  Format phone numbers
    */
    function formatPhone(s) {
      var regexObj = /^(?:\+?1[-. ]?)?(?:\(?([0-9]{3})\)?[-. ]?)?([0-9]{3})[-. ]?([0-9]{4})$/;
      if (regexObj.test(s)) {
          var parts = s.match(regexObj);
          var phone = "";
          if (parts[1]) { phone += " (" + parts[1] + ") "; }
          phone += parts[2] + "-" + parts[3];
          return phone;
      }
      else {
          //invalid phone number
          return s;
      }
    }

      /*----N/A for Vision Insurance we are showing inside My Calender when we click on week View--end--*/
      if (booking.patient_medical_insurance == 'hide'){
        booking_details = "&nbsp;&nbsp;"+ booking.patient_name+ ' , '+birthdate+' , Age: '+booking.patient_age +' , Vision Insurance: '+ booking.patient_vision_insurance+ ((booking.comment == "") ? "" : ' , Comment: '+booking.comment) +  ((appointment_status == "") ? "" : ' ,Status: '+ appointment_status)
      }
      else{
        
        booking_details = "&nbsp;&nbsp;"+ booking.patient_name+ ' , '+birthdate+' , Age: '+booking.patient_age +' , Vision Insurance: '+ booking.patient_vision_insurance+ ', Medical Insurance: ' +booking.patient_medical_insurance+((booking.comment == "") ? "" : ' , Comment: '+booking.comment) +  ((appointment_status == "") ? "" : ' ,Status: '+ appointment_status)
      }

      
      if(booking.patient_id == null){
        element.find(".fc-event-inner").append('<button type="button" data-eventid="'+booking._id+'" class="close closeon btn" ><span style="color: white; font-size: 22px; background-color: #000;">&times;</span></button>');
        element.find(".closeon").click(function(e) {
          e.stopImmediatePropagation();
          $('.popover').remove();
          $('#calendar').fullCalendar( 'refetchEvents' );
        });
      }
      else
      {element.find(".fc-event-inner").append(booking_details);}

      var popup_content;
      var title;
      if(typeof booking.patient_id === "undefined")
      {
        title= "Book Schedule for patient";
        popup_content = '<form class="event_form" action="/bookings" accept-charset="UTF-8" data-remote="true" method="post"><input type="hidden" name="booking[doctor_id]" value="'+doctor_id+'"><input type="hidden" name="booking[store_id]" value="'+store_id+'"><input type="hidden" name="booking[joint_resource_id]"><input type="hidden" name="booking[start_time]" value="'+booking.start+'"> <input type="hidden" name="booking[end_time]" value="'+booking.end+'"> <div class="row"> <div class="col-sm-6"> Patient exist : No<input name="patient_existence" type="radio" checked value="false" class="patient_existance"/>Yes<input name="patient_existence" type="radio" value="true" class="patient_existance patient_already_exists"/> </div><div class="col-sm-6"> <input type="text" name="patient_name" id="patient_name" value="" class="form-control ui-autocomplete-input" placeholder="Enter a patient name" data-autocomplete="/autocompletes/autocomplete_patient_first_name" autocomplete="off" style="display:none"/> </div></div><div class="row"> <div class="col-sm-6"> <label>Name</label><div class="row"><div class="col-sm-6"><input type="text" name="patient[first_name]" id="patient_first_name" class="form-control" required="true" placeholder="first"></div><div class="col-sm-6"><input type="text" name="patient[last_name]" id="patient_last_name" class="form-control" required="true" placeholder="last"></div></div></div><div class="col-sm-6"> <label>Provider</label> <select class="select optional doctor-list-options form-control" style="width: 150px; overflow-x: hidden;" name="booking[doctor_id]"></select> </div></div><div class="row"> <div class="col-sm-6"> <label>DOB</label> <input type="text" name="patient[birthday]" placeholder="mm-dd-yyyy" class="form-control"> </div><div class="col-sm-6"><div class="form-group select optional booking_exam_procedures"> <label>Reason Of Visit</label><input name="booking[exam_procedure_ids][]" type="hidden" value=""><select multiple="multiple" data-value= "" class="select optional multi-select-exams form-control" style="width: 200px; overflow-x: hidden; display: none;" name="booking[exam_procedure_ids][]" id="booking_exam_procedure_ids"></select></div></div></div><div class="row"><div class="col-sm-6"> <label>Phone</label> <input name="tel_input" class="form-control tel-input" placeholder="2025550172"><input name="patient[phone]" class="form-control hidden"> </div><div class="col-sm-6"> <label>Comments</label> <input type="string" name="booking[comment]" class="form-control"> </div> <div class="col-sm-6"> <div class="col-sm-10" style="padding-left: 0px;padding-right: 10px;"> <label>Email</label> <input type="email" name="patient[email]" id="patient_email"  required="true"  class="form-control" onblur="duplicate_email_check();"></div> <div class="col-sm-2" style="padding-left: 5px;padding-top: 27px;padding-right: 0px;" > <button type="button" class="btn btn-primary btn-sm pull-right" style="width: 155%;" id="append_random">N/A</button> </div> <input type="hidden" name="create_family" id="create_family" value="No"><input type="hidden" name="relation" id="relation"> </div><div class="col-sm-6"><label>Vision Insurance</label><select class="select optional vision-insurance-options form-control" style="width: 150px; overflow-x: hidden;" name="patient[vision_insurance_id]" id="patient_vision_insurance_id"></select> </div><div class="col-md-6"></div><div class="col-sm-6 medical_insurance"> </div></div><button type="submit" class="btn btn-primary btn-sm pull-right"> Save</button></form>'; }
      else if(booking.patient_name != null)
      {
        title= "Patient Detail";
        if (booking.family_member_id == "") {
          reconfirm_url = '<a href="/dashboards/'+ booking.patient_id + '/reconfirm_notify?booking=' + booking.id +'&patient_class=Patient'
          patient_url = '<a href="/doctor/patient/profile?patient_id='+booking.patient_id
        }
        else {
          reconfirm_url = '<a href="/dashboards/'+ booking.family_member_id + '/reconfirm_notify?booking=' + booking.id +'&patient_class=FamilyMember'
          patient_url = '<a href="/family_members/'+booking.family_member_id
        }
        popup_content = '<form class="event_form" action="/bookings/'+booking.id+'" accept-charset="UTF-8" data-remote="true" method="post"><input type="hidden" name="_method" value="put" /><div class="row"> <div class="col-sm-6"> <p>Name : '+booking.patient_name+'</p> <p style="word-wrap:break-word;">Email: '+booking.patient_email+'</p> '+dob_tag+' <p>Phone: '+formatPhone(booking.patient_phone)+'</p> <p>Age: '+booking.patient_age+'</p> <p><label>Vision Insurance</label><select class="select optional vision-insurance-options form-control" style="width: 150px; overflow-x: hidden;" name="patient[vision_insurance_id]" id="patient_vision_insurance_id"></select> </p>        <p class="medical_insurance"> </p>       <p>Store: '+booking.store_name+'</p> </div> <div class="col-sm-6"> <p>Status: <select name="booking[appointment_status]" class="form-control appoint_status" style="margin-bottom: 10px"><option value="confirmed"> Confirmed </option><option value="unconfirmed"> Unconfirmed </option><option value="left_message"> Left Message </option><option value="other"> Other </option></select></p> <p><input type="text" name="other_status" id="other_status" style="display:none" /></p> <p>Provider: <select name="booking[doctor_id]" class="form-control popover-provider" style="margin-bottom: 10px">'+booking.store_doctor+'</select></p> <p><div class="form-group select optional booking_exam_procedures"> <label>Reason Of Visit</label><input name="booking[exam_procedure_ids][]" type="hidden" value=""><select multiple="multiple" class="select optional multi-select-exams form-control" style="width: 200px; overflow-x: hidden; display: none;" name="booking[exam_procedure_ids][]" data-value= "'+booking.procedure_ids+'" id="booking_exam_procedure_ids"></select></div></p> <p>Comments: <input type="string" name="booking[comment]" value= "'+booking.comment+'" class="form-control"></p><div class="pull-right"><ul class="dropdown"> <li class="btn-group" style="width: 170px;" id="actionsliststyle"> <button type="submit" class="btn btn-primary btn-sm pull-right"> Update</button><a class="btn btn-sm default dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-cogs"></i>Actions<i class="fa fa-angle-down"></i></a> <ul class="dropdown-menu pull-right"> <li>'+ patient_url +'" target="_blank">View Profile </a></li><li>'+ reconfirm_url +'">Reconfirm</a></li> <li><a class="delete-appointment-btn" data-bookingid='+booking.id+'>Cancel Appointment</a></li>  </ul> </li> </ul></div></div> </div></form>';
      }
      else
      {
        title= "Patient Detail";
        popup_content = '<div style="width: 200px"><div>Name: '+booking.patient_name+'</div> <div>Email: '+booking.patient_email+'</div> <div>Phone: '+booking.patient_phone+'</div>';
      }

	  
      var set_popover_position = function(){
        var position;
        if((view.name == "month") || (view.name == "agendaWeek"))
        {
          day_name = moment(booking.start).format('ddd')
          if(day_name == 'Wed' || day_name == 'Thu' || day_name == 'Fri' || day_name == 'Sat')
            position = 'left';
        }
        else
          position = booking.start.getHours()>12?'top':'bottom';

        return position;
      }
      
      element.popover({
        title: "Date: " + moment(booking.start).format("MM-DD-YYYY") + " Time: " + moment(booking.start).format("hh:mm a") + '<button id="popovercloseid" type="button" class="close" style >&times;</button>',
        placement: set_popover_position,
        html:true,
        content: popup_content,
        container:'body'
      }).on('shown.bs.popover', function() {
        $.getJSON("/bookings/medical_insurance_select", function(data) {
          if (data.show == undefined){
            $(".medical_insurance").html('<label>Medical Insurance</label><select class="select optional medical-insurance-options form-control" style="width: 150px; overflow-x: hidden;" name="patient[medical_insurance_id]" id="patient_medical_insurance_id"></select>');
            $(".medical-insurance-options").html('');
            $.each(data, function(index, item) { 
              $(".medical-insurance-options").append($('<option/>', { 
                  value: item.id,
                  text : item.label 
              }));
            });
          }
        });
        $.getJSON("/bookings/doctor_list_select?store_id="+ store_id, function(data) { 
          $(".doctor-list-options").html('');
          $.each(data, function(index, item) { 
            $(".doctor-list-options").append($('<option/>', { 
                value: item.id,
                text : item.label 
            }));
          });
        });
        $.getJSON("/bookings/multiselect_procedure_select", function(data) { 
          $.each(data, function(index, item) { 
            $(".multi-select-exams").append($('<option/>', { 
                value: item.id,
                text : item.label 
            }));            
          });
        }).done(function() {
            var selected_value = $(".multi-select-exams").attr("data-value");    
            if (typeof(selected_value) != "undefined")
              var dataarray=selected_value.split(",");
            $('.multi-select-exams').val(dataarray);
            $('.multi-select-exams').multiselect({
              enableFiltering: true,
              includeSelectAllOption: true,
              maxHeight: '300',
              buttonWidth: '170',
              templates: {
                filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default multiselect-clear-filter" type="button"><b>X<b></button></span>',
                saveBtn: '<span class="input-group-btn"><button type="button" class="btn btn-default"><b>√<b></button></span>'
              }
            }); 
            $('.dropdown-toggle').dropdown();  
          })
        

        $.getJSON("/bookings/vision_insurance_select", function(data) { 
          $(".vision-insurance-options").html('');
          $.each(data, function(index, item) { 
            $(".vision-insurance-options").append($('<option/>', { 
                value: item.id,
                text : item.label 
            }));
          });
        });
        telInput();
        $('.popover-provider').val(booking.doctor_id);
        if($.inArray(booking.appointment_status, ['confirmed','unconfirmed', 'left_message', '']) == -1)
        {
          $('#other_status').show();
          $('.appoint_status').val('other');
          if (booking.appointment_status != "other")
          {
            $('#other_status').val(booking.appointment_status);
          }
        }
        else
        {
          $('.appoint_status').val(booking.appointment_status);
        }
        $(this).parent().parent().parent().css({"overflow-y": "hidden", "border-right": "11px rgb(238, 238, 238) solid"});
      }).on('hidden.bs.popover', function(){
        $(this).parent().parent().parent().css({"overflow-y": "auto", "border-right": "none"});
      });

      /* popup close functionality -----start-------- */
      $(document).on('click', '.fc-event', function (e) {     
        $('.popover').remove();
        $(this).popover('show');
      });

      $(document).on('click', '.fc-year-day', function (e) {
        /*$('.fc-year-day').not(this).popover('hide');*/
        $('.popover').remove();
        $(this).popover('show')  
      });

      $(document).on('click', '.fc-day, #popovercloseid', function (e) {
        $('.popover').remove();
      });

      $(document).on('click', '#popovercloseid', function (e) {
        $('.popover').remove();
        $(".fc-event").parent().parent().parent().css({"overflow-y": "auto", "border-right": "none"});

      });

      $(document).on('change', '.appoint_status', function (e) {
        e.preventDefault();
        if ($(this).val() == "other")
        {
          $('#other_status').show();
        }
        else
        {
          $('#other_status').hide();
        }
      });
      /* popup close functionality -----end-------- */



    }
    ,
    eventAfterRender: function(booking, element, view) {
      if (view.name === "agendaDay" && booking.id != undefined) {
        $(element).css('width','700px');
      }
    }
    ,
    eventAfterAllRender: function (view) {
     if (view.name == 'month') {
        $('td.fc-day').each(function () {
          var EventCount = $('.' + $(this).attr('data-date')).length;
          if (EventCount == 0)
            $(this).find('.viewMore').remove();
        });
     }
      /* holiday render*/
      holiday_render(view);
      
      set_working_schedule(doctorAvailablePeriods, $("#calendar"));
      if (view.name == 'agendaDay') {
        $(".unavailable").parent().parent().parent().css("pointer-events", "none")
        $('.fc-widget-content').hover(function(){
          $(this).children().first().addClass('fa fa-plus');
          $(this).children().first().css('float','right');
          $('.fc-agenda-divider').next().css('overflow-y','hidden')
        },
        function(){
          $(this).children().first().removeClass('fa fa-plus');
          $('.fc-agenda-divider').next().css('overflow-y','auto')
        })
      }
    },


    select: function(start, end, allDay) {
      if (window.location.href.match(/new/)) {
        if(!(view_name=="store" || view_name=="all") && !(doctor_id === undefined || doctor_id === null) ) {
          var length = (end-start)/(3600000)*60;
          var last_undefined_event_id = "";
          last_undefined_event_id  = $(".closeon").first().attr('data-eventid');
          if (last_undefined_event_id  !== undefined) {
            $('#calendar').fullCalendar('removeEvents', last_undefined_event_id);
            $('.popover').remove();
          }
          $('#calendar').fullCalendar('renderEvent', 
            {
              start: start,
              end: end,
              allDay: false
            }
          );
        } 
        else 
        {
          alert("Warning! You are trying to create Booking, Please select the doctor first!");
        }
      }
    },
    viewDisplay: function(view){
      
    }

  });

   function adjustWorkHourSlotSize(day_num) {
      $(".day"+day_num+"slot").width($(".fc-col"+day_num).width()-2);

   }
   function set_working_schedule(availablePeriods, calendar_element) {
    if (availablePeriods !== undefined) {
        numberOfAvailablePeriods =  availablePeriods.length;

        //slots.addClass('nySchedule_unavailable_slots'); 
        //iterate trough days and get avail periods for each day of week
        currentView = calendar_element.fullCalendar('getView');
        currentView =  currentView.name;
        if (currentView === 'agendaWeek' || currentView === 'agendaDay') {



          scheduleStartTime = timeToFloat(calendar_element.fullCalendar( 'option', 'minTime'));            
          scheduleSlotSize = calendar_element.fullCalendar( 'option', 'slotMinutes') /60;
          /* function to calculate slotindex for a certain time (e.g. '8:00') */    
          getSlotIndex = function(time) {
            time = timeToFloat(time);            
            return Math.round((time-scheduleStartTime)/scheduleSlotSize);
          }
          $(".dayslot").remove()
          slots_content = calendar_element.find('.fc-agenda-slots tr .fc-widget-content div');
          for (var i=0; i!=numberOfAvailablePeriods; i++) {
            if (currentView === 'agendaWeek') {
              slots_content.append("<div class='day"+i+"slot dayslot'></div>");
              $(".day"+i+"slot").addClass('unavailable');
              adjustWorkHourSlotSize(i);
            }
            var current_day = $("#calendar").fullCalendar('getDate').getDay() ;
            if(currentView === 'agendaDay' && current_day == i)
            {
              slots_content.html("<div class='day"+i+"slot dayslot'></div>");
              $(".day"+i+"slot").addClass('unavailable');
            }

            
            dayPeriodsLength=availablePeriods[i].length;
            for (var j=0; j!=dayPeriodsLength; j++) {
              start=moment(availablePeriods[i][j][0]).format('HH:mm');
              end=moment(availablePeriods[i][j][1]).format('HH:mm');

              startOfPeriodSlot = getSlotIndex(timeToFloat(start));
              endOfPeriodSlot = getSlotIndex(timeToFloat(end));
              for (k=startOfPeriodSlot; k<endOfPeriodSlot; k++) {
                $(".day"+i+"slot").eq(k).removeClass("unavailable");
              }
            }                
          }
        }
    }
   }
  function timeToFloat(time) {      
    var returnValue, timeAsArray, separator, i, timeSeparators = [':', '.'], numberOfSeparators;

    /* is time an integer or a float? */
    if (parseInt(time, 10) === time || parseFloat(time) === time) {
      returnValue = time;
    } else {
      /* time will be considered a string, parse it */
      time = (typeof time != 'undefined') ? time.toString() : "";

      numberOfSeparators = timeSeparators.length;

      for (i = 0; i < numberOfSeparators; i = i + 1) {
        separator = timeSeparators[i];

        if (time.indexOf(separator) > 0) {
          timeAsArray = time.split(separator);

          returnValue = parseInt(timeAsArray[0], 10) + parseInt(timeAsArray[1], 10) / 60;

          /* does string contain 'p' or 'pm'? */
          if (time.indexOf('p') > 0 && returnValue <= 12) {
            returnValue = returnValue + 12;
          }
        }
      }
    }
    return returnValue;
  }


};
