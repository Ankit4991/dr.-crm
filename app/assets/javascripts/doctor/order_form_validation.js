/* lens suborder form validaton --------start----------*/

function lensorderFormValidation(element_id){
    var form = $('#'+element_id);
    var error = $('.alert-danger', form);
    var success = $('.alert-success', form);
    form.validate({
        doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true,
        onfocusout: function(e) {
          this.element(e);
        },
        invalidHandler: function (event, validator) { //display error alert on form submit
            success.hide();
            error.show();
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            
            label
                .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
        }
    });
}

/* lens suborder form validaton --------end----------*/

/* glass suborder form validaton --------start----------*/
function glassorderFormValidation(element_id){
    var form = $('#'+element_id);
    var error = $('.alert-danger', form);
    var success = $('.alert-success', form);
    form.validate({
        doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true,
        onfocusout: function(e) {
          this.element(e);
        },
        invalidHandler: function (event, validator) { //display error alert on form submit
            success.hide();
            error.show();
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            
            label
                .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
        }
    });
}

/* glass suborder form validaton --------end----------*/

/* initializing all contact lens order forms validators ---------------start ----------*/
function InitializeLensFormsValidator(){
    var lens_forms = $('.lens_suborder_form');
    $.each(lens_forms,function(i,element){
      lensorderFormValidation(element.id);
    });
}
/* initializing all contact lens order forms validators ----------------end -----------*/

/* initializing all eye glass order forms validators -----------------start -----------*/
function InitializeGlassFormsValidator(){
    var glass_forms = $('.glass_suborder_form');
    $.each(glass_forms,function(i,element){
      glassorderFormValidation(element.id);
    });
}
/* initializing all eye glass order forms validators ----------------- end ------------*/