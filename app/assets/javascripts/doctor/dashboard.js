// -------- patient_log page start  -------------
function dashboardsPatientLogPage(formatted_date){
  // for yadcf filter UI changes
 

  // function yadcfAddBootstrapClass() {
  //   var filterWrapper = $('.yadcf-filter-wrapper'),
  //       filterInput = $('input.yadcf-filter, input.yadcf-filter-range, input.yadcf-filter-date'),
  //       filterSelect = $('select.yadcf-filter'),
  //       filterReset = $('input.yadcf-filter-reset-button');

  //   if ($('.table-filter').hasClass('js-table-filter-group')) {
  //       filterWrapper.addClass('input-group');
  //       filterInput.addClass('form-control');
  //       filterSelect.addClass('form-control');
  //       filterReset.addClass('btn btn-default').wrap("<span class='input-group-btn'></span>");
  //   } else {
  //       filterInput.addClass('form-control');
  //       filterSelect.addClass('form-control');
  //       filterReset.addClass('btn btn-default').wrap("<span></span>");
  //   }
  // }
  
  $('.multi-select-exams').multiselect({
      enableFiltering: true,
      includeSelectAllOption: true,
      maxHeight: '300',
      buttonWidth: '256',
      templates: {
        filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default multiselect-clear-filter" type="button"><b>X<b></button></span>',
        saveBtn: '<span class="input-group-btn"><button type="button" class="btn btn-default"><b>√<b></button></span>'
      }
  });
  
  // for datatable init along with default options
    // $('.dropdown-toggle').dropdown();
    // $('#example1').dataTable({
    //   bSort: true,
    //   bJQueryUI: true,
    //   "sDom": 'R<C><"#ShowToday"><"#DefaultView">H<"clear"><"ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix"lfr>t<"ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix"ip>',
    //   bDestroy: true,
    //   "fnRowCallback": function (nRow, aData, iDisplayIndex) {
    //       // Bind click event
    //       $('td:gt(0):lt(5)',nRow).click(function(){ console.log('td with header actions not bind to event');
    //       //$(nRow).click(function() {
    //              $.ajax({ 
    //                     type: "GET", 
    //                     url: '/doctor/patient/info?jr_id='+aData[7],
    //                     dataType: 'text',
    //                     success: function(data){ 
    //                       arrinfo = $.parseJSON(data);
    //                       var patientid;
    //                       htmlData = function(){ var list = '<div>';
    //                                    $.each(arrinfo,function(key,valarry){ 
    //                                       var infolist = "<div class='logtb' style='margin-left: 20%;'>";
    //                                       list += '<h5><b>'+valarry[0]+': </b>';
    //                                       $.each(valarry[1],function(q,w){ 
    //                                         if(valarry[0]=='Personal'){ patientid = valarry[1][0][1];}
    //                                         infolist += '<h5>'+w[0]+':<span style=\'margin-right: 10%;float: right;\'>'+w[1]+'</span></h5>';
    //                                       });
    //                                       list+= infolist+'</div></h5>'; 
    //                                    }); 
    //                                    list += '</div>';
    //                                    return list;
    //                                  },
    //                       $('#mymodal').find('.modal-body').html(htmlData);
    //                       $('#mymodal').find('.modal-footer').html("<button class='btn btn-default' data-dismiss='modal' type='button'>Close</button> <a href='/doctor/patient/"+patientid+"' class='btn btn-primary'>Edit</a>");
    //                       $('#mymodal').modal('show');
    //                     }
    //                   });
    //           return false;
    //       });
  
    //       return nRow;
    //   },
    //   "fnDrawCallback": function (oSettings) {
    //                       // $("#actionsliststyle > a").addClass("btn btn-sm yellow");
    //                       $(".fa-shopping-cart").addClass("fa");
    //                       $(".fa-send").addClass("fa");
    //                       $(".fa-user").addClass("fa");
    //                       $(".fa-edit").addClass("fa");
    //                       $(".fa-thumbs-o-up").addClass("fa");
    //                     },
    //   "order": [[ 5, "desc" ]],
    //   aoColumnDefs:  [ { sTitle: "Name" ,aTargets: [0],bSortable: false,sWidth: "10%" }, { sTitle: "Birthdate", aTargets: [1],bSortable: false,sWidth: "10%" }, { sTitle: "Email" ,aTargets: [2],bSortable: false,sWidth: "10%"},{ sTitle: "Phone" ,aTargets: [3],bSortable: false,sWidth: "10%"},{sTitle: "Last Payment" ,aTargets: [4],bSortable: false,sWidth: "10%"},{ sTitle: "Latest Entry" ,aTargets: [5],bSortable: true,sWidth: "10%"},{ sTitle: "Actions" ,aTargets: [6],bSortable: false,sWidth: "10%"}, { sTitle: "Id" ,aTargets: [7],bSortable: false,sWidth: "10%"} ],
    //   aoColumns: [ { aTargets: [0],sWidth: "10%" }, { aTargets: [1],sWidth: "10%" }, { aTargets: [2],sWidth: "10%"},{ aTargets: [3],sWidth: "10%"},{ aTargets: [4],sWidth: "10%"},{ aTargets: [5],bSortable: true,sWidth: "10%"},{aTargets: [6],sWidth: "10%"} ],
    //   sPaginationType: "full_numbers"
    // }).yadcf([
    //   {column_number : 0,filter_type: "text",filter_reset_button_text: false},
    //   {column_number : 1, filter_type: "text",filter_reset_button_text: false},
    //   {column_number : 2, filter_type: "text",filter_reset_button_text: false},
    //   {column_number : 3, filter_type: "text",filter_reset_button_text: false},
    //   {column_number : 5, filter_type: "range_date",datepicker_type: 'jquery-ui',date_format: 'mm/dd/yyyy', filter_reset_button_text: false,externally_triggered: true},
    //   ],{cumulative_filtering : true });
  
    // $('input.booking-date.datepicker').data({behaviour: "datepicker"}).datepicker().css("z-index","2");
    // $('input.booking-date.timepicker').datetimepicker({ format: 'HH:mm:ss'});
    // yadcfAddBootstrapClass();
   
    // var DefaultView = $("#DefaultView").html("<a class='defview btn btn-sm default'><i class='fa fa-refresh'></i>&nbsp;&nbsp;Default View</a>");
    // var ShowToday = $("#ShowToday").html("<a class='stday btn btn-sm default'><i class='fa fa-clock-o'></i>&nbsp;&nbsp;Show Today</a>");
    // $('#DefaultView').hide();
  
   // --------- end of datatable initialization ------------
  
   // -------  for new exam & order log action --------------
  $(document).on('click','.exam-order-booking',function(){
    $('.dropdown-toggle').dropdown();
    $('#exam-order-booking-modal').modal('show');
    // var doctor_id = $(this).data('doctor-id');
    var join_id = $(this).data('join-id');
    var vision_insurance_id = $(this).data('vision-insurance-id');
    $('#booking_joint_resource_id').val(join_id);
    $('.vision_insurance_select').val(vision_insurance_id);
    $('#only_exam').val("false");
    var patient_name  = $(this).data('name').replace(/[^a-z0-9\s]/gi, '');
    $('#user-name').html("<h3><i class='fa fa-user'></i>&nbsp;&nbsp;"+patient_name+"&nbsp;<small>Create Exam<small></h3>");
    return false;
  });
  
  //  only exam booking -----start ---------------------------
  $(document).on('click','.exam-booking',function(){
    $('.dropdown-toggle').dropdown();
    $('#exam-order-booking-modal').modal('show');
    // var doctor_id = $(this).data('doctor-id');
    var join_id = $(this).data('join-id');
    var vision_insurance_id = $(this).data('vision-insurance-id');
    $('#booking_joint_resource_id').val(join_id);
    $('.vision_insurance_select').val(vision_insurance_id);
    $('#only_exam').val("true");
    var patient_name  = $(this).data('name').replace(/[^a-z0-9\s]/gi, '');
    $('#user-name').html("<h3><i class='fa fa-user'></i>&nbsp;&nbsp;"+patient_name+"&nbsp;<small>New Exam Booking<small></h3>");
    return false;
  });
  // only exam booking ---------end --------------------------
  
  $("#new_booking").bind('ajax:complete', function () {
    $('.dropdown-toggle').dropdown();
    $('#exam-order-booking-modal').modal('hide');
    $('.spinner').hide();
  });
  
  // To remove content from date filters on choosing default view
  $(".defview").on("click",function(){
      $("#yadcf-filter--example1-from-date-5").val("");
      $("#yadcf-filter--example1-to-date-5").val("");
      yadcf.exFilterExternallyTriggered($('#example1').dataTable());
      $('#DefaultView').animate({width: '0'}, 1000, function(){
        $(this).hide();
      });
  });
  
  // To add current date to date column filters on choosing show today option
  $(".stday").on("click",function(){
      $("#yadcf-filter--example1-from-date-5").val(formatted_date);
      $("#yadcf-filter--example1-to-date-5").val(formatted_date);
      yadcf.exFilterExternallyTriggered($('#example1').dataTable());
      $('#DefaultView').animate({width: '0'}, 1000, function(){
        $(this).show();
      });
  });
  
  $(document).on('change','#yadcf-filter--example1-from-date-5,#yadcf-filter--example1-to-date-5',function(){
    yadcf.exFilterExternallyTriggered($('#example1').dataTable());
  });
  
  // To list patient follow ups in modal box
  $(document).on('click','.followups',function(){
    var patient_id = $(this).data('id');
    var patient_name  = $(this).data('name');
    $.ajax({ 
           type: "GET",
           url: "/follow_ups",
           data: 'id='+patient_id,
           dataType: "JSON",
           async: false
           }).success(function(data){
               $('#followup-modal').find('.modal-header').html("<h3><i class='fa fa-user'></i>&nbsp;&nbsp;"+patient_name+"&nbsp;Follow up Log</h3>");
               $('#followup-modal').find('.modal-body').html(data.html);
               $('#followup-modal').find('.modal-footer').html("<a class='btn btn-default' data-dismiss='modal'>Close</a> <a href=# data-id="+patient_id+" data-name="+patient_name+" class='add_new_follow_up btn btn-primary'>Add New</a>");
               $('#followup-modal').modal('show');
          });
  });
  
  // To add new follow up log through modal box
  $(document).on('click','.add_new_follow_up',function(){
    var patient_id = $(this).data('id');
    var patient_name  = $(this).data('name');
    $.ajax({ 
           type: "GET",
           url: "/follow_ups/new",
           data: 'id='+patient_id,
           dataType: "JSON",
           async: true
           }).success(function(data){
              if(data.status == "Error"){
                $('#followup-modal').modal('hide');
                jQuery.gritter.add({ title: data.status, text: data.msg });
              } else { 
                $('#followup-modal').find('.modal-header').html("<h3><i class='fa fa-user'></i>&nbsp;&nbsp;"+patient_name+"&nbsp;Follow up Log</h3>");
                $('#followup-modal').find('.modal-body').html(data.html);
                $('#followup-modal').find('.modal-footer').html("<a class='btn btn-default' data-dismiss='modal'>Close</a> <a href=# data-id="+patient_id+" data-name="+patient_name+" class='save_follow_up btn btn-primary'>Save</a>");
                $('#followup-modal').modal('show');
              }
          });
  });
  
  // To save the newly added follow up log
  $(document).on('click','.save_follow_up',function(){
    var myform = $('#new_follow_up').serialize();
    var patient_id = $(this).data('id');
    var patient_name  = $(this).data('name');
    $.ajax({ 
           type: "POST",
           url: "/follow_ups",
           data: myform,
           dataType: "JSON"
           }).success(function(data){
             var element;
             $('.followups').each(function(){ if($(this).data('id')== patient_id ){ element=$(this) } });
             if( typeof element != "undefined" ) { element.trigger('click');}
          }).error(function(data){
             $('#error_explanation').html(data.responseJSON.error);
             $('#error_explanation').show();
             return false;
          });
  });
  
  // To show default view option on changing column filter content
  $(document).on('change','.text_filter',function(e){
    e.stopPropagation();
    $('#DefaultView').show();
  });

}
// -------- patient_log page end -------------


// One click page js ----------- Start -------------------
  
// One click page js ----------- End -------------------

// -------- dashboards index page start  -------------
function dashboardsIndexPage(managePatientFirebaseRef,current_doctor,doctorDashboardFirebaseRef){

  // var callback_change_firebase = function(childSnapshot,event){

  //   $.ajax({ 
  //             type: "GET", 
  //             url: "/dashboards/todays_appointments",
  //             success: function(data){
  //           }
  //   });
  // }

  // var callback_added_firebase = function(childSnapshot,event){

  //   $.ajax({ 
  //             type: "GET", 
  //             url: "/dashboards/todays_appointments",
  //             success: function(data){
  //           }
  //   });
  
  // }


  //managePatientFirebaseRef.on('child_changed', callback_change_firebase);
  //managePatientFirebaseRef.on("child_added", callback_added_firebase);

  //var initial_load = true
  
  doctorDashboardFirebaseRef.on('child_changed', function(childSnapshot,event){
  
    //if(initial_load === false){
      $.ajax({ 
              type: "GET", 
              dataType: 'script',
              url: "/dashboards/todays_appointments",
              success: function(data){
            }
      });
    //}
    //initial_load = false
  });
  
  doctorDashboardFirebaseRef.on("child_added", function(childSnapshot,event){
    
    //if(initial_load === false){
      $.ajax({ 
              type: "GET", 
              dataType: 'script',
              url: "/dashboards/todays_appointments",
              success: function(data){
            }
      });
    //}
    //initial_load = false
  });
 
  $(document).on('click','.booking_cancel',function(event){
    event.preventDefault();
    var booking_id = $(this).data('booking-id');
    $.ajax({ 
              type: "DELETE",
              dataType: 'JSON',
              url: $(this).data('url'),
              success: function(data){ 
                 //location.reload()
                 jQuery.gritter.add({ title: "Success", text: "Booking has been canceled successfully" });
              }
    });
    
    $.ajax({ 
                type: "GET", 
                dataType: 'script',
                url: "/dashboards/todays_appointments",
                success: function(data){
              }
      });
      var new_user_hash = {};    
      new_user_hash[booking_id] = $.now();
      doctorDashboardFirebaseRef.child('booking').transaction(function(data){ return $.extend(data, new_user_hash)  });

    });

  $(document).on('click','.booking_checkin',function(event){
    event.preventDefault();
    var booking_id = $(this).data('booking-id');
    $.ajax({ 
              type: "GET",
              dataType: 'JSON',
              url: $(this).data('url'),
              success: function(data){ 
                 //location.reload()
                 jQuery.gritter.add({ title: "Success", text: "Patient is Checked In successfully" });
              }
    });


    
    //managePatientFirebaseRef.child('booking').transaction(function(data){ return $.extend(data, new_user_hash)  });

    //location.reload()
    $.ajax({ 
              type: "GET", 
              dataType: 'script',
              url: "/dashboards/todays_appointments",
              success: function(data){
            }
    });
    var new_user_hash = {};    
    new_user_hash[booking_id] = $.now();
    doctorDashboardFirebaseRef.child('booking').transaction(function(data){ return $.extend(data, new_user_hash)  });

  });

  $(window).on('beforeunload', function(){
    doctorDashboardFirebaseRef.remove();
  });
}

/*----Below functions called inside /doctor/dashboards/dashboard_partial/_overview.html.haml----start----*/

  function load_exams_tab(){
    $(document).on('click', "#load_exam_tab", function(){
      $('.spinner').show();
      exam_tab = '/dashboards/exam_section';
      $('#exam_tab_1_33').load(exam_tab, function( response, status, xhr ) {
        $('.spinner').hide();
        $('.exam_audits_popup').bind('ajax:complete', function () {
          $('.spinner').hide();
        });
        var oTable = $('#recentexams').dataTable({
          "sPaginationType": "full_numbers",
          "bSort": false,
          "fnDrawCallback":function(){
            if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1) {
              $('.dataTables_paginate').css("display", "block");  
              $('.dataTables_length').css("display", "block");
              $('.dataTables_filter').css("display", "block");  
            } 
            else {
              $('.dataTables_paginate').css("display", "none");
              $('.dataTables_length').css("display", "none");
            }
          }
        });
        $(function() {
          $(".pagination").on("click", 'a',function() {
          $(".pagination").html("Page is loading...");
          $.getScript(this.href);
          return false;
          });
        });
      });
    })
  }

  function load_orders_tab(){
    $(document).on('click', "#load_order_tab", function(){
      $('.spinner').show();
      order_tab = '/dashboards/order_section';
      $('#order_tab_1_44').load(order_tab, function( response, status, xhr ) {
        $('.spinner').hide();
        var oTable1 = $('#recentorders').dataTable({
          "bSort" : false,
          "sPaginationType": "full_numbers",
          "fnDrawCallback":function(){
            if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1) {
              $('.dataTables_paginate ').css("display", "block");  
              $('.dataTables_length').css("display", "block");
              $('.dataTables_filter').css("display", "block");  
            } 
            else {
              $('.dataTables_paginate').css("display", "none");
              $('.dataTables_length').css("display", "none");
            }
          }
        });
        $(function() {
          $(".pagination").on("click", 'a',function() {
          $(".pagination").html("Page is loading...");
          $.getScript(this.href);
          return false;
          });
        });
      });
    })
  }

  function load_dispenses_tab(){
    $(document).on('click', "#load_dispense_tab", function(){
      $('.spinner').show();
      dispense_tab = '/dashboards/dispense_section';
      $('#dispense_tab_1_66').load(dispense_tab, function( response, status, xhr ) {
        $('.spinner').hide();
      });
    })
  }

  function load_follow_up(){
    $(document).on('click', "#load_follow_tab", function(){
      $('.spinner').show();
      follow_tab = '/dashboards/follow_section';
      $('#follow_tab_1_55').load(follow_tab, function( response, status, xhr ) {
        $('.spinner').hide();
        var oTable1 = $('#followups_tab').dataTable({
          "sPaginationType": "full_numbers",
          "bSort": false,
          "fnDrawCallback":function(){
            if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1) {
              $('.dataTables_paginate').css("display", "block");  
              $('.dataTables_length').css("display", "block");
              $('.dataTables_filter').css("display", "block");  
            } 
            else {
              $('.dataTables_paginate').css("display", "none");
              $('.dataTables_length').css("display", "none");
            }
          }
        });
        $(function() {
          $(".pagination").on("click", 'a',function() {
          $(".pagination").html("Page is loading...");
          $.getScript(this.href);
          return false;
          });
        });
      });
    })
  }
  
/*----Below functions called inside /doctor/dashboards/dashboard_partial/_overview.html.haml----end----*/

/*----Below function called inside _patients_listing.html.haml used at patient log page----start----*/
function paginate_records(){
  $(".pagination").on("click", 'a',function() {
    $(".pagination").html("Page is loading...");
    $.getScript(this.href);
    return false;
  });
}
/*----Below function called inside _patients_listing.html.haml used at patient log page----end----*/
var element ;
  function assign_element(el){
    element = el;
  }

function remove_element(){
  $(element).parent().find('form').submit(); 
  $('.spinner').hide(); 
  $(element).closest('tr').first().remove();
}
