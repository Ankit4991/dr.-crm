/* contact form validaton --------start----------*/


function contactFormValidation(){
    var form = $('#new_contact_form');
    var error = $('.alert-danger', form);
    var success = $('.alert-success', form);
    form.validate({
        doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        rules: {
            //payment
            'contact_form[name]': {
                required: true
            },
            'contact_form[email]': {
                required: true
            },
            'contact_form[phone_number]': {           
                required: true
            },
            'contact_form[comment]': {
                required: true,
            },
            'contact_form[exixting]': {
                required: true
            }
        },

        messages: { // custom messages for radio buttons and checkboxes
            'payment[]': {
                required: "Please select at least one option",
                minlength: jQuery.validator.format("Please select at least one option")
            }
        },    

        invalidHandler: function (event, validator) { //display error alert on form submit
            success.hide();
            error.show();
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            
            label
                .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
        
        },

        submitHandler: function (form) {
            success.show();
            error.hide();
            form.submit()
            //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
        }

    });
}

/* contact form validaton --------end----------*/