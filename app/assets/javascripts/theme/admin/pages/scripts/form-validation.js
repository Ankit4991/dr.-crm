// custom code by enbake for continue button validation  --------start------
$(document).on("click", "#button-payment-address-continue", function() {
    var flag = true;
    $("#payment-address-content input, #payment-address-content select").each(function()
    {   
        if(this.value == "")
        {
            flag = false;
            $(this).parent().addClass('has-error');
        }
        else
        {
            $(this).parent().removeClass('has-error');
        }
    })
    if(flag == false)
        return false;
    else 
        $("#button-payment-address").trigger("click");
})

$(document).on("click", "#button-shipping-address-continue", function() {
    var flag = true;
    $("#shipping-address-content  input, #shipping-address-content  select").each(function()
    {   
        if(this.value == "")
        {
            flag = false;
            $(this).parent().addClass('has-error');
        }
        else
        {
            $(this).parent().removeClass('has-error');
        }
    })
    if(flag == false)
        return false;
    else 
        $("#button-shipping-address").trigger("click");
})

$(document).on("click", "#button-confirm", function() {
    var flag = true;
    $("#confirm-content  input, #confirm-content  select").each(function()
    {   
        if(this.value == "")
        {
            flag = false;
            $(this).parent().addClass('has-error');
        }
        else
        {
            $(this).parent().removeClass('has-error');
        }
    })
    if(flag == false)
        return false;
});

$(document).on("click", "#payment_method_update_submit_btn", function() {
    var flag = true;
    $("#confirm-content  input, #confirm-content  select,#confirm-content  checkbox").each(function()
    {   
        if(this.value == "")
        {
            flag = false;
            $(this).parent().addClass('has-error');
        }
        else
        {
            $(this).parent().removeClass('has-error');
        }
    });
    if(flag == false)
        return false;
});

function creditCardValidation(){
var form = $('#card_detail_form');
var error = $('.alert-danger', form);
var success = $('.alert-success', form);
form.validate({
    doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
    errorClass: 'help-block help-block-error', // default input error message class
    focusInvalid: false, // do not focus the last invalid input
    rules: {
        //payment
        'creditcard[first_name]': {
            required: true
        },
        'creditcard[last_name]': {
            required: true
        },
        'creditcard[number]': {
            minlength: 15,
            maxlength: 16,
            required: true
        },
        'creditcard[verification_value]': {
            digits: true,
            required: true,
            minlength: 3,
            maxlength: 4
        },
        'creditcard[month]': {
            required: true
        },
        'creditcard[year]': {
            required: true
        },
        'payment[]': {
            required: true,
            minlength: 1
        }
    },

    messages: { // custom messages for radio buttons and checkboxes
        'payment[]': {
            required: "Please select at least one option",
            minlength: jQuery.validator.format("Please select at least one option")
        }
    },

    errorPlacement: function (error, element) { // render error placement for each input type
        if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
            error.insertAfter("#form_gender_error");
        } else if (element.attr("name") == "payment[]") { // for uniform radio buttons, insert the after the given container
            error.insertAfter("#form_payment_error");
        } else {
            error.insertAfter(element); // for other inputs, just perform default behavior
        }
    },

    invalidHandler: function (event, validator) { //display error alert on form submit
        success.hide();
        error.show();
        Metronic.scrollTo(error, -200);
    },

    highlight: function (element) { // hightlight error inputs
        $(element)
            .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
    },

    unhighlight: function (element) { // revert the change done by hightlight
        $(element)
            .closest('.form-group').removeClass('has-error'); // set error class to the control group
    },

    success: function (label) {
        if (label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
            label
                .closest('.form-group').removeClass('has-error').addClass('has-success');
            label.remove(); // remove error label here
        } else { // display success icon for other inputs
            label
                .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
        }
    },

    submitHandler: function (form) {
        $('.spinner').show();
        success.show();
        error.hide();
        var url = form.action; // the script where you handle the form input.

            $.ajax({
               type: "POST",
               url: url,
               data: $(form).serialize(), // serializes the form's elements.
               // success: function(data)
               // {    
               //      $('.spinner').hide();
               //      if(data.status == 'Failure')
               //      {
               //          $('.auth_message').addClass('btn-warning').html("<i class='fa fa-exclamation-triangle' ></i> Card Not Authorized !") 
               //          if(typeof data.status != "undefined"){ 
               //            jQuery.gritter.add({ title: data.status, text: data.msg });
               //          }
               //      }
               //      else
               //      {  
               //          $('#change_credit_card_details_tab_2-6').html(data);
               //      }
               // }, error: {

               // }
               dataType: 'json',
               success: function(data) {
                    if(data.status == "error"){
                        if ($("#add_profile_card_modal").hasClass('in') == true || $("#registered-payment-method").hasClass('in') == true){  
                          $('#add_profile_card_modal').modal('hide');
                          $("#registered-payment-method").modal('hide');
                          $('.order_complete_options').click();
                        }
                        jQuery.gritter.add({ title: data.status, text: data.msg });
                    }
                    else
                       {
                         if ($("#add_profile_card_modal").hasClass('in') == true)
                          {
                            setTimeout(function(){
                              $('#add_profile_card_modal').modal('hide');
                              $("#registered-payment-method").modal('hide');
                              $('.order_complete_options').click();
                            },3000);    
                          }
                       }
                },
                error: function(data){
                   if ($("#add_profile_card_modal").hasClass('in') == true || $("#registered-payment-method").hasClass('in') == true){  
                     $('#add_profile_card_modal').modal('hide');
                     $("#registered-payment-method").modal('hide');
                   }
                   jQuery.gritter.add({ title: data.responseJSON.status, text: data.responseJSON.msg });
               }
            });

        return false
        //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
    }

});
}