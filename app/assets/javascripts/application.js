// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require raphael-min
//= require raphael.sketchpad
//= require jquery_ujs
//= require autocomplete-rails
//= require cocoon
//= require jquery-ui
//= require moment
//= require gritter

//= require twitter/bootstrap

//= require bootstrap-multiselect
//= require pickers
//= require jquery.dataTables.yadcf.js
//= require jquery.dataTables.js
//= require common
//= require calendar-editable
//= require bootstrap-slider
//= require star-rating

//= require_tree ../javascripts/theme/global/plugins
//= require_tree ../javascripts/theme/global/scripts
//= require_tree ../javascripts/theme/admin
//= require patient/patient_registration_wizard
//= require doctor/organisation_registration_wizard
//= require contact_form_validation
//= require 'icheck'
//= require endless_page
//= require intlTelInput


// You may include any languages (optional)
// require moment/<your locale>

$(document).ready(function () {
  $(".alert").fadeOut(10000);
  telInput();

  $(".dropdown-toggle").click(function(){
    var dropmenu = $(this).next('.dropdown-menu');
    
    
    if(dropmenu.offset().top + dropmenu.outerHeight() > $(window).innerHeight() + $(window).scrollTop()){
        $(this).parent().addClass("dropup");
    }
    else{
        $(this).parent().removeClass("dropup");
    }
  });
});

  function telInput(){
	$(".tel-input").intlTelInput({
	  utilsScript: "/assets/libphonenumber/utils.js"
	});
	$(document).on("change",".tel-input",function() {
	  this.parentElement.nextElementSibling.value = $(this).intlTelInput("getNumber");
	});
	$(".tel-input").on("countrychange", function(e, countryData) {
	  this.parentElement.nextElementSibling.value = $(this).intlTelInput("getNumber");
	    });
	  }
	  
