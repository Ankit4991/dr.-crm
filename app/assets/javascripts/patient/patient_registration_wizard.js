var patientRegistrationFormWizard = function () {
    return {
        //main function to initiate the module
        init: function () {

            var form = $('#patient_registration_wizard_form');
            function submitForm()
            {
              $('.spinner').show();
              $(this).attr("disabled", "true");
              $.ajax({
                url: form.attr('action'),
                type: 'POST',
                data: form.serialize(),
                dataType: 'json',
                success: function(data) {
                  $('.spinner').hide();
                  if(data.status == "error")
                  {
                    $(".button-submit").removeAttr("disabled");
                    jQuery.gritter.add({ title: data.status, text: data.msg });   
                  }
                  else
                  {
                    if(data.redirect_to_root == true){
                      window.location = "/doctor/patient/logs"
                    }
                    else{
                      $("#form_wizard_2").html('<div align= "center" style= "height: 500px; width: 500px;"><center style="margin-top: 200px; font-size: 30px" class="text-success">'+data.msg+'</center></div>');
                    }
                  }
                },
                error: function(data){
                   $('.spinner').hide();
                   $(".button-submit").removeAttr("disabled");
                   jQuery.gritter.add({ title: data.status, text: data.msg });
                }
              });
            }

            $('#form_wizard_2 .button-submit-skip-payment').click(function () { 
              submitForm();
              return false;
            }).hide();


            if (!jQuery().bootstrapWizard) {
                return;
            }

            function format(state) {
                if (!state.id) return state.text; // optgroup
                return "<img class='flag' src='../../assets/global/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
            }

            $("#country_list1").select2({
                placeholder: "Select",
                allowClear: true,
                formatResult: format,
                formatSelection: format,
                escapeMarkup: function (m) {
                    return m;
                }
            });
            $("#country_list2").select2({
                placeholder: "Select",
                allowClear: true,
                formatResult: format,
                formatSelection: format,
                escapeMarkup: function (m) {
                    return m;
                }
            });
            
            var form = $('#patient_registration_wizard_form');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

            form.validate({
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    //account
                    'patient[first_name]': {
                        minlength: 2,
                        required: true
                    },
                    'patient[last_name]': {
                        minlength: 2,
                        required: true
                    },
                    'patient[password]': {
                        minlength: 8,
                        required: true
                    },
                    'patient[email]': {
                        required: true,
                        email: true
                    },
                    'patient[password_confirmation]': {
                        minlength: 8,
                        required: true,
                        equalTo: "#patient_password"
                    },
                    'patient[phone]': {
                        minlength: 10,
                        // maxlength: 10,
                        required: true
                    },
                    'patient[birthday]': {
                        required: true
                    },
                    'patient[vision_insurance_id]': {
                        required: true
                    },
                    'patient[home_address_attributes][address1]': {
                        required: true
                    },
                    'patient[home_address_attributes][city]': {
                        required: true
                    },
                    'patient[home_address_attributes][postal_code]': {
                        required: true
                    },
                    //payment
                    'creditcard[first_name]': {
                        required: true
                    },
                    'creditcard[last_name]': {
                        required: true
                    },
                    'creditcard[number]': {
                        minlength: 16,
                        maxlength: 16,
                        required: true
                    },
                    'creditcard[verification_value]': {
                        digits: true,
                        required: true,
                        minlength: 3,
                        maxlength: 4
                    },
                    'creditcard[month]': {
                        required: true
                    },
                    'creditcard[year]': {
                        required: true
                    },
                    'payment[]': {
                        required: true,
                        minlength: 1
                    }
                },

                messages: { // custom messages for radio buttons and checkboxes
                    'payment[]': {
                        required: "Please select at least one option",
                        minlength: jQuery.validator.format("Please select at least one option")
                    }
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_gender_error");
                    } else if (element.attr("name") == "payment[]") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_payment_error");
                    } else if (element.attr("name") == "patient[birthday]") {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success.hide();
                    error.show();
                    Metronic.scrollTo(error, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    if ($(element).attr("name") == "patient[birthday]"){
                        $(element)
                            .parent().parent().removeClass('has-success').addClass('has-error'); // set error class to the control group
                    } else {
                        $(element)
                            .parent().removeClass('has-success').addClass('has-error'); // set error class to the control group
                    }
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    if ($(element).attr("name") == "patient[birthday]"){
                        $(element)
                            .parent().parent().removeClass('has-error'); 
                    } else {
                        $(element)
                            .parent().removeClass('has-error'); // set error class to the control group
                    }
                },

                success: function (label) {
                    if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                        label
                            .closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid') // mark the current input as valid and display OK icon
                        .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },

                submitHandler: function (form) {
                    success.show();
                    error.hide();
                    //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
                }

            });

            var displayConfirm = function() {
                $('#tab_5 .form-control-static', form).each(function(){
                    var input = $('[name="'+$(this).attr("data-display")+'"]', form);
                    if (input.is(":radio")) {
                        input = $('[name="'+$(this).attr("data-display")+'"]:checked', form);
                    }
                    if (input.is(":text") || input.is("textarea")) {
                        $(this).html(input.val());
                    } else if (input.is("select")) {
                        $(this).html(input.find('option:selected').text());
                    } else if (input.is(":radio") && input.is(":checked")) {
                        $(this).html(input.attr("data-title"));
                    } else if ($(this).attr("data-display") == 'payment') {
                        var payment = [];
                        $('[name="payment[]"]:checked').each(function(){
                            payment.push($(this).attr('data-title'));
                        });
                        $(this).html(payment.join("<br>"));
                    }
                });
            };

            var handleTitle = function(tab, navigation, index) {
                var total = navigation.find('li').length;
                var current = index + 1;
                // set wizard title
                $('.step-title', $('#form_wizard_2')).text('Step ' + (index + 1) + ' of ' + total);
                // set done steps
                jQuery('li', $('#form_wizard_2')).removeClass("done");
                var li_list = navigation.find('li');
                for (var i = 0; i < index; i++) {
                    jQuery(li_list[i]).addClass("done");
                }

                if (current == 1) {
                    $('#form_wizard_2').find('.button-previous').hide();
                } else {
                    $('#form_wizard_2').find('.button-previous').show();
                }

                if (current >= total) {
                    $('#form_wizard_2').find('.button-next').hide();
                    $('#form_wizard_2').find('.button-submit').show();
                    $('#form_wizard_2').find('.button-submit-skip-payment').show();
                    displayConfirm();
                } else {
                    $('#form_wizard_2').find('.button-next').show();
                    $('#form_wizard_2').find('.button-submit').hide();
                    $('#form_wizard_2').find('.button-submit-skip-payment').hide();
                }
                Metronic.scrollTo($('#form_wizard_2'),50);
            };

            // default form wizard
            $('#form_wizard_2').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index, clickedIndex) {
                    success.hide();
                    error.hide();
                    if (form.valid() == false) {
                        return false;
                    }
                    handleTitle(tab, navigation, clickedIndex);
                },
                onNext: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    if (form.valid() == false) {
                        return false;
                    }

                    handleTitle(tab, navigation, index);
                },
                onPrevious: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    handleTitle(tab, navigation, index);
                },
                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#form_wizard_2').find('.progress-bar').css({
                        width: $percent + '%'
                    });
                }
            });

            $('#form_wizard_2').find('.button-previous').hide();
            $('#form_wizard_2 .button-submit').click(function () { 
              $(this).attr("disabled", "true");
              if(form.valid())  
              {
                submitForm();
              }
            }).hide();
        }
    };

}();