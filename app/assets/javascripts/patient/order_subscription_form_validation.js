/* order subscription form validaton --------start----------*/
function orderSubscriptionFormValidation(){
    var form = $('.order_subscription_form');
    var error = $('.alert-danger', form);
    var success = $('.alert-success', form);
    form.validate({
        doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true,
        rules: {
                    //start_date
                    'order_subscription[start_time]': {
                        required: true
                    }
        },
        messages: { // custom messages
                    'order_subscription[start_time]': {
                        required: "Please select any date"
                    }
        },
        invalidHandler: function (event, validator) { //display error alert on form submit
            success.hide();
            error.show();
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            
            label
                .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
        },
        
        submitHandler: function(form) {
          $.ajax({
             url: form.action,
             type: form.method,
             data: $(form).serialize(),
             success: function(response) {
               $.ajax({
                 url: '/order_subscriptions?patient_id='+response['current_patient'],
               });
             }
          });
        }
    });
}

/* order subscription form validaton --------end----------*/