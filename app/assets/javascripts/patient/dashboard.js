//index page js ------------start-------------------------  

  function dashboardIndexPage()
  {
    dashboardLoadTabs();

    $(document).on("click", ".payment-modal-link", function(){
      var order_id = $(this).attr("data-value");
      var order_type = $(this).attr("data-order_type");
      $("#order_transaction_orderable_id").val(order_id);
      $("#order_transaction_orderable_type").val(order_type);
      $("#payment-modal").modal();
    })

    $('#payment-form').on('ajax:success', function(e, data, status, xhr){
      $('#payment-modal').modal('hide');
    })
    
    // prevent to make copied reports    --------Start--------
      $('.blocked_section').bind('copy paste cut drag drop', function (e) {
        e.preventDefault();
      });
      $('.blocked_section').disableSelection();
    // prevent to make copied reports    --------End--------

    $('.card-tools').tooltip();$("#referral-btn").bind('ajax:complete', function () {
      $loading.hide();
      $("#referral-modal").modal();
    });

    $('.re_new_order').click(function(){
      $('.re_new_order_form').submit();
    });
    
    //$(".family_member_edit_btn").bind('ajax:complete', function () {
      //$('.spinner').hide();
      //$("#family_member_edit_modal").modal();
    //})
    
    // to reload page on completing any profile payment
  }

  function dashboardLoadTabs()
  {
    reports_tab = '/family_members/reports';
    account_section_tab = '/dashboards/account_section';
    family_members_tab = '/family_members';
    $('.reports').load(reports_tab, function( response, status, xhr ) {
      $('.spinner').hide();
    });
    $('#account_tab_1_2').load(account_section_tab, function( response, status, xhr ) {
      $('.spinner').hide();
    });
    $('#family_tab_1_4').load(family_members_tab, function( response, status, xhr ) {
      $('.spinner').hide();
    });
  }
//Index page js ------------End-------------------------