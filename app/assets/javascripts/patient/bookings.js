//new page js ------------start-------------------------
function bookingsNewPage(){  

  $(document).on('click','#back-to-calendar', function(){
    $('#registration-info-section').hide();
    $('#login_with_booking').hide();
    $('#calendar-section').show();
  });

  $(document).bind('ajax:complete', ".free_slot_link", function(evt, data, status, xhr){    
    $('#calendar-section').hide();
    $('#login_with_booking').removeClass('hidden').show();
  });

  var doctor_id = $("input[name=doctor_id]:radio:checked").val();
  var store_id = $("#store_select").val();
  get_slots(doctor_id, store_id);

  $('.date-picker').datepicker({
    }).on('changeDate', function(e){
      doctor_id = $("input[name=doctor_id]:radio:checked").val();
      var store_id = $("#store_select").val();
      $('.day').addClass('available-date')
      $('.disabled.day').removeClass('available-date')
      get_slots(doctor_id, store_id);   
  });

  $(document).on('click', '.datepicker th.prev,.datepicker th.next', function (ev) {
    $('.day').addClass('available-date')
    $('.disabled.day').removeClass('available-date')
  });

  $(document).on('click','.doctor-btn', function(){
    var doctor_id = $(this).find('input').val();
    var store_id = $("#store_select").val();
    get_slots(doctor_id, store_id);
  })

  function get_slots(doctor_id,store_id){
    var $selectWrapper = $("#slot-wrapper"),
    date =   $.datepicker.formatDate('yy/mm/dd', $('.date-picker').datepicker("getDate")),
    url, 
    is_doctor=true;
    $("select", $selectWrapper).attr("disabled", true);
    url = '/bookings/get_free_slot?date='+date+'&doctor_id='+doctor_id+'&store_id='+store_id;
    $selectWrapper.load(url);
  }  


  $(document).on("change", "#store_select", function () {
    var storeId = $(this).val(),
        url;
    $("#doctor_list").attr("disabled", true);
    url = '/bookings/doctor_list?store_id='+storeId;
    $("#doctor_list").load(url, function( response, status, xhr ) {
      var doctor_id = $("input[name=doctor_id]:radio:checked").val();
      var store_id = $("#store_select").val();
      get_slots(doctor_id, store_id);
    });
    
  });
  
}

//Analytic page js ------------end-------------------------