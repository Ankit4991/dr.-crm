var currentPage = 1;

function checkScroll() {
  if (nearBottomOfPage()) {
  	if($('.notifications').is(":visible")){
      $('.notification_spinner').show();
      currentPage++;
      new $.ajax($('.notifications').attr('data-url') + currentPage, {asynchronous:true, evalScripts:true, method:'get'});
      setTimeout(function(){
        $('.notification_spinner').hide();
      }, 2000);
    }
    if($('.messages').is(":visible")){
      $('.message_spinner').show();
      currentPage++;
      new $.ajax($('.messages').attr('data-url') + currentPage, {asynchronous:true, evalScripts:true, method:'get'});
      setTimeout(function(){
        $('.message_spinner').hide();
      }, 2000);
    }
    
  } else {
    setTimeout("checkScroll()", 250);
    
  }
}

function nearBottomOfPage() {
  if($('.notifications').is(":visible")){
	 return parseInt($('.notifications')[0].scrollHeight - $('.notifications').scrollTop()) == parseInt($('.notifications').height());
	}
  else if($('.messages').is(":visible")){
    return parseInt($('.messages')[0].scrollHeight - $('.messages').scrollTop()) == parseInt($('.messages').height());
  }
}
$( document ).ready(function() {
	checkScroll();
});