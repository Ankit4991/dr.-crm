// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require application
//= require doctor/custom_calendar
//= require doctor/order_form_validation
//= require doctor/examinations
//= require doctor/campaigns
//= require doctor/settings
//= require doctor/store
//= require doctor/analytics
//= require doctor/fullcalendar
//= require doctor/custom_orders
//= require doctor/manage_patients
//= require doctor/dashboard
//= require doctor/favourites
//= require patient/order_subscription_form_validation
//= require doctor/manage_examination
//= require turbolinks
	  
(function($){
  	var old = $.parseJSON;
	$.parseJSON = function(data){
	  	if (data === null || data === undefined) {
	      return data;
	  	}
	  	return old.call($,data);
  	}
})(jQuery)