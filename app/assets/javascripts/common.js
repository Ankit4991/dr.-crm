$(document).ready(function(){
  var $loading = $('.spinner').hide();
  $(document).ajaxStart(function(){
    $loading.show();
  });
  $(document).ajaxStop(function(){
    $loading.hide();
  });
  
  

  ComponentsPickers.init();
  $('.dropdown-toggle').dropdown();

  //slim scroller intilization-----start----------
    $('.scroller').each(function () {
      var height;
      if ($(this).attr("data-height")) {
          height = $(this).attr("data-height");
      } else {
          height = $(this).css('height');
      }
      $(this).slimScroll({
          allowPageScroll: true, // allow page scroll when the element scroll is ended
          size: '7px',
          color: ($(this).attr("data-handle-color")  ? $(this).attr("data-handle-color") : '#bbb'),
          railColor: ($(this).attr("data-rail-color")  ? $(this).attr("data-rail-color") : '#eaeaea'),
          position: 'right',
          height: height,
          alwaysVisible: ($(this).attr("data-always-visible") == "1" ? true : false),
          railVisible: ($(this).attr("data-rail-visible") == "1" ? true : false),
          disableFadeOut: true
      });
    });
  //slim scroller intilization-----end----------
  //Ajax used to do notification count 0 --------Start----------------
  // $(document).on("click", ".notifications_view", function () {
  $(".notify .notifications_view").click(function () {
    var data_type=$(this).attr("data-type");
    var count=$(this).parents(".notify").find(".badge");
    $.ajax(
      '/pages/resource_info',
      { 'type': 'get',
        dataType: 'json',
        success: function(data) {
           $.ajax(
            '/pages/update_notification?resource_type='+data["resource_type"]+"&resource_id="+data["resource_id"]+"&notify_for="+data_type,
            { 'type': 'get',
              dataType: 'json',
              success: function(data) {
                if(data["success"]==true){
                  count.html("0");
                }
              }
            });
        }
      }
    );
  });
  //Ajax used to do notification count 0 --------end----------------
});



//Upload profile image with ajax---------Start----------------
  $(document).on("click", "#file_uploading", function (e) {
      e.preventDefault();
      var file = new FormData();
      var path = $(this).attr('data-href')
      file.append(this.value, $('#update_profile_avatar')[0].files[0]);
      $.ajax({
        url: path,
        type: 'patch',
        data: file,
        contentType: false,
        processData: false,
        success: function(data){
          jQuery.gritter.add({ title: data.status, text: data.msg });
        }
      });
  });
//Upload profile image with ajax---------end----------------

  function diseaseAutocompleteField(){
    $('.disease_autocomplete_field').bind('railsAutocomplete.select', function(event, data){
      if (data.item.id == "pmhxes")
      {  
        $('#medical_history_modal_1').modal();
        $("#disease_pmhxes_1").val(data.item.value);
      }
      else if(data.item.id == "pohxes")
      {
        $('#oculus_history_modal_1').modal();
        $('#disease_pohxes_1').val(data.item.value);
      }
      else if(data.item.id == "medications")
      {
        $('#psychology_history_modal').modal();
        $('#disease_medications').val(data.item.value);
      }
      else if(data.item.id == "sxhxes")
      {
        $('#surgery_history_modal').modal();
        $('#disease_surgery').val(data.item.value);
      }
      else if(data.item.id == "allergies")
      {
        $('#allergy_history_modal').modal();
        $('#disease_allergy').val(data.item.value);
      }
      else
      {
        var data_type=$(this).attr("data-type");
        var time = (new Date).getTime();
        var patient_type_parmas = $(this).attr("data-patient-type-params");
        if(data.item.id!="")
        {
          $(this).before('<div class="row"><div class="col-md-offset-1 col-md-5 child-toggler"><div class="form-group boolean optional examination_'+data_type+'_checked"><div class="checkbox"><input value="" type="hidden" name="'+patient_type_parmas+'['+data_type+'_attributes]['+time+'][checked]"><label class="boolean optional" for="examination_'+data_type+'_attributes_'+time+'_checked"><input data-toggle-element="#'+data_type+'_'+data.item.value+'" toggle="" class="boolean optional icheck-me checkbox-la" data-skin="line" data-color="blue" data-text="'+data.item.label+'" type="checkbox" value="1" name="'+patient_type_parmas+'['+data_type+'_attributes]['+time+'][checked]" id="examination_'+data_type+'_attributes_'+time+'_checked" checked="checked"></label></div></div></div><div class="disease_notes col-md-6" id="'+data_type+'_'+data.item.value+'" style="display: block;"><input type="hidden" value="'+data.item.id+'" name="'+patient_type_parmas+'['+data_type+'_attributes]['+time+'][disease_id]"id="examination_'+data_type+'_attributes_'+time+'_disease_id"><div class="string optional examination_'+data_type+'_notes"><input class="string optional form-control" type="text" name="'+patient_type_parmas+'['+data_type+'_attributes]['+time+'][notes]" id="examination'+data_type+'_attributes_'+time+'_notes" placeholder="add details"></div></div>')
          icheck();
          disease_toggle();
        }
        this.value="";
      }
    });
  }

  function familyDiseaseAutocompleteField(){
    $(document).on( "railsAutocomplete.select", '.family_disease_autocomplete_field', function(event, data){
      if (data.item.id == "fmhxes")
      {
        $('#family_medical_history_modal_1').modal();
        $('#disease_fmhxes_1').val(data.item.value);
      }
      else if (data.item.id == "fohxes")
      {
        $('#family_oculus_history_modal_1').modal();
        $('#disease_fohxes_1').val(data.item.value);
      }
      else{
        var data_type=$(this).attr("data-type");
        var relation_value = $(this).parent().find('select').val()
        var patient_type_parmas = $(this).attr("data-patient-type-params");
        var time = (new Date).getTime();
        if(data.item.id!="")
        {
          $(this).before('<div class="row"><div class="col-md-3 col-md-offset-1 "><input  type="text" value='+relation_value+' name="'+patient_type_parmas+'['+data_type+'_attributes]['+time+'][relation]" id="examination_'+data_type+'_attributes_'+time+'_relation" class="hidden relation_field"></div><div class="col-md-5 child-toggler"><div class="form-group boolean optional examination_'+data_type+'_checked"><div class="checkbox"><input value="" type="hidden" name="'+patient_type_parmas+'['+data_type+'_attributes]['+time+'][checked]"><label class="boolean optional" for="examination_'+data_type+'_attributes_'+time+'_checked"><input data-toggle-element="#'+data_type+'_'+data.item.value+'" toggle="" class="boolean optional icheck-me checkbox-la" data-skin="line" data-color="blue" data-text="'+data.item.label+'" type="checkbox" value="1" name="'+patient_type_parmas+'['+data_type+'_attributes]['+time+'][checked]" id="examination_'+data_type+'_attributes_'+time+'_checked" checked="checked"></label></div></div></div><div class="col-md-3" id="'+data_type+'_'+data.item.value+'" style="display: block;"><input type="hidden" value="'+data.item.id+'" name="'+patient_type_parmas+'['+data_type+'_attributes]['+time+'][disease_id]"id="examination_'+data_type+'_attributes_'+time+'_disease_id"><div class="string optional examination_'+data_type+'_notes"><input class="string optional form-control" type="text" name="'+patient_type_parmas+'['+data_type+'_attributes]['+time+'][notes]" id="examination_'+data_type+'_attributes_'+time+'_notes" placeholder="Add Notes"></div></div></div>');

          icheck();
          
          disease_toggle();
          
        }
        this.value="";
      }
    });
    
    $(document).on( "railsAutocomplete.select", '.add_new_family_disease_autocomplete_field', function(event, data) {
      if (data.item.id == "fmhxes")
      {
        $('#family_medical_history_modal_1').modal();
        $('#disease_fmhxes_1').val(data.item.value);
      }
      else if (data.item.id == "fohxes")
      {
        $('#family_oculus_history_modal_1').modal();
        $('#disease_fohxes_1').val(data.item.value);
      }
      else{
        var data_type=$(this).attr("data-type");
        var patient_type_parmas = $(this).attr("data-patient-type-params");
        var time = (new Date).getTime();
        if(data.item.id!="")
        {
          $(this).before('<div class="md-container"><div class="row"><div class="col-md-3 col-md-offset-1 "><select class="form-control relation_select" type="text" name="'+patient_type_parmas+'['+data_type+'_attributes]['+time+'][relation]" id="examination_'+data_type+'_attributes_'+time+'_relation" ><option value="select relation">Select Relation</option><option value="great_grandparent">great grandparent</option><option value="grandparent">grandparent</option><option value="parent">parent</option><option value="parent-father">parent father</option><option value="parent-mother">parent mother</option><option value="sibling">sibling</option><option value="sibling-sister">sibling sister</option><option value="sibling-brother">sibling brother</option><option value="child">child</option><option value="child-daughter">child daughter</option><option value="child-son">child son</option><option value="others">others</option><option value="unknown">unknown</option></select></div><div class="col-md-5 child-toggler"><div class="form-group boolean optional examination_'+data_type+'_checked"><div class="checkbox"><input value="" type="hidden" name="'+patient_type_parmas+'['+data_type+'_attributes]['+time+'][checked]"><label class="boolean optional" for="examination_'+data_type+'_attributes_'+time+'_checked"><input data-toggle-element="#'+data_type+'_'+data.item.value+'" toggle="" class="boolean optional icheck-me checkbox-la" data-skin="line" data-color="blue" data-text="'+data.item.label+'" type="checkbox" value="1" name="'+patient_type_parmas+'['+data_type+'_attributes]['+time+'][checked]" id="examination_'+data_type+'_attributes_'+time+'_checked" checked="checked"></label></div></div></div><div class="col-md-3" id="'+data_type+'_'+data.item.value+'" style="display: block;"><input type="hidden" value="'+data.item.id+'" name="'+patient_type_parmas+'['+data_type+'_attributes]['+time+'][disease_id]"id="examination_'+data_type+'_attributes_'+time+'_disease_id"><div class="string optional examination_'+data_type+'_notes"><input class="string optional form-control" type="text" name="'+patient_type_parmas+'['+data_type+'_attributes]['+time+'][notes]" id="examination_'+data_type+'_attributes_'+time+'_notes" placeholder="Add Notes"></div></div></div><input type="text" name="disease_name" id="disease_name" value="" class="family_disease_autocomplete_field form-control pull-right ui-autocomplete-input" data-type="fmhxes" data-patient-type-params="patient" placeholder="Add disease" style="width: 61%; -webkit-user-select: text;" data-autocomplete="/autocompletes/autocomplete_disease_name?type=" autocomplete="off"><div class= "clearfix"></div><br></div>');
          icheck();
          disease_toggle();
        }
      }
    });

    $(document).on('change', '.relation_select', function(){
      var relation_value = $(this).val();
      $(this).parent().parent().parent().parent().find('.relation_field').each(function( index ) {
        $( this ).val(relation_value); 
      });
    });
    
  }

  
// add html of disease---------End----------------

// these function used for show image at upload time---------start----------------
  function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.profile_image')
                .attr('src', e.target.result)
                .width(195)
                .height(142);
        };

        reader.readAsDataURL(input.files[0]);
    }
  }

  function readUrlForBackground(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
          $('#background_image')
              .attr('src', e.target.result)
              .width(195)
              .height(142);
      };

      reader.readAsDataURL(input.files[0]);
    }
  }
// these function used for show image at upload time---------end----------------

// Load state corresponding to country ------------Start--------------
  $(document).on("change", ".country_select", function () {
      var $selectWrapper = $($(this).attr("data-target")),
          countryCode = $(this).val(),
          url;
      $("select", $selectWrapper).attr("disabled", true);
      url = '/pages/subregion_options?parent_region='+countryCode+'&address_name='+$(this).attr('name');
      $selectWrapper.load(url);
  });
// Load doctor corresponding to store------------End--------------

// Smooth Scroll for home page link-------------Start-------------
  $(document).on("click", ".link_home a", function (event) {
    if (window.location.pathname == '/')
    {
      event.preventDefault();
      var hash = this.hash;
      $('html, body').animate({scrollTop: $(hash).offset().top}, 1000);
    }
  });
// Smooth Scroll for home page link-------------End-------------

// Load doctor corresponding to store------------Start--------------
//Used in doctor calendar
  $(document).on("change", "#store_id", function () {
    var $selectWrapper = $($(this).attr("data-target")),
        storeId = $(this).val(),
        url;
    $("select", $selectWrapper).attr("disabled", true);
    url = '/bookings/doctor_list?store_id='+storeId
    $selectWrapper.load(url, function( response, status, xhr ) {
      $('#calendar').html("");
      calendar();
    });
  });

//Used in new order exam and order patient log
  $(document).on("change", "#booking_store_id", function () {
    var $selectWrapper = $($(this).attr("data-target")),
        $dont_need_store_list = $(this).attr("data-dont_need_store_list"),
        storeId = $(this).val(),
        url;
    $("select", $selectWrapper).attr("disabled", true);
    url = '/pages/store_doctors_option?store_id='+storeId+'&dont_need_store_list='+$dont_need_store_list;
    $selectWrapper.load(url, function( response, status, xhr ) {
      $('#calendar').html("");
      calendar();
    });    
  });
// Load doctor corresponding to store------------end--------------

// Radio buton to Icheck button------------Start--------------
  $(document).on('page:change', icheck);
  $('.icheckbox_line-blue *').click(_checkboxAddHandleChange);
  function icheck(){
    
    if($(".icheck-me").length > 0){
      $(".icheck-me").each(function(){
        if(!$(this).parent().hasClass('icheckbox_line-blue'))
        {
          var $el = $(this);
          var skin = ($el.attr('data-skin') !== undefined) ? "_" + $el.attr('data-skin') : "",
          label_text = ($el.attr('data-text') !== undefined) ? " " + $el.attr('data-text') : "",
          color = ($el.attr('data-color') !== undefined) ? "-" + $el.attr('data-color') : "";
          var opt = {
            checkboxClass: 'icheckbox' + skin + color,
            radioClass: 'iradio' + skin + color,
            insert: '<div class="icheck_line-icon"></div>' + label_text,
            inheritClass: false,
            inheritID: true
          }
          $el.iCheck(opt);
        }
      });
    }
  }

 
// Radio buton to Icheck button------------End--------------



// Used for make navbar link active------------Start--------------
  $(function(){
    $('.hor-menu .nav a').filter(function(){return this.href==location.href}).parents("li").addClass('active').siblings().removeClass('active')
    var selectedSpan = document.createElement('span')
    selectedSpan.setAttribute('class', 'selected');
    $('.hor-menu .nav a').filter(function(){return this.href==location.href}).parents("li").find('a').first().append(selectedSpan)
    $('.hor-menu .nav a').click(function(){
      $(this).parent().addClass('active').siblings().removeClass('active')  
    });
  });
// Used for make navbar link active------------End--------------


$(document).bind('ajax:complete', function(e, data, status, xhr){
  if(data.responseJSON !== undefined) 
  {
    jQuery.gritter.add({ title: data.responseJSON.status, text: data.responseJSON.msg }); 
  }
});


initializePlugins();

// initializePlugins method will initialized the disease checkbox on exam form and patient registartion ----start-----
  function initializePlugins() {
    checkboxToggler();
    checkboxAdd();
  }

  function checkboxAdd() {
    $('[checkboxAdd]').each(_checkboxAddInitialize);
  }

  function _checkboxAddInitialize(index, element) {
    _checboxAddSaveSiblings(element);
    _checkboxAddToggle($(element).find('[checkboxAddChanger]')[0]);
    _checkboxAddRegisterEvents(element);
  }


  function _checkboxAddRegisterEvents(element) {
    $(element).find('[checkboxAddChanger]').change(_checkboxAddHandleChange);
  }

  function _checkboxAddHandleChange(event) {
    _checkboxAddToggle(event.target);
  }

  function _checkboxAddToggle(element) {
    $e = $(element);
    $parent = $e.parents('[checkboxAdd]').first();
    if(!$e.is(':checked')) {
      $parent.find('[checkboxAddContent]').detach();
    }
    else {
      $parent.data('checkboxAddContent').appendTo($parent);
    }
  }

  function _checboxAddSaveSiblings(element) {
    $e = $(element);
    $e.data('checkboxAddContent', $e.find('[checkboxAddContent]'));
  }

  function checkboxToggler() {
    $toggler = $("[toggle]");
    $.map($toggler, _checkboxTogglerInitialize)
  }

  function _checkboxTogglerInitialize(element) {
    _checkboxTogglerUpdateState(element);
    $(element).change(_checkboxTogglerChangeHandler);
  }

  function _checkboxTogglerChangeHandler(event) { 
    _checkboxTogglerUpdateState(event.target);
  }

  function _checkboxTogglerUpdateState(element) {
    $toggler = $(element);
    $element = $($toggler.data('toggleElement'));
    if($toggler.is(':checked')){
      $element.show();
    }
    else {
      $element.hide();
    }
  }

  /* Js for form checkbox(Medical history) toggle with Icheck------Start--------*/
  function disease_toggle(){
    $('.parent-toggle').on('ifToggled', function(event){
      _checkboxAddInitialize();
      _checkboxAddHandleChange(event);
      icheck();
    });

    $('.child-toggler').on('ifToggled', function(event){
      _checkboxTogglerChangeHandler(event);
      _checkboxTogglerUpdateState();
    });
  }
  /* Js for form checkbox (Medical history) toggle with Icheck------End--------*/

// initializePlugins method will initialized the disease checkbox on exam form and patient registartion----end-----

// Comments
// var eyeMeasurementCommentsBag;
// function eyeMeasurementCommentsTextGeneration() {
//   eyeMeasurementCommentsBag = $('.eye-measurement-comments');
//   eyeMeasurementCommentsBag.each(eyeMeasurementCommentsTextGenerationInitializeComments);
// }

// function eyeMeasurementCommentsTextGenerationInitializeComments(index, element) {
//   var $element = $(element);
//   $element.parents('.glass_prescription').first().find('input[type="checkbox"]').on('change', function () {
//     eyeMeasurementCommentsTextGenerationRebuildComments($element);
//   })
// }

// function eyeMeasurementCommentsTextGenerationRebuildComments(commentElement) {
//   commentElement.val('');
//   commentElement.parents('.glass_prescription').first().find('input[type="checkbox"]:checked').each( function(index, checkbox){
//     if(commentElement.val().length > 0) {
//       commentElement.val(commentElement.val() + ', ' + checkbox.closest('label').textContent);
//     } else {
//       commentElement.val(checkbox.closest('label').textContent);
//     }

//   } )
// }

function eyeMeasurementAddExpired (element) {
  $element = $(element)
  $glassPrescription = $element.parents('.glass_prescription')
  issue_date = $glassPrescription.find('.prescription_issue_date').val().split('/');
  issue_date = [issue_date[2], issue_date[1], issue_date[0]] ;
  momenk = moment(issue_date);
  $glassPrescription.find('.prescription_expire_date').val(momenk.add('years', $(element).val()).format('DD/MM/YYYY'))

}

$(window).on('scroll', function() {
  if (window.location.pathname == '/')
  {
    $('.target').each(function() {
      if($(window).scrollTop() >= $(this).offset().top) {
        var id = $(this).attr('id');
        $('.link_home a').removeClass('active');
        $('.link_home a').focusout();
        if(!window.matchMedia('(max-width: 480px)').matches){
          $('.link_home a[href="/#'+ id +'"]').focus();
        }
      }
    });
  }
});

//Validating Patients first name and last name not to allow special characters---start---
  function validates_name(){
 
    $("#patient_first_name,#patient_last_name").keyup(function()
    {
      var Input_name = $(this).val();
      regex = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
      var isSplChar = regex.test(Input_name);
      if(isSplChar)
      {
        var no_spl_char = Input_name.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
        $(this).val(no_spl_char);
      }
    });
  }

// on click of NA button raddom id generate
function append_random_id_under_account_tab(){
  $(document).on('click','#append_random_id',function(){
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i <10; i++ )
       text += possible.charAt(Math.floor(Math.random() * possible.length));
      $("#patient_email").val("itrust.io.app+"+text+"@gmail.com");
  });
}
//Validating Patients first name and last name not to allow special characters---end---
function med_rx_report_tab() {
  $('#medical_print').on('click',function(){
    $(".nav-tabs").hide();
    $(".tab_account_section").hide();
    $('.page-header, .procedure, .impression, .portlet-title, .exam_tabs, .del').hide();
    $(this).hide();
    window.print();
    $('.page-header, .procedure, .impression, .portlet-title, .exam_tabs, .del').show();
    $(this).show();
    $(".nav-tabs").show();
    $(".tab_account_section").show();
  })

  $(document).on('click','.remove_med_rx',function(){
    $(this).parent().parent().parent().parent().prev().remove();
    $(this).parent().parent().parent().parent().remove();
    if ($("div.delete_data").length == 0){
      $(".portlet box, .medical_rx").remove();
    }
  })
}
  
function printreferralform()
{
  $('.hide_btn').hide();
  $('#referral-modal').children().removeClass("modal-dialog");
  $('#referral-modal').children().children().removeClass("modal-content");
  $('#referral-modal').children().children().children().removeClass("modal-body");
  $('#referral-modal').removeClass("modal fade in");
  $('#referral-modal').children().css("width","");
  $('.modal-backdrop').removeClass("modal-backdrop fade in");
  $('#print_title').show();
  $('.page-header').hide();
  $('.portlet-title').hide();
  $('.exam_tabs').hide();
  $('#print_overview_tab').hide();
  $(".refract_s").css('font-size', '11px');
  $(".data_off_address").css('font-size', '9px');
  window.print()
  $(".refract_s").css('font-size', '11px');
  $('#print_overview_tab').show();
  $('.page-header').show();
  $('.portlet-title').show();
  $('.exam_tabs').show();
  $('#print_title').hide();
  $('#referral-modal').children().addClass("modal-dialog");
  $('#referral-modal').children().children().addClass("modal-content");
  $('#referral-modal').children().children().children().addClass("modal-body");
  $('#referral-modal').addClass("modal fade in");
  $('.modal-backdrop').addClass("modal-backdrop fade in");
  $('.hide_btn').show();
  $(".data_off_address").css('font-size', '13px');
  $('#referral-modal').children().css("width","60%");
}