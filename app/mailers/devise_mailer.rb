class DeviseMailer < Devise::Mailer
  helper :application # gives access to all helpers defined within `application_helper`.
  include Devise::Controllers::UrlHelpers # Optional. eg. `confirmation_url`
  default template_path: 'devise/mailer' # to make sure that your mailer uses the devise views

  def confirmation_instructions(record, token, opts={})
    unless record.class == Patient && record.email_not_supplied
      headers['X-Mailgun-Tag'] = "patient_account"
      headers['campaign'] = "patient_account"
      headers['organisation_id'] = record.organisation.id
      headers['trackable_type'] = "Patient"
      headers['trackable_id'] = record.id
      headers['sub_type'] = "confirmation_instructions"
      @patient = record
      opts[:subject] = "Activate your #{record.organisation.company_name} Account powered by itrust.io"
      opts[:from] = "#{record.organisation.owner.email.present? ? record.organisation.owner.email : 'itrust@itrust.io'}"
      opts[:reply_to] ="#{record.organisation.owner.email.present? ? record.organisation.owner.email : 'itrust@itrust.io'}"
      super
    end
  end

  def reset_password_instructions(record, token, opts={})
    unless record.class == Patient && record.email_not_supplied
      headers['X-Mailgun-Tag'] = "patient_account"
      headers['campaign'] = "patient_account"
      headers['organisation_id'] = record.organisation.id
      headers['trackable_type'] = "Patient"
      headers['trackable_id'] = record.id
      headers['sub_type'] = "reset_password_instructions"
      @patient = record #record may have patient or Doctor
      opts[:subject] = "Reset your #{record.organisation.company_name} Account"
      opts[:from] = "#{record.organisation.owner.email.present? ? record.organisation.owner.email : 'itrust@itrust.io'}"
      opts[:reply_to] = "#{record.organisation.owner.email.present? ? record.organisation.owner.email : 'itrust@itrust.io'}"   
      super
    end
  end
end