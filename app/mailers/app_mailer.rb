##
# This is our main app mailer. Mostaly mail sent through this mailer.
# We also send some extra value in email Header that are used by Mailgun and get back in mailgun webhook response.
# From these value we can Identified  webhook related to which email and update records according to that.

class AppMailer < ActionMailer::Base
  default from: "\"ITRUST\" <itrust@itrust.io>"
  add_template_helper(ApplicationHelper)
  include ActionView::Helpers::NumberHelper
  
  def send_referral_to_user(joint_resource, referral)
    @joint_resource = joint_resource
    if joint_resource.class == FamilyMember
      @patient = joint_resource.patient
    else
      @patient = @joint_resource
    end
    @referral_form = referral
    @doctor = @referral_form.examination.doctor
    attachments["referral.pdf"] = WickedPdf.new.pdf_from_string(render_to_string(:pdf => "finalized referral",:template => 'patient/reports/_referral.html.haml', layout: 'layouts/application.pdf.haml'))
    if @patient.notification_method == 1 || @patient.notification_method == 3 && @patient.authentication_token
      one_click_url = root_url(patient_email: @patient.email, authentication_token: @patient.authentication_token, subdomain: @patient.subdomain)
      one_click_url.gsub!(@patient.subdomain, @patient.subdomain + ".dev") if Rails.env.include?('staging') && @patient.subdomain != 'dev'
      @one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
      mail(from: get_store, to: @patient.email, subject: "#{@referral_form.examination.exam_date} Doctor's Referral", organisation_id: @patient.try(:organisation_id), :'X-Mailgun-Tag'=> "appointment", campaign: "appointment",  "trackable_type"=> "Referral", "trackable_id"=> @referral_form.id, "sub_type"=> "send_referral_to_user")
    else
      puts "Patient don't want to recive notification through email! "
    end
  end

  def generate_pdf_fax_worker(referral, file_name, url, fax)
    @referral_form = referral
    @exam = @referral_form.examination
    file_name = "#{SecureRandom.hex(6)}#{file_name}"
    pdf = WickedPdf.new.pdf_from_string(render_to_string(:pdf => "fax referral",:template => 'patient/reports/_referral.html.haml', layout: 'layouts/application.pdf.haml'))
    save_path = Rails.root.join('public', 'uploads', file_name)
    File.open(save_path, 'wb') do |file|
      file << pdf
    end
    path = "#{url}/uploads/#{file_name}"
    TwilioFaxWorker.perform_async({'src'=> @exam.organisation.twilio_number, 'dst' => @exam.try(:joint_resource).try(:member).try(:phone),'org_sid'=> @exam.organisation.twilio_acc_sid,'org_auth_token'=> @exam.organisation.twilio_auth_token ,'fax'=> fax , 'pdf_path' => path})
  end

  def send_referral_to_cc_email(referral)
    @referral_form = referral
    @exam = @referral_form.examination
    @joint_resource = @exam.joint_resource
    @patient = @joint_resource.patient
    @doctor = @referral_form.examination.doctor
    attachments["Referral for #{@patient.name} from #{@exam.try(:store).try(:description)} on #{@exam.exam_date}.pdf".downcase.tr(" ", "_")] = WickedPdf.new.pdf_from_string(render_to_string(:pdf => "Referral Report",:template => 'patient/reports/_referral.html.haml', layout: 'layouts/application.pdf.haml'))
    mail(from: get_store, cc: @referral_form.cc_to, subject: "You've received a referral from #{@doctor.name} at #{@exam.try(:store).try(:description)}", organisation_id: @doctor.try(:organisation_id), :'X-Mailgun-Tag'=> "appointment", campaign: "appointment",  "trackable_type"=> "Referral", "trackable_id"=> @referral_form.id, "sub_type"=> "send_referral_to_cc_email")
  end
  
  def send_order_confirmation_to_patient(order)
    @order = order
    @joint_resource = order.joint_resource
    @patient = @joint_resource.patient
    @contact_lens_order = order.contact_lens_orders.order(:updated_at => :desc).first if order.contact_lens_orders.present?
    @eye_glass_order = order.eye_glass_orders.order(:updated_at => :desc).first if order.eye_glass_orders.present?
    if @patient.notification_method == 1 || @patient.notification_method == 3
      one_click_url = root_url(patient_email: @patient.email, authentication_token: @patient.authentication_token, subdomain: @patient.subdomain)
      one_click_url.gsub!(@patient.subdomain, @patient.subdomain + ".dev") if Rails.env.include?('staging') && @patient.subdomain != 'dev'
      @one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
      mail(from: get_store, to: @patient.email, subject: "Eyetrust: Order Confirmation ##{@order.id}", :'X-Mailgun-Tag'=> "order", organisation_id: @order.try(:organisation_id), campaign: "order",  "trackable_type"=> "Order", "trackable_id"=> @order.id, "sub_type"=> "send_order_confirmation_to_patient")
    else
      puts "Patient don't want to receive notification through email! "
    end
  end
  
  def send_prescription_to_patient(joint_resource, exam)
    @joint_resource = joint_resource
    if joint_resource.class == FamilyMember
      @patient = joint_resource.patient
    else
      @patient = @joint_resource
    end
    @exam = exam
    attachments["prescription.pdf"] = WickedPdf.new.pdf_from_string(render_to_string(:pdf => "finalized prescription",:template => 'patient/reports/_prescription.html', layout: 'layouts/application.pdf.haml'))
    one_click_url = root_url(patient_email: @patient.email, authentication_token: @patient.authentication_token, subdomain: @patient.subdomain, :anchor => 'prescription_reports_tab_3-2')
    one_click_url.gsub!(@patient.subdomain, @patient.subdomain + ".dev") if Rails.env.include?('staging') && @patient.subdomain != 'dev'
    @one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
    mail from: get_store, :subject => "Your Last Examination Prescription", :to => @patient.email, :'X-Mailgun-Tag'=> "order", organisation_id: @exam.try(:organisation_id), campaign: "order", "trackable_type"=> "Examination", "trackable_id"=> @exam.id, "sub_type"=> "send_prescription_to_patient"
  end


  def send_order_to_contact(emails, doctor, subject, account_numbers)
    @store = doctor.store
    @doctor = doctor
    @acount_numbers = account_numbers
    @contact_lens_distribution_orders = @store.contact_lens_distribution_orders
    attachments["contact_lens_order.pdf"] = WickedPdf.new.pdf_from_string(render_to_string(:pdf => "order trial",:template => 'doctor/dashboards/dashboard_partial/contact_lens_pdf.html', layout: 'layouts/application.pdf.haml'))
    mail(from: get_store, to: emails, subject: subject , :'X-Mailgun-Tag'=> "order",  campaign: "order", "trackable_type"=> "ContactLensDistributionOrder", "trackable_id"=> doctor.id, "sub_type"=> "conatct_lens_distribution_order")
    @contact_lens_distribution_orders.destroy_all
  end

  def send_renewed_order_confirmation_to_patient(order)
    @favourite_order = order
    @joint_resource = @favourite_order.joint_resource
    @patient= @joint_resource.patient
    @lens_order = @favourite_order.lens_prescription
    if @patient.notification_method == 1 || @patient.notification_method == 3
      one_click_url = root_url(patient_email: @patient.email, authentication_token: @patient.authentication_token, subdomain: @patient.subdomain)
      one_click_url.gsub!(@patient.subdomain, @patient.subdomain + ".dev") if Rails.env.include?('staging') && @patient.subdomain != 'dev'
      @one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
      mail(from: get_store, to: @patient.email, subject: "Eyetrust: Re-Newed Order Confirmation ##{@favourite_order.id}",:'X-Mailgun-Tag'=> "order", organisation_id: @favourite_order.try(:organisation_id), campaign: "order", "trackable_type"=> "FavouriteOrder", "trackable_id"=> @favourite_order.id, "sub_type"=> "send_renewed_order_confirmation_to_patient")
    else
      puts "Patient don't want to receive notification through email! "
    end
  end
  
  def send_failed_subscription_to_patient(subscription)
    @subscription = subscription
    @joint_resource = @subscription.joint_resource
    @patient = @joint_resource.patient
    @lens_order = @subscription.lens_prescription
    if @patient.notification_method == 1 || @patient.notification_method == 3
      one_click_url = root_url(patient_email: @patient.email, authentication_token: @patient.authentication_token, subdomain: @patient.subdomain, :anchor => 'subscriptions_tab_1_6')
      one_click_url.gsub!(@patient.subdomain, @patient.subdomain + ".dev") if Rails.env.include?('staging') && @patient.subdomain != 'dev'
      @one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
      mail(from: get_store, to: @patient.email, subject: "Eyetrust: Subscription Failed ##{@subscription.id}",:'X-Mailgun-Tag'=> "order", organisation_id: @subscription.try(:organisation_id), campaign: "order", "trackable_type"=> "OrderSubscription", "trackable_id"=> @subscription.id, "sub_type"=> "send_failed_subscription_to_patient")
    else
      puts "Patient don't want to receive notification through email! "
    end
  end
  
  def send_order_dispensed_to_patient(order)
    @order=order
    @joint_resource = @order.joint_resource
    @patient = @joint_resource.patient
    if @patient.notification_method == 1 || @patient.notification_method == 3
      one_click_url = root_url(patient_email: @patient.email, authentication_token: @patient.authentication_token, subdomain: @patient.subdomain)
      one_click_url.gsub!(@patient.subdomain, @patient.subdomain + ".dev") if Rails.env.include?('staging') && @patient.subdomain != 'dev'
      @one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
      mail(from: get_store, to: @patient.email, subject: "Your Order is ready for pick up!", :'X-Mailgun-Tag'=> "order", organisation_id: @order.try(:organisation_id), campaign: "order", "trackable_type"=> "Order", "trackable_id"=> @order.id, "sub_type"=> "send_order_dispensed_to_patient")
    else
      puts "Patient don't want to receive notification through email! "
    end
  end
  
  def send_renewed_order_dispensed_to_patient(order)  #common email sent for an order(created by doctor) and renewed order( created by patient)
    @favourite_order=order
    @joint_resource = @favourite_order.joint_resource
    @patient = @joint_resource.patient
    if @patient.notification_method == 1 || @patient.notification_method == 3
      one_click_url = root_url(patient_email: @patient.email, authentication_token: @patient.authentication_token, subdomain: @patient.subdomain)
      one_click_url.gsub!(@patient.subdomain, @patient.subdomain + ".dev") if Rails.env.include?('staging') && @patient.subdomain != 'dev'
      @one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
      mail(from: get_store, to: @patient.email, subject: "Eyetrust: Re-Newed Order Dispensed ##{@favourite_order.id}", :'X-Mailgun-Tag'=> "order", organisation_id: @favourite_order.try(:organisation_id), campaign: "order", "trackable_type"=> "FavouriteOrder", "trackable_id"=> @favourite_order.id, "sub_type"=> "send_renewed_order_dispensed_to_patient")
    else
      puts "Patient don't want to receive notification through email! "
    end
  end

  def send_order_billing_confirmation_to_patient(order)
    @order = order
    @joint_resource = order.joint_resource
    @patient = @joint_resource.patient
    if @patient.notification_method == 1 || @patient.notification_method == 3
      one_click_url = root_url(patient_email: @patient.email, authentication_token: @patient.authentication_token, subdomain: @patient.subdomain)
      one_click_url.gsub!(@patient.subdomain, @patient.subdomain + ".dev") if Rails.env.include?('staging') && @patient.subdomain != 'dev'
      @one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
      mail(from: get_store, to: @patient.email, subject: "A payment has been applied to your account ", campaign: "order", :'X-Mailgun-Tag'=> "order", organisation_id: @order.try(:organisation_id), "trackable_type"=> "Order", "trackable_id"=> @order.id, "sub_type"=> "send_order_billing_confirmation_to_patient")
    else
      puts "Patient don't want to receive notification through email! "
    end 
  end
 
  
  def send_order_cancellation_to_patient(order)
    @joint_resource = order.joint_resource
    @patient = @joint_resource.patient
    @procedures = order.procedures.present? ? order.procedures.map{|prcd| prcd.name }.join(","):"";
    if @patient.notification_method == 1 || @patient.notification_method == 3
      one_click_url = root_url(patient_email: @patient.email, authentication_token: @patient.authentication_token, subdomain: @patient.subdomain)
      one_click_url.gsub!(@patient.subdomain, @patient.subdomain + ".dev") if Rails.env.include?('staging') && @patient.subdomain != 'dev'
      @one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
      mail(from: get_store, to: @patient.email, subject: "Eyetrust: Appointment Canceled for Order ##{order.id} #{@procedures}", :'X-Mailgun-Tag'=> "order", organisation_id: order.try(:organisation_id), campaign: "order", "trackable_type"=> "Order", "trackable_id"=> order.id, "sub_type"=> "send_order_cancellation_to_patient")
    else
      puts "Patient don't want to receive notification through email! "
    end
  end
  
  def send_dispensed_removal_to_patient(object)
    if object.class == Examination.new.class #this line changed to check class name
      @joint_resource = object.joint_resource # replaced joint_resource with patient
      #below lines are not changed
      @patient =  @joint_resource.patient
      @order = object.order
      @procedures = object.exam_procedures.present? ? object.exam_procedures.map{|prcd| prcd.name }.join(","):"";
    elsif object.class == FavouriteOrder.new.class
      @joint_resource = object.joint_resource
      @order = object
      @patient = @joint_resource.patient
    else
      @patient = object.patient
      @order = object
      @procedures = object.procedures.present? ? object.procedures.map{|prcd| prcd.name }.join(","):"";
    end
    one_click_url = root_url(patient_email: @patient.email, authentication_token: @patient.authentication_token, subdomain: @patient.subdomain)
    one_click_url.gsub!(@patient.subdomain, @patient.subdomain + ".dev") if Rails.env.include?('staging') && @patient.subdomain != 'dev'
    @one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
    mail(from: get_store, to: @patient.email, subject: "Eyetrust: Dispensed Removal for Order##{@order.id}", :'X-Mailgun-Tag'=> "order", organisation_id: @order.try(:organisation_id), campaign: "order", "trackable_type"=> "Order", "trackable_id"=> @order.id, "sub_type"=> "send_dispensed_removal_to_patient")
  end
  
  def send_credit_card_warning(joint_resource)
    @patient = joint_resource.patient
    @doctor = joint_resource.try(:examinations).try(:last).try(:doctor) || joint_resource.try(:bookings).try(:last).try(:doctor) || joint_resource.try(:orders).try(:last).try(:doctor)
    one_click_url = root_url(patient_email: @patient.email, authentication_token: @patient.authentication_token, subdomain: @patient.subdomain, :anchor => 'account_tab_1_2')
    one_click_url.gsub!(@patient.subdomain, @patient.subdomain + ".dev") if Rails.env.include?('staging') && @patient.subdomain != 'dev'
    @one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
    mail(from: get_store, to: @patient.try(:email), subject: "Eyetrust: Problems with your Account Credit Card", :'X-Mailgun-Tag'=> "patient_account", organisation_id: joint_resource.try(:organisation_id), campaign: "patient_account", "trackable_type"=> "Patient", "trackable_id"=> @patient.try(:id), "sub_type"=> "send_credit_card_warning")
  end
  
  def send_booking_confirmation(booking_id)
    @booking= Booking.with_deleted.find(booking_id)
    @joint_resource = @booking.joint_resource
    @patient = @joint_resource.patient
    @doctor = @booking.doctor
    if @patient.notification_method == 1 || @patient.notification_method == 3
      one_click_url = root_url(patient_email: @patient.email, authentication_token: @patient.authentication_token, subdomain: @patient.subdomain)
      one_click_url.gsub!(@patient.subdomain, @patient.subdomain + ".dev") if Rails.env.include?('staging') && @patient.subdomain != 'dev'
      @one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
      mails = mail(from: get_store, to: @patient.try(:email), subject: "Your appointment has been confirmed!", :'X-Mailgun-Tag'=> "appointment" ,organisation_id: @booking.try(:organisation_id), campaign: "appointment", "trackable_type"=> "Booking", "trackable_id"=> @booking.try(:id), "sub_type"=> "send_booking_confirmation")do |format|
        format.html { render "send_booking_confirmation" }
        format.ics {
          ical = Icalendar::Calendar.new
          ical.append_custom_property("X-MS-OLK-FORCEINSPECTOROPEN", "TRUE")
          ical.timezone.tzid = "UTC"
          ical.event do |e|
            e.dtstart = DateTime.parse(@booking.start_time.utc.strftime("%a, %d %b %Y %k:%M:%S %z"))
            e.dtend =  DateTime.parse(@booking.end_time.utc.strftime("%a, %d %b %Y %k:%M:%S %z"))
            e.ip_class = "PRIVATE"
            e.summary = "Appointment with #{@doctor.name}"
            e.description = @joint_resource.is_family_member? ? "Your family member #{@booking.joint_resource.name} appointment has been fixed with #{@doctor.name} on #{@booking.start_time.strftime('%A %m/%d/%Y %T')} to #{@booking.end_time.strftime('%T')}" : "Your appointment has been fixed with #{@doctor.name} on #{@booking.start_time.strftime('%A %m/%d/%Y %T')} to #{@booking.end_time.strftime('%T')}."
            e.location = @doctor.try(:store).try(:description)
          end
          ical.append_custom_property("METHOD", "REQUEST")
          render :text => ical.to_ical
        }
      end
      mails.parts[1].content_type = 'text/calendar charset=UTF-8 method=REQUEST' unless mails.parts.empty?
    else
      puts "Patient don't want to receive notification through email! "
    end
  end

  def send_booking_cancellation_to_patient(booking_id)
    @booking = Booking.with_deleted.find(booking_id)
    @joint_resource = @booking.joint_resource
    @patient = @joint_resource.patient
    @doctor = @booking.doctor
    if @patient.notification_method == 1 || @patient.notification_method == 3
      one_click_url = root_url(patient_email: @patient.email, authentication_token: @patient.authentication_token, subdomain: @patient.subdomain)
      one_click_url.gsub!(@patient.subdomain, @patient.subdomain + ".dev") if Rails.env.include?('staging') && @patient.subdomain != 'dev'
      @one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
      mail(from: get_store, to: @patient.try(:email), subject: "Your appointment with us has been canceled", :'X-Mailgun-Tag'=> "appointment", organisation_id: @booking.try(:organisation_id), campaign: "appointment", "trackable_type"=> "Booking", "trackable_id"=> @booking.try(:id), "sub_type"=> "send_booking_cancellation_to_patient")
    else
      puts "Patient don't want to receive notification through email! "
    end
  end

  def send_bookings_details_confirmation future_bookings
    @future_bookings = future_bookings    
    @patient = @future_bookings.last.joint_resource.patient
    if @patient.notification_method == 1 || @patient.notification_method == 3
      one_click_url = root_url(patient_email: @patient.email, authentication_token: @patient.authentication_token, subdomain: @patient.subdomain)
      one_click_url.gsub!(@patient.subdomain, @patient.subdomain + ".dev") if Rails.env.include?('staging') && @patient.subdomain != 'dev'
      @one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
      mail(from: get_store, to: @patient.try(:email), subject: "Your upcomming appointment's details", :'X-Mailgun-Tag'=> "appointment", organisation_id: @patient.try(:organisation_id), campaign: "appointment", "trackable_type"=> "Booking", "trackable_id"=> @patient.try(:id), "sub_type"=> "send_booking_cancellation_to_patient")
    else
      puts "Patient don't want to receive notification through email! "
    end
  end

  def send_reinvite_for_new_exam_one_year_follow_up(exam)
    @exam = exam
    @joint_resource = @exam.joint_resource
    @patient = @exam.joint_resource.patient
    if @patient.notification_method == 1 || @patient.notification_method == 3
      one_click_url = root_url(patient_email: @patient.email, authentication_token: @patient.authentication_token, subdomain: @patient.subdomain)
      one_click_url.gsub!(@patient.subdomain, @patient.subdomain + ".dev") if Rails.env.include?('staging') && @patient.subdomain != 'dev'
      @one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
      mail(from: get_store, to: @patient.try(:email), subject: "Eyetrust: Yearly recall reminder from #{@exam.store.description}", :'X-Mailgun-Tag'=> "follow_up", organisation_id: @exam.try(:organisation_id), campaign: "follow_up", "trackable_type"=> "Examination", "trackable_id"=> @exam.try(:id), "sub_type"=> "send_reinvite_for_new_exam_one_year_follow_up")
    end
  end

  def send_survey_link(patient)
    @patient = patient
    one_click_url = root_url(patient_email: @patient.email, authentication_token: @patient.authentication_token, subdomain: @patient.subdomain)
    one_click_url.gsub!(@patient.subdomain, @patient.subdomain + ".dev") if Rails.env.include?('staging') && @patient.subdomain != 'dev'
    @one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
    mail(from: get_store, to: @patient.try(:email), subject: "Eyetrust: Survey", :'X-Mailgun-Tag'=> "survey", organisation_id: @patient.try(:organisation_id), campaign: "survey", "trackable_type"=> "Patient", "trackable_id"=> @patient.try(:id), "sub_type"=> "send_survey_link")
  end

  def send_contact_form_email_reply(contact_form, message)
    @contact_form = contact_form
    @message = message
    @patient = Patient.where(email: @contact_form.email).first
    if @patient.present?
      one_click_url = root_url(patient_email: @patient.email, authentication_token: @patient.authentication_token, subdomain: @patient.subdomain)
      one_click_url.gsub!(@patient.subdomain, @patient.subdomain + ".dev") if Rails.env.include?('staging') && @patient.subdomain != 'dev'
      @one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
    end
    mail(from: get_store, to: @contact_form.try(:email), subject: "Eyetrust: Contact Form Response", :'X-Mailgun-Tag'=> "contact_form", organisation_id: @contact_form.try(:organisation_id), campaign: "contact_form", "trackable_type"=> "ContactForm", "trackable_id"=> @contact_form.try(:id), "sub_type"=> "send_contact_form_email_reply")
  end

  def send_contact_form_details(contact_form)
    @contact_form = contact_form
    email = "drchrycy@gmail.com"
    mail(from: "Eyetrust Vision <itrust@itrust.io>", to: email, subject: "Eyetrust: Contact Form Details", :'X-Mailgun-Tag'=> "contact_form", organisation_id: @contact_form.try(:organisation_id), campaign: "contact_form", "trackable_type"=> "ContactForm", "trackable_id"=> @contact_form.id, "sub_type"=> "send_contact_form_details")
  end

  def send_missed_appointments_to_patient(booking)
    @booking = booking
    @patient = @booking.joint_resource.patient
    mail(from: get_store, to: @patient.try(:email), subject: "Eyetrust: Missed Appointment")
  end

  def send_email_changed_confirmation(patient,new_email)
    @patient = patient
    @new_email = new_email
    one_click_url = root_url(patient_email: @patient.email, authentication_token: @patient.authentication_token, subdomain: @patient.subdomain)
    one_click_url.gsub!(@patient.subdomain, @patient.subdomain + ".dev") if Rails.env.include?('staging') && @patient.subdomain != 'dev'
    @one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
    mail(from: get_store, to: @patient.try(:email), subject: "Eyetrust: Email changed confirmation")
  end

  def send_auto_receipt_to_patient(order,payment_information,transaction_time)
    @transaction_time = transaction_time
    @order = order
    @patient = @order.joint_resource.patient
    @joint_resource = @order.joint_resource
    @payment_information = payment_information
    one_click_url = root_url(patient_email: @patient.email, authentication_token: @patient.authentication_token, subdomain: @patient.subdomain)
    one_click_url.gsub!(@patient.subdomain, @patient.subdomain + ".dev") if Rails.env.include?('staging') && @patient.subdomain != 'dev'
    @one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
    mail(from: get_store,to: @patient.try(:email),reply_to: get_store,subject: "Transaction Receipt From #{get_store}",:'X-Mailgun-Tag'=> "order", organisation_id: @order.try(:organisation_id), campaign: "order", "trackable_type"=> "Order", "trackable_id"=> @order.try(:id), "sub_type"=> "send_auto_receipt_to_patient")
  end

  def organization_request_email(organisation)
    @organisation = organisation
    @owner = Doctor.find(organisation.owner_id)
    mail(from: "drchrycy@gmail.com",to: @owner.email,subject: "Your request with iTrust.io has been received ")
  end
  

  def send_org_create_to_admin(organisation)
    @organisation = organisation
    @owner = Doctor.find(organisation.owner_id)
    mail(from: @owner.email ,to: "drchrycy@gmail.com",reply_to: "drchrycy@gmail.com",subject: "Organisation Sign Up Request By #{@owner.first_name} #{@owner.last_name} for #{@organisation.company_name}")
  end

  def send_anonymous_booking_to_store_contact(booking)
    @booking = booking
    @patient = @booking.try(:joint_resource).try(:member)
    @store = @booking.try(:store)
    mail(from: get_store,to: @store.try(:contact_email),subject: "#{@patient.try(:name)} has booked an appointment")
  end

  def send_earned_reward_points_to_patient(order,earned_rewards)
    @order=order
    @earned_rewards = earned_rewards
    @joint_resource = @order.joint_resource
    @patient = @joint_resource.patient
    if @patient.notification_method == 1 || @patient.notification_method == 3
      one_click_url = root_url(patient_email: @patient.email, authentication_token: @patient.authentication_token, subdomain: @patient.subdomain)
      one_click_url.gsub!(@patient.subdomain, @patient.subdomain + ".dev") if Rails.env.include?('staging') && @patient.subdomain != 'dev'
      @one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
      mail(from: get_store, to: @patient.try(:email), subject: "You've earned $ #{number_with_precision(@earned_rewards, :precision =>2)} at our office", :'X-Mailgun-Tag'=> "order", organisation_id: @order.try(:organisation_id), campaign: "order", "trackable_type"=> "Order", "trackable_id"=> @order.try(:id), "sub_type"=> "send_earned_reward_points_to_patient")
    else
      puts "Patient don't want to receive notification through email! "
    end
  end

  def send_approve_org_details_to_patient(organisation)
    @organisation = organisation
    @owner = Doctor.find(organisation.owner_id)
    mail(from: "drchrycy@gmail.com",to: @owner.email,reply_to: @owner.email,subject: "Your Account is Approved Now!")
  end

  def send_booking_reminder_to_patient(booking,duration)
    @booking = booking
    @duration = duration
    @joint_resource = @booking.joint_resource
    @patient = @joint_resource.patient
    @doctor = @booking.doctor
    if @patient.notification_method == 1 || @patient.notification_method == 3
      one_click_url = root_url(patient_email: @patient.email, authentication_token: @patient.authentication_token, subdomain: @patient.subdomain)
      one_click_url.gsub!(@patient.subdomain, @patient.subdomain + ".dev") if Rails.env.include?('staging') && @patient.subdomain != 'dev'
      @one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
      mail(from: get_store, to: @patient.try(:email), subject: "Reminder of your appointment at our office on #{@booking.start_time.strftime('%m/%d/%Y %I:%M%P')}", :'X-Mailgun-Tag'=> "reminder", organisation_id: @booking.try(:organisation_id), campaign: "appointment", "trackable_type"=> "Booking", "trackable_id"=> @booking.try(:id), "sub_type"=> "send_booking_reminder_to_patient")
    else
      puts "Patient don't want to receive notification through email! "
    end
  end

  private
    # before sending any email we just check mail is associated to any Store through any object. 
    # if yes we send email with store Info like logo, Social media url. else we send email with organisation info.
    def get_store
      @store = @favourite_order.try(:doctor).try(:store) || @order.try(:doctor).try(:store) || @doctor.try(:store) || @booking.try(:store) || @exam.try(:store) || @referral_form.try(:doctor).try(:store)
      if @store.present?
        host = "#{@store.try(:organisation).try(:domain_name)}.itrust.io"
        ActionMailer::Base.default_url_options = {:host => host}
        "#{@store.try(:description).to_s + ' <' +  (@store.try(:contact_email).present? ? @store.try(:contact_email) + '>' : 'itrust@itrust.io>')}"
      else
        host = "#{@patient.try(:organisation).try(:domain_name)}.itrust.io"
        ActionMailer::Base.default_url_options = {:host => host}
        "#{@patient.try(:organisation).try(:company_name).to_s + ' <' + (@patient.try(:organisation).try(:email).present? ? @patient.try(:organisation).try(:email) + '>' : 'itrust@itrust.io>')}"
      end
    end
end