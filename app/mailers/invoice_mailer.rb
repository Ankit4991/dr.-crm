class InvoiceMailer < ActionMailer::Base
  default from: "\"ITRUST\" <itrust@itrust.io>"
  add_template_helper(Doctor::OrdersHelper)
  add_template_helper(ApplicationHelper)
  
  def send_invoice_to_patient(joint_resource, order)
    @joint_resource = joint_resource
    if joint_resource.class == FamilyMember
      @patient = joint_resource.patient
    else
      @patient = @joint_resource
    end
    @order = order
    unless @patient.email_not_supplied
      if @order.class.name == "Order"
        @store = get_store
        attachments["invoice.pdf"] = WickedPdf.new.pdf_from_string(
          render_to_string(:pdf => "invoice",:template => 'doctor/orders/invoice.pdf.haml', :layout => 'application.pdf.haml')
        )
      elsif @order.class.name == "FavouriteOrder"
        @lens_order = @order.lens_prescription
        @store = get_store
        attachments["invoice.pdf"] = WickedPdf.new.pdf_from_string(
          render_to_string(:pdf => "invoice",:template => 'doctor/favourites/invoice.pdf.haml',:layout => 'application.pdf.haml')
        )
      end
      one_click_url = root_url(patient_email: @patient.email, authentication_token: @patient.authentication_token, subdomain: @patient.subdomain, :anchor => 'invoices_reports_tab_3-3')
      one_click_url.gsub!(@patient.subdomain, @patient.subdomain + ".dev") if Rails.env.include?('staging') && @patient.subdomain != 'dev'
      @one_click_url = Googl.shorten( one_click_url , (Net::HTTP.get URI "https://api.ipify.org"), ENV["GOOGLE_API_KEY"]).short_url
      mail(from: get_store_name, to: @patient.email, subject: "Your Invoice from #{@order.try(:doctor).try(:store).description}",:'X-Mailgun-Tag'=> "order", organisation_id: @order.organisation_id, campaign: "order", "trackable_type"=> @order.class.name, "trackable_id"=> @order.id, "sub_type"=> "send_invoice_to_patient")
    end
  end

  private
    def get_store_name
      if @store.present?
        host = "#{@store.try(:organisation).try(:domain_name)}.itrust.io"
        ActionMailer::Base.default_url_options = {:host => host}
        "#{@store.try(:description).to_s + ' <' +  (@store.try(:contact_email).present? ? @store.try(:contact_email) + '>' : '<itrust@itrust.io>')}"
      else
        host = "#{@patient.try(:organisation).try(:domain_name)}.itrust.io"
        ActionMailer::Base.default_url_options = {:host => host}
        "#{@patient.try(:organisation).try(:company_name).to_s + ' <' + (@patient.try(:organisation).try(:email).present? ? @patient.try(:organisation).try(:email) + '>' : 'itrust@itrust.io>')}"
      end
    end

    def get_store
      @favourite_order.try(:doctor).try(:store) || @order.try(:doctor).try(:store) || @doctor.try(:store) || @booking.try(:store) || @exam.try(:doctor).try(:store) ||  @lens_order.try(:examination).try(:doctor).try(:store) || @referral.try(:doctor).try(:store)
    end
end

