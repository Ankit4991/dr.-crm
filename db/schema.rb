# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160223094329) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "addon_details", force: :cascade do |t|
    t.string   "color"
    t.integer  "eye_glass_order_id"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "organisation_id"
    t.integer  "glass_addons_price_id"
    t.boolean  "checkmark",             default: false
  end

  create_table "addresses", force: :cascade do |t|
    t.string   "address1"
    t.string   "address2"
    t.string   "city"
    t.string   "postal_code"
    t.string   "phone"
    t.string   "email"
    t.string   "state_id"
    t.string   "country_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "patient_id"
    t.string   "addr_type"
    t.integer  "doctor_id"
    t.boolean  "shipping_address"
    t.boolean  "biling_address"
    t.integer  "organisation_id"
  end

  add_index "addresses", ["doctor_id"], name: "index_addresses_on_doctor_id", using: :btree

  create_table "audits", force: :cascade do |t|
    t.integer  "auditable_id"
    t.string   "auditable_type"
    t.integer  "associated_id"
    t.string   "associated_type"
    t.integer  "user_id"
    t.string   "user_type"
    t.string   "username"
    t.string   "action"
    t.text     "audited_changes"
    t.integer  "version",         default: 0
    t.string   "comment"
    t.string   "remote_address"
    t.string   "request_uuid"
    t.datetime "created_at"
  end

  add_index "audits", ["associated_id", "associated_type"], name: "associated_index", using: :btree
  add_index "audits", ["auditable_id", "auditable_type"], name: "auditable_index", using: :btree
  add_index "audits", ["created_at"], name: "index_audits_on_created_at", using: :btree
  add_index "audits", ["request_uuid"], name: "index_audits_on_request_uuid", using: :btree
  add_index "audits", ["user_id", "user_type"], name: "user_index", using: :btree

  create_table "backgrounds", force: :cascade do |t|
    t.string   "url"
    t.integer  "setting_id"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "doctor_id"
    t.integer  "patient_id"
  end

  add_index "backgrounds", ["doctor_id"], name: "index_backgrounds_on_doctor_id", using: :btree
  add_index "backgrounds", ["patient_id"], name: "index_backgrounds_on_patient_id", using: :btree

  create_table "binocular_testings", force: :cascade do |t|
    t.integer  "examination_id"
    t.string   "ctd"
    t.string   "ctn"
    t.string   "nra"
    t.string   "pra"
    t.string   "hpd"
    t.string   "hpn"
    t.string   "vpd"
    t.string   "vpn"
    t.string   "notes"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "npc"
  end

  create_table "bookings", force: :cascade do |t|
    t.datetime "start_time"
    t.datetime "end_time"
    t.integer  "length",            default: 30
    t.integer  "doctor_id"
    t.string   "comment"
    t.integer  "organisation_id"
    t.integer  "joint_resource_id"
    t.integer  "store_id"
    t.datetime "deleted_at"
    t.integer  "destroyer_id"
    t.string   "destroyer_type"
    t.datetime "check_in"
    t.integer  "status",            default: 0
  end

  add_index "bookings", ["deleted_at"], name: "index_bookings_on_deleted_at", using: :btree
  add_index "bookings", ["doctor_id"], name: "index_bookings_on_doctor_id", using: :btree
  add_index "bookings", ["joint_resource_id"], name: "index_bookings_on_joint_resource_id", using: :btree

  create_table "bookings_exam_procedures", force: :cascade do |t|
    t.integer "booking_id"
    t.integer "exam_procedure_id"
  end

  create_table "contact_forms", force: :cascade do |t|
    t.integer  "organisation_id"
    t.integer  "patient_id"
    t.string   "name"
    t.string   "email"
    t.string   "phone_number"
    t.string   "comment"
    t.string   "priority"
    t.boolean  "existing",        default: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  create_table "contact_lens_exams", force: :cascade do |t|
    t.integer  "doctor_id"
    t.integer  "patient_id"
    t.json     "contact_lens_evaluation"
    t.json     "current_clrx"
    t.hstore   "trial"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "examination_id"
    t.integer  "organisation_id"
  end

  add_index "contact_lens_exams", ["doctor_id"], name: "index_contact_lens_exams_on_doctor_id", using: :btree
  add_index "contact_lens_exams", ["patient_id"], name: "index_contact_lens_exams_on_patient_id", using: :btree

  create_table "contact_lens_orders", force: :cascade do |t|
    t.float    "material_copay",         default: 0.0
    t.float    "allowance",              default: 0.0
    t.float    "cl_fit",                 default: 0.0
    t.float    "total",                  default: 0.0
    t.integer  "od_qnt",                 default: 0
    t.integer  "os_qnt",                 default: 0
    t.integer  "order_id"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.float    "discount",               default: 0.15
    t.string   "comments"
    t.float    "od_patient_pays"
    t.float    "os_patient_pays"
    t.float    "od_price"
    t.float    "os_price"
    t.boolean  "hold",                   default: false
    t.integer  "organisation_id"
    t.boolean  "od_checkmark",           default: false
    t.boolean  "os_checkmark",           default: false
    t.boolean  "order_checkmark",        default: false
    t.integer  "od_lens_types_price_id"
    t.integer  "os_lens_types_price_id"
    t.integer  "lens_prescription_id"
  end

  create_table "corneal_diseases", force: :cascade do |t|
    t.json     "draw_data"
    t.string   "notes"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "external_exam_id"
  end

  create_table "corneal_diseases_diseases", force: :cascade do |t|
    t.integer "corneal_disease_id"
    t.integer "disease_id"
  end

  create_table "diseases", force: :cascade do |t|
    t.string   "name"
    t.string   "groups",                                       array: true
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "key_name"
    t.boolean  "default_active",  default: false
    t.integer  "organisation_id"
  end

  create_table "diseases_social_histories", id: false, force: :cascade do |t|
    t.integer "disease_id"
    t.integer "social_history_id"
  end

  create_table "dispense_logs", force: :cascade do |t|
    t.string   "note"
    t.datetime "logged_at"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.boolean  "emailed",          default: false
    t.integer  "organisation_id"
    t.integer  "dispensable_id"
    t.string   "dispensable_type"
  end

  create_table "doctor_recommendations", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "doctors", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "type"
    t.string   "email",                  default: "",     null: false
    t.string   "encrypted_password",     default: "",     null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,      null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "theme_color",            default: "blue"
    t.string   "mobile_number"
    t.string   "designation"
    t.string   "about"
    t.string   "portfolio"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "bgurl"
    t.date     "DOB"
    t.integer  "current_address_id"
    t.string   "role",                   default: [],                  array: true
    t.integer  "organisation_id"
    t.string   "subdomain"
    t.integer  "store_id"
    t.string   "gender",                 default: "male"
  end

  add_index "doctors", ["confirmation_token"], name: "index_doctors_on_confirmation_token", unique: true, using: :btree
  add_index "doctors", ["email", "subdomain"], name: "index_doctors_on_email_and_subdomain", unique: true, using: :btree
  add_index "doctors", ["reset_password_token"], name: "index_doctors_on_reset_password_token", unique: true, using: :btree

  create_table "doctors_notifications", force: :cascade do |t|
    t.integer "doctor_id"
    t.integer "notification_id"
    t.boolean "status"
  end

  create_table "exam_impressions", force: :cascade do |t|
    t.integer  "impression_id"
    t.integer  "examination_id"
    t.string   "plan"
    t.string   "eye",            default: "OU"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "exam_procedures", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.float    "price"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "organisation_id"
    t.float    "patient_pays"
  end

  create_table "exam_procedures_examinations", force: :cascade do |t|
    t.integer  "examination_id"
    t.integer  "exam_procedure_id"
    t.datetime "created_at",        default: '2016-01-07 09:44:31', null: false
    t.datetime "updated_at",        default: '2016-01-07 09:44:31', null: false
  end

  create_table "exam_procedures_orders", force: :cascade do |t|
    t.integer "order_id"
    t.integer "exam_procedure_id"
  end

  create_table "examinations", force: :cascade do |t|
    t.integer  "doctor_id"
    t.string   "meds"
    t.string   "sxhx"
    t.text     "chief_complain"
    t.boolean  "tobacco_use"
    t.boolean  "alcohol_use"
    t.string   "eoms"
    t.text     "pupils"
    t.boolean  "pupils_brisk"
    t.boolean  "pupils_dull"
    t.string   "cf"
    t.string   "color_vision"
    t.string   "steropsis"
    t.string   "cover_test"
    t.string   "nra"
    t.string   "pra"
    t.string   "npc"
    t.text     "fdt_visual_field"
    t.boolean  "n30_1_screening"
    t.boolean  "n30_1_threshold"
    t.text     "primlinary_exams_notes"
    t.integer  "objective_id"
    t.integer  "subjective_id"
    t.integer  "current_glass_id"
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.integer  "status",                    default: 0
    t.boolean  "medical_sec_visibility",    default: false
    t.boolean  "social_sec_visibility",     default: false
    t.boolean  "binocular_sec_visibility",  default: false
    t.boolean  "primlinary_sec_visibility", default: false
    t.boolean  "visual_sec_visibility",     default: false
    t.boolean  "refraction_sec_visibility", default: false
    t.boolean  "contact_sec_visibility",    default: false
    t.boolean  "external_sec_visibility",   default: false
    t.boolean  "internal_sec_visibility",   default: false
    t.boolean  "procedure_sec_visibility",  default: false
    t.boolean  "impression_sec_visibility", default: false
    t.integer  "organisation_id"
    t.integer  "booking_id"
    t.integer  "joint_resource_id"
    t.date     "exam_date",                 default: '2016-02-29'
  end

  add_index "examinations", ["current_glass_id"], name: "index_examinations_on_current_glass_id", using: :btree
  add_index "examinations", ["doctor_id"], name: "index_examinations_on_doctor_id", using: :btree
  add_index "examinations", ["joint_resource_id"], name: "index_examinations_on_joint_resource_id", using: :btree
  add_index "examinations", ["objective_id"], name: "index_examinations_on_objective_id", using: :btree
  add_index "examinations", ["subjective_id"], name: "index_examinations_on_subjective_id", using: :btree

  create_table "external_exams", force: :cascade do |t|
    t.integer  "examination_id"
    t.string   "tear_film",      default: "Clear OU"
    t.string   "cornea",         default: "+FL large demarcation stain, -stroma involvement"
    t.string   "iris",           default: "Flat OU"
    t.string   "bulbar_conj",    default: "Clear OU"
    t.string   "palpebral_conj", default: "Clear OU"
    t.string   "sclera",         default: "Clear OU"
    t.integer  "angles_od"
    t.integer  "angles_os"
    t.string   "lids_lashes",    default: "Mod MGD OU"
    t.string   "ac",             default: "D/Q OU"
    t.string   "lens",           default: "Clear OU"
    t.datetime "created_at",                                                                  null: false
    t.datetime "updated_at",                                                                  null: false
    t.string   "gat_first"
    t.string   "gat_last"
    t.json     "eye_draw_data"
    t.string   "gat_time"
  end

  create_table "eye_glass_orders", force: :cascade do |t|
    t.float    "material_copay",           default: 0.0
    t.float    "frame_allowance",          default: 0.0
    t.float    "overage_discount",         default: 0.0
    t.float    "discount",                 default: 0.0
    t.float    "total",                    default: 0.0
    t.integer  "order_id"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "model"
    t.string   "eye_size"
    t.string   "bridge"
    t.string   "glass_b"
    t.string   "glass_ed"
    t.string   "color"
    t.string   "material"
    t.string   "temple"
    t.string   "comments"
    t.float    "glass_type_price",         default: 0.0
    t.boolean  "hold",                     default: false
    t.float    "lens_material_price",      default: 0.0
    t.float    "addons_price",             default: 0.0
    t.float    "frame_price",              default: 0.0
    t.integer  "organisation_id"
    t.integer  "lens_material_id"
    t.integer  "eyeglass_types_price_id"
    t.integer  "glass_frames_price_id"
    t.boolean  "od_checkmark",             default: false
    t.boolean  "os_checkmark",             default: false
    t.boolean  "lens_material_checkmark",  default: false
    t.boolean  "eye_glass_type_checkmark", default: false
    t.boolean  "frame_checkmark",          default: false
    t.boolean  "order_checkmark",          default: false
  end

  create_table "eye_measurements", force: :cascade do |t|
    t.integer  "glass_id"
    t.hstore   "drugs"
    t.time     "measurement_time"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "measurement_type"
    t.string   "notes"
    t.integer  "examination_id"
    t.boolean  "finalize",         default: false
    t.boolean  "visibility",       default: false
  end

  add_index "eye_measurements", ["glass_id"], name: "index_eye_measurements_on_glass_id", using: :btree

  create_table "eyeglass_types_prices", force: :cascade do |t|
    t.string   "glasstype"
    t.float    "price"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "organisation_id"
    t.float    "patient_pays"
  end

  create_table "family_members", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.date     "birthday"
    t.string   "phone"
    t.string   "occupation"
    t.string   "ssn"
    t.boolean  "contact_lenses"
    t.boolean  "interested_contact_lens"
    t.boolean  "in_lasik"
    t.string   "ethnicity"
    t.string   "eye_color"
    t.boolean  "pregnate"
    t.boolean  "breast_feeding"
    t.boolean  "tobacco_use"
    t.boolean  "alcohol_use"
    t.integer  "patient_id"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.integer  "organisation_id"
    t.string   "gender",                  default: "male"
    t.integer  "vision_insurance_id"
  end

  add_index "family_members", ["organisation_id"], name: "index_family_members_on_organisation_id", using: :btree
  add_index "family_members", ["patient_id"], name: "index_family_members_on_patient_id", using: :btree

  create_table "favourite_orders", force: :cascade do |t|
    t.integer  "doctor_id"
    t.hstore   "lens_orders"
    t.integer  "status",                default: 0
    t.float    "total",                 default: 0.0
    t.float    "cash_amount",           default: 0.0
    t.float    "check_amount",          default: 0.0
    t.string   "check_number"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "organisation_id"
    t.integer  "shippment_method",      default: 1
    t.float    "reward_points_used",    default: 0.0
    t.float    "discount",              default: 0.15
    t.boolean  "lens_od_checkmark",     default: false
    t.boolean  "lens_os_checkmark",     default: false
    t.integer  "joint_resource_id"
    t.integer  "order_subscription_id"
    t.integer  "lens_prescription_id"
    t.boolean  "rewards_applied",       default: false
    t.integer  "dispensed_delay_count", default: 0
  end

  create_table "follow_ups", force: :cascade do |t|
    t.string   "reason"
    t.integer  "doctor_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "outcome"
    t.datetime "due_date"
    t.datetime "deleted_at"
    t.integer  "joint_resource_id"
  end

  add_index "follow_ups", ["deleted_at"], name: "index_follow_ups_on_deleted_at", using: :btree
  add_index "follow_ups", ["joint_resource_id"], name: "index_follow_ups_on_joint_resource_id", using: :btree

  create_table "gats", force: :cascade do |t|
    t.string   "gat_first"
    t.string   "gat_last"
    t.time     "gat_time"
    t.integer  "external_exam_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "glass_addons_prices", force: :cascade do |t|
    t.string   "addon"
    t.float    "price"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "organisation_id"
    t.float    "patient_pays"
  end

  create_table "glass_frames_prices", force: :cascade do |t|
    t.string   "frame"
    t.float    "price"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "organisation_id"
    t.float    "patient_pays"
  end

  create_table "glasses", force: :cascade do |t|
    t.string   "od_sphere",          default: "pl"
    t.string   "od_cylinder",        default: "pl"
    t.string   "od_axis"
    t.string   "os_sphere",          default: "pl"
    t.string   "os_cylinder",        default: "pl"
    t.string   "os_axis"
    t.integer  "visual_acuity_id"
    t.string   "glasses_type"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "examination_id"
    t.string   "od_prism"
    t.string   "os_prism"
    t.boolean  "visibility",         default: false
    t.integer  "eye_glass_order_id"
    t.string   "od_segment_height"
    t.string   "os_segment_height"
    t.string   "od_base_curve"
    t.string   "os_base_curve"
    t.string   "ou_add"
    t.integer  "organisation_id"
    t.integer  "joint_resource_id"
    t.boolean  "selected_finalize",  default: false
  end

  add_index "glasses", ["joint_resource_id"], name: "index_glasses_on_joint_resource_id", using: :btree
  add_index "glasses", ["visual_acuity_id"], name: "index_glasses_on_visual_acuity_id", using: :btree

  create_table "glasses_prescriptions", force: :cascade do |t|
    t.integer  "eye_measurement_id"
    t.integer  "examination_id"
    t.json     "comments"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.boolean  "visibility",         default: false
    t.date     "issued_date"
    t.date     "expired_date"
  end

  add_index "glasses_prescriptions", ["examination_id"], name: "index_glasses_prescriptions_on_examination_id", using: :btree
  add_index "glasses_prescriptions", ["eye_measurement_id"], name: "index_glasses_prescriptions_on_eye_measurement_id", using: :btree

  create_table "gonioscopies", force: :cascade do |t|
    t.integer  "external_exam_id"
    t.string   "od_inferior"
    t.string   "od_superior"
    t.string   "od_nasal"
    t.string   "od_temporal"
    t.string   "od_pigmentation"
    t.string   "os_inferior"
    t.string   "os_superior"
    t.string   "os_nasal"
    t.string   "os_temporal"
    t.string   "os_pigmentation"
    t.string   "notes"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "holidays", force: :cascade do |t|
    t.datetime "date"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "reason"
    t.integer  "organisation_id"
    t.integer  "trackable_id"
    t.string   "trackable_type"
  end

  create_table "impressions", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "organisation_id"
  end

  create_table "internal_exams", force: :cascade do |t|
    t.integer  "examination_id"
    t.string   "od",             default: "0.3"
    t.boolean  "od_pink",        default: false
    t.boolean  "od_pallor",      default: false
    t.boolean  "od_edema",       default: false
    t.string   "os",             default: "0.3"
    t.boolean  "os_pink",        default: false
    t.boolean  "os_pallor",      default: false
    t.boolean  "os_edema",       default: false
    t.string   "vessels",        default: "2/3 OU"
    t.string   "macula",         default: "(+) Foveal Reflex OU"
    t.string   "post_pole",      default: "Clear OU"
    t.string   "periphery",      default: "Clear OU"
    t.string   "vitreous",       default: "Clear OU"
    t.string   "draw_html"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.json     "draw_data"
  end

  create_table "joint_resources", force: :cascade do |t|
    t.integer  "patient_id"
    t.integer  "family_member_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "organisation_id"
  end

  add_index "joint_resources", ["family_member_id"], name: "index_joint_resources_on_family_member_id", using: :btree
  add_index "joint_resources", ["organisation_id"], name: "index_joint_resources_on_organisation_id", using: :btree
  add_index "joint_resources", ["patient_id"], name: "index_joint_resources_on_patient_id", using: :btree

  create_table "lens", force: :cascade do |t|
    t.integer  "contact_lens_exam_id"
    t.integer  "visual_acuity_id"
    t.string   "od_sphere",              default: "pl"
    t.string   "od_cylinder",            default: "pl"
    t.string   "od_axis"
    t.string   "od_add"
    t.string   "od_base_curve"
    t.string   "od_diameter"
    t.string   "od_type"
    t.string   "os_sphere",              default: "pl"
    t.string   "os_cylinder",            default: "pl"
    t.string   "os_axis"
    t.string   "os_add"
    t.string   "os_base_curve"
    t.string   "os_diameter"
    t.string   "os_type"
    t.string   "notes"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.json     "od_draw_data"
    t.json     "os_draw_data"
    t.boolean  "finalize",               default: false
    t.boolean  "visibility",             default: false
    t.string   "lens_type"
    t.integer  "contact_lens_order_id"
    t.integer  "organisation_id"
    t.string   "od_over_refraction"
    t.string   "os_over_refraction"
    t.integer  "od_lens_types_price_id"
    t.integer  "os_lens_types_price_id"
  end

  add_index "lens", ["contact_lens_exam_id"], name: "index_lens_on_contact_lens_exam_id", using: :btree
  add_index "lens", ["visual_acuity_id"], name: "index_lens_on_visual_acuity_id", using: :btree

  create_table "lens_materials", force: :cascade do |t|
    t.string   "name"
    t.float    "price",           default: 0.0
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "organisation_id"
    t.float    "patient_pays"
  end

  create_table "lens_prescriptions", force: :cascade do |t|
    t.integer  "lens_id"
    t.integer  "examination_id"
    t.string   "cleaning_solution"
    t.boolean  "first_instruction",  default: true
    t.boolean  "second_instruction", default: true
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "organisation_id"
    t.integer  "modality_period"
    t.date     "issued_date"
    t.date     "expired_date"
  end

  add_index "lens_prescriptions", ["examination_id"], name: "index_lens_prescriptions_on_examination_id", using: :btree
  add_index "lens_prescriptions", ["lens_id"], name: "index_lens_prescriptions_on_lens_id", using: :btree

  create_table "lens_types_prices", force: :cascade do |t|
    t.string   "lens_type"
    t.float    "price"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "brand_name"
    t.string   "base_curve"
    t.string   "diameter"
    t.integer  "organisation_id"
    t.float    "patient_pays"
    t.integer  "modality",        default: 1
  end

  create_table "logs", force: :cascade do |t|
    t.string   "trackable_type"
    t.integer  "trackable_id"
    t.string   "log_type"
    t.string   "sid"
    t.string   "message"
    t.integer  "organisation_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "action",          default: "send"
  end

  create_table "mailgun_webhooks", force: :cascade do |t|
    t.string   "x_mailgun_sid"
    t.text     "message_id"
    t.string   "recipient"
    t.string   "subject"
    t.string   "trackable_type"
    t.string   "trackable_id"
    t.string   "sub_type"
    t.string   "events",          default: [],              array: true
    t.integer  "open_count",      default: 0
    t.integer  "click_count",     default: 0
    t.text     "description"
    t.string   "reason"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "campaign"
    t.integer  "organisation_id"
    t.string   "referances",      default: [],              array: true
  end

  create_table "medical_histories", force: :cascade do |t|
    t.integer  "examination_id"
    t.integer  "disease_id"
    t.string   "notes"
    t.string   "type"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "patient_id"
    t.integer  "family_member_id"
    t.integer  "joint_resource_id"
    t.string   "relation"
  end

  add_index "medical_histories", ["family_member_id"], name: "index_medical_histories_on_family_member_id", using: :btree

  create_table "notifications", force: :cascade do |t|
    t.integer  "event_id"
    t.string   "event_type"
    t.boolean  "status"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "patient_id"
    t.boolean  "is_deleted", default: false
  end

  create_table "odos_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "order_subscriptions", force: :cascade do |t|
    t.integer  "organisation_id"
    t.date     "start_time"
    t.date     "end_time"
    t.integer  "interval"
    t.integer  "joint_resource_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "shippment_method",     default: 1
    t.boolean  "active",               default: true
    t.integer  "lens_prescription_id"
    t.integer  "od_lens_quantity",     default: 1
    t.integer  "os_lens_quantity",     default: 1
  end

  add_index "order_subscriptions", ["joint_resource_id"], name: "index_order_subscriptions_on_joint_resource_id", using: :btree

  create_table "order_transactions", force: :cascade do |t|
    t.integer  "status"
    t.float    "amount"
    t.string   "response_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "organisation_id"
    t.integer  "orderable_id"
    t.string   "orderable_type"
  end

  add_index "order_transactions", ["orderable_type", "orderable_id"], name: "index_order_transactions_on_orderable_type_and_orderable_id", using: :btree

  create_table "orders", force: :cascade do |t|
    t.integer  "examination_id"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.float    "total",                     default: 0.0
    t.integer  "doctor_id"
    t.integer  "organisation_id"
    t.integer  "status",                    default: 0
    t.integer  "joint_resource_id"
    t.float    "insurance_paid"
    t.boolean  "remove_from_insurance_due", default: false
    t.float    "cash_amount",               default: 0.0
    t.float    "check_amount",              default: 0.0
    t.string   "check_number"
    t.integer  "dispensed_delay_count",     default: 0
  end

  add_index "orders", ["joint_resource_id"], name: "index_orders_on_joint_resource_id", using: :btree

  create_table "organisations", force: :cascade do |t|
    t.string   "company_name",        default: ""
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "description",         default: ""
    t.string   "slogan",              default: ""
    t.string   "size"
    t.string   "domain_name"
    t.string   "admins",              default: [],                                        array: true
    t.integer  "owner_id"
    t.datetime "created_at",                                                 null: false
    t.datetime "updated_at",                                                 null: false
    t.boolean  "approved",            default: false
    t.string   "twilio_number"
    t.boolean  "default_org",         default: false
    t.string   "time_zone",           default: "Eastern Time (US & Canada)"
  end

  create_table "patient_profiles", force: :cascade do |t|
    t.string   "customer_profile_id"
    t.string   "customer_payment_profile_id"
    t.integer  "patient_id"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.boolean  "validated",                   default: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "brand_name"
    t.date     "card_expiry_date"
  end

  create_table "patients", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.date     "birthday"
    t.string   "phone"
    t.string   "employer"
    t.string   "occupation"
    t.integer  "current_address_id"
    t.integer  "current_glass_id"
    t.string   "type"
    t.string   "email",                   default: "",     null: false
    t.string   "encrypted_password",      default: "",     null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",           default: 0,      null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "history_info",            default: [],                  array: true
    t.string   "ssn"
    t.string   "member_id"
    t.string   "referred_by"
    t.boolean  "contact_lenses"
    t.boolean  "interested_contact_lens"
    t.boolean  "in_lasik"
    t.string   "ethnicity"
    t.string   "eye_color"
    t.integer  "views_count",             default: 0
    t.float    "reward_point",            default: 0.0
    t.string   "survey_token"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "country"
    t.string   "intrests"
    t.string   "about"
    t.string   "profile_url"
    t.string   "bgurl"
    t.integer  "organisation_id"
    t.boolean  "pregnate"
    t.boolean  "breast_feeding"
    t.string   "place"
    t.date     "prev_exam"
    t.boolean  "tobacco_use"
    t.boolean  "alcohol_use"
    t.string   "subdomain"
    t.string   "gender",                  default: "male"
    t.string   "authentication_token"
    t.integer  "notification_method",     default: 3
    t.integer  "vision_insurance_id"
  end

  add_index "patients", ["authentication_token"], name: "index_patients_on_authentication_token", using: :btree
  add_index "patients", ["email", "subdomain"], name: "index_patients_on_email_and_subdomain", unique: true, using: :btree
  add_index "patients", ["reset_password_token"], name: "index_patients_on_reset_password_token", unique: true, using: :btree

  create_table "primlinary_exams", force: :cascade do |t|
    t.integer  "examination_id"
    t.string   "eoms",            default: "SAFE OD / OS"
    t.string   "pupils",          default: "PERRL (-) APD"
    t.string   "reaction"
    t.string   "confrontational", default: "FTFC OD / OS"
    t.string   "color_vision"
    t.string   "steropsis"
    t.string   "notes"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  create_table "referrals", force: :cascade do |t|
    t.integer  "examination_id"
    t.boolean  "medical",         default: true
    t.boolean  "social",          default: true
    t.boolean  "binocular",       default: true
    t.boolean  "primlinary",      default: true
    t.boolean  "visual",          default: true
    t.boolean  "procedure",       default: true
    t.boolean  "impression",      default: true
    t.boolean  "refraction",      default: true
    t.boolean  "contact",         default: true
    t.boolean  "external",        default: true
    t.boolean  "internal",        default: true
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "refer_to"
    t.string   "referral_reason"
    t.string   "message"
  end

  create_table "relatives", force: :cascade do |t|
    t.string   "name"
    t.string   "relationship"
    t.string   "phone"
    t.integer  "patient_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "seos", force: :cascade do |t|
    t.integer  "organisation_id"
    t.string   "view_name"
    t.string   "title"
    t.string   "description"
    t.string   "keywords"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "settings", force: :cascade do |t|
    t.string   "lens_sphere_start",    default: "-19.0"
    t.string   "lens_sphere_end",      default: "11.25"
    t.string   "lens_cylinder_start",  default: "-10.0"
    t.string   "lens_cylinder_end",    default: "-0.25"
    t.string   "lens_add_start",       default: "0.25"
    t.string   "lens_add_end",         default: "6.0"
    t.string   "glass_sphere_start",   default: "-19.0"
    t.string   "glass_sphere_end",     default: "11.25"
    t.string   "glass_cylinder_start", default: "-10.0"
    t.string   "glass_cylinder_end",   default: "-0.25"
    t.string   "glass_add_start",      default: "0.25"
    t.string   "glass_add_end",        default: "6.0"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "bgurl"
    t.integer  "organisation_id"
  end

  create_table "social_histories", force: :cascade do |t|
    t.integer  "examination_id"
    t.boolean  "tobacco_use",    default: false
    t.boolean  "alcohol_use",    default: false
    t.string   "notes"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "stores", force: :cascade do |t|
    t.integer  "organisation_id"
    t.integer  "manager_id"
    t.string   "phone_number"
    t.string   "description"
    t.integer  "office_address_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "facebook_url"
    t.string   "twitter_url"
    t.string   "linked_in_url"
    t.string   "youtube_url"
    t.string   "pintrest_url"
    t.string   "google_url"
    t.string   "instagram_url"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
  end

  create_table "surveys", force: :cascade do |t|
    t.integer  "patient_id"
    t.float    "professionalism",    default: 0.0
    t.float    "work_quality",       default: 0.0
    t.float    "schedule_adherence", default: 0.0
    t.float    "communication",      default: 0.0
    t.float    "cleanliness",        default: 0.0
    t.float    "staff",              default: 0.0
    t.text     "feedback"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "organisation_id"
  end

  create_table "twilio_webhooks", force: :cascade do |t|
    t.string   "sms_status",      default: [],              array: true
    t.string   "message_id"
    t.string   "from"
    t.string   "to"
    t.string   "from_country"
    t.string   "from_city"
    t.string   "body"
    t.integer  "organisation_id"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "vision_insurances", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.integer  "organisation_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "visual_acuities", force: :cascade do |t|
    t.string   "near_od"
    t.string   "near_os"
    t.string   "near_ou"
    t.string   "position"
    t.integer  "examination_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "distance_od"
    t.string   "distance_os"
    t.string   "distance_ou"
    t.string   "glass_type"
    t.boolean  "using_glasses",   default: false
    t.string   "intermediate_od"
    t.string   "intermediate_os"
    t.string   "intermediate_ou"
  end

  add_index "visual_acuities", ["examination_id"], name: "index_visual_acuities_on_examination_id", using: :btree

  create_table "working_schedules", force: :cascade do |t|
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.datetime "monday_start_time"
    t.datetime "monday_end_time"
    t.datetime "tuesday_start_time"
    t.datetime "tuesday_end_time"
    t.datetime "wednesday_start_time"
    t.datetime "wednesday_end_time"
    t.datetime "thursday_start_time"
    t.datetime "thursday_end_time"
    t.datetime "friday_start_time"
    t.datetime "friday_end_time"
    t.datetime "saturday_start_time"
    t.datetime "saturday_end_time"
    t.datetime "sunday_start_time"
    t.datetime "sunday_end_time"
  end

  add_foreign_key "addresses", "doctors"
  add_foreign_key "backgrounds", "doctors"
  add_foreign_key "backgrounds", "patients"
  add_foreign_key "bookings", "doctors"
  add_foreign_key "bookings", "joint_resources"
  add_foreign_key "examinations", "joint_resources"
  add_foreign_key "family_members", "organisations"
  add_foreign_key "family_members", "patients"
  add_foreign_key "follow_ups", "joint_resources"
  add_foreign_key "glasses", "joint_resources"
  add_foreign_key "joint_resources", "family_members"
  add_foreign_key "joint_resources", "organisations"
  add_foreign_key "joint_resources", "patients"
  add_foreign_key "medical_histories", "family_members"
  add_foreign_key "order_subscriptions", "joint_resources"
  add_foreign_key "orders", "joint_resources"
end
