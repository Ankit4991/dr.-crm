# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# create Doctors


# Dr Mathew Chrycy
mathew = Doctor.where(email: 'mathew@gmail.com').first_or_create(
   first_name: 'Mathew',
   last_name: 'Chrycy',
   role: ['super_administrator', 'administrator'],
   type: 'Doctor',
   subdomain: "brickellvision",
   email: 'mathew@gmail.com',
   mobile_number: '+13055879898',
   password: 12345678,
   confirmed_at: DateTime.now
)


# Dr Sara Thompson
sara = Doctor.where(first_name: 'Sara').first_or_create(
   first_name: 'Sara',
   last_name: 'Thompson',
   role: ['administrator'],
   type: 'Doctor',
   email: 'sara@gmail.com',
   subdomain: "brickellvision",
   mobile_number: '+13055879898',   
   password: 12345678,
   confirmed_at: DateTime.now
)

organisation = Organisation.where(company_name: 'Eyetrust Vision').first_or_create(
    company_name: 'Eyetrust Vision',
    admins: [mathew.id, sara.id],
    domain_name: "brickellvision",
    owner_id: mathew.id,
    approved: true,
    default_org: true
)

mathew.organisation_id = organisation.id
mathew.save!
sara.organisation_id = organisation.id
sara.save!

#create default store
brickellvision = organisation.stores.find_or_create_by(manager_id: mathew.id, description: "brickellvision", phone_number: "+13055879898")
brickellvision.doctor_ids=[mathew.id, sara.id]
brickellvision.save!
unless brickellvision.office_address.present?
 address=Address.new(address1: "Eyetrust Vision of Brickell", address2: "1201 Brickell Ave #300 ", city: "Miami", postal_code: 33131, email: "eyetrustvision@gmail.com", state_id: "FL", country_id: "US", organisation_id: organisation.id)
 address.save!
 brickellvision.office_address_id = address.id
 brickellvision.save!
end

site = Comfy::Cms::Site.where(identifier: organisation.domain_name).first_or_create(
  label: organisation.domain_name,
  identifier: organisation.domain_name,
  hostname: "itrust.io",
  locale: "en"
)
site.save!

layout = Comfy::Cms::Layout.where(app_layout: "application").first_or_create(
  site_id: site.id,
  app_layout: "application",
  label: "Application",
  identifier: "application",
  content: "{{ cms:field:meta_description:string }} {{ cms:page:content:rich_text }}",
  css: "",
  js: ""
)
layout.save!

page = Comfy::Cms::Page.where(label: "home").first_or_create(
  label: "home",
  site_id: site.id,
  layout_id: layout.id,
  full_path: "/",
  slug: nil
)
page.save!

CreateOrganisationBasicRecordsWorker.new.perform organisation