class CreateJoinTableExamProceduresOrders < ActiveRecord::Migration
  def change
    create_table :exam_procedures_orders do |t|
      t.references :order
      t.references :exam_procedure
    end
  end
end
