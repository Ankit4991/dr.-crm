class DropJoinTableContactLensOrdersLensTypesPrices < ActiveRecord::Migration
  def change
    drop_table :contact_lens_orders_lens_types_prices do |t|
      t.references :contact_lens_order
      t.references :lens_types_price
    end
    add_column :contact_lens_orders, :od_lens_types_price_id, :integer
    add_column :contact_lens_orders, :os_lens_types_price_id, :integer
  end
end
