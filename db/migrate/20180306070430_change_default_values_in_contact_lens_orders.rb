class ChangeDefaultValuesInContactLensOrders < ActiveRecord::Migration
  def up
    change_column :contact_lens_orders, :discount, :float, :default => 15.00
  end

  def down
    change_column :contact_lens_orders, :discount, :float, :default => 0.15
  end
end
