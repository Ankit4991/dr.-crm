class DropJoinTableEyeGlassOrdersEyeglassTypesPrices < ActiveRecord::Migration
  def change
    drop_table :eye_glass_orders_eyeglass_types_prices do |t|
      t.references :eye_glass_order
      t.references :eyeglass_types_price
    end
    add_column :eye_glass_orders, :eyeglass_types_price_id, :integer
  end
end
