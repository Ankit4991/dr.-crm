class AddFamilyMemberIdToMedicalHistory < ActiveRecord::Migration
  def change
    add_reference :medical_histories, :family_member, index: true, foreign_key: true
  end
end
