class AddUuidToOrdersAndFavouriteOrders < ActiveRecord::Migration
  def up
  	add_column :orders, :uuid, :string
  	add_column :favourite_orders, :uuid, :string
  end

  def down
  	remove_column :orders, :uuid, :string
  	remove_column :favourite_orders, :uuid, :string
  end
end
