class ChangeDefaultValueInContactLensOrder < ActiveRecord::Migration
  def up
    change_column :contact_lens_orders, :od_interval, :integer, :default => 0
    change_column :contact_lens_orders, :os_interval, :integer, :default => 0
  end

  def down
    change_column :contact_lens_orders, :od_interval, :integer
    change_column :contact_lens_orders, :os_interval, :integer
  end
end
