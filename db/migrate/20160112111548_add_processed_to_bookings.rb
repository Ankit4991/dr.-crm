class AddProcessedToBookings < ActiveRecord::Migration
  def up
    add_column :bookings, :processed, :boolean, :default => false
  end
  
  def down
    remove_column :bookings, :processed, :boolean, :default => false
  end
end
