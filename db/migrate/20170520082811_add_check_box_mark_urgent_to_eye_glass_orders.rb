class AddCheckBoxMarkUrgentToEyeGlassOrders < ActiveRecord::Migration
  def change
    add_column :eye_glass_orders, :check_box_mark_urgent, :boolean, :default => false
  end
end
