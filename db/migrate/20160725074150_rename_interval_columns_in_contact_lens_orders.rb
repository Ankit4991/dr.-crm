class RenameIntervalColumnsInContactLensOrders < ActiveRecord::Migration
  def change
  	rename_column :contact_lens_orders, :interval, :od_interval
  	add_column :contact_lens_orders, :os_interval, :integer
  end
end
