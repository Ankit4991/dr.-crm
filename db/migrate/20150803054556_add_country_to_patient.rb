class AddCountryToPatient < ActiveRecord::Migration
  def change
    add_column :patients, :country, :string
    add_column :patients, :intrests, :string
    add_column :patients, :about, :string
    add_column :patients, :profile_url, :string
    add_column :patients, :bgurl, :string
  end
end
