class AddOrganisationIdToLensesAndGlasses < ActiveRecord::Migration
  def change
  	add_column :lens, :organisation_id, :integer
  	add_column :glasses, :organisation_id, :integer
  end
end
