class AddMedicalFieldsToFamilyMembers < ActiveRecord::Migration
  def change
    add_column :family_members, :pmhxes_check, :boolean, :default => false
    add_column :family_members, :fmhxes_check, :boolean, :default => false
    add_column :family_members, :fohxes_check, :boolean, :default => false
    add_column :family_members, :pohxes_check, :boolean, :default => false
    add_column :family_members, :sxhxes_check, :boolean, :default => false
    add_column :family_members, :allergies_check, :boolean, :default => false
    add_column :family_members, :medications_check, :boolean, :default => false
  end
end
