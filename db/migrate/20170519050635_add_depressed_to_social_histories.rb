class AddDepressedToSocialHistories < ActiveRecord::Migration
  def change
    add_column :social_histories, :depressed, :boolean, :default => false
  end
end
