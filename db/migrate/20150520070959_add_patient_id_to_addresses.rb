class AddPatientIdToAddresses < ActiveRecord::Migration
  def change
    add_column :addresses, :patient_id, :integer
  end
end
