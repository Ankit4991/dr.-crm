class CreateVisualAcuities < ActiveRecord::Migration
  def change
    create_table :visual_acuities do |t|
      t.integer :od
      t.integer :os
      t.integer :ou
      t.string  :glass_type
      t.string  :position
      t.integer :examination_id, index: true
      t.timestamps null: false
    end
  end
end
