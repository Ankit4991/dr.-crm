class RemoveOdAddOsAddFromGlass < ActiveRecord::Migration
  def change
    remove_column :glasses, :od_add, :string
    remove_column :glasses, :os_add, :string
  end
end
