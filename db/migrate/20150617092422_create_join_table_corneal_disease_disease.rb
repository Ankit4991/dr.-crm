class CreateJoinTableCornealDiseaseDisease < ActiveRecord::Migration
  def change

  	create_table :corneal_diseases_diseases do |t|
      t.references :corneal_disease
      t.references :disease
  	end
  end
end
