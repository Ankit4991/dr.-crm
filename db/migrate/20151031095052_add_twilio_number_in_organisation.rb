class AddTwilioNumberInOrganisation < ActiveRecord::Migration
  def change
    add_column :organisations, :twilio_number, :string
  end
end
