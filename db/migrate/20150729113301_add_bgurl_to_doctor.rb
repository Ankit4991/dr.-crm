class AddBgurlToDoctor < ActiveRecord::Migration
  def change
    add_column :doctors, :bgurl, :string
    add_column :doctors, :DOB, :date
    add_column :doctors, :current_address_id, :integer
  end
end
