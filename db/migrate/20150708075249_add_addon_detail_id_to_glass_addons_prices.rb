class AddAddonDetailIdToGlassAddonsPrices < ActiveRecord::Migration
  def change
    add_column :glass_addons_prices, :addon_detail_id, :integer
  end
end
