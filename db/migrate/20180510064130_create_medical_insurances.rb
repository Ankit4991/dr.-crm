class CreateMedicalInsurances < ActiveRecord::Migration
  def change
    create_table :medical_insurances do |t|
      t.string :name
      t.string :code
      t.integer :organisation_id

      t.timestamps null: false
    end
  end
end
