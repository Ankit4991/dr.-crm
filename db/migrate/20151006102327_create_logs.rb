class CreateLogs < ActiveRecord::Migration
  def change
    create_table :logs do |t|
      t.string :trackable_type
      t.integer :trackable_id
      t.string :log_type
      t.string :sid
      t.string :message
      t.integer :organisation_id

      t.timestamps null: false
    end
  end
end
