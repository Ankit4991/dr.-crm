class AddFieldToExamProcedures < ActiveRecord::Migration
  def change
    add_column :exam_procedures, :position, :integer, :default => 0
  end
end
