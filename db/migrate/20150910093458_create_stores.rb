class CreateStores < ActiveRecord::Migration
  def change
    create_table :stores do |t|
      t.integer "organisation_id"
      t.integer "manager_id"
      t.string  "phone_number"
      t.string  "description"
      t.integer "office_address_id"

      t.timestamps null: false
    end
  end
end
