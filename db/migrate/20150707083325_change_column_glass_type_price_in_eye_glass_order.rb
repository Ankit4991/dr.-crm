class ChangeColumnGlassTypePriceInEyeGlassOrder < ActiveRecord::Migration
  def change
    change_column :eye_glass_orders, :glass_type_price, :float, :default => 0.0
  end
end
