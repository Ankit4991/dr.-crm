class CeateJoinTableDoctorPatientNotification < ActiveRecord::Migration
  def change
    
    create_table :doctors_notifications do |t|
      t.references :doctor
      t.references :notification
      t.boolean :status
    end
    
  end
end
