class AddHoldToEyeGlassOrders < ActiveRecord::Migration
  def change
    add_column :eye_glass_orders, :hold, :boolean,default: false
  end
end
