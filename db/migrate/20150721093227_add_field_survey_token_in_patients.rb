class AddFieldSurveyTokenInPatients < ActiveRecord::Migration
  def change
    add_column :patients, :survey_token, :string
  end
end
