class AddFlagForPhoneToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :flag_for_phone, :boolean
  end
end
