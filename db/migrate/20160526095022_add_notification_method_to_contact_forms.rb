class AddNotificationMethodToContactForms < ActiveRecord::Migration
  def change
    add_column :contact_forms, :notification_method, :integer, default: 3
  end
end
