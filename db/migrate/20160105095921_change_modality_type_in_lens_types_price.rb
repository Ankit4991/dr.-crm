class ChangeModalityTypeInLensTypesPrice < ActiveRecord::Migration
  def up
    remove_column :lens_types_prices, :modality, :string
    add_column :lens_types_prices, :modality, :integer, :default => 1
  end
  
  def down
    remove_column :lens_types_prices, :modality, :integer, :default => 1
    add_column :lens_types_prices, :modality, :string
  end
end
