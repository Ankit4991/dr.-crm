class CreateMailgunWebhooks < ActiveRecord::Migration
  def change
    create_table :mailgun_webhooks do |t|
      t.string "x_mailgun_sid" 
      t.text "message_id"     
      t.string "recipient"
      t.string "subject"
      t.string "trackable_type"
      t.string "trackable_id"
      t.string "sub_type"
      t.string  "events", array: true, default: []
      t.integer "open_count", default: 0
      t.integer "click_count", default: 0
      t.text "description"
      t.string "reason"
      t.timestamps null: false
    end
  end
end

