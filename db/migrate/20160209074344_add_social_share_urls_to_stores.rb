class AddSocialShareUrlsToStores < ActiveRecord::Migration
  def up
    add_column :stores, :facebook_url, :string
    add_column :stores, :twitter_url, :string
    add_column :stores, :linked_in_url, :string
    add_column :stores, :youtube_url, :string
    add_column :stores, :pintrest_url, :string
    add_column :stores, :google_url, :string
    add_column :stores, :instagram_url, :string
  end

  def down
  	remove_column :stores, :facebook_url, :string
    remove_column :stores, :twitter_url, :string
    remove_column :stores, :linked_in_url, :string
    remove_column :stores, :youtube_url, :string
    remove_column :stores, :pintrest_url, :string
    remove_column :stores, :google_url, :string
    remove_column :stores, :instagram_url, :string
  end

end
