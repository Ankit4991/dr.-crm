class AddEmailToStore < ActiveRecord::Migration
  def change
    add_column :stores, :contact_email, :string
  end
end
