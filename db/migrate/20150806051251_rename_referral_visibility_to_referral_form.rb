class RenameReferralVisibilityToReferralForm < ActiveRecord::Migration
  def change
  	 rename_table :referral_visibilities, :referrals
  end
end
