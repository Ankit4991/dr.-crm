class AddGenderToTables < ActiveRecord::Migration
  def change
  	add_column :patients, :gender, :string, :default => 'male'
  	add_column :doctors, :gender, :string, :default => 'male'
  	add_column :family_members, :gender, :string, :default => 'male'
  end
end
