class AddNotificationMethodFieldInPatient < ActiveRecord::Migration
  def change
    add_column :patients, :notification_method, :integer, default: 3
  end
end
