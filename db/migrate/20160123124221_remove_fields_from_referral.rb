class RemoveFieldsFromReferral < ActiveRecord::Migration
  def change
      remove_column :referrals, :r_objective, :boolean
      remove_column :referrals, :r_subjective, :boolean
      remove_column :referrals, :r_binocular, :boolean
      remove_column :referrals, :r_final_glass, :boolean
      remove_column :referrals, :r_cycloplegic, :boolean
      remove_column :referrals, :c_keratometry, :boolean
      remove_column :referrals, :c_subjective, :boolean
      remove_column :referrals, :c_current_lens, :boolean
      remove_column :referrals, :c_trial_lens, :boolean
      remove_column :referrals, :c_final_lens, :boolean
      remove_column :referrals, :e_corneal, :boolean
      remove_column :referrals, :e_gonioscopy, :boolean
      remove_column :referrals, :e_drawing, :boolean
      remove_column :referrals, :i_drawing, :boolean 

      add_column :referrals, :message, :string   
  end
end
