class AddFieldFinalizeInLensAndGlass < ActiveRecord::Migration
  def change
  	add_column :lens, :finalize, :boolean, default: false
  	add_column :eye_measurements, :finalize, :boolean, default: false
  end
end
