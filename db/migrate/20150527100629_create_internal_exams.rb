class CreateInternalExams < ActiveRecord::Migration
  def change
    create_table :internal_exams do |t|
    	t.integer :examination_id
    	t.string :od, default: '0.3'
    	t.boolean :od_pink, default: false
    	t.boolean :od_pallor, default: false
    	t.boolean :od_edema, default: false
    	t.string :os, default: '0.3'
    	t.boolean :os_pink, default: false
    	t.boolean :os_pallor, default: false
    	t.boolean :os_edema, default: false
    	t.string :vessels, default: '2/3 OU'
    	t.string :macula, default: '(+) Foveal Reflex OU'
    	t.string :post_pole, default: 'Clear OU'
    	t.string :periphery, default: 'Clear OU'
    	t.string :vitreous, default: 'Clear OU' 
    	t.string :draw_html  	
      t.timestamps null: false
    end
  end
end
