class RemovePatientIdFromExamination < ActiveRecord::Migration
  def change
    remove_column :examinations, :patient_id, :integer
    remove_column :examinations, :family_member_id, :integer
    remove_column :bookings, :patient_id, :integer
    remove_column :bookings, :family_member_id, :integer
    remove_column :glasses, :patient_id, :integer
    remove_column :glasses, :family_member_id, :integer
  end
end
