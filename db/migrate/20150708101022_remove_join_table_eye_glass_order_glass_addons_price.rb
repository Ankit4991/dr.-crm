class RemoveJoinTableEyeGlassOrderGlassAddonsPrice < ActiveRecord::Migration
  def change
    drop_join_table :eye_glass_orders, :glass_addons_prices
  end
end
