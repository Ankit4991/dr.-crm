class AddStatusToExaminations < ActiveRecord::Migration
  def change
    add_column :examinations, :status, :integer ,default: 0
  end
end
