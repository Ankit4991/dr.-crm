class DeviseCreatePatientForSubdomain < ActiveRecord::Migration
  def change
    add_column :patients, :subdomain, :string
    remove_index :patients, :email
    add_index :patients, [:email, :subdomain], :unique => true
  end
end
