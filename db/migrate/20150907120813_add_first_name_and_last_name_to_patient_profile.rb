class AddFirstNameAndLastNameToPatientProfile < ActiveRecord::Migration
  def change
    add_column :patient_profiles, :first_name, :string
    add_column :patient_profiles, :last_name, :string
  end
end
