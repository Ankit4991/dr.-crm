class AddDoctorIdToContactLensOrderAndEyeGlassOrder < ActiveRecord::Migration
  def change
    add_column :contact_lens_orders, :doctor_id, :integer
    add_column :eye_glass_orders, :doctor_id, :integer
  end
end
