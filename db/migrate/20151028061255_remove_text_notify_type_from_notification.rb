class RemoveTextNotifyTypeFromNotification < ActiveRecord::Migration
  def change
    remove_column :notifications, :text, :string
    remove_column :notifications, :notify_type, :string
  end
end
