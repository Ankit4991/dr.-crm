class AddMedicalToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :medical_insurance_id, :integer
  end
end
