class AddDestroyerIdDestroyerTypeToBooking < ActiveRecord::Migration
  def change
    add_column :bookings, :destroyer_id, :integer
    add_column :bookings, :destroyer_type, :string
  end
end
