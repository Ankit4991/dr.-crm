class AddAnxiousToSocialHistories < ActiveRecord::Migration
  def change
    add_column :social_histories, :anxious, :boolean, :default => false
  end
end
