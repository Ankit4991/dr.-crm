class AddGatColumnsInExternalExam < ActiveRecord::Migration
  def change
  	add_column :external_exams, :gat_first, :string
  	add_column :external_exams, :gat_last, :string
  end
end
