class ChangeDefaultValueInInternalExam < ActiveRecord::Migration
  def up
    change_column :internal_exams, :macula, :string, :default => 'Flat & Intact OU'
  end

  def down
    change_column :internal_exams, :macula, :string, :default => '(+) Foveal Reflex OU'
  end
end
