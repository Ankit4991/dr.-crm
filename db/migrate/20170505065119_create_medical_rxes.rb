class CreateMedicalRxes < ActiveRecord::Migration
  def change
    create_table :medical_rxes do |t|
      t.string  :medication
      t.string  :dosage
      t.integer :refill
      t.integer :examination_id

      t.timestamps null: false
    end
  end
end
