class AddFieldsToEyeMeasurements < ActiveRecord::Migration
  def change
    add_column :eye_measurements, :installed_at, :datetime
    add_column :eye_measurements, :cyclopelgic, :boolean
    add_column :eye_measurements, :cyclopelgic_text, :string
  end
end
