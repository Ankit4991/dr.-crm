class AddDefaultFieldInDiseases < ActiveRecord::Migration
  def change
    add_column :diseases, :default_active, :boolean, :default => false
  end
end
