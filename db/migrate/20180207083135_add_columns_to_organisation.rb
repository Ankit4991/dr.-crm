class AddColumnsToOrganisation < ActiveRecord::Migration
  def change
    add_column :organisations, :org_braintree_merchant_id, :string
    add_column :organisations, :org_braintree_public_key, :string
    add_column :organisations, :org_braintree_private_key, :string
  end
end
