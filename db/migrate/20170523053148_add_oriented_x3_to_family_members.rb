class AddOrientedX3ToFamilyMembers < ActiveRecord::Migration
  def change
    add_column :family_members, :oriented_x3, :boolean, :default => true
  end
end
