class AddTermsDateToPatient < ActiveRecord::Migration
  def change
    add_column :patients, :accept_terms, :boolean, :default => false
    add_column :patients, :sign_date, :date
  end
end
