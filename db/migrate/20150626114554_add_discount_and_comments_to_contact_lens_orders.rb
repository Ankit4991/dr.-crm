class AddDiscountAndCommentsToContactLensOrders < ActiveRecord::Migration
  def change
    add_column :contact_lens_orders, :discount, :float,default: 0.15
    add_column :contact_lens_orders, :comments, :string
  end
end
