class AddRewardPointsUsedToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :reward_points_used, :float
  end
end
