class AddFieldBrandAndExpiryDateInPayementProfile < ActiveRecord::Migration
  def change
    add_column :patient_profiles, :brand_name, :string
    add_column :patient_profiles, :card_expiry_date, :date
  end
end
