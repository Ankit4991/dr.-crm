class AddFieldsToMedicalRxes < ActiveRecord::Migration
  def change
    add_column :medical_rxes, :issued_date, :date
    add_column :medical_rxes, :expired_date, :date
  end
end
