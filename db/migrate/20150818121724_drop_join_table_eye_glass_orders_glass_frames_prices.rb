class DropJoinTableEyeGlassOrdersGlassFramesPrices < ActiveRecord::Migration
  def change
    drop_table :eye_glass_orders_glass_frames_prices do |t|
      t.references :eye_glass_order
      t.references :glass_frames_price
    end
    add_column :eye_glass_orders, :glass_frames_price_id, :integer
  end
end
