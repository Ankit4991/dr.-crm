class RemoveIssuedDateFromLensPrescriptions < ActiveRecord::Migration
  def change
  	remove_column :lens_prescriptions, :issued_date, :date
  	remove_column :lens_prescriptions, :expired_date, :date
  	remove_column :glasses_prescriptions, :issued_date, :date
  	remove_column :glasses_prescriptions, :expired_date, :date
  end
end
