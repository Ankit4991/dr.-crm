class AddOrderIdToExamProcedures < ActiveRecord::Migration
  def change
    add_column :exam_procedures, :order_id, :integer
  end
end
