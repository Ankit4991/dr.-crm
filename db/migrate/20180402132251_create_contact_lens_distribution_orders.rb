class CreateContactLensDistributionOrders < ActiveRecord::Migration
  def change
    create_table :contact_lens_distribution_orders do |t|
      t.string :comments
      t.integer :quantity
      t.integer :organisation_id
      t.integer :doctor_id
      t.integer :lens_id

      t.timestamps null: false
    end
  end
end
