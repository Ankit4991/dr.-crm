class AddAuthenticationTokenToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :authentication_token, :string
    add_index :patients, :authentication_token
  end
end
