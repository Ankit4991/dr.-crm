class AddFieldCommentAndRemoveCommentsFromGlassesPrescription < ActiveRecord::Migration
  def change
    add_column :glasses_prescriptions, :comment, :string
    remove_column :glasses_prescriptions, :comments
  end
end
