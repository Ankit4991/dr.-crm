class AddOrganisationIdAndOrderableToOrderTransactions < ActiveRecord::Migration
  def up
    add_column :order_transactions, :organisation_id, :integer
    add_reference :order_transactions, :orderable, polymorphic: true, index: true
    remove_column :order_transactions, :order_id, :integer
    remove_column :order_transactions, :favourite_order_id, :integer
  end
  
  def down
    remove_column :order_transactions, :organisation_id, :integer
    remove_reference :order_transactions, :orderable, polymorphic: true, index: true
    add_column :order_transactions, :order_id, :integer
    add_column :order_transactions, :favourite_order_id, :integer
  end
end
