class AddFieldToDoctorRecommendations < ActiveRecord::Migration
  def change
    add_column :doctor_recommendations, :position, :integer
  end
end
