class AddOdLensQuantityAndOsLensQuantityToOrderSubscriptions < ActiveRecord::Migration
  def change
    add_column :order_subscriptions, :od_lens_quantity, :integer, :default => 1
    add_column :order_subscriptions, :os_lens_quantity, :integer, :default => 1
  end
end
