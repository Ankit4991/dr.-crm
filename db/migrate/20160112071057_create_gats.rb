class CreateGats < ActiveRecord::Migration
  def change
    create_table :gats do |t|
    	t.string :gat_first
    	t.string :gat_last
    	t.time :gat_time
    	t.integer :external_exam_id

      t.timestamps null: false
    end
  end
end
