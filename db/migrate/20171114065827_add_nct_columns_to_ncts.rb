class AddNctColumnsToNcts < ActiveRecord::Migration
  def change
    add_column :ncts, :nct_first, :string
    add_column :ncts, :nct_last, :string
    add_column :ncts, :nct_time, :time
    add_column :ncts, :external_exam_id, :integer
    add_column :ncts, :created_at, :datetime, null: false
    add_column :ncts, :updated_at, :datetime, null: false
  end
end
