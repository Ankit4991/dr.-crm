class ChangeCorneaDefaultValueInExaternalExam < ActiveRecord::Migration
  def up
    change_column :external_exams, :cornea, :string, :default => "Clear OU"
  end
end