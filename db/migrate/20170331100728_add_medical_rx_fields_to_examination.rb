class AddMedicalRxFieldsToExamination < ActiveRecord::Migration
  def change
    add_column :examinations, :medication, :string
    add_column :examinations, :dosage, :string
    add_column :examinations, :refill_start, :integer
    add_column :examinations, :refill_end, :integer
  end
end
