class AddVisualFieldToExternalExams < ActiveRecord::Migration
  def change
    add_column :external_exams, :visual_field, :string
  end
end
