class AddMedicalNotesToFamilyMembers < ActiveRecord::Migration
  def change
    add_column :family_members, :medical_notes, :string
  end
end
