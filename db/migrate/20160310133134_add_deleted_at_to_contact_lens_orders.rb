class AddDeletedAtToContactLensOrders < ActiveRecord::Migration
  def change
    add_column :contact_lens_orders, :deleted_at, :datetime
    add_index :contact_lens_orders, :deleted_at
  end
end
