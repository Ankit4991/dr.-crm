class AddRequestToSeeAndReferralReasonColumnInExamination < ActiveRecord::Migration
  def change
  	add_column :examinations, :refer_to, :string
  	add_column :examinations, :referral_reason, :string
  end
end
