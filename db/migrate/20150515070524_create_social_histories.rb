class CreateSocialHistories < ActiveRecord::Migration
  def change
    create_table :social_histories do |t|    	
      t.integer :examination_id
      t.boolean :tobacco_use, default: false
      t.boolean :alcohol_use, default: false
      t.string  :notes
      t.timestamps null: false      
    end
  end
end
