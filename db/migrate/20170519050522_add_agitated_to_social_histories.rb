class AddAgitatedToSocialHistories < ActiveRecord::Migration
  def change
    add_column :social_histories, :agitated, :boolean, :default => false
  end
end
