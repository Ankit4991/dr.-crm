class RemoveOrderIdFromExamProcedures < ActiveRecord::Migration
  def change
    remove_column :exam_procedures, :order_id, :integer
  end
end
