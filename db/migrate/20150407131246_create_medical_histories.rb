class CreateMedicalHistories < ActiveRecord::Migration
  def change
    create_table :medical_histories do |t|
      t.integer :examination_id
      t.integer :disease_id
      t.string  :disease_name
      t.string  :notes
      t.string  :type
      t.timestamps null: false
    end
  end
end
