class CreateExamProcedures < ActiveRecord::Migration
  def change
    create_table :exam_procedures do |t|
    	t.string :name
    	t.string :code
    	t.float :price
      t.timestamps null: false
    end
  end
end
