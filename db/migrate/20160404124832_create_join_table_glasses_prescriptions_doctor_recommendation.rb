class CreateJoinTableGlassesPrescriptionsDoctorRecommendation < ActiveRecord::Migration
  def change
    create_table :doctor_recommendations_glasses_prescriptions do |t|
      t.references :doctor_recommendation
      t.references :glasses_prescription
    end
  end
end
