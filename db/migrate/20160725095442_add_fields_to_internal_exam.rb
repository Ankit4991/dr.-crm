class AddFieldsToInternalExam < ActiveRecord::Migration
  def change
    add_column :internal_exams, :drops_used, :string
    add_column :internal_exams, :installed_at, :datetime
    add_column :internal_exams, :dilation_od, :string
    add_column :internal_exams, :dilation_os, :string
  end
end
