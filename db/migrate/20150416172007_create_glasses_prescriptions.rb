class CreateGlassesPrescriptions < ActiveRecord::Migration
  def change
    create_table :glasses_prescriptions do |t|
      t.integer   :eye_measurement_id, index: true
      t.integer   :examination_id, index: true
      t.date      :issued_date
      t.date      :expired_date
      t.json      :comments
      t.timestamps null: false
    end
  end
end
