class AddOrganisationIdToExamFormDefaults < ActiveRecord::Migration
  def change
    add_column :exam_form_defaults, :organisation_id, :integer
  end
end
