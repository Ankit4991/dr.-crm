class AddContactIdReferrals < ActiveRecord::Migration
  def change
    add_column :referrals, :contact_ids, :string, array: true, default: []
  end
end
