class AddFavouriteOrderIdToOrderTransaction < ActiveRecord::Migration
  def change
    add_column :order_transactions, :favourite_order_id, :integer
  end
end
