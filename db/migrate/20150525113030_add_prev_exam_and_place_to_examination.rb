class AddPrevExamAndPlaceToExamination < ActiveRecord::Migration
  def change
    add_column :examinations, :place, :string
    add_column :examinations, :prev_exam, :date
  end
end
