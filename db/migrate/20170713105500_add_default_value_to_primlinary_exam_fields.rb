class AddDefaultValueToPrimlinaryExamFields < ActiveRecord::Migration
  def up
    change_column :primlinary_exams, :color_vision, :string, default: "8/8"
  end

  def down
    change_column :primlinary_exams, :color_vision, :string
  end
end
