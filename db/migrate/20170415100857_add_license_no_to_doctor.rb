class AddLicenseNoToDoctor < ActiveRecord::Migration
  def change
    add_column :doctors, :license_no, :string
  end
end
