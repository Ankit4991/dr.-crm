class AddAnxiousToFamilyMembers < ActiveRecord::Migration
  def change
    add_column :family_members, :anxious, :boolean, :default => false
  end
end
