class UpdateFieldLidsLashesInExternalExam < ActiveRecord::Migration
  def change
    change_column :external_exams, :lids_lashes, :string, :default => "Clear OU"
  end
end
