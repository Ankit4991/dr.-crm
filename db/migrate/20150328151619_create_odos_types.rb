class CreateOdosTypes < ActiveRecord::Migration
  def change
    create_table :odos_types do |t|
      t.string :name
      t.timestamps null: false
    end
  end
end
