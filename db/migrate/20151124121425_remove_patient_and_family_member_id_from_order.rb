class RemovePatientAndFamilyMemberIdFromOrder < ActiveRecord::Migration
  def change
    remove_column :orders, :patient_id, :integer
    remove_column :orders, :family_member_id, :integer
  end
end
