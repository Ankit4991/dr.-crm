class AddFieldsToLensTypesPrice < ActiveRecord::Migration
  def change
    add_column :lens_types_prices, :sphere, :string, :default => ''
    add_column :lens_types_prices, :cylinder, :string, :default => ''
    add_column :lens_types_prices, :axis, :string, :default => ''
    add_column :lens_types_prices, :add, :string, :default => ''
  end
end
