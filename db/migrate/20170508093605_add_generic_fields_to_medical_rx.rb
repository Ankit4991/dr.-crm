class AddGenericFieldsToMedicalRx < ActiveRecord::Migration
  def change
    add_column :medical_rxes, :generic, :boolean, :default => false
  end
end
