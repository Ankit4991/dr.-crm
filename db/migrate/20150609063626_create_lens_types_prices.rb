class CreateLensTypesPrices < ActiveRecord::Migration
  def change
    create_table :lens_types_prices do |t|
      t.string :lenstype
      t.float :price

      t.timestamps null: false
    end
  end
end
