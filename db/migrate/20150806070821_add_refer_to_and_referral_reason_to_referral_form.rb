class AddReferToAndReferralReasonToReferralForm < ActiveRecord::Migration
  def change
  	add_column :referrals, :refer_to, :string
  	add_column :referrals, :referral_reason, :string
  end
end
