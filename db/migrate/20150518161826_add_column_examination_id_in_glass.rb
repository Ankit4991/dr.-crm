class AddColumnExaminationIdInGlass < ActiveRecord::Migration
  def change
  	add_column :glasses, :examination_id, :integer
  end
end
