class AddCheckmarkToFavouriteOrders < ActiveRecord::Migration
  def change
    add_column :favourite_orders, :lens_od_checkmark, :boolean,default: false
    add_column :favourite_orders, :lens_os_checkmark, :boolean,default: false
    add_column :favourite_orders, :joint_resource_id, :integer
  end
end
