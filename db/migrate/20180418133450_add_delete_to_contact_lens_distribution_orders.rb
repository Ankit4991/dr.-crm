class AddDeleteToContactLensDistributionOrders < ActiveRecord::Migration
  def change
    add_column :contact_lens_distribution_orders, :soft_delete, :boolean, default: false
  end
end
