class AddStoreIdToDoctor < ActiveRecord::Migration
  def change
    add_column :doctors, :store_id, :integer
  end
end
