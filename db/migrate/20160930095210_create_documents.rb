class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.attachment :docs
      t.integer :examination_id
      t.timestamps null: false
      t.string :type_of_file, :default => ''
    end
  end
end
