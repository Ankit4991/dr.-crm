class CreateAddonDetails < ActiveRecord::Migration
  def change
    create_table :addon_details do |t|
      t.string :color
      t.integer :eye_glass_order_id
      t.timestamps null: false
    end
  end
end
