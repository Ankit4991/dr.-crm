class AddSegmentHeightAndBaseCurveFieldsToGlasses < ActiveRecord::Migration
  def change
    add_column :glasses, :od_segment_height, :string
    add_column :glasses, :os_segment_height, :string
    add_column :glasses, :od_base_curve, :string
    add_column :glasses, :os_base_curve, :string
  end
end
