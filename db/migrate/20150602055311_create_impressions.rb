class CreateImpressions < ActiveRecord::Migration
  def change
    create_table :impressions do |t|
    	t.string :name
    	t.string :code
      t.timestamps null: false
    end
  end
end
