class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.integer :event_id
      t.string :event_type
      t.string :notify_type
      t.string :text
      t.boolean :status

      t.timestamps null: false
    end
  end
end
