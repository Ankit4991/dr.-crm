class RemoveAddonDetailIdFromGlassAddonsPrice < ActiveRecord::Migration
  def change
    remove_column :glass_addons_prices, :addon_detail_id, :integer
  end
end
