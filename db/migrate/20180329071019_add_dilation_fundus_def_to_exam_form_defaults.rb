class AddDilationFundusDefToExamFormDefaults < ActiveRecord::Migration
  def change
    add_column :exam_form_defaults, :dilation_fundus_def, :string, :default => "Patient refuses dilated fundus examination after explanation of tests"
  end
end
