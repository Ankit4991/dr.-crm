class AddHoldToContactLensOrders < ActiveRecord::Migration
  def change
    add_column :contact_lens_orders, :hold, :boolean,default: false
  end
end
