class ChangeFormatInContacts < ActiveRecord::Migration
   def change
    change_column :contacts, :phone_number, :string
    change_column :contacts, :fax, :string
    change_column :contacts, :account_number, :string
  end
end
