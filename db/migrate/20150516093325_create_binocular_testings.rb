class CreateBinocularTestings < ActiveRecord::Migration
  def change
    create_table :binocular_testings do |t|
    	t.integer :examination_id
    	t.string  :ctd  #Cover Test Distance
    	t.string  :ctn  #Cover Test near
    	t.string  :nra
    	t.string  :pra
    	t.string  :hpd  #Horizontal Phorias Distance
    	t.string  :hpn  #Horizontal Phorias Near
    	t.string  :vpd  #Vertical Phorias Distance
    	t.string  :vpn  #Vertical Phorias Near
    	t.string  :notes
      t.timestamps null: false
    end
  end
end
