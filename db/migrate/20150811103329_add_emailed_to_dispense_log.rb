class AddEmailedToDispenseLog < ActiveRecord::Migration
  def change
    add_column :dispense_logs, :emailed, :boolean,default: false
  end
end
