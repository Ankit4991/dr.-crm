class AddUpdatedAtToExamProcedureExamination < ActiveRecord::Migration
  def change
    change_table :exam_procedures_examinations do |t|
      t.timestamps null: false, default: Time.now
    end
  end
end
