class RemoveFieldDaysFromFollowUps < ActiveRecord::Migration
  def change
    remove_column :follow_ups, :days, :integer
  end
end
