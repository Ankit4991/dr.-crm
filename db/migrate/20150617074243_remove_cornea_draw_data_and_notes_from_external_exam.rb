class RemoveCorneaDrawDataAndNotesFromExternalExam < ActiveRecord::Migration
  def change
    remove_column :external_exams, :cornea_draw_data, :json
    remove_column :external_exams, :notes, :string
  end
end
