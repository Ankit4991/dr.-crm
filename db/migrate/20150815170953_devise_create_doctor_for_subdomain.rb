class DeviseCreateDoctorForSubdomain < ActiveRecord::Migration
  def change
    add_column :doctors, :subdomain, :string
    remove_index :doctors, :email
    add_index :doctors, [:email, :subdomain], :unique => true
  end
end
