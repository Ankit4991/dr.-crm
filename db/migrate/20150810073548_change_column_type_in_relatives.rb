class ChangeColumnTypeInRelatives < ActiveRecord::Migration
  def change
    change_column :relatives, :name, :string
  end
end
