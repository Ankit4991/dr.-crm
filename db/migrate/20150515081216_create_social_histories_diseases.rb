class CreateSocialHistoriesDiseases < ActiveRecord::Migration
  def change
    create_table :diseases_social_histories, :id => false do |t|
        t.references :disease
        t.references :social_history
    end
  end
end
