class CreateExamCleaningSolutions < ActiveRecord::Migration
  def change
    create_table :exam_cleaning_solutions do |t|
      t.references :lens_prescription, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
