class AddLensPrescriptionIdToContactLensOrders < ActiveRecord::Migration
  def change
    add_column :contact_lens_orders, :lens_prescription_id, :integer
  end
end
