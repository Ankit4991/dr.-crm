class AddTransactionTypeToOrderTransactions < ActiveRecord::Migration
  def change
    add_column :order_transactions, :transaction_type, :integer ,default: 0
    add_column :order_transactions, :reference_trans_id, :string
  end
end
