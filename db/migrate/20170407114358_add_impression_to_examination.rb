class AddImpressionToExamination < ActiveRecord::Migration
  def change
    add_column :examinations, :impression, :string
  end
end
