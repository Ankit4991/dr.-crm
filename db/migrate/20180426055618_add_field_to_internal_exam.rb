class AddFieldToInternalExam < ActiveRecord::Migration
  def change
    add_column :internal_exams, :dilation_drop3, :boolean, default: false
    add_column :internal_exams, :dilation_drop4, :boolean, default: false
  end
end
