class AddInternalExamFieldsToExamFormDefaults < ActiveRecord::Migration
  def change
    add_column :exam_form_defaults, :od_def, :string
    add_column :exam_form_defaults, :vessels_def, :string
    add_column :exam_form_defaults, :os_def, :string
    add_column :exam_form_defaults, :macula_def, :string
    add_column :exam_form_defaults, :post_pole_def, :string
    add_column :exam_form_defaults, :vitreous_def, :string
    add_column :exam_form_defaults, :periphery_def, :string
    add_column :exam_form_defaults, :reason_field_def, :string
    add_column :exam_form_defaults, :dilation_od_def, :string
    add_column :exam_form_defaults, :dilation_os, :string
  end
end
