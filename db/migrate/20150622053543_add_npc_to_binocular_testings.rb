class AddNpcToBinocularTestings < ActiveRecord::Migration
  def change
    add_column :binocular_testings, :npc, :string
  end
end
