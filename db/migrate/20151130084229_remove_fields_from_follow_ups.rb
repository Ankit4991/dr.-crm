class RemoveFieldsFromFollowUps < ActiveRecord::Migration
  def change
    remove_column :follow_ups, :patient_id, :integer
    remove_column :follow_ups, :family_member_id, :integer
  end
end
