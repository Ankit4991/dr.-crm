class AddFieldToExamFormDefault < ActiveRecord::Migration
  def change
    add_column :exam_form_defaults, :tropicomide, :string
    add_column :exam_form_defaults, :phenylephrine, :string
    add_column :exam_form_defaults, :fundus, :string
    add_column :exam_form_defaults, :ophthalmoscopy, :string

    add_column :exam_form_defaults, :cyclopelgic_text, :string
    add_column :exam_form_defaults, :cyclopelgic1_text, :string
    add_column :exam_form_defaults, :cyclopelgic2_text, :string
    add_column :exam_form_defaults, :cyclopelgic3_text, :string
  end
end
