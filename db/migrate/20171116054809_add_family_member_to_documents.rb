class AddFamilyMemberToDocuments < ActiveRecord::Migration
  def change
    add_reference :documents, :family_member, index: true, foreign_key: true
  end
end
