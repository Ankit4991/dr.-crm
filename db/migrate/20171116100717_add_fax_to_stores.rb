class AddFaxToStores < ActiveRecord::Migration
  def change
    add_column :stores, :fax, :string
  end
end
