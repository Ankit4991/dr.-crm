class AddIntervalToContactLensOrders < ActiveRecord::Migration
  def change
  	add_column :contact_lens_orders, :interval, :integer, :default => 1
  end
end
