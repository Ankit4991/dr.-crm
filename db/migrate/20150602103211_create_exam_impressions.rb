class CreateExamImpressions < ActiveRecord::Migration
  def change
    create_table :exam_impressions do |t|
    	t.integer :impression_id
    	t.integer :examination_id
    	t.string  :plan
    	t.string  :eye, default: "OU"

      t.timestamps null: false
    end
  end
end
