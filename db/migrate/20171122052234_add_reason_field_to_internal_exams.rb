class AddReasonFieldToInternalExams < ActiveRecord::Migration
  def change
    add_column :internal_exams, :reason_field, :string
  end
end
