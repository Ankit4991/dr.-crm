class AddPrelmNotesDefToExamFormDefaults < ActiveRecord::Migration
  def change
    add_column :exam_form_defaults, :prelm_notes_def, :string
  end
end
