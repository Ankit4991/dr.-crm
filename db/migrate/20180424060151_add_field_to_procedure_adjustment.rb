class AddFieldToProcedureAdjustment < ActiveRecord::Migration
  def change
    add_column :procedure_adjustments, :prior_exam_follow_up, :boolean, default: false
  end
end
