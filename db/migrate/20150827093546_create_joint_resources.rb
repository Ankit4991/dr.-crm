class CreateJointResources < ActiveRecord::Migration
  def change
    create_table :joint_resources do |t|
      t.references :patient, index: true, foreign_key: true
      t.references :family_member, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
