class AddStoreIdInWorkingSchedule < ActiveRecord::Migration
  def change
    add_column :working_schedules, :trackable_id, :integer
    add_column :working_schedules, :trackable_type, :string
    remove_column :working_schedules, :doctor_id
  end
end
