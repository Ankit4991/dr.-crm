class AddNctColumnsToExternalExams < ActiveRecord::Migration
  def change
    add_column :external_exams, :nct_first, :string
    add_column :external_exams, :nct_last, :string
    add_column :external_exams, :nct_time, :string
  end
end
