class AddFieldThemeColorInDoctor < ActiveRecord::Migration
  def change
  	 add_column :doctors, :theme_color, :string, default: "blue"
  end
end
