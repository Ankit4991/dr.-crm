class CreateContactForms < ActiveRecord::Migration
  def change
    create_table :contact_forms do |t|
      t.integer "organisation_id"
      t.integer "patient_id"
      t.string  "name"
      t.string "email"
      t.string  "phone_number"
      t.string  "comment"
      t.string  "priority"
      t.boolean "existing", default: false
      t.timestamps null: false
    end
  end
end
