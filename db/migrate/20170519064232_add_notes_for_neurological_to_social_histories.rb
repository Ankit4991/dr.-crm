class AddNotesForNeurologicalToSocialHistories < ActiveRecord::Migration
  def change
    add_column :social_histories, :notes_for_neurological, :string
  end
end
