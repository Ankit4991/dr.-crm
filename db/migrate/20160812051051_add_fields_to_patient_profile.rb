class AddFieldsToPatientProfile < ActiveRecord::Migration
  def change
    add_column :patient_profiles, :address, :string, :default => ''
    add_column :patient_profiles, :city, :string, :default => ''
    add_column :patient_profiles, :zip_code, :string, :default => ''
    add_column :patient_profiles, :state, :string, :default => ''
    add_column :patient_profiles, :country, :string, :default => ''
    add_column :patient_profiles, :customer_shipping_address_id, :string, :default => ''
  end
end
