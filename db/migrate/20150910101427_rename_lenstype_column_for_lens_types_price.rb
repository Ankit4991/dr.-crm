class RenameLenstypeColumnForLensTypesPrice < ActiveRecord::Migration
  def change
    rename_column :lens_types_prices, :lenstype, :lens_type
  end
end
