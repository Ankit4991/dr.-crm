class CreateCornealDiseases < ActiveRecord::Migration
  def change
    create_table :corneal_diseases do |t|
      t.json :draw_data
      t.string :notes

      t.timestamps null: false
    end
  end
end
