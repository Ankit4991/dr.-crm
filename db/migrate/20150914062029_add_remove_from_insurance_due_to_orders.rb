class AddRemoveFromInsuranceDueToOrders < ActiveRecord::Migration
  def change
  	add_column :orders, :remove_from_insurance_due, :boolean, default: false 
  end
end
