class CreatePatientProfiles < ActiveRecord::Migration
  def change
    create_table :patient_profiles do |t|
      t.string :customer_profile_id
      t.string :customer_payment_profile_id
      t.integer :patient_id
      t.timestamps null: false
    end
  end
end
