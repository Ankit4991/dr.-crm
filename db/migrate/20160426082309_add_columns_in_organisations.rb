class AddColumnsInOrganisations < ActiveRecord::Migration
  def up
    add_column :organisations, :facebook_url, :string
    add_column :organisations, :twitter_url, :string
    add_column :organisations, :linked_in_url, :string
    add_column :organisations, :youtube_url, :string
    add_column :organisations, :pintrest_url, :string
    add_column :organisations, :google_url, :string
    add_column :organisations, :instagram_url, :string
  end

  def down
    remove_column :organisations, :facebook_url, :string
    remove_column :organisations, :twitter_url, :string
    remove_column :organisations, :linked_in_url, :string
    remove_column :organisations, :youtube_url, :string
    remove_column :organisations, :pintrest_url, :string
    remove_column :organisations, :google_url, :string
    remove_column :organisations, :instagram_url, :string
  end
end
