class AddFieldDoctorIdInHolidays < ActiveRecord::Migration
  def change
    add_column :holidays, :doctor_id, :integer
    add_column :holidays, :reason, :string
  end
end
