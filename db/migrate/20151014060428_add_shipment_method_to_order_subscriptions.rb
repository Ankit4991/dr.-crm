class AddShipmentMethodToOrderSubscriptions < ActiveRecord::Migration
  def change
  	add_column :order_subscriptions, :shippment_method, :integer,default: 1
  	add_column :order_subscriptions, :status, :integer,default: 0
  end
end
