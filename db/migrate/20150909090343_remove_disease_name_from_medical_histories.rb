class RemoveDiseaseNameFromMedicalHistories < ActiveRecord::Migration
  def change
  	remove_column :medical_histories, :disease_name, :string
  end
end
