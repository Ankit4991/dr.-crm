class ChangeColumnBaseCurveDatatypeInLensTypesPrice < ActiveRecord::Migration
  def change
    remove_column :lens_types_prices, :base_curve, :string
    add_column :lens_types_prices, :base_curve, :text, array:true, default: []
  end
end
