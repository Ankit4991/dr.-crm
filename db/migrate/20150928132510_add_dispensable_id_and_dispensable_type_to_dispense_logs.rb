class AddDispensableIdAndDispensableTypeToDispenseLogs < ActiveRecord::Migration
  def up
    add_column :dispense_logs, :dispensable_id, :integer
    add_column :dispense_logs, :dispensable_type, :string
    remove_column :dispense_logs,:order_id,:integer
  end
  def down
    remove_column :dispense_logs, :dispensable_id, :integer
    remove_column :dispense_logs, :dispensable_type, :string
    add_column :dispense_logs,:order_id,:integer
  end
end
