class AddAdjustedExamProceduresToOrders < ActiveRecord::Migration
  def up
    add_column :orders, :adjusted_exam_procedures, :hstore, default: '', null: false
  end

  def down
  	remove_column :orders, :adjusted_exam_procedures, :hstore, default: '', null: false
  end
end
