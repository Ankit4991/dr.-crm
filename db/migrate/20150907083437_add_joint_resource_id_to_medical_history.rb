class AddJointResourceIdToMedicalHistory < ActiveRecord::Migration
  def change
    add_column :medical_histories, :joint_resource_id, :integer
  end
end
