class AddOldExamShowToExaminations < ActiveRecord::Migration
  def change
    add_column :examinations, :old_glass_show, :boolean, :default => true
    add_column :examinations, :old_lens_show, :boolean, :default => true
  end
end
