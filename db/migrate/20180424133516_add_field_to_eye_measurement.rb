class AddFieldToEyeMeasurement < ActiveRecord::Migration
  def change
    add_column :eye_measurements, :cyclopelgic1, :boolean
    add_column :eye_measurements, :cyclopelgic2, :boolean
    add_column :eye_measurements, :cyclopelgic3, :boolean
  end
end
