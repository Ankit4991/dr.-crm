class CreateOrderTransactions < ActiveRecord::Migration
  def change
    create_table :order_transactions do |t|
      t.integer :order_id
      t.integer :status
      t.float :amount
      t.string :response_id

      t.timestamps null: false
    end
  end
end
