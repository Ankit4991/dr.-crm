class AddFieldToExamination < ActiveRecord::Migration
  def change
    add_column :examinations, :current_glass_show, :boolean, :default => false
  end
end
