class RemoveOrganisationIdFromOrderTransactions < ActiveRecord::Migration
  def change
  	remove_column :order_transactions, :organisation_id, :integer
  end
end
