class AddDefaultValueToReaction < ActiveRecord::Migration
  def up
    change_column :primlinary_exams, :reaction, :string, default: "brisk"
  end

  def down
    change_column :primlinary_exams, :reaction, :string
  end
end
