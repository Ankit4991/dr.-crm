class AddWithCorrectionShowToExaminations < ActiveRecord::Migration
  def change
    add_column :examinations, :with_correction_show, :boolean, :default => false
  end
end
