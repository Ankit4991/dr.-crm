class AddDepressedToFamilyMembers < ActiveRecord::Migration
  def change
    add_column :family_members, :depressed, :boolean, :default => false
  end
end
