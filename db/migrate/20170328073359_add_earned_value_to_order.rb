class AddEarnedValueToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :earned_point_on_val, :float, :default => 0.0
  end
end
