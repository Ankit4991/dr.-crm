class AddFieldDueDateToFollowUps < ActiveRecord::Migration
  def change
    add_column :follow_ups, :due_date, :datetime
  end
end
