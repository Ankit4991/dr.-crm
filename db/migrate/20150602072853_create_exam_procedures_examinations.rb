class CreateExamProceduresExaminations < ActiveRecord::Migration
  def change
    create_table :exam_procedures_examinations do |t|
    	t.references :examination
      t.references :exam_procedure
    end
  end
end
