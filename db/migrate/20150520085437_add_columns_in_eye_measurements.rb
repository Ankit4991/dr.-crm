class AddColumnsInEyeMeasurements < ActiveRecord::Migration
  def up
  	add_column :eye_measurements, :measurement_type, :string
  	add_column :eye_measurements, :notes, :string
  	add_column :eye_measurements, :examination_id, :integer
  end
  def down
  	remove_column :eye_measurements, :measurement_type, :string
  	remove_column :eye_measurements, :notes, :string
  	remove_column :eye_measurements, :examination_id, :integer
  end
end
