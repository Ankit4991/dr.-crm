class AddShippmentMethodToFavouriteOrders < ActiveRecord::Migration
  def change
    add_column :favourite_orders, :shippment_method, :integer,default: 1
    add_column :favourite_orders, :reward_points_used, :float
    add_column :favourite_orders, :discount, :float
  end
end
