class AddSelectedFinalizeToGlasses < ActiveRecord::Migration
  def change
    add_column :glasses, :selected_finalize, :boolean,:default => false
  end
end
