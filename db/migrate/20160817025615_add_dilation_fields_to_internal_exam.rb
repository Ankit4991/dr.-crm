class AddDilationFieldsToInternalExam < ActiveRecord::Migration
  def change
    add_column :internal_exams, :tropicomide, :boolean, :default => false
    add_column :internal_exams, :fundus, :boolean, :default => false
    add_column :internal_exams, :phenylephrine, :boolean, :default => false
    add_column :internal_exams, :ophthalmoscopy, :boolean, :default => false
    remove_column :internal_exams, :drops_used, :string
  end
end
