class UpdateVisualAcutiesFields < ActiveRecord::Migration
 	def up
    change_table :visual_acuities do |t|
      t.rename :od, :near_od
      t.rename :os, :near_os
      t.rename :ou, :near_ou
      t.remove :glass_type
      t.string :distance_od
      t.string :distance_os
      t.string :distance_ou
    end
    change_column :visual_acuities, :near_od,  :string 
    change_column :visual_acuities, :near_os,  :string 
    change_column :visual_acuities, :near_ou,  :string 
  end
  def down
  	change_table :visual_acuities do |t|
      t.rename  :near_od, :od
      t.rename :near_os, :os
      t.rename :near_ou, :ou
      t.string :glass_type
      t.remove :distance_od
      t.remove :distance_os
      t.remove :distance_ou
    end

  end
end
