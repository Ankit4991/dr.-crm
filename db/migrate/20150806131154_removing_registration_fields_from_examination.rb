class RemovingRegistrationFieldsFromExamination < ActiveRecord::Migration
  def change
    remove_column :examinations, :pregnate, :boolean
    remove_column :examinations, :breast_feeding, :boolean
    remove_column :examinations, :place, :string
    remove_column :examinations, :prev_exam, :date
    add_column :patients, :pregnate, :boolean
    add_column :patients, :breast_feeding, :boolean
    add_column :patients, :place, :string
    add_column :patients, :prev_exam, :date
    add_column :patients, :tobacco_use, :boolean
    add_column :patients, :alcohol_use, :boolean
    add_column :medical_histories, :patient_id, :integer
  end
end
