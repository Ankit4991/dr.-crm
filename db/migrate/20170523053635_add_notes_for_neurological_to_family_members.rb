class AddNotesForNeurologicalToFamilyMembers < ActiveRecord::Migration
  def change
    add_column :family_members, :notes_for_neurological, :string
  end
end
