class CreateJoinTableEyeGlassOrderLensMaterial < ActiveRecord::Migration
  def change
    create_table :eye_glass_orders_lens_materials do |t|
      t.references :eye_glass_order
      t.references :lens_material
    end
  end
end
