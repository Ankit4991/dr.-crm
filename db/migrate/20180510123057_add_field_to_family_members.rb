class AddFieldToFamilyMembers < ActiveRecord::Migration
  def change
    add_column :family_members, :medical_insurance_id, :integer
  end
end
