class DropJoinTableEyeGlassOrdersLensMaterials < ActiveRecord::Migration
  def change
    drop_table :eye_glass_orders_lens_materials do |t|
      t.references :eye_glass_order
      t.references :lens_material
    end
    add_column :eye_glass_orders, :lens_material_id, :integer
  end
end
