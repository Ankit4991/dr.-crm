class CreatePrimlinaryExamNotes < ActiveRecord::Migration
  def change
    create_table :primlinary_exam_notes do |t|
      t.string :notes
      t.references :primlinary_exam, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
