class AddOrganisationIdToFamilyMember < ActiveRecord::Migration
  def change
    add_reference :family_members, :organisation, index: true, foreign_key: true
  end
end
