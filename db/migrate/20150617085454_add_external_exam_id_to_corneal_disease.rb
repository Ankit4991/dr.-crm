class AddExternalExamIdToCornealDisease < ActiveRecord::Migration
  def change
    add_column :corneal_diseases, :external_exam_id, :integer
  end
end
