class AddOrientedX3ToSocialHistories < ActiveRecord::Migration
  def change
    add_column :social_histories, :oriented_x3, :boolean, :default => true
  end
end
