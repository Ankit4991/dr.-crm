class AddGlassTypePriceToEyeGlassOrders < ActiveRecord::Migration
  def change
    add_column :eye_glass_orders, :glass_type_price, :float
  end
end
