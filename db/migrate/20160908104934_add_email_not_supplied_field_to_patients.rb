class AddEmailNotSuppliedFieldToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :email_not_supplied, :boolean, :default => false
  end
end
