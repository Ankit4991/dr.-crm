class ChangeDefaultValueForOsSphereOsCylinderInGlassAndLens < ActiveRecord::Migration
  def up
    change_column :lens, :os_sphere, :string, default: ''
    change_column :lens, :os_cylinder,:string, default: ''
    change_column :glasses, :os_sphere,:string, default: ''
    change_column :glasses, :os_cylinder,:string, default: ''
    change_column :lens, :od_sphere, :string, default: ''
    change_column :lens, :od_cylinder,:string, default: ''
    change_column :glasses, :od_sphere,:string, default: ''
    change_column :glasses, :od_cylinder,:string, default: ''
  end
  def down
     change_column :lens, :os_sphere, :string, default: 'pl'
    change_column :lens, :os_cylinder,:string, default: 'pl'
    change_column :glasses, :os_sphere,:string, default: 'pl'
    change_column :glasses, :os_cylinder,:string, default: 'pl'
    change_column :lens, :od_sphere, :string, default: 'pl'
    change_column :lens, :od_cylinder,:string, default: 'pl'
    change_column :glasses, :od_sphere,:string, default: 'pl'
    change_column :glasses, :od_cylinder,:string, default: 'pl'  
  end
end
