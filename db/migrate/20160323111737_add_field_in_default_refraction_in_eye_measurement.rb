class AddFieldInDefaultRefractionInEyeMeasurement < ActiveRecord::Migration
  def change
    add_column :eye_measurements, :default_refraction, :boolean, :default => false
  end
end
