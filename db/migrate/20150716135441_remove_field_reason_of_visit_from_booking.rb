class RemoveFieldReasonOfVisitFromBooking < ActiveRecord::Migration
  def change
    remove_column :bookings, :reason_of_visit, :string
  end
end
