class AddFieldReasonOfVisitInBookings < ActiveRecord::Migration
  def change
    add_column :bookings, :reason_of_visit, :string
  end
end
