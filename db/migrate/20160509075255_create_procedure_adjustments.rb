class CreateProcedureAdjustments < ActiveRecord::Migration
  def change
    create_table :procedure_adjustments do |t|
      t.integer :order_id
      t.integer :examination_id
      t.integer :exam_procedure_id
      t.float :price

      t.timestamps null: false
    end
  end
end
