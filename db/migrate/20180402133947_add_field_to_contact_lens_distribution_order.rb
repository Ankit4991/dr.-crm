class AddFieldToContactLensDistributionOrder < ActiveRecord::Migration
  def change
    add_column :contact_lens_distribution_orders, :os_quantity, :integer
    rename_column :contact_lens_distribution_orders, :quantity, :od_quantity
    add_column :contact_lens_distribution_orders, :store_id, :integer
    add_column :contact_lens_distribution_orders, :patient_id, :integer
  end
end
