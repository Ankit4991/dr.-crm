class RemoveModalityFromOrderSubscriptions < ActiveRecord::Migration
  def up
    remove_column :order_subscriptions, :modality, :integer
  end
  
  def down
    add_column :order_subscriptions, :modality, :integer
  end
end
