class AddContactLensOrderIdToLens < ActiveRecord::Migration
  def change
    add_column :lens, :contact_lens_order_id, :integer
  end
end
