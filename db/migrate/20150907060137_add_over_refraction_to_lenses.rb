class AddOverRefractionToLenses < ActiveRecord::Migration
  def change
  	add_column :lens, :od_over_refraction, :string
  	add_column :lens, :os_over_refraction, :string
  end
end
