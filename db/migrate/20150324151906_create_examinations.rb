class CreateExaminations < ActiveRecord::Migration
  def change
    create_table :examinations do |t|
      t.integer  :doctor_id, index: true
      t.integer  :patient_id, index: true
      t.string   :meds
      t.string   :sxhx
      t.text     :chief_complain
      # Social Histories
      t.boolean  :tobacco_use
      t.boolean  :alcohol_use

      ## Primlinary Exams
      t.string   :eoms
      t.text     :pupils
      t.boolean  :pupils_brisk
      t.boolean  :pupils_dull
        # Brisk Dull
      t.string   :cf
      t.string   :color_vision
      t.string   :steropsis
      t.string   :cover_test
      t.string   :nra
      t.string   :pra
      t.string   :npc
      t.text     :fdt_visual_field
        # N30-1 Screening
        # N30-1 Threshold
        # OD is defective
        # OS Shows no VF defects
      t.boolean  :n30_1_screening
      t.boolean  :n30_1_threshold
      t.text     :primlinary_exams_notes

      t.integer  :objective_id, index: true
      t.integer  :subjective_id, index: true
      t.integer  :current_glass_id, index: true
      
      t.timestamps null: false
    end
  end
end
