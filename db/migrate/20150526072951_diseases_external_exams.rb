class DiseasesExternalExams < ActiveRecord::Migration
   def change
    create_table :diseases_external_exams, :id => false do |t|
        t.references :disease
        t.references :external_exam
    end
  end
end
