class AddLensPrescriptionIdToFavouriteOrdersAndOrderSubscriptions < ActiveRecord::Migration
  def up
    add_column :favourite_orders, :lens_prescription_id, :integer
    add_column :order_subscriptions, :lens_prescription_id, :integer
    add_column :order_subscriptions, :modality, :integer
    change_column :favourite_orders,:total, :float, :default => 0.0
  end
  
  def down
    remove_column :favourite_orders, :lens_prescription_id, :integer
    remove_column :order_subscriptions, :lens_prescription_id, :integer
    remove_column :order_subscriptions, :modality, :integer
    change_column :favourite_orders,:total, :float
  end
end
