class AddDefaultValueToFundus < ActiveRecord::Migration
  def up
    change_column :internal_exams, :fundus, :boolean, default: true
  end

  def down
    change_column :internal_exams, :fundus, :boolean, default: false
  end
end
