class AddFlagForRewardToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :flag_for_reward, :boolean, :default => false
  end
end
