class UpdateRoleColumnTypeInDoctor < ActiveRecord::Migration
  def change
    remove_column  :doctors, :role, :string
    add_column  :doctors, :role, :string, array: true, default: []
  end
end
