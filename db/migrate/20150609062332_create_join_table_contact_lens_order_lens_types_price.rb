class CreateJoinTableContactLensOrderLensTypesPrice < ActiveRecord::Migration
  def change
    create_table :contact_lens_orders_lens_types_prices do |t|
      t.references :contact_lens_order
      t.references :lens_types_price
    end
  end
end
