class AddFieldDeletedAtInDoctors < ActiveRecord::Migration
  def change
    add_column :doctors, :deleted_at, :datetime
  end
end
