class AddFieldsToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :ssn, :string
    add_column :patients, :vision_insurance, :string
    add_column :patients, :member_id, :string
    add_column :patients, :referred_by, :string
    add_column :patients, :contact_lenses, :boolean
    add_column :patients, :interested_contact_lens, :boolean
    add_column :patients, :in_lasik, :boolean
  end
end
