class AddProfileImageToContacts < ActiveRecord::Migration
  def up
    add_attachment :contacts, :profile_image
  end
 
  def down
    remove_attachment :contacts, :profile_image
  end
end
