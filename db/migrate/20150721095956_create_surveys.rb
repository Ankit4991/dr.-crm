class CreateSurveys < ActiveRecord::Migration
  def change
    create_table :surveys do |t|
      t.integer :patient_id
      t.float :professionalism, default: 0.0
      t.float :work_quality, default: 0.0
      t.float :schedule_adherence, default: 0.0
      t.float :communication, default: 0.0
      t.float :cleanliness, default: 0.0
      t.float :staff, default: 0.0
      t.text :feedback

      t.timestamps null: false
    end
  end
end
