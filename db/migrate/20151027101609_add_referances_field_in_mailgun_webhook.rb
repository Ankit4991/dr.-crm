class AddReferancesFieldInMailgunWebhook < ActiveRecord::Migration
  def change
    add_column :mailgun_webhooks, :referances, :string, array: true, default: []
  end
end
