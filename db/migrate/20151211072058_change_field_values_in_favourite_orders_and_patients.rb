class ChangeFieldValuesInFavouriteOrdersAndPatients < ActiveRecord::Migration
  def up
    change_column :favourite_orders,:reward_points_used, :float, :default => 0.0
    change_column :favourite_orders,:discount, :float, :default => 0.15
    change_column :patients,:reward_point, :float, :default => 0.0
    add_column :favourite_orders, :rewards_applied, :boolean, :default => false
    remove_column :favourite_orders, :order_id, :integer
    remove_column :favourite_orders, :glass_orders, :hstore
  end
  
  def down
    change_column :favourite_orders,:reward_points_used, :float
    change_column :favourite_orders,:discount, :float
    change_column :patients,:reward_point, :float
    remove_column :favourite_orders, :rewards_applied, :boolean, :default => false
    add_column :favourite_orders, :order_id, :integer
    add_column :favourite_orders, :glass_orders, :hstore
  end
end
