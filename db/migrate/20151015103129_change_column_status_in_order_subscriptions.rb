class ChangeColumnStatusInOrderSubscriptions < ActiveRecord::Migration
  def change
  	remove_column :order_subscriptions, :status, :integer
  	add_column :order_subscriptions, :active, :boolean, default: true
  end
end
