class AddColumnExaminationIdInContactLensExam < ActiveRecord::Migration
  def change
  	add_column :contact_lens_exams, :examination_id, :integer
  end
end
