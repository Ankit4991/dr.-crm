class AddColumnGlassTypeInVisualAcuties < ActiveRecord::Migration
  def change
  	add_column :visual_acuities, :glass_type, :string
  	add_column :visual_acuities, :using_glasses, :boolean, default: false
  end
end
