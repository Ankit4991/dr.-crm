class ChangeVisionInsuranceFieldsInPatientsAndFamilyMembers < ActiveRecord::Migration
  def up
    remove_column :patients, :vision_insurance, :string
    remove_column :family_members, :vision_insurance, :string
    add_column :patients, :vision_insurance_id, :integer
    add_column :family_members, :vision_insurance_id, :integer
  end
  
  def down
    remove_column :patients, :vision_insurance_id, :integer
    remove_column :family_members, :vision_insurance_id, :integer
    add_column :patients, :vision_insurance, :string
    add_column :family_members, :vision_insurance, :string
  end
end
