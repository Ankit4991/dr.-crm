class AddInsurancePaidToOrders < ActiveRecord::Migration
  def change
  	add_column :orders, :insurance_paid, :float
  end
end
