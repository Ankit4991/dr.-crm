class AddEyeGlassOrderIdToGlasses < ActiveRecord::Migration
  def change
    add_column :glasses, :eye_glass_order_id, :integer
  end
end
