class CreateJoinTableEyeGlassOrderGlassAddonsPrice < ActiveRecord::Migration
  def change
    create_table :eye_glass_orders_glass_addons_prices do |t|
      t.references :eye_glass_order
      t.references :glass_addons_price
    end
  end
end
