class ChangeCoulmnInLensTypesPrices < ActiveRecord::Migration
  def change
    remove_column :lens_types_prices, :diameter, :string
    add_column :lens_types_prices, :diameters, :text, default: ""
    change_column :lens_types_prices, :base_curve, :text, default: ""
  end
end
