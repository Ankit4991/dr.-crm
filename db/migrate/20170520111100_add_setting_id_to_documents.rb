class AddSettingIdToDocuments < ActiveRecord::Migration
  def change
    add_column :documents, :setting_id, :integer
  end
end
