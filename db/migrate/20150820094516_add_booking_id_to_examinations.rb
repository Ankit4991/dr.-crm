class AddBookingIdToExaminations < ActiveRecord::Migration
  def change
    add_column :examinations, :booking_id, :integer
  end
end
