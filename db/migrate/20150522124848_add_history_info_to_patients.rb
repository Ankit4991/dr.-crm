class AddHistoryInfoToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :history_info, :string, array: true, default: []
  end
end
