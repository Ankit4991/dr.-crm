class AddRemainingToContactLensOrders < ActiveRecord::Migration
  def change
    add_column :contact_lens_orders, :remaining, :float , default:0.00
  end
end
