class AddOrderCheckmarks < ActiveRecord::Migration
  def up
    add_column :eye_glass_orders, :order_checkmark, :boolean,default: false
    add_column :contact_lens_orders, :order_checkmark, :boolean,default: false
  end
 
  def down
    remove_column :eye_glass_orders, :order_checkmark, :boolean,default: false
    remove_column :contact_lens_orders, :order_checkmark, :boolean,default: false
  end
end
