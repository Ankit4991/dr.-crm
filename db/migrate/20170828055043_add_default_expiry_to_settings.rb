class AddDefaultExpiryToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :default_expiry, :string
  end
end
