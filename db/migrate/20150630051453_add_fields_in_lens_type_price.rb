class AddFieldsInLensTypePrice < ActiveRecord::Migration
  def change
  	add_column :lens_types_prices, :brand_name, :string
  	add_column :lens_types_prices, :base_curve, :string
  	add_column :lens_types_prices, :diameter, :string
  	add_column :lens_types_prices, :modality, :string
  end
end
