class AddColumnOrganisationIdInHolidays < ActiveRecord::Migration
  def change
  	add_column :holidays, :organisation_id, :integer
  	add_column :dispense_logs, :organisation_id, :integer
  end
end
