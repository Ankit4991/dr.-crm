class CreateContactLensExams < ActiveRecord::Migration
  def up
    execute 'CREATE EXTENSION IF NOT EXISTS hstore' 
    create_table :contact_lens_exams do |t|
      t.integer :doctor_id, index: true
      t.integer :patient_id, index: true
      t.json :contact_lens_evaluation
      t.json :current_clrx
      t.hstore :trial
      t.timestamps null: false
      
    end
  end
  def down
    execute 'DROP EXTENSION hstore'
    drop_table :contact_lens_exams
  end
end
