class CreateCashPayments < ActiveRecord::Migration
  def change
    create_table :cash_payments do |t|
      t.float :amount
      t.integer :orderable_id
      t.string :orderable_type
      t.integer :doctor_id
      t.integer :status, default: 0
      t.integer :organisation_id

      t.timestamps null: false
    end
  end
end
