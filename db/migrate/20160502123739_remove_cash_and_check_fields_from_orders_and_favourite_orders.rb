class RemoveCashAndCheckFieldsFromOrdersAndFavouriteOrders < ActiveRecord::Migration
  def change
    remove_column :orders, :cash_amount, :float
    remove_column :orders, :check_amount, :float
    remove_column :orders, :check_number, :string
    remove_column :favourite_orders, :cash_amount, :float
    remove_column :favourite_orders, :check_amount, :float
    remove_column :favourite_orders, :check_number, :string
  end
end
