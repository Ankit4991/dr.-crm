class AddCoulmOrganisationIdIn < ActiveRecord::Migration
  def change
    add_column :addon_details, :organisation_id, :integer
    add_column :bookings, :organisation_id, :integer
    add_column :contact_lens_exams, :organisation_id, :integer
    add_column :contact_lens_orders, :organisation_id, :integer
    add_column :diseases, :organisation_id, :integer
    add_column :doctors, :organisation_id, :integer
    add_column :exam_procedures, :organisation_id, :integer
    add_column :examinations, :organisation_id, :integer
    add_column :eye_glass_orders, :organisation_id, :integer
    add_column :eyeglass_types_prices, :organisation_id, :integer
    add_column :glass_addons_prices, :organisation_id, :integer
    add_column :glass_frames_prices, :organisation_id, :integer
    add_column :impressions, :organisation_id, :integer
    add_column :lens_types_prices, :organisation_id, :integer
    add_column :orders, :organisation_id, :integer
    add_column :order_transactions, :organisation_id, :integer
    add_column :patients, :organisation_id, :integer
    add_column :settings, :organisation_id, :integer
    add_column :surveys, :organisation_id, :integer
  end
end
