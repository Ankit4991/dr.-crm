class AddNameToExamCleaningSolutions < ActiveRecord::Migration
  def change
    add_column :exam_cleaning_solutions, :name, :string
  end
end
