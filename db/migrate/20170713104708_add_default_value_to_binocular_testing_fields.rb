class AddDefaultValueToBinocularTestingFields < ActiveRecord::Migration
  def up
    change_column :binocular_testings, :npc, :string, default: "TTN"
    change_column :binocular_testings, :hpd, :string, default: "Ortho"
    change_column :binocular_testings, :nra, :string, default: "+2.50"
    change_column :binocular_testings, :pra, :string, default: "-2.50"
    change_column :binocular_testings, :hpn, :string, default: "3XP"
    change_column :binocular_testings, :ctn, :string, default: "3XP"
    change_column :binocular_testings, :vpn, :string, default: "Ortho"
    change_column :binocular_testings, :vpd, :string, default: "Ortho"
    change_column :binocular_testings, :ctd, :string, default: "Ortho"
  end

  def down
    change_column :binocular_testings, :npc, :string
    change_column :binocular_testings, :hpd, :string
  	change_column :binocular_testings, :nra, :string
  	change_column :binocular_testings, :pra, :string
  	change_column :binocular_testings, :hpn, :string
  	change_column :binocular_testings, :ctn, :string
  	change_column :binocular_testings, :vpn, :string
  	change_column :binocular_testings, :vpd, :string
  	change_column :binocular_testings, :ctd, :string
  end
end
