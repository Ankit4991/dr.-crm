class AddEthnicityAndEyeColorToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :ethnicity, :string
    add_column :patients, :eye_color, :string
  end
end
