class ChangeColumsDatatypeInGlasses < ActiveRecord::Migration
  def up
  	change_column :glasses, :od_sphere, :string
  	change_column :glasses, :od_cylinder, :string
  	change_column :glasses, :od_axis, :string
  	change_column :glasses, :od_add, :string
  	change_column :glasses, :os_sphere, :string
  	change_column :glasses, :os_cylinder, :string
  	change_column :glasses, :os_axis, :string
  	change_column :glasses, :os_add, :string
  end
  def down
  	change_column :glasses, :od_sphere, :integer
  	change_column :glasses, :od_cylinder, :integer
  	change_column :glasses, :od_axis, :integer
  	change_column :glasses, :od_add, :integer
  	change_column :glasses, :os_sphere, :integer
  	change_column :glasses, :os_cylinder, :integer
  	change_column :glasses, :os_axis, :integer
  	change_column :glasses, :os_add, :integer
  end
end
