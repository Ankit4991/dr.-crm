class AddNewStoreIdInBookings < ActiveRecord::Migration
  
  def change
    add_column :bookings, :store_id, :integer
  end
end
