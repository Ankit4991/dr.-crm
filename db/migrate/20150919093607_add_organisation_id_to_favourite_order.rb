class AddOrganisationIdToFavouriteOrder < ActiveRecord::Migration
  def change
    add_column :favourite_orders, :organisation_id, :integer
  end
end
