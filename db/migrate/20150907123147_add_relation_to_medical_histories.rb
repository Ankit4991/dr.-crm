class AddRelationToMedicalHistories < ActiveRecord::Migration
  def change
  	add_column :medical_histories, :relation, :string
  end
end
