class AddDoctorIdToBackground < ActiveRecord::Migration
  def change
    add_reference :backgrounds, :doctor, index: true, foreign_key: true
    add_reference :backgrounds, :patient, index: true, foreign_key: true
  end
end
