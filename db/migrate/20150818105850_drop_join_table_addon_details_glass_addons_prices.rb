class DropJoinTableAddonDetailsGlassAddonsPrices < ActiveRecord::Migration
  def change
    drop_table :addon_details_glass_addons_prices do |t|
      t.references :addon_detail
      t.references :glass_addons_price
    end
    add_column :addon_details, :glass_addons_price_id, :integer
  end
end
