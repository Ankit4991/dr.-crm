class AddAgitatedToFamilyMembers < ActiveRecord::Migration
  def change
    add_column :family_members, :agitated, :boolean, :default => false
  end
end
