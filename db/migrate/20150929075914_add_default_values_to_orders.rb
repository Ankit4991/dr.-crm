class AddDefaultValuesToOrders < ActiveRecord::Migration
  def up
    change_column :orders,:cash_amount, :float, :default => 0.0
    change_column :orders,:check_amount, :float, :default => 0.0
    change_column :favourite_orders,:cash_amount, :float, :default => 0.0
    change_column :favourite_orders,:check_amount, :float, :default => 0.0
  end
  def down
    change_column :orders,:cash_amount, :float
    change_column :orders,:check_amount, :float
    change_column :favourite_orders,:cash_amount, :float
    change_column :favourite_orders,:check_amount, :float
  end
end
