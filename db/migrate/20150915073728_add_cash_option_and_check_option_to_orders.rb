class AddCashOptionAndCheckOptionToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :cash_amount, :float
    add_column :orders, :check_amount, :float
    add_column :orders, :check_number, :string
  end
end
