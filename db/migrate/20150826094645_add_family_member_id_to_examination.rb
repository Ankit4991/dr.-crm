class AddFamilyMemberIdToExamination < ActiveRecord::Migration
  def change
    add_reference :examinations, :family_member, index: true, foreign_key: true
    add_reference :glasses, :family_member, index: true, foreign_key: true
    add_reference :orders, :family_member, index: true, foreign_key: true
    add_reference :bookings, :family_member, index: true, foreign_key: true
    add_reference :follow_ups, :family_member, index: true, foreign_key: true
  end
end
