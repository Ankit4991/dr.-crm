class AddFieldsToOrganisations < ActiveRecord::Migration
  def change
    add_column :organisations, :twilio_acc_sid, :string
    add_column :organisations, :twilio_auth_token, :string
  end
end
