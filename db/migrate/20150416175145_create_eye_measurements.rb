class CreateEyeMeasurements < ActiveRecord::Migration
  def change
    create_table :eye_measurements do |t|
      t.integer   :glass_id, index: true
      t.hstore    :drugs
      t.time      :measurement_time
      t.timestamps null: false
    end
  end
end
