class AddOrganisationIdToJointResource < ActiveRecord::Migration
  def change
    add_reference :joint_resources, :organisation, index: true, foreign_key: true
  end
end
