class AddValidatedToPatientProfile < ActiveRecord::Migration
  def change
    add_column :patient_profiles, :validated, :boolean, default: false
  end
end
