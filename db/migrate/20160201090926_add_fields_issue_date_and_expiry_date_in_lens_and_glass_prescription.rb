class AddFieldsIssueDateAndExpiryDateInLensAndGlassPrescription < ActiveRecord::Migration
  def change
    add_column :lens_prescriptions, :issued_date, :date
    add_column :lens_prescriptions, :expired_date, :date
    add_column :glasses_prescriptions, :issued_date, :date
    add_column :glasses_prescriptions, :expired_date, :date
  end
end