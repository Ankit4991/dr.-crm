class AddColumsForDrawData < ActiveRecord::Migration
  def up  	
  	add_column :external_exams, :eye_draw_data, :json
  	add_column :external_exams, :cornea_draw_data, :json
  	remove_column :external_exams, :draw_data
  	add_column :internal_exams, :draw_data, :json
  	add_column :lens, :od_draw_data, :json
  	add_column :lens, :os_draw_data, :json
  end
  def down
  	remove_column :external_exams, :eye_draw_data, :json
  	remove_column :external_exams, :cornea_draw_data, :json
  	add_column :external_exams, :draw_data, :json
  	remove_column :internal_exams, :draw_data, :json
  	remove_column :lens, :od_draw_data, :json
  	remove_column :lens, :os_draw_data, :json
  end
end
