class RemoveReferToAndReferralReasonFromExamination < ActiveRecord::Migration
  def change
  	remove_column :examinations, :refer_to, :string
  	remove_column :examinations, :referral_reason, :string
  end
end
