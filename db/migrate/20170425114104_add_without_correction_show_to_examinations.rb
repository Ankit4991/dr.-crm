class AddWithoutCorrectionShowToExaminations < ActiveRecord::Migration
  def change
    add_column :examinations, :without_correction_show, :boolean, :default => false
  end
end
