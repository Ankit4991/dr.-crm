class UpdateWorkingScheduleFieldsDatatype < ActiveRecord::Migration
  def change
     remove_column :working_schedules, :monday_start_time, :time
     add_column :working_schedules, :monday_start_time, :datetime
     remove_column :working_schedules, :monday_end_time, :time
     add_column :working_schedules, :monday_end_time, :datetime
     remove_column :working_schedules, :tuesday_start_time, :time
     add_column :working_schedules, :tuesday_start_time, :datetime
     remove_column :working_schedules, :tuesday_end_time, :time
     add_column :working_schedules, :tuesday_end_time, :datetime
     remove_column :working_schedules, :wednesday_start_time, :time
     add_column :working_schedules, :wednesday_start_time, :datetime
     remove_column :working_schedules, :wednesday_end_time, :time
     add_column :working_schedules, :wednesday_end_time, :datetime
     remove_column :working_schedules, :thursday_start_time, :time
     add_column :working_schedules, :thursday_start_time, :datetime
     remove_column :working_schedules, :thursday_end_time, :time
     add_column :working_schedules, :thursday_end_time, :datetime
     remove_column :working_schedules, :friday_start_time, :time
     add_column :working_schedules, :friday_start_time, :datetime
     remove_column :working_schedules, :friday_end_time, :time
     add_column :working_schedules, :friday_end_time, :datetime
     remove_column :working_schedules, :saturday_start_time, :time
     add_column :working_schedules, :saturday_start_time, :datetime
     remove_column :working_schedules, :saturday_end_time, :time
     add_column :working_schedules, :saturday_end_time, :datetime
     remove_column :working_schedules, :sunday_start_time, :time
     add_column :working_schedules, :sunday_start_time, :datetime
     remove_column :working_schedules, :sunday_end_time, :time
     add_column :working_schedules, :sunday_end_time, :datetime
  end
end
