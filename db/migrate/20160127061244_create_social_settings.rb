class CreateSocialSettings < ActiveRecord::Migration
  def change
    create_table :social_settings do |t|
      t.attachment :avatar
      t.string :url
      t.string :name
      t.references :store
      t.timestamps null: false
    end
  end
end
