class AddOrganisationIdToCreateDoctorRecommendations < ActiveRecord::Migration
  def change
  	  	add_column :doctor_recommendations, :organisation_id, :integer
  end
end
