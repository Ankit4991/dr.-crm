class AddBloodPressureColumnsToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :blood_pressure, :string
    add_column :patients, :blood_pressure_time, :string
  end
end
