class CreatePrimlinaryExams < ActiveRecord::Migration
  def change
    create_table :primlinary_exams do |t|
    	t.integer :examination_id
    	t.string  :eoms,         default: "SAFE OD / OS"         
		  t.string  :pupils,       default: "PERRL (-) APD"         
		  t.string  :reaction        
		  t.string  :confrontational,  default: "FTFC OD / OS"
		  t.string  :color_vision     
		  t.string  :steropsis
		  t.string  :notes
      t.timestamps null: false
    end
  end
end
