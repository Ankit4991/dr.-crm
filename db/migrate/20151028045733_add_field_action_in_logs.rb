class AddFieldActionInLogs < ActiveRecord::Migration
  def change
    add_column :logs, :action, :string, default: "send"
  end
end
