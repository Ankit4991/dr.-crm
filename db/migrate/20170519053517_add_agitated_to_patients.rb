class AddAgitatedToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :agitated, :boolean, :default => false
  end
end
