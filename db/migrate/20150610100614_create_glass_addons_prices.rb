class CreateGlassAddonsPrices < ActiveRecord::Migration
  def change
    create_table :glass_addons_prices do |t|
      t.string :addon
      t.float :price

      t.timestamps null: false
    end
  end
end
