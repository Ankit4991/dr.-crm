class CreateDispenseLogs < ActiveRecord::Migration
  def change
    create_table :dispense_logs do |t|
      t.string :note
      t.datetime :logged_at
      t.integer :order_id

      t.timestamps null: false
    end
  end
end
