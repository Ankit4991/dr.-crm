class RenameColumnPatientPaysInLensTypesPriceToCostOfGood < ActiveRecord::Migration
  def change
  	rename_column :lens_types_prices, :patient_pays, :cost_of_good
  end
end
