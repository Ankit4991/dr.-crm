class AddLogoToStores < ActiveRecord::Migration
  def up
  	add_attachment :stores, :logo
  end
  def down
  	remove_attachment :stores, :logo
  end
end
