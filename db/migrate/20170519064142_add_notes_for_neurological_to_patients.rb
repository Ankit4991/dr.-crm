class AddNotesForNeurologicalToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :notes_for_neurological, :string
  end
end
