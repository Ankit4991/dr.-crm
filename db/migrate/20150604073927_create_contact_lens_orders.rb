class CreateContactLensOrders < ActiveRecord::Migration
  def change
    create_table :contact_lens_orders do |t|
      t.float :material_copay,default: 0.00
      t.float :allowance,default: 0.00
      t.float :cl_fit,default: 0.00
      t.float :total,default: 0.00
      t.integer :od_qnt,default: 0
      t.integer :os_qnt,default: 0
      t.integer :order_id

      t.timestamps null: false
    end
  end
end
