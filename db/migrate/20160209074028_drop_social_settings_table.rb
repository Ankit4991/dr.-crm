class DropSocialSettingsTable < ActiveRecord::Migration
  def up
    drop_table :social_settings
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
