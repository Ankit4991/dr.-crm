class CreateExamProceduresBookings < ActiveRecord::Migration
  def change
    create_table :bookings_exam_procedures do |t|
      t.references :booking
      t.references :exam_procedure
    end
  end
end
