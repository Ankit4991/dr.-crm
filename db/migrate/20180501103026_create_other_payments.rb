class CreateOtherPayments < ActiveRecord::Migration
  def change
    create_table :other_payments do |t|

      t.float :other_amount
      t.integer :orderable_id
      t.string :orderable_type
      t.integer :doctor_id
      t.integer :status, default: 0
      t.integer :organisation_id
      t.string :comments
      t.string :additional_notes

      t.timestamps null: false
    end
  end
end
