class AddApprovedFieldInOrganisation < ActiveRecord::Migration
  def change
    add_column :organisations, :approved, :boolean, default: false
  end
end
