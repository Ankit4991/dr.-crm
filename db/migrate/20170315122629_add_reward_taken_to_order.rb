class AddRewardTakenToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :reward_taken, :boolean, :default => false
  end
end
