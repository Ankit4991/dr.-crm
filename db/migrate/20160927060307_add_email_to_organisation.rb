class AddEmailToOrganisation < ActiveRecord::Migration
  def change
    add_column :organisations, :email, :string, :default => ''
  end
end
