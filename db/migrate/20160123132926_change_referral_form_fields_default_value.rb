class ChangeReferralFormFieldsDefaultValue < ActiveRecord::Migration
  def change
    change_column :referrals, :medical, :boolean, default: true
    change_column :referrals, :social, :boolean, default: true
    change_column :referrals, :binocular, :boolean, default: true
    change_column :referrals, :primlinary, :boolean, default: true
    change_column :referrals, :visual, :boolean, default: true
    change_column :referrals, :procedure, :boolean, default: true
    change_column :referrals, :impression, :boolean, default: true
    change_column :referrals, :refraction, :boolean, default: true
    change_column :referrals, :contact, :boolean, default: true
    change_column :referrals, :external, :boolean, default: true
    change_column :referrals, :internal, :boolean, default: true
  end
end
