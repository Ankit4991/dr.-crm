class RenameFieldNoteToReasonFromFollowUps < ActiveRecord::Migration
  def change
    rename_column :follow_ups, :note, :reason
  end
end
