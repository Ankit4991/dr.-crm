class AddDilationFieldToInternalExam < ActiveRecord::Migration
  def change
    add_column :internal_exams, :dilation, :boolean, :default => false
  end
end
