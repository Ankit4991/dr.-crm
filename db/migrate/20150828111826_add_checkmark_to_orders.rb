class AddCheckmarkToOrders < ActiveRecord::Migration
  def change
    add_column :eye_glass_orders, :od_checkmark, :boolean,default: false
    add_column :eye_glass_orders, :os_checkmark, :boolean,default: false
    add_column :eye_glass_orders, :lens_material_checkmark, :boolean,default: false
    add_column :eye_glass_orders, :eye_glass_type_checkmark, :boolean,default: false
    add_column :eye_glass_orders, :frame_checkmark, :boolean,default: false
    add_column :contact_lens_orders, :od_checkmark, :boolean,default: false
    add_column :contact_lens_orders, :os_checkmark, :boolean,default: false
    add_column :addon_details, :checkmark, :boolean,default: false
  end
end
