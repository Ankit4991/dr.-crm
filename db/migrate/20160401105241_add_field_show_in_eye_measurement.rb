class AddFieldShowInEyeMeasurement < ActiveRecord::Migration
  def change
    add_column :eye_measurements, :show, :boolean, default: false
  end
end
