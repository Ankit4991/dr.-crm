class CreateJoinTableEyeGlassOrderEyeglassTypesPrice < ActiveRecord::Migration
  def change
    create_table :eye_glass_orders_eyeglass_types_prices do |t|
      t.references :eye_glass_order
      t.references :eyeglass_types_price
    end
  end
end
