class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string   "address1"
      t.string   "address2"
      t.string   "city"
      t.string   "postal_code"
      t.string   "phone"
      t.string   "email"
      t.string   "state_id"
      t.integer  "country_id"
      t.timestamps null: false
    end
  end
end
