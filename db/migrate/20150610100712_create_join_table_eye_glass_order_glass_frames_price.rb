class CreateJoinTableEyeGlassOrderGlassFramesPrice < ActiveRecord::Migration
  def change
    create_table :eye_glass_orders_glass_frames_prices do |t|
      t.references :eye_glass_order
      t.references :glass_frames_price
    end
  end
end
