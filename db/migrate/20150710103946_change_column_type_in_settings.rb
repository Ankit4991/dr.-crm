class ChangeColumnTypeInSettings < ActiveRecord::Migration
  def change
  	change_column :settings, :lens_sphere_start, :string, :default => -19.00
  	change_column :settings, :lens_sphere_end, :string, :default => 11.25
  	change_column :settings, :lens_cylinder_start, :string, :default => -10.00
  	change_column :settings, :lens_cylinder_end, :string, :default => -0.25
  	change_column :settings, :lens_add_start, :string, :default => 0.25
  	change_column :settings, :lens_add_end, :string, :default => 6.00

  	change_column :settings, :glass_sphere_start, :string, :default => -19.00
  	change_column :settings, :glass_sphere_end, :string, :default => 11.25
  	change_column :settings, :glass_cylinder_start, :string, :default => -10.00
  	change_column :settings, :glass_cylinder_end, :string, :default => -0.25
  	change_column :settings, :glass_add_start, :string, :default => 0.25
  	change_column :settings, :glass_add_end, :string, :default => 6.00
  end
end
