class AddBloodPressureColumnsToFamilyMembers < ActiveRecord::Migration
  def change
    add_column :family_members, :blood_pressure, :string
    add_column :family_members, :blood_pressure_time, :string
  end
end
