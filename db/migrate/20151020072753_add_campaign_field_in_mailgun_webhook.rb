class AddCampaignFieldInMailgunWebhook < ActiveRecord::Migration
  def change
    add_column :mailgun_webhooks, :campaign, :string
    add_column :mailgun_webhooks, :organisation_id, :integer
  end
end
