class AddLensMaterialPriceAddonPriceAndFramePriceToEyeGlassOrders < ActiveRecord::Migration
  def change
    add_column :eye_glass_orders, :lens_material_price, :float, :default => 0.0
    add_column :eye_glass_orders, :addons_price, :float, :default => 0.0
    add_column :eye_glass_orders, :frame_price, :float, :default => 0.0
  end
end
