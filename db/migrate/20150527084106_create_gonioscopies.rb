class CreateGonioscopies < ActiveRecord::Migration
  def change
    create_table :gonioscopies do |t|
    	t.integer :external_exam_id
    	t.string :od_inferior  #top
    	t.string :od_superior #bottom
    	t.string :od_nasal #left
    	t.string :od_temporal #right
    	t.string :od_pigmentation
    	t.string :os_inferior  #top
    	t.string :os_superior #bottom
    	t.string :os_nasal #left
    	t.string :os_temporal #right
    	t.string :os_pigmentation
    	t.string :notes
      t.timestamps null: false
    end
  end
end
