class AddAppointmentStatusToBooking < ActiveRecord::Migration
  def change
    add_column :bookings, :appointment_status, :string, :default => ""
  end
end
