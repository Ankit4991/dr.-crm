class AddViewsCountRewardPointToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :views_count, :integer,default: 0
    add_column :patients, :reward_point, :float,default: 0.0
  end
end
