class AddDeletedAtToEyeGlassOrders < ActiveRecord::Migration
  def change
    add_column :eye_glass_orders, :deleted_at, :datetime
    add_index :eye_glass_orders, :deleted_at
  end
end
