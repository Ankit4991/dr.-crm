class AddCommentsToEyeGlassOrders < ActiveRecord::Migration
  def change
    add_column :eye_glass_orders, :comments, :string
  end
end
