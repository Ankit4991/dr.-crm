class AddPatientIdToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :patient_id, :integer
  end
end
