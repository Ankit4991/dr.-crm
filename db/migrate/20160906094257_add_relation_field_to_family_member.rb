class AddRelationFieldToFamilyMember < ActiveRecord::Migration
  def change
    add_column :family_members, :relation, :string, :default => ""
  end
end
