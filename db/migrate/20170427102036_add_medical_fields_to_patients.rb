class AddMedicalFieldsToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :pmhxes_check, :boolean, :default => false
    add_column :patients, :fmhxes_check, :boolean, :default => false
    add_column :patients, :fohxes_check, :boolean, :default => false
    add_column :patients, :pohxes_check, :boolean, :default => false
    add_column :patients, :sxhxes_check, :boolean, :default => false
    add_column :patients, :allergies_check, :boolean, :default => false
    add_column :patients, :medications_check, :boolean, :default => false
  end
end
