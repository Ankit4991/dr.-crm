class AddMsgLabToEyeGlassOrders < ActiveRecord::Migration
  def change
    add_column :eye_glass_orders, :msg_lab, :text
  end
end
