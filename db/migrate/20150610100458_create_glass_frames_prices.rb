class CreateGlassFramesPrices < ActiveRecord::Migration
  def change
    create_table :glass_frames_prices do |t|
      t.string :frame
      t.float :price

      t.timestamps null: false
    end
  end
end
