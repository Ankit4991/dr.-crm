class AddMedicalNotesToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :medical_notes, :string
  end
end
