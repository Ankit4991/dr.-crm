class CreateLensMaterials < ActiveRecord::Migration
  def change
    create_table :lens_materials do |t|
      t.string :name
      t.float :price,default: 0.0
      t.timestamps null: false
    end
  end
end
