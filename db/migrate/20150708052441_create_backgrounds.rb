class CreateBackgrounds < ActiveRecord::Migration
  def change
    create_table :backgrounds do |t|
      t.string :url
      t.integer :setting_id
      t.attachment :avatar

      t.timestamps null: false
    end
  end
end
