class AddOrderSubscriptionIdToFavouriteOrders < ActiveRecord::Migration
  def change
    add_column :favourite_orders, :order_subscription_id, :integer
  end
end
