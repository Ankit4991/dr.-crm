class AddDepressedToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :depressed, :boolean, :default => false
  end
end
