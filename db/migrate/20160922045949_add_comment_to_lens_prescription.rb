class AddCommentToLensPrescription < ActiveRecord::Migration
  def change
    add_column :lens_prescriptions, :comment, :string, :default => ''
  end
end
