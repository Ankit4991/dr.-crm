class CreateFavouriteOrders < ActiveRecord::Migration
  def change
    create_table :favourite_orders do |t|
      t.integer :doctor_id
      t.integer :order_id
      t.hstore :lens_orders
      t.hstore :glass_orders
      t.integer :status ,default: 0
      t.float :total
      t.float :cash_amount
      t.float :check_amount
      t.string :check_number
      t.timestamps null: false
    end
  end
end
