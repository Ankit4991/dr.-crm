class AddCheckInToBookings < ActiveRecord::Migration
  def up
    add_column :bookings, :check_in, :datetime
  end
  def down
  	remove_column :bookings, :check_in, :datetime
  end
end
