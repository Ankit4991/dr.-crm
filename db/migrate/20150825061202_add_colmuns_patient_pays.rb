class AddColmunsPatientPays < ActiveRecord::Migration
  def change
    add_column :eyeglass_types_prices, :patient_pays, :float
    add_column :lens_materials, :patient_pays, :float
    add_column :glass_addons_prices, :patient_pays, :float
    add_column :glass_frames_prices, :patient_pays, :float
    add_column :lens_types_prices, :patient_pays, :float
    add_column :exam_procedures, :patient_pays, :float
  end
end
