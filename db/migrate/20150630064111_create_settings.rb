class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
    	t.float :lens_sphere_start, default: -19.00
    	t.float :lens_sphere_end, default: 11.25
    	t.float :lens_cylinder_start, default: -10.00
    	t.float :lens_cylinder_end, default: -0.25
    	t.float :lens_add_start, default: 0.25
    	t.float :lens_add_end, default: 6.00

    	t.float :glass_sphere_start, default: -19.00
    	t.float :glass_sphere_end, default: 11.25
    	t.float :glass_cylinder_start, default: -10.00
    	t.float :glass_cylinder_end, default: -0.25
    	t.float :glass_add_start, default: 0.25
    	t.float :glass_add_end, default: 6.00

      t.timestamps null: false
    end
  end
end
