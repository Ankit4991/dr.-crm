class CreateCheckPayments < ActiveRecord::Migration
  def change
    create_table :check_payments do |t|
      t.float :amount
      t.integer :orderable_id
      t.string :orderable_type
      t.string :check_number
      t.integer :doctor_id
      t.integer :status, default: 0
      t.integer :organisation_id

      t.timestamps null: false
    end
  end
end
