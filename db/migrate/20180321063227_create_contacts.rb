class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :company_name
      t.string :email
      t.integer :account_number
      t.integer :phone_number
      t.string :address
      t.integer :fax
      t.string :country
      t.string :state
      t.string :city
      t.integer :zipcode
      t.string :website

      t.timestamps null: false
    end
  end
end
