class AddJointResourceIdToExamination < ActiveRecord::Migration
  def change
    add_reference :examinations, :joint_resource, index: true, foreign_key: true
    add_reference :glasses, :joint_resource, index: true, foreign_key: true
    add_reference :orders, :joint_resource, index: true, foreign_key: true
    add_reference :bookings, :joint_resource, index: true, foreign_key: true
    add_reference :follow_ups, :joint_resource, index: true, foreign_key: true
  end
end
