class CreateRelatives < ActiveRecord::Migration
  def change
    create_table :relatives do |t|
      t.integer :name
      t.string :relationship
      t.string :phone
      t.integer :patient_id
      t.timestamps null: false
    end
  end
end
