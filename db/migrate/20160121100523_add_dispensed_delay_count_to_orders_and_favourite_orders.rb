class AddDispensedDelayCountToOrdersAndFavouriteOrders < ActiveRecord::Migration
  def up
    add_column :orders, :dispensed_delay_count, :integer, default: 0
    add_column :favourite_orders, :dispensed_delay_count, :integer, default: 0
  end
  def down
    remove_column :orders, :dispensed_delay_count, :integer
    remove_column :favourite_orders, :dispensed_delay_count, :integer
  end
end
