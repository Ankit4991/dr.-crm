class CreateDoctorRecommendations < ActiveRecord::Migration
  def change
    create_table :doctor_recommendations do |t|
      t.string :name
      t.timestamps null: false
    end
  end
end
