class ChangeCountryIdType < ActiveRecord::Migration
  def change
  	change_column :addresses, :country_id, :string
  end
end
