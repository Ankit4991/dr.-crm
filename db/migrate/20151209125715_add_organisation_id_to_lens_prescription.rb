class AddOrganisationIdToLensPrescription < ActiveRecord::Migration
  def up
    add_column :lens_prescriptions, :organisation_id, :integer
  end
  
  def down
    remove_column :lens_prescriptions, :organisation_id, :integer
  end
end
