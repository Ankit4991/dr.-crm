class AddExternalDefFieldsToExamFormDefaults < ActiveRecord::Migration
  def change
    add_column :exam_form_defaults, :tear_film_def, :string
    add_column :exam_form_defaults, :cornea_def, :string
    add_column :exam_form_defaults, :iris_def, :string
    add_column :exam_form_defaults, :bulbar_conj_def, :string
    add_column :exam_form_defaults, :palpebral_conj_def, :string
    add_column :exam_form_defaults, :sclera_def, :string
    add_column :exam_form_defaults, :angles_od_def, :string
    add_column :exam_form_defaults, :angles_os_def, :string
    add_column :exam_form_defaults, :lids_lashes_def, :string
    add_column :exam_form_defaults, :ac_def, :string
    add_column :exam_form_defaults, :lens_def, :string
    add_column :exam_form_defaults, :visual_field_def, :string
  end
end
