class AddIntermediateDistanceToVisualAcuities < ActiveRecord::Migration
  def change
    add_column :visual_acuities, :intermediate_od, :string
    add_column :visual_acuities, :intermediate_os, :string
    add_column :visual_acuities, :intermediate_ou, :string
  end
end
