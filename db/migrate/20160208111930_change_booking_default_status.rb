class ChangeBookingDefaultStatus < ActiveRecord::Migration
  def up
  	remove_column :bookings, :status, :string
    add_column :bookings, :status, :integer, :default => 0
  	remove_column :bookings, :processed
  end
  def down
  	remove_column :bookings, :status, :integer
    add_column :bookings, :status, :string
  	add_column :bookings, :processed, :boolean, :default => false
  end
end
