class AddFieldExamDateInExamination < ActiveRecord::Migration
  def change
  	add_column :examinations, :exam_date, :date, :default => Time.now
  end
end
