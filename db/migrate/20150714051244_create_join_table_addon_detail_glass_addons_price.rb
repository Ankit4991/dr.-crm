class CreateJoinTableAddonDetailGlassAddonsPrice < ActiveRecord::Migration
  def change
    create_table :addon_details_glass_addons_prices do |t|
      t.references :addon_detail
      t.references :glass_addons_price
    end
  end
end
