class CreateReferralVisibilities < ActiveRecord::Migration
  def change
    create_table :referral_visibilities do |t|
    	t.integer :examination_id
    	t.boolean :medical, default: false
    	t.boolean :social, default: false
    	t.boolean :binocular, default: false
    	t.boolean :primlinary, default: false
    	t.boolean :visual, default: false
        t.boolean :procedure, default: false
        t.boolean :impression, default: false


    	t.boolean :refraction, default: false
    	t.boolean :r_objective, default: false
    	t.boolean :r_subjective, default: false
    	t.boolean :r_binocular, default: false
    	t.boolean :r_cycloplegic, default: false
    	t.boolean :r_final_glass, default: false

    	t.boolean :contact, default: false
    	t.boolean :c_keratometry, default: false
    	t.boolean :c_subjective, default: false
    	t.boolean :c_current_lens, default: false
    	t.boolean :c_trial_lens, default: false
    	t.boolean :c_final_lens, default: false

    	t.boolean :external, default: false
    	t.boolean :e_corneal, default: false
    	t.boolean :e_gonioscopy, default: false
    	t.boolean :e_drawing, default: false

    	t.boolean :internal, default: false
    	t.boolean :i_drawing, default: false

      t.timestamps null: false
    end
  end
end
