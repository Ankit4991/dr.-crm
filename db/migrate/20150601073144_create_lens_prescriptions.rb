class CreateLensPrescriptions < ActiveRecord::Migration
  def change
    create_table :lens_prescriptions do |t|
    	t.integer   :lens_id, index: true
      t.integer   :examination_id, index: true
      t.date      :issued_date
      t.date      :expired_date
      t.string    :cleaning_solution
      t.boolean   :first_instruction, default: true
      t.boolean   :second_instruction, default: true
      t.timestamps null: false
    end
  end
end
