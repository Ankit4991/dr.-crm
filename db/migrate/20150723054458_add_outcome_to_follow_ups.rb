class AddOutcomeToFollowUps < ActiveRecord::Migration
  def change
    add_column :follow_ups, :outcome, :string
  end
end
