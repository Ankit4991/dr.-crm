class CreateOrganisations < ActiveRecord::Migration
  def change
    create_table :organisations do |t|
      t.string :company_name, default: ""
      t.attachment :avatar
      t.string :description, default: ""
      t.string :slogan, default: ""
      t.string :size
      t.string :domain_name
      t.string :admins, array: true, default: []
      t.integer :owner_id

      t.timestamps null: false
    end
  end
end