class CreateEyeglassTypesPrices < ActiveRecord::Migration
  def change
    create_table :eyeglass_types_prices do |t|
      t.string :glasstype
      t.float :price

      t.timestamps null: false
    end
  end
end
