class ChangeDefaultValueInExternalExam < ActiveRecord::Migration
  def up
    change_column :external_exams, :angles_od, :string
    change_column :external_exams, :angles_os, :string
  end

  def down
    change_column :external_exams, :angles_od, :integer
    change_column :external_exams, :angles_os, :integer
  end
end
