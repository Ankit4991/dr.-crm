class CreateTwilioWebhooks < ActiveRecord::Migration
  def change
    create_table :twilio_webhooks do |t|
      t.string :sms_status, array: true, default: []
      t.string :message_id
      t.string :from
      t.string :to
      t.string :from_country
      t.string :from_city
      t.string :body
      t.integer :organisation_id
      t.timestamps null: false
    end
  end
end

