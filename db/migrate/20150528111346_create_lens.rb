class CreateLens < ActiveRecord::Migration
  def change
    create_table :lens do |t|
    	t.integer :contact_lens_exam_id, index: true
      t.integer :visual_acuity_id, index: true

      t.string :od_sphere
      t.string :od_cylinder
      t.string :od_axis
      t.string :od_add
      t.string :od_base_curve
      t.string :od_diameter
      t.string :od_type

      t.string :os_sphere
      t.string :os_cylinder
      t.string :os_axis
      t.string :os_add
      t.string :os_base_curve
      t.string :os_diameter
      t.string :os_type
      t.string :notes      
      t.timestamps null: false
    end
  end
end
