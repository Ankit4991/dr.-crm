class AddfieldsTofamilymembers < ActiveRecord::Migration
  def change
  	add_column :family_members, :last_eye_exam, :string
  	add_column :family_members, :medical_details, :string
  	add_column :family_members, :referred_by_data, :string
  	add_column :family_members, :history_info, :string, array: true, default: []
  	add_column :family_members, :referred_by, :string
  	add_column :family_members, :last_med_details, :string
  end
end
