class ChangeOrderSubscriptionFieldsTypes < ActiveRecord::Migration
  def up
    change_column :order_subscriptions,:start_time, :date
    change_column :order_subscriptions,:end_time, :date
    remove_column :order_subscriptions, :order_id, :integer
    remove_column :order_subscriptions, :lens_order_id, :integer
  end
  
  def down
    change_column :order_subscriptions,:start_time, :datetime
    change_column :order_subscriptions,:end_time, :datetime
    add_column :order_subscriptions, :order_id, :integer
    add_column :order_subscriptions, :lens_order_id, :integer
  end
end
