class CreateGlasses < ActiveRecord::Migration
  def change
    create_table :glasses do |t|
      t.integer :patient_id, index: true

      t.integer :od_sphere
      t.integer :od_cylinder
      t.integer :od_axis
      t.integer :od_add

      t.integer :os_sphere
      t.integer :os_cylinder
      t.integer :os_axis
      t.integer :os_add

      t.integer :visual_acuity_id, index: true

      t.string  :glasses_type
      t.timestamps null: false
    end
  end
end
