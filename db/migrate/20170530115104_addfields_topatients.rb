class AddfieldsTopatients < ActiveRecord::Migration
  def change
  	add_column :patients, :last_eye_exam, :string
  	add_column :patients, :medical_details, :string
  	add_column :patients, :referred_by_data, :string
  end
end
