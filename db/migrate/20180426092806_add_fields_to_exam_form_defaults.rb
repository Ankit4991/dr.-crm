class AddFieldsToExamFormDefaults < ActiveRecord::Migration
  def change
    add_column :exam_form_defaults, :drop3_text, :string
    add_column :exam_form_defaults, :drop4_text, :string
  end
end
