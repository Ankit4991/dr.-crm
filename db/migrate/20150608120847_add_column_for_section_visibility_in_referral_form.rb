class AddColumnForSectionVisibilityInReferralForm < ActiveRecord::Migration
  def change
  	add_column :examinations, :medical_sec_visibility, :boolean, default: false
  	add_column :examinations, :social_sec_visibility, :boolean, default: false
  	add_column :examinations, :binocular_sec_visibility, :boolean, default: false
  	add_column :examinations, :primlinary_sec_visibility, :boolean, default: false
  	add_column :examinations, :visual_sec_visibility, :boolean, default: false
  	add_column :examinations, :refraction_sec_visibility, :boolean, default: false
  	add_column :examinations, :contact_sec_visibility, :boolean, default: false
  	add_column :examinations, :external_sec_visibility, :boolean, default: false
  	add_column :examinations, :internal_sec_visibility, :boolean, default: false
  	add_column :examinations, :procedure_sec_visibility, :boolean, default: false
  	add_column :examinations, :impression_sec_visibility, :boolean, default: false
  	add_column :glasses, :visibility, :boolean, default: false
  	add_column :eye_measurements, :visibility, :boolean, default: false
  	add_column :glasses_prescriptions, :visibility, :boolean, default: false
  	add_column :lens, :visibility, :boolean, default: false
  end
end
