class AddAttachmentAvatarToFamilyMembers < ActiveRecord::Migration
  def self.up
    change_table :family_members do |t|
      t.attachment :avatar
    end
  end

  def self.down
    remove_attachment :family_members, :avatar
  end
end
