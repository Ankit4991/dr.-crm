class CreateFamilyMembers < ActiveRecord::Migration
  def change
    create_table :family_members do |t|
      t.string :first_name
      t.string :last_name
      t.date :birthday
      t.string :phone
      t.string :occupation
      t.string :ssn
      t.string :vision_insurance
      t.boolean :contact_lenses
      t.boolean :interested_contact_lens
      t.boolean :in_lasik
      t.string :ethnicity
      t.string :eye_color
      t.boolean :pregnate
      t.boolean :breast_feeding
      t.boolean :tobacco_use
      t.boolean :alcohol_use
      t.references :patient, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
