class AddDefaultOrgToOrganisations < ActiveRecord::Migration
  def change
  	add_column :organisations, :default_org, :boolean, default: false
  end
end
