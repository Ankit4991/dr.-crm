class AddPatientToDocuments < ActiveRecord::Migration
  def change
    add_reference :documents, :patient, index: true, foreign_key: true
  end
end
