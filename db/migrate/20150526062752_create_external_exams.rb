class CreateExternalExams < ActiveRecord::Migration
  def change
    create_table :external_exams do |t|
        t.integer :examination_id
    	t.string :tear_film, default: "Clear OU"
    	t.string :cornea, default: "+FL large demarcation stain, -stroma involvement"
    	t.string :iris, default: "Flat OU"
    	t.string :bulbar_conj, default: "Clear OU"
    	t.string :palpebral_conj, default: "Clear OU"
    	t.string :sclera, default: "Clear OU"
    	t.integer :angles_od
    	t.integer :angles_os
    	t.string :lids_lashes, default: "Mod MGD OU"
    	t.string :ac, default: "D/Q OU"
    	t.string :lens, default: "Clear OU"
        t.json   :draw_data
      t.timestamps null: false
    end
  end
end
