class AddDoctorIdToAddress < ActiveRecord::Migration
  def change
    add_reference :addresses, :doctor, index: true, foreign_key: true
  end
end
