class CreateFollowUps < ActiveRecord::Migration
  def change
    create_table :follow_ups do |t|
      t.string :note
      t.integer :days,default: 0
      t.integer :patient_id
      t.integer :doctor_id
      t.timestamps null: false
    end
  end
end
