class RemoveDoctorIdFromHoliday < ActiveRecord::Migration
  def change
    remove_column :holidays, :doctor_id, :integer
    add_column :holidays, :trackable_id, :integer
    add_column :holidays, :trackable_type, :string
  end
end
