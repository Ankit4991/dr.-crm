class AddRewardPercentUsedToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :reward_percent_used, :float
  end
end
