class CreateExamFormDefaults < ActiveRecord::Migration
  def change
    create_table :exam_form_defaults do |t|
      t.string :ctd_def
      t.string :ctn_def
      t.string :nra_def
      t.string :pra_def
      t.string :npc_def
      t.string :notes_def
      t.string :hpd_def
      t.string :hpn_def
      t.string :vpd_def
      t.string :vpn_def
      t.string :eoms_def
      t.string :pupils_def
      t.string :confrontational_def
      t.string :color_vision_def
      t.string :steropsis_def
    end
  end
end
