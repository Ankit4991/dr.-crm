class AddOdLensTypePriceIdAndOsLensTypePriceIdToLenses < ActiveRecord::Migration
  def up
    add_column :lens, :od_lens_types_price_id, :integer
    add_column :lens, :os_lens_types_price_id, :integer
  end
  
  def down
    remove_column :lens , :od_lens_types_price_id, :integer
    remove_column :lens , :os_lens_types_price_id, :integer
  end
end
