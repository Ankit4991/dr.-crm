class AddColumnOrganisationIdInLensMeterials < ActiveRecord::Migration
  def change
    
    add_column :lens_materials, :organisation_id, :integer
  end
end
