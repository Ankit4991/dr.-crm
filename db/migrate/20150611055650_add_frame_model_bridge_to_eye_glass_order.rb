class AddFrameModelBridgeToEyeGlassOrder < ActiveRecord::Migration
  def change
    add_column :eye_glass_orders, :model, :string
    add_column :eye_glass_orders, :eye_size, :string
    add_column :eye_glass_orders, :bridge, :string
    add_column :eye_glass_orders, :glass_b, :string
    add_column :eye_glass_orders, :glass_ed, :string
    add_column :eye_glass_orders, :color, :string
    add_column :eye_glass_orders, :material, :string
    add_column :eye_glass_orders, :temple, :string
  end
end
