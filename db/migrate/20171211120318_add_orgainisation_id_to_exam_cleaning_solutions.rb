class AddOrgainisationIdToExamCleaningSolutions < ActiveRecord::Migration
  def change
    add_column :exam_cleaning_solutions, :organisation_id, :integer
  end
end
