class AddMobileNumberToDoctor < ActiveRecord::Migration
  def change
    add_column :doctors, :mobile_number, :string
    add_column :doctors, :designation, :string
    add_column :doctors, :about, :string
    add_column :doctors, :portfolio, :string
  end
end
