class AddGlassIdToGlassesPrescription < ActiveRecord::Migration
  def change
    add_column :glasses_prescriptions, :glass_id, :integer
  end
end
