class AddModalityPeriodToLensPrescription < ActiveRecord::Migration
  def change
    add_column :lens_prescriptions, :modality_period, :integer
  end
end
