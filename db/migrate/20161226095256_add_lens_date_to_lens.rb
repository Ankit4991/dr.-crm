class AddLensDateToLens < ActiveRecord::Migration
  def change
    add_column :lens, :lens_date, :date
  end
end
