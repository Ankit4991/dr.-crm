class AddOdPatientPaysAndOsPatientPaysToContactLensOrders < ActiveRecord::Migration
  def change
    add_column :contact_lens_orders, :od_patient_pays, :float
    add_column :contact_lens_orders, :os_patient_pays, :float
    add_column :contact_lens_orders, :od_price, :float
    add_column :contact_lens_orders, :os_price, :float
  end
end
