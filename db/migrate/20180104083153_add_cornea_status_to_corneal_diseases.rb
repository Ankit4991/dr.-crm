class AddCorneaStatusToCornealDiseases < ActiveRecord::Migration
  def change
    add_column :corneal_diseases, :cornea_status, :boolean
  end
end
