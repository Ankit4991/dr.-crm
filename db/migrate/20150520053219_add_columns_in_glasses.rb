class AddColumnsInGlasses < ActiveRecord::Migration
  def up
  	add_column :glasses, :od_prism, :string
  	add_column :glasses, :os_prism, :string
  end
  def down
  	remove_column :glasses, :od_prism, :string
  	remove_column :glasses, :os_prism, :string
  end
end
