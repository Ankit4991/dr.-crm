class AddTotalToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :total, :float,default: 0.0
  end
end
