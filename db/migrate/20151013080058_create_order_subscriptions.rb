class CreateOrderSubscriptions < ActiveRecord::Migration
  def change
    create_table :order_subscriptions do |t|
      t.integer :organisation_id
      t.datetime :start_time
      t.datetime :end_time
      t.integer :interval
      t.integer :lens_order_id
      t.belongs_to :order, index: true, foreign_key: true
      t.belongs_to :joint_resource, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
