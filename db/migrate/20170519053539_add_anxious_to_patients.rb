class AddAnxiousToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :anxious, :boolean, :default => false
  end
end
