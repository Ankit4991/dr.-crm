class CreateEyeGlassOrders < ActiveRecord::Migration
  def change
    create_table :eye_glass_orders do |t|
      t.float :material_copay,default: 0.00
      t.float :frame_allowance,default: 0.00
      t.float :overage_discount,default: 0.00
      t.float :discount,default: 0.00
      t.float :total,default: 0.00
      t.integer :order_id

      t.timestamps null: false
    end
  end
end
